import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:icanfight/controller/singletonShareData.dart';

class GoogleAnalytics {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  static final GoogleAnalytics _instance = GoogleAnalytics._internal();

  factory GoogleAnalytics() {
    return _instance;
  }

  GoogleAnalytics._internal() {}

  Future<void> gaLogLogin({FirebaseUser user}) async {
    SingletonShareData data = SingletonShareData();
    data.alreadyLogin = true;
    await analytics.logLogin();
   // await testChinese();
    gaSetUserID(userID: user.uid);
  }

  Future<void> gaLogSignUp() async {
    await analytics.logSignUp();
    //await testChinese();
    print("gaLogSignUp");

  }

  Future<void> gaSetUserID({String userID}) async {
    await analytics.setUserId(userID);
    print("gaSetUserID:$userID");
  }

  Future<void> gaSetCurrentScreen({String screenName}) async {
    await analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenName,
    );
    print("gaSetCurrentScreen:$screenName");
  }

  Future<void> testChinese() async {
    await gaEvent(name: "learn_learn_learn");
    await gaEvent(name: "learn -learn - learn");
    await gaEvent(name: "學習");
    await gaEvent(name: "學習-乳癌治療-赫賽汀®️(Herceptin®️)(eBC)");
    await gaEvent(name: "學習-活得精彩-患上乳癌後");
    await gaEvent(name: "學習_活得精彩_患上乳癌後");
    await gaEvent(name: "學習-活得精彩-患上乳癌後妳要怎樣照顧自己的身體");
    await gaEvent(name: "學習_活得精彩_患上乳癌後妳要怎樣照顧自己的身體");
    await gaEvent(name: "學習-活得精彩-患上乳癌後妳要怎樣照顧自己的身體妳要怎樣照顧自己的身體妳要怎樣照顧自己的身體");
    await gaEvent(name: "學習_活得精彩_患上乳癌後妳要怎樣照顧自己的身體妳要怎樣照顧自己的身體妳要怎樣照顧自己的身體");
    await gaEvent(name: " If Q1第一期 or 第二期 or 第三期 + Q11帕羅嘉® (Perjeta®) > then show D4");
    await gaEvent(name: "If Q10現時沒有計劃接受任何治療 > then 唔可以揀其他任何答案灰 (ken:done) If Q10現時沒有計劃接受任何治療 > then 唔可以揀其他任何答案灰 (ken:done) If Q10現時沒有計劃接受任何治療 > then 唔可以揀其他任何答案灰 (ken:done)");
    print("gatestChinese");
  }
//  Future<void> gaTestingEvent() async {
//    await analytics.logEvent(
//      name: 'test_event',
//      parameters: {
//        'string': 'string',
//        'int': 42,
//        'long': 12345678910,
//        'double': 42.0,
//        'bool': true,
//      },
//    );
//  }

//  Future<void> gaTestingEvent2() async {
//    await analytics.logEvent(
//      name: 'Q2您的乳癌是HER2型乳癌嗎？',
//      parameters: {
//        'Q2是HER2型乳癌': 'Q2是HER2型乳癌',
//      },
//    );
//    print("gaEventName:Q2您的乳癌是HER2型乳癌嗎？");
//    print("gaEventParameters:Q2是HER2型乳癌");
//  }

   gaEvent({String name}) async {
    await analytics.logEvent(
      name: name,
//      parameters: {"event":"event23456"},
    );
    print("gaEventName:$name");
    //print("gaEventParameters:${{"gaEvent":gaEvent}}");
  }
}
GoogleAnalytics ga = GoogleAnalytics();