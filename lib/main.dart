import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/data/dataPageMiddleware.dart';
import 'package:icanfight/psp/app/keys.dart';
import 'package:icanfight/psp/pages/loadingPage/loadingPagemiddleware.dart';
import 'package:icanfight/psp/pages/step2/invoiceDetailPage/UpdateInvoiceInputPage.dart';
import 'package:icanfight/psp/pages/step2/uploadBankBook/UpdateBankBookInputPage.dart';
import 'package:icanfight/psp/reducers/app_reducer.dart';
import 'package:icanfight/psp/routing/navigation_middleware.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:provider/provider.dart';
import 'package:redux/redux.dart';
import 'api_provider/crudModel.dart';
import 'controller/baseViewController.dart';
import 'controller/bottomNavigationBarController.dart';
import 'model/locator.dart';
import 'utilities/themes.dart';
import 'controller/welcomeController.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:icanfight/psp/pages/aboutPage/aboutPage.dart';
import 'package:icanfight/psp/pages/formPage/formPage.dart';
import 'package:icanfight/psp/pages/homePage/homePage.dart';
import 'package:icanfight/psp/pages/loadingPage/loadingPage.dart';
import 'package:icanfight/psp/pages/loginPage/LoginPage.dart';
import 'package:icanfight/psp/pages/step1/drugStatus1Page/DrugStatus1Page.dart';
import 'package:icanfight/psp/pages/step1/drugStatus2Page/DrugStatus2Page.dart';
import 'package:icanfight/psp/pages/step1/drugStatus3Page/DrugStatus3Page.dart';
import 'package:icanfight/psp/pages/step1/familyIncomePage/FamilyIncomePage.dart';
import 'package:icanfight/psp/pages/step1/fundedInfoPage/FundedInfoPage.dart';
import 'package:icanfight/psp/pages/step1/personalBankPage/PersonalBankPage.dart';
import 'package:icanfight/psp/pages/step1/personalDataPage/PersonalDataPage.dart';
import 'package:icanfight/psp/pages/step1/pubOrPriHospital/PubOrPriHospital.dart';
import 'package:icanfight/psp/pages/step1/whereDrugInfoPage/WhereDrugInfoPage.dart';
import 'package:icanfight/psp/pages/step2/bankDataTypePage/BankDataTypePage.dart';
import 'package:icanfight/psp/pages/step2/idDetailPage/IdDetailPage.dart';
import 'package:icanfight/psp/pages/step2/IncomeDetailPage/IncomeDetailPage.dart';
import 'package:icanfight/psp/pages/step2/incomePage/IncomePage.dart';
import 'package:icanfight/psp/pages/step2/TaxDetailPage/TaxDetailPage.dart';
import 'package:icanfight/psp/pages/step2/invoiceDetailPage/InvoiceDetailPage.dart';
import 'package:icanfight/psp/pages/step2/invoiceDetailPage/invoiceInputPage.dart';
import 'package:icanfight/psp/pages/step2/medicalReportPage/MedicalReportPage.dart';
import 'package:icanfight/psp/pages/step2/refLetterPage/RefLetterPage.dart';
import 'package:icanfight/psp/pages/step2/step2LandingPage/Step2LandingPage.dart';
import 'package:icanfight/psp/pages/step2/uploadBankBook/BankBookDetailPage.dart';
import 'package:icanfight/psp/pages/step2/uploadBankBook/BankBookInputPage.dart';
import 'package:icanfight/psp/pages/step2/uploadBankStatementPage/BankStatementDetailPage.dart';
import 'package:icanfight/psp/pages/step2/uploadBankStatementPage/UploadBankStatementPage.dart';
import 'package:icanfight/psp/pages/step2/uploadDocListPage/UploadDocListPage.dart';
import 'package:icanfight/psp/pages/step3/AgreePage/agreePage.dart';
import 'package:icanfight/psp/pages/step3/FinishPage/FinishPage.dart';
import 'package:icanfight/psp/pages/step3/Step3LandingPage.dart';
import 'package:icanfight/psp/pages/step3/statementPage/StatementPage.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Crashlytics.instance.enableInDevMode = true;
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  setupLocator();

  SingletonShareData data = SingletonShareData();
  data.store = Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: [
      LoadingPageMiddleware(),
      NavigationMiddleware(),
      DataMiddleware()
    ],
  );

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

//  data.getPost();
  data.getPspHomePageData();
  data.getSmpt();
  data.getCategory();
  data.getTag();
  data.getDisclaimer();
  data.getTNC();
  data.setVersion();


    initializeDateFormatting("zh_HK",null).then((_) => {
      data.getCurrentUser().then((FirebaseUser user) {
        data.alreadyLogin = user!= null;
        print("alreadyLogin:$user");
        if(data.alreadyLogin){
         data.getChoiceValuesFromFireBase(user);
         // data.clearUserPasswordData(user: user);
        }
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: kLightHealthTheme.primaryColor
        ));
        runApp(StoreProvider(store: data.store, child: MyApp())

        );
      })
    });

}

class MyApp extends StatelessWidget {

//  static FirebaseAnalytics analytics = FirebaseAnalytics();
//  static FirebaseAnalyticsObserver observer =
//  FirebaseAnalyticsObserver(analytics: analytics);

  MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

     return MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => locator<CRUDModel>()),
    ],
    child: MaterialApp(
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      title: 'BC App',
        navigatorKey: Keys.navKey,
      theme: kLightHealthTheme,
      home: (SingletonShareData().alreadyLogin) ? BaseViewController(BottomNavigationBarController()) : BaseViewController(WelcomeController()),
        routes: <String, WidgetBuilder>{
          "/mainPage": (BuildContext context) => BaseViewController(BottomNavigationBarController(index: 3)),
          "/home": (BuildContext context) => HomePage(),
          "/loading": (BuildContext context) => LoadingPage(),
          "/login": (BuildContext context) => LoginPage(),
          "/form": (BuildContext context) => FormPage(),
          "/about": (BuildContext context) => AboutPage(),
          "/personalData": (BuildContext context) => PersonalDataPage(),
          "/personalBank": (BuildContext context) => PersonalBankPage(),
          "/familyIncome": (BuildContext context) => FamilyIncomePage(),
          "/fundedInfo": (BuildContext context) => FundedInfoPage(),
          "/pubOrPriHospital": (BuildContext context) => PubOrPriHospitalPage(),
          "/drugStatus1": (BuildContext context) => DrugStatus1Page(),
          "/drugStatus2": (BuildContext context) => DrugStatus2Page(),
          "/drugStatus3": (BuildContext context) => DrugStatus3Page(),
          "/whereDrugInfo": (BuildContext context) => WhereDrugInfoPage(),
          "/step2Landing": (BuildContext context) => Step2LandingPage(),
          "/uploadDocList": (BuildContext context) => UploadDocListPage(),
          "/invoiceDetail": (BuildContext context) => InvoiceDetailPage(),
          "/idDetail": (BuildContext context) => IdDetailPage(),
          "/invoiceInput": (BuildContext context) =>
              InvoiceInputPage(args: ModalRoute.of(context).settings.arguments),

          "/updateInvoiceInput": (BuildContext context) =>
              UpdateInvoiceInputPage(
                  args: ModalRoute.of(context).settings.arguments),

          "/refLetter": (BuildContext context) => RefLetterPage(),
          "/medicalReport": (BuildContext context) => MedicalReportPage(),
          //  "/income": (BuildContext context) => IncomePage(userType: ModalRoute.of(context).settings.arguments),
          "/income": (BuildContext context) =>
              IncomePage(args: ModalRoute.of(context).settings.arguments),

          // "/famIncome": (BuildContext context) => FamIncomePage(),
          "/taxDetail": (BuildContext context) =>
              TaxDetailPage(args: ModalRoute.of(context).settings.arguments),

          "/incomeDetail": (BuildContext context) =>
              IncomeDetailPage(args: ModalRoute.of(context).settings.arguments),
          "/step3Landing": (BuildContext context) => Step3LandingPage(),
          "/agree": (BuildContext context) => AgreePage(),
          "/statement": (BuildContext context) => StatementPage(),
          "/finish": (BuildContext context) => FinishPage(),

          "/uploadBankStatement": (BuildContext context) =>
              UploadBankStatementPage(
                  args: ModalRoute.of(context).settings.arguments),

          // "/bankData": (BuildContext context) => BankDataTypePage(),
          "/bankData": (BuildContext context) =>
              BankDataTypePage(args: ModalRoute.of(context).settings.arguments),
          "/bankStatementDetail": (BuildContext context) =>
              BankStatementDetailPage(
                  args: ModalRoute.of(context).settings.arguments),

          "/updateBankBookInput": (BuildContext context) =>
              UpdateBankBookInputPage(
                  args: ModalRoute.of(context).settings.arguments),
          "/bankBookDetail": (BuildContext context) => BankBookDetailPage(
              args: ModalRoute.of(context).settings.arguments),
          "/bankBookInput": (BuildContext context) => BankBookInputPage(
              args: ModalRoute.of(context).settings.arguments),
    }
    )
    );
  }

//  Widget moveToOtherPage() {
//    SingletonShareData data = SingletonShareData();
//    return FutureBuilder<FirebaseUser>(
//        future: data.getCurrentUser(),
//        builder: (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
//          if (snapshot.hasData) {
//            return BottomNavigationBarController();
//          } else {
//            return (alreadyLogin) ? SignInController() : WelcomeController();
//          }
//        }
//    );
//  }

}