import 'package:icanfight/psp/models/UserType.dart';

import '../app/data/dataState.dart';

class ScreenArguments {
  final UserType userType;
  final List<Location> imageLocationList;
  final int memberID;

  ScreenArguments(this.userType, this.imageLocationList, this.memberID);
}
