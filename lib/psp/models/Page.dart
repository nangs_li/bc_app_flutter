enum PageName{
  None,
  PersonalDataPage,
  PersonalBankPage,
  FamilyIncomePage,
  FundedInfoPage,
  PubOrPriHospital,
  DrugStatus1Page,
  DrugStatus2Page,
  DrugStatus3Page,
  WhereDrugInfoPage,

  Step2LandingPage,
  Step3LandingPage,
  UploadDocListPage,

  AgreePage,
  FinishPage,
  StatementPage
}