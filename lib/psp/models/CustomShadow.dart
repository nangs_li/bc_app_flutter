import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final BoxShadow popOverCardShadow = BoxShadow(
  color: Color.fromARGB(80, 15, 76, 41),
  blurRadius: 30.0, // has the effect of softening the shadow
  spreadRadius: 30.0, // has the effect of extending the shadow
  offset: Offset(
    0, // horizontal, move right 10
    3, // vertical, move down 10
  ),
);

final BoxShadow cardShadow = BoxShadow(
  color: Color.fromARGB(80, 15, 76, 41),
  blurRadius: 30.0, // has the effect of softening the shadow
  spreadRadius: 30.0, // has the effect of extending the shadow
  offset: Offset(
    0, // horizontal, move right 10
    -25, // vertical, move down 10
  ),
);

final BoxShadow selectingBoxShadow = BoxShadow(
  color: Color.fromARGB(80, 15, 76, 51),
  blurRadius: 20.0, // has the effect of softening the shadow
  spreadRadius: 20.0, // has the effect of extending the shadow
  offset: Offset(
    0, // horizontal, move right 10
    3, // vertical, move down 10
  ),
);

final BoxShadow normalBoxShadow = BoxShadow(
  color: Color.fromARGB(80, 15, 76, 13),
  blurRadius: 15.0, // has the effect of softening the shadow
  spreadRadius: 15.0, // has the effect of extending the shadow
  offset: Offset(
    0, // horizontal, move right 10
    8, // vertical, move down 10
  ),
);