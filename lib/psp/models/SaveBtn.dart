import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../app/app_state.dart';
import '../app/config.dart';
import '../app/data/dataPageAction.dart';
import '../app/theme.dart';
import 'package:icanfight/psp/routing/routingAction.dart';

class SaveBtnWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isSubmitted =
        StoreProvider.of<AppState>(context).state.dataState.isSentEmail;
    return FlatButton(
            onPressed: () {
              print("tap save button");
              StoreProvider.of<AppState>(context)
                  .dispatch(SaveAppDataToLocalAction());

              StoreProvider.of<AppState>(context)
                  .dispatch(NavigateToHomePageAction());
            },
            child: Text(
              isSubmitted ?  "返回首頁" : uiData_Text['general']['saveBtn'].toString(),
              style: saveBtntextStyle,
            ),
          );
  }
}
