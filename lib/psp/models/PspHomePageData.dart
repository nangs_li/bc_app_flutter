class PspHomePageData {
  List<PageData> data;

  PspHomePageData({this.data});

  PspHomePageData.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<PageData>();
      json['data'].forEach((v) {
        data.add(new PageData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PageData {
  String group;
  String districtLocation;
  String name;
  String nameDetail;
  String location;
  String phone;
  String email;
  String website;
  String image;

  PageData(
      {this.group,
        this.districtLocation,
        this.name,
        this.nameDetail,
        this.location,
        this.phone,
        this.email,
        this.website,
        this.image});

  PageData.fromJson(Map<String, dynamic> json) {
    group = json['group'];
    districtLocation = json['districtLocation'];
    name = json['name'];
    nameDetail = json['nameDetail'];
    location = json['location'];
    phone = json['phone'];
    email = json['email'];
    website = json['website'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['group'] = this.group;
    data['districtLocation'] = this.districtLocation;
    data['name'] = this.name;
    data['nameDetail'] = this.nameDetail;
    data['location'] = this.location;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['website'] = this.website;
    data['image'] = this.image;
    return data;
  }
}