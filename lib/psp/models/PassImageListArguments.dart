import 'package:icanfight/psp/models/UserType.dart';
import 'package:flutter/services.dart';

class PassImageListArguments {
  final UserType userType;
  // final List<Location> imageLocationList;
  final List<ByteData> byteDateList;
  final int memberID;

  PassImageListArguments(this.userType, this.byteDateList, this.memberID);
}
