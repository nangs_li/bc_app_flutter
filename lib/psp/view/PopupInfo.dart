import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PopupInfo extends StatefulWidget {
  const PopupInfo({Key key, this.isPop, this.onClose}) : super(key: key);
  final bool isPop;
  final Function onClose;
  @override
  PopupInfoState createState() => PopupInfoState();
}

class PopupInfoState extends State<PopupInfo> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedPositioned(
        duration: Duration(milliseconds: 250),
        curve: Curves.easeIn,
        bottom: widget.isPop ? -MediaQuery.of(context).size.height*.17 : -MediaQuery.of(context).size.height,
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [config_boxshadow],
            ),
            child: Padding(
                padding: EdgeInsets.only(left: 26, right: 26),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(top: 36),
                        alignment: Alignment.centerRight,
                        child: IconButton(
                          iconSize: 30.0,
                          icon: Icon(
                            Icons.clear,
                            color: config_primaryColour,
                            size: 30.0,
                          ),
                          onPressed: () => widget.onClose(),
                        )),
                      //Title
                      Container(
                        padding: EdgeInsets.only(top: 124*(MediaQuery.of(context).size.height/846)),
                        alignment: Alignment.centerLeft,
                        child: Text(uiData_Text['familyIncomePage']['famDefinitionTit'].toString(), style: defineTitleStyle)
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 4.5),
                        child: Divider(thickness: 1, height: 1, color: config_bodyTextColour,)),
                      Container(
                        padding: EdgeInsets.only(top: 11),
                        alignment: Alignment.centerLeft,
                        child: Text(uiData_Text['familyIncomePage']['famDefinition'].toString(), style: customeThemeData.textTheme.caption.copyWith(height: 1.76))
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 6),
                        alignment: Alignment.centerLeft,
                        child: InkWell(
                          onTap: (){
                            launch(uiData_Text['familyIncomePage']['famDefinitionLink']);
                          },
                          child: Text("${uiData_Text['familyIncomePage']['famDefinitionLink'].toString()} ${uiData_Text['familyIncomePage']['famDefinitionLast'].toString()}", style: customeThemeData.textTheme.caption.copyWith(height: 1.76))
                        )
                      )
                  ],
                ))));
  }
}
