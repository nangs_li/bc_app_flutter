import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';

import 'CircularCheckbox.dart';

class CheckBoxGroup extends StatefulWidget {
  const CheckBoxGroup(
      {Key key,
      this.title,
      this.value,
      this.labelFirst,
      this.labelSecond,
      this.labelThird,
      this.choiceFirst,
      this.choiceSecond,
      this.choiceThird,
      this.isThirdChoice,
      this.isEnable = true,
      this.check,
      this.isValid = true})
      : super(key: key);
  final String title;
  final String value;
  final String labelFirst;
  final String labelSecond;
  final String labelThird;
  final String choiceFirst;
  final String choiceSecond;
  final String choiceThird;
  final bool isThirdChoice;
  final bool isEnable;
  final bool isValid;
  final Function check;  
  @override
  CheckBoxGroupState createState() => CheckBoxGroupState();
}

class CheckBoxGroupState extends State<CheckBoxGroup> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
          padding: rowPadding,
          alignment: Alignment.centerLeft,
          child: Text(widget.title,
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: widget.isValid ? placeholderStyle : placeholderStyle.copyWith(color: config_alertColour)),
        ),
        Container(
            padding: EdgeInsets.only(top: 11),
            alignment: Alignment.centerLeft,
            child: GestureDetector(
                onTap: () {
                  if(widget.isEnable){
                    if (widget.isThirdChoice == true) {
                      widget.check(widget.choiceFirst);
                    } else {
                      widget.check(true);
                    }
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        width: 26,
                        height: 26,
                        child: CircularCheckBox(
                          //dataState.gender,
                          // checkColor: config_primaryColour,
                          backgroundActiveColor: false,
                          activeColor: config_primaryColour,
                          inactiveColor: config_checkBoxBorderColour,
                          value: widget.value == widget.choiceFirst,
                          onChanged: (data) {
                            // TODO
                            if(widget.isEnable){
                              // print('ChangeValue: $data');
                              // setState(() {
                              if (widget.isThirdChoice == true) {
                                widget.check(widget.choiceFirst);
                              } else {
                                widget.check(true);
                              }
                            }
                            // });
                          },
                        )),
                    Padding(
                        padding: EdgeInsets.only(left: 8.75),
                        child: Text(widget.labelFirst,
                            textAlign: TextAlign.center,
                            style: placeholderStyle))
                  ],
                ))),
        Container(
            padding: EdgeInsets.only(top: 11),
            alignment: Alignment.centerLeft,
            child: GestureDetector(
                onTap: () {
                  if(widget.isEnable){
                    if (widget.isThirdChoice == true) {
                      widget.check(widget.choiceSecond);
                    } else {
                      widget.check(false);
                    }
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        width: 26,
                        height: 26,
                        child: CircularCheckBox(
                            //dataState.gender,
                            // checkColor: config_primaryColour,
                            backgroundActiveColor: false,
                            activeColor: config_primaryColour,
                            inactiveColor: config_checkBoxBorderColour,
                            value: widget.value == widget.choiceSecond,
                            onChanged: (data) {
                              // TODO
                              if(widget.isEnable){
                                // print('ChangeValue: $data');
                                // setState(() {
                                if (widget.isThirdChoice == true) {
                                  widget.check(widget.choiceSecond);
                                } else {
                                  widget.check(false);
                                }
                              }
                              // });
                              /*setState(() {
                            widget.value = data == true
                                ? widget.choiceSecond
                                : widget.choiceFirst;
                          });*/
                            })),
                    Padding(
                        padding: EdgeInsets.only(left: 8.75),
                        child: Text(widget.labelSecond,
                            textAlign: TextAlign.center,
                            style: placeholderStyle))
                  ],
                ))),
        widget.isThirdChoice == true
            ? Container(
                padding: EdgeInsets.only(top: 11),
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                        width: 26,
                        height: 26,
                        child: CircularCheckBox(
                            //dataState.gender,
                            // checkColor: config_primaryColour,
                            backgroundActiveColor: false,
                            activeColor: config_primaryColour,
                            inactiveColor: config_checkBoxBorderColour,
                            value: widget.value == widget.choiceThird,
                            onChanged: (data) {
                              // TODO
                              if(widget.isEnable){
                                print('ChangeValue: $data');
                                // setState(() {
                                if (widget.isThirdChoice == true) {
                                  widget.check(widget.choiceThird);
                                }
                              }
                              // });
                              /*setState(() {
                            widget.value = data == true
                                ? widget.choiceSecond
                                : widget.choiceFirst;
                          });*/
                            })),
                    GestureDetector(
                        onTap: () {
                          if(widget.isEnable){
                            if (widget.isThirdChoice == true) {
                              widget.check(widget.choiceThird);
                            }
                          }
                        },
                        child: Padding(
                            padding: EdgeInsets.only(left: 8.75),
                            child: Text(widget.labelThird,
                                textAlign: TextAlign.center,
                                style: placeholderStyle)))
                  ],
                ))
            : Container()
      ],
    );
  }
}
