import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/view/CheckBoxGroup.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:flutter/material.dart';

import './DropdownGroup.dart';

class MemberCard extends StatefulWidget {
  const MemberCard(
      {Key key,
      this.data,
      this.index = 0,
      this.updateName,
      this.updateAge,
      this.updateGender,
      this.updateRelations,
      this.updateJob,
      this.updateIncome,
      this.controller,
      this.isValid = true,
      this.isEnable = true})
      : super(key: key);
  final FamilyMemberIncome data;
  final int index;
  final Function updateName,
      updateAge,
      updateGender,
      updateRelations,
      updateJob,
      updateIncome;
  final MemberCardController controller;
  final bool isValid;
  final bool isEnable;
  @override
  MemberCardState createState() => MemberCardState();
}

class MemberCardState extends State<MemberCard> {
  List<FocusNode> focusNode;
  List<DropdownMenuItem> memRelations;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focusNode = List.generate(6, (index) => FocusNode());
    memRelations = List.generate(
        4,
        (index) => DropdownMenuItem(
            value: uiData_Text['familyIncomePage']['famRelationList'][index]
                .toString(),
            child: Text(
                uiData_Text['familyIncomePage']['famRelationList'][index]
                    .toString(),
                style: customeThemeData.textTheme.bodyText1)));
  }

  @override
  Widget build(BuildContext context) {
    FamilyMemberIncome familyData = widget.data;
    print('familyData: ${familyData}');
    // TODO: implement build
    if(widget.isValid==false){
      setState(() {
        widget.controller.updateValidList();
      });
    }
    return Container(
        padding: EdgeInsets.only(top: 22),
        decoration: BoxDecoration(boxShadow: [config_cardShadow]),
        child: Card(
            color: Colors.white,
            borderOnForeground: true,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
                padding: EdgeInsets.only(left: 24, right: 24, bottom: 32),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 32),
                          child: Text(
                              "${uiData_Text['familyIncomePage']['cardtitle'].toString()} ${widget.index + 1}",
                              style: memberTitleStyle)),
                      DataTextFieldGroup(
                          isValid: widget.controller.validList[0],
                          title: uiData_Text['familyIncomePage']['famNameLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']['famNameHin']
                              .toString(),
                          initValue: familyData.memNam,
                          keyboardType: TextInputType.text,
                          onChange: (data) => widget.updateName(data),
                          focusNode: focusNode[0],
                          nextFocusNode: focusNode[1],
                          isEnable:widget.isEnable),
                      DataTextFieldGroup(
                          isValid: widget.controller.validList[1],
                          title: uiData_Text['familyIncomePage']['famAgeLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']['famAgeHin']
                              .toString(),
                          initValue: familyData.memAge,
                          keyboardType: TextInputType.number,
                          onChange: (data) => widget.updateAge(data),
                          width: MediaQuery.of(context).size.width * .2,
                          length: 2,
                          focusNode: focusNode[1],
                          nextFocusNode: null,
                          isEnable:widget.isEnable),
                      CheckBoxGroup(
                          title: uiData_Text['familyIncomePage']['famGender']
                              .toString(),
                          value: familyData.memGender,
                          isValid: widget.controller.validList[2],
                          labelFirst: uiData_Text['familyIncomePage']
                                  ['famGender_f']
                              .toString(),
                          labelSecond: uiData_Text['familyIncomePage']
                                  ['famGender_m']
                              .toString(),
                          choiceFirst: uiData_Text['familyIncomePage']
                                  ['famGender_f']
                              .toString(),
                          choiceSecond: uiData_Text['familyIncomePage']
                                  ['famGender_m']
                              .toString(),
                              isEnable:widget.isEnable,
                          check: (data) => {
                                /*    widget.updateGender(data) */
                                data == true
                                    ? widget.updateGender(
                                        uiData_Text['familyIncomePage']
                                            ['famGender_f'])
                                    : widget.updateGender(
                                        uiData_Text['familyIncomePage']
                                            ['famGender_m'])
                              }),
                      /*DropdownGroup(
                          title: uiData_Text['familyIncomePage']
                                  ['famRelationLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']
                                  ['famRelationHin']
                              .toString(),
                          items: memRelations,
                          value: familyData.memRelation.isNotEmpty
                              ? familyData.memRelation
                              : null,
                          onChange: (data) => widget.updateRelations(data)),*/
                      DataTextFieldGroup(
                          isValid: widget.controller.validList[3],
                          title: uiData_Text['familyIncomePage']
                                  ['famRelationLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']
                                  ['famRelationHin']
                              .toString(),
                          initValue: familyData.memRelation,
                          keyboardType: TextInputType.text,
                          onChange: (data) => widget.updateRelations(data),
                          focusNode: focusNode[3],
                          nextFocusNode: focusNode[4],
                          isEnable:widget.isEnable),
                      DataTextFieldGroup(
                          isValid: widget.controller.validList[4],
                          title: uiData_Text['familyIncomePage']['famJobLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']['famJobHin']
                              .toString(),
                          initValue: familyData.memCareer,
                          keyboardType: TextInputType.text,
                          onChange: (data) => widget.updateJob(data),
                          focusNode: focusNode[4],
                          nextFocusNode: focusNode[5],
                          isEnable:widget.isEnable),
                      DataTextFieldGroup(
                          isValid: widget.controller.validList[5],
                          title: uiData_Text['familyIncomePage']
                                  ['famIncomeLabel']
                              .toString(),
                          hint: uiData_Text['familyIncomePage']['famIncomeHin']
                              .toString(),
                          initValue: familyData.memIncome,
                          keyboardType: TextInputType.number,
                          onChange: (data) => widget.updateIncome(data),
                          focusNode: focusNode[5],
                          nextFocusNode: null,
                          isPrice: true,
                          isEnable:widget.isEnable),
                    ]))));
  }
}

class MemberCardController {
  List<FamilyMemberIncome> data;
  List<bool> validList;
  int id;

  MemberCardController({this.data, this.id}){
    validList = List.generate(6, (index) => true);
  }

  bool getAllValid() {
    return data[id].memAge != '' &&
        //data.memCareer != '' &&
        data[id].memGender != '' &&
        data[id].memIncome != '' &&
        data[id].memNam != '' &&
        data[id].memRelation != '';
  }

  void updateValidList() {
    validList[0] = data[id].memNam != '';
    validList[1] = data[id].memAge != '';
    validList[2] = data[id].memGender != '';
    validList[3] = data[id].memRelation != '';
    validList[4] = true;//data.memCareer != '';
    validList[5] = data[id].memIncome != '';
    print('MemberCard (${id}) updateValidList: ${data[id].memNam}, ${validList[0]}, ${validList[1]}, ${validList[2]}, ${validList[3]},${ validList[4]},${ validList[5]}');
  }
}
