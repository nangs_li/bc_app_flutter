import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:math' as math;

import '../controller/CameraCtr.dart';

class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen(
      {Key key, this.camera, this.imageType, this.memberId, this.metaData})
      : super(key: key);
  final List<CameraDescription> camera;
  final IMAGE_TYPE imageType;
  final int memberId;
  final DateTime metaData;
  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  int currentCameraId = 0;

  @override
  initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    if (widget.camera.length != 0) {
      _controller = CameraController(
        // Get a specific camera from the list of available cameras.
        widget.camera[currentCameraId],
        // Define the resolution to use.
        ResolutionPreset.high,
      );

      // Next, initialize the controller. This returns a Future.
      _initializeControllerFuture = _controller.initialize();
    } else {
      openGallery(widget.metaData);
    }
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    if (_controller != null) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String title = getTitle(widget.imageType);

    return Scaffold(
      appBar: AppBar(
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Text(title, style: titleTxtStyle))),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return Container(
                color: Colors.black,
                child: Stack(children: [
                  Container(
                      color: Colors.transparent,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height),
                  /*Container(
                  color: Colors.black,
                  child: Transform.scale(
                      scale: _getImageZoom(MediaQuery.of(context)),
                      child: Center(
                          child: AspectRatio(
                              aspectRatio: _controller.value.aspectRatio,
                              child: CameraPreview(_controller))))),*/
                  CameraPreview(_controller),
                  Positioned(
                      bottom: 20,
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: DecoratedBox(
                          decoration: BoxDecoration(
                              color: Colors.transparent), //Colors.black54
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              IconButton(
                                iconSize: 50,
                                icon: Icon(Icons.photo,
                                    color: Colors.white, size: 24.0),
                                onPressed: () => openGallery(widget.metaData),
                              ),
                              InkWell(
                                  child: Container(
                                      height: 76,
                                      width: 76,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: config_primaryColour)),
                                  onTap: () async {
                                    try {
                                      // Ensure that the camera is initialized.
                                      await _initializeControllerFuture;

                                      // Construct the path where the image should be saved using the
                                      // pattern package.
                                      int date =
                                          DateTime.now().millisecondsSinceEpoch;
                                      final path = join(
                                        // Store the picture in the temp directory.
                                        // Find the temp directory using the `path_provider` plugin.
                                        (await getApplicationDocumentsDirectory())
                                            .path,
                                        '${date}.png',
                                      );

                                      // Attempt to take a picture and log where it's been saved.
                                      await _controller.takePicture(path);

                                      // If the picture was taken, display it on a new screen.
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DisplayPictureScreen(
                                                  imagePath: path,
                                                  imageType: widget.imageType,
                                                  memberId: widget.memberId,
                                                  metaData: widget.metaData,
                                                  value: _controller.value,
                                                  filename: date),
                                        ),
                                      );
                                    } catch (e) {
                                      // If an error occurs, log the error to the console.
                                      print(e);
                                    }
                                  }),
                              widget.camera.length >= 1
                                  ? IconButton(
                                      iconSize: 50,
                                      icon: Icon(Icons.switch_camera,
                                          color: Colors.white, size: 24.0),
                                      onPressed: () => change_camera(),
                                    )
                                  : Container(width: 50, height: 50),
                            ],
                          )))
                ]));
          } else {
            // Otherwise, display a loading indicator.
            return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.black,
                child: Center(
                    child: CircularProgressIndicator(
                        strokeWidth: 10,
                        backgroundColor: Colors.black,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.white))));
          }
        },
      ),
    );
  }

  double _getImageZoom(MediaQueryData data) {
    final double logicalWidth = data.size.width;
    final double logicalHeight =
        _controller.value.previewSize.aspectRatio * logicalWidth;

    final EdgeInsets padding = data.padding;
    final double maxLogicalHeight =
        data.size.height - padding.top - padding.bottom;

    return maxLogicalHeight / logicalHeight;
  }

  change_camera() async {
    currentCameraId++;
    if (currentCameraId >= widget.camera.length) {
      currentCameraId = 0;
    }
    await _controller.dispose();

    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera[currentCameraId],
      // Define the resolution to use.
      ResolutionPreset.high,
    );
    _initializeControllerFuture = _controller.initialize();
    await Future.delayed(Duration(milliseconds: 300));
    if (mounted) {
      setState(() {});
    }
  }

  Future<List<ByteData>> photo_take() async {
    try {
      File file = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (file != null) {
        Uint8List bytes = await file.readAsBytes();
        List<ByteData> byteData = [ByteData.view(bytes.buffer)];
        return byteData;
      } else {
        return null;
      }
    } on Exception catch (e) {
      print("cancel: ${e.toString()}");
    }
  }

  String getTitle(IMAGE_TYPE type) {
    switch (type) {
      case IMAGE_TYPE.hkid:
        return uiData_Text['cameraPage']['id'];
        break;
      case IMAGE_TYPE.invoice:
        return uiData_Text['cameraPage']['invoice'];
        break;
      case IMAGE_TYPE.refletter:
        return uiData_Text['cameraPage']['refLetter'];
        break;
      case IMAGE_TYPE.medicalreport:
        return uiData_Text['cameraPage']['medicalReport'];
        break;
      case IMAGE_TYPE.income_tax:
        return uiData_Text['cameraPage']['incomeTax'];
        break;
      case IMAGE_TYPE.bank_statement:
        return uiData_Text['cameraPage']['incomeBankSheet'];
        break;
      case IMAGE_TYPE.passbook:
        return uiData_Text['cameraPage']['incomeBankBook'];
        break;
    }
  }

  openGallery(DateTime metaData) async {
    bool isAllow = await requestPermission(PermissionGroup.photos);
    if (isAllow == false) {
      // print('isAllow: $isAllow');
      await openDialog(context, PermissionGroup.photos);
      // open_camera(_context, imageType, memberID, metaData);
    } else {
      List<ByteData> byteData = List();
      switch (widget.imageType) {
        case IMAGE_TYPE.hkid:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.invoice:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.refletter:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.medicalreport:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.income_tax:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.bank_statement:
          byteData = await photo_take();
          break;
        case IMAGE_TYPE.passbook:
          byteData = await photo_take();
          break;
      }
      if (byteData != null) {
        print('metaData: ${metaData}');
        /*if(widget.imageType == IMAGE_TYPE.bank_statement){
      }*/
        save_photo(context, widget.imageType, widget.memberId, byteData,
            metaData); //null
      } else {
        /*     StoreProvider.of<AppState>(context)
          .dispatch(PopToUploadDocListPageAction(context)); */
        Navigator.pop(context);
      }
    }
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final IMAGE_TYPE imageType;
  final int memberId;
  final DateTime metaData;
  final CameraValue value;
  final int filename;

  const DisplayPictureScreen(
      {Key key,
      this.imagePath,
      this.imageType,
      this.memberId,
      this.metaData,
      this.value,
      this.filename})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Image img =
        Image.file(File(imagePath), fit: BoxFit.cover); //, fit: BoxFit.cover
    //Uint8List bytes = File(imagePath).readAsBytesSync();
    print('filename:$filename');
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.black,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.clear, color: Colors.white, size: 30.0),
            onPressed: () =>
                /* StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context)), */
                Navigator.pop(context),
          ),
        ),
        // The image is stored as a file on the device. Use the `Image.file`
        // constructor with the given path to display the image.
        body: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.transparent),
          Container(
              color: Colors.black,
              child: Transform.scale(
                  scale: _getImageZoom(MediaQuery.of(context)),
                  child: Center(
                      child: AspectRatio(
                          aspectRatio: value.aspectRatio, child: img)))),
          Positioned(
            bottom: 20,
            width: MediaQuery.of(context).size.width,
            height: 100,
            child: DecoratedBox(
                decoration: BoxDecoration(color: Colors.transparent),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 12),
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40.0),
                                    color: Colors.white),
                                child: Transform(
                                    alignment: Alignment.center,
                                    transform: Matrix4.rotationY(math.pi),
                                    child: Icon(Icons.refresh,
                                        size: 24.0,
                                        color: config_bodyTextColour))))),
                    InkWell(
                        onTap: () async {
                          Uint8List bytes =
                              await readImageByteFromLocal('$filename.png');
                          List<ByteData> byteData = [
                            ByteData.view(bytes.buffer)
                          ];
                          save_photo(
                              context, imageType, memberId, byteData, metaData);
                        },
                        child: Container(
                          height: 76,
                          width: 76,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40.0),
                              color: config_primaryColour),
                          child: Icon(Icons.check,
                              size: 60.0, color: Colors.white),
                        )),
                    Container(
                      width: 30,
                      height: 30,
                    )
                  ],
                )),
          )
        ])
        //Image.file(File(imagePath)),
        );
  }

  double _getImageZoom(MediaQueryData data) {
    final double logicalWidth = data.size.width;
    final double logicalHeight = value.previewSize.aspectRatio * logicalWidth;

    final EdgeInsets padding = data.padding;
    final double maxLogicalHeight =
        data.size.height - padding.top - padding.bottom;

    return maxLogicalHeight / logicalHeight;
  }
}
