import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';

class PrograssBar extends StatefulWidget {
  final int percentage;
  final int step;
  PrograssBar({Key key, this.percentage, this.step}) : super(key: key);
  @override
  _PrograssBarState createState() => _PrograssBarState();
}

class _PrograssBarState extends State<PrograssBar> {
  String percentStr = "";
  String fullTxt = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    percentStr = " (" + widget.percentage.toString() + "%)";
    switch (widget.step) {
      case 0:
        fullTxt = uiData_Text['general']['cur_percentageText'].toString();
        break;
      case 1:
        fullTxt = uiData_Text['general']['cur_percentageText2'].toString();
        break;
      case 2:
        fullTxt = uiData_Text['general']['cur_percentageText3'].toString();
        break;
    }
    return Stack(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              flex: widget.percentage, // 20%
              child: Container(height: 20.0, color: config_progressBarColour),
            ),
            Expanded(
              flex: 100 - widget.percentage, // 20%
              child: Container(
                height: 20.0,
                child: Container(height: 20.0, color: Colors.white),
              ),
            ),
          ],
        ),
        Container(
            padding: EdgeInsets.only(left: 25, right: 25),
            height: 20.0,
            alignment: Alignment.centerLeft,
            child: Text(fullTxt + percentStr,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: widget.percentage <= 10
                    ? progressBarStyle
                    : progressBarStyle.copyWith(color: Colors.white))),
      ],
    );
  }
}
