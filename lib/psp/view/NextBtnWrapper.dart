import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/pages/step2/uploadBankStatementPage/UploadBankStatementPage.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/MemberCard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

import '../app/app_state.dart';
import '../app/config.dart';
import '../app/theme.dart';
import '../routing/routingAction.dart';

class NextBtnWrapper extends StatefulWidget {
  const NextBtnWrapper({
    Key key,
    this.clickEvent,
    this.title,
    //this.dataList,
    //this.validList,
    this.controller,
    //this.checkValid
  }) : super(key: key);
  final String title;
  final Function clickEvent;
  //MemberIncome
  //Original
  //final List<String> dataList;
  //final List<bool> validList;
  final NextBtnController controller;
  //final Function checkValid;
  @override
  _NextBtnWrapperState createState() => _NextBtnWrapperState();
}

class _NextBtnWrapperState extends State<NextBtnWrapper> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            // color: config_nextPageBtnColour,
//            boxShadow: widget.controller.getAllValid()
//                ? [config_btnshadow]
//                : [config_disableBtnShadow]
        ),
        child: ButtonTheme(
          minWidth: MediaQuery.of(context).size.width,
          height: 56,
          buttonColor: widget.controller.getAllValid()
              ? config_nextPageBtnColour
              : config_nextPageDisableBtnColour,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0)),
            onPressed: () {
              if (widget.controller.getAllValid()) {
                /*if (widget.controller.validTypeList.isNotEmpty) {
                  setState(() {
                    widget.controller.updateValidStatus();
                    if (widget.controller.validList.every((v) => v == true)) {
                      widget.clickEvent();
                    }
                  });
                } else {
                  */
                widget.clickEvent();
                //}
              } else if (widget.controller.isNotEmpty)
              //if(widget.checkValid!=null)
              //{widget.checkValid()}
              {
                setState(() {
                  widget.controller.updateValidStatus();
                });
              } else {
                print('click but will do nothing');
              }
            },
            child: Text(
              widget.title,
              style: submitBtnTextStyle,
            ),
          ),
        ));
  }

  /*getAllValid() {
    if (widget.dataList == null) {
      return true;
    } else {
      if (widget.dataList.contains('')) {
        return false;
      } else {
        return true;
      }
    }
  }*/
}

class NextBtnController {
  List<String> dataList;
  List<bool> validList;
  bool isNotEmpty;
  Function updateValid;
  List<ValidType> validTypeList;
  List<int> exceptionList;

  NextBtnController(
      {this.dataList,
      this.updateValid,
      this.validTypeList = const [],
      this.exceptionList = const []}) {
    if (dataList != null) {
      validList = List.generate(dataList.length, (index) => true);
      isNotEmpty = true;
    } else {
      isNotEmpty = false;
    }
  }

  getAllValid() {
    if (dataList == null) {
      return true;
    } else {
      if (dataList.contains('') && exceptionList.isEmpty) {
        return false;
      } else {
        if (validTypeList.isNotEmpty) {
          for (int i = 0; i < dataList.length; i++) {
            if (exceptionList.contains(i) && dataList[i].isEmpty) {
              validList[i] = true;
            } else {
              validList[i] = dataList[i] != '';
              if (validTypeList[i] != null && validList[i] == true) {
                switch (validTypeList[i]) {
                  case ValidType.None:
                    break;
                  case ValidType.Hkid:
                    // print('check hkid: ${i}');
                    if (dataList[2].isNotEmpty &&
                        dataList[3].isNotEmpty &&
                        dataList[4].isNotEmpty) {
                      validList[i] =
                          isHkid(dataList[2], dataList[3], dataList[4]);
                    }
                    break;
                  case ValidType.Birth:
                    validList[i] = isBirthRangeCorrect(dataList[i]);
                    break;
                  case ValidType.Eng:
                    validList[i] = isEng(dataList[i]);
                    break;
                  case ValidType.Chi:
                    validList[i] = isChi(dataList[i]);
                    break;
                  case ValidType.Email:
                    validList[i] = isEmail(dataList[i]);
                    break;
                  case ValidType.Phone:
                    validList[i] = isPhone(dataList[i]);
                    break;
                }
              }
            }
          }
          return validList.every((v) => v == true);
        } else {
          return true;
        }
      }
    }
  }

  updateValidStatus() {
    for (int i = 0; i < dataList.length; i++) {
      // print('i: ${exceptionList.contains(i)} && ${dataList[i].isEmpty}');
      if (exceptionList.contains(i) && dataList[i].isEmpty) {
        validList[i] = true;
      } else {
        validList[i] = dataList[i] != '';
        if (validTypeList[i] != null && validList[i] == true) {
          switch (validTypeList[i]) {
            case ValidType.None:
              break;
            case ValidType.Hkid:
              if (dataList[2].isNotEmpty &&
                  dataList[3].isNotEmpty &&
                  dataList[4].isNotEmpty) {
                validList[i] = isHkid(dataList[2], dataList[3], dataList[4]);
              }
              break;
            case ValidType.Birth:
              validList[i] = isBirthRangeCorrect(dataList[i]);
              break;
            case ValidType.Eng:
              validList[i] = isEng(dataList[i]);
              break;
            case ValidType.Chi:
              validList[i] = isChi(dataList[i]);
              break;
            case ValidType.Email:
              validList[i] = isEmail(dataList[i]);
              break;
            case ValidType.Phone:
              validList[i] = isPhone(dataList[i]);
              break;
          }
        }
      }
    }
    // print('NextBtnController updateValidStatus: ${validList}');
    if (updateValid != null) {
      updateValid();
    }
  }
}

class UploadDocNextBtnController extends NextBtnController {
  Data_State state;
  UploadDocNextBtnController({this.state}) {
    validList = List.generate(1, (index) => true);
    isNotEmpty = true;
  }
  @override
  getAllValid() {
    // TODO: implement getAllValid
    bool isPubHos = state.isPublicHospial ==
        uiData_Text['pubOrPriHospitalPage']['radioGroup'][0];
    bool hkidValid = state.hkidLocation.isNotEmpty;
    bool invoiceValid = state.invoiceLocation.isNotEmpty;
    bool refLetterValid = isPubHos ? true : state.refLetterLocation.isNotEmpty;
    bool medicalValid =
        isPubHos ? true : state.medicalReportLocation.isNotEmpty;
    bool incomeValid = _getIncomeValid(state);
    List<bool> familyValid = List();
    if (state.familyMemberIncome.isNotEmpty) {
      for (int i = 0; i < state.familyMemberIncome.length; i++) {
        familyValid.add(_getFamilyPhotots(state, i));
      }
    } else {
      familyValid = [true];
    }
    return hkidValid &&
        invoiceValid &&
        refLetterValid &&
        medicalValid &&
        incomeValid &&
        familyValid.every((e) => e == true);
  }

  @override
  updateValidStatus() {
    validList[0] = false;
  }

  bool _getIncomeValid(Data_State state) {
    switch (state.incomeCopyType) {
      case 'IMAGE_TYPE.income_tax':
      case 'IMAGE_TYPE.passbook':
        return state.incomeCopyLocation.isNotEmpty;
        break;
      case 'IMAGE_TYPE.bank_statement':
        DateTime now = DateTime.now();
        var formatter = DateFormat('yM');
        List<MonthBtnModel> monthBtnModel = List();
        for (var i = 0; i < 12; i++) {
          DateTime newDate = DateTime(
              now.year,
              now.month - 1 - i,
              now.day,
              now.hour,
              now.minute,
              now.second,
              now.millisecond,
              now.microsecond);
          monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
        }
        //assign location into the month location list
        for (var location in state.incomeCopyLocation) {
          print("locaiton uploadDate " +
              formatter.format(DateTime.parse(location.stdate)));

          for (var a_monthBtnModel in monthBtnModel) {
            if (formatter.format(a_monthBtnModel.btnDate) ==
                formatter.format(DateTime.parse(location.stdate))) {
              a_monthBtnModel.imageLocationList.add(location);
            }
          }
        }
        return monthBtnModel
            .every((month) => month.imageLocationList.isNotEmpty);
        break;
      default:
        return false;
    }
  }

  bool _getFamilyPhotots(Data_State state, int index) {
    if (state.familyMemberIncome.isNotEmpty) {
      switch (state.familyMemberIncome[index].incomeCopyType) {
        case 'IMAGE_TYPE.income_tax':
        case 'IMAGE_TYPE.passbook':
          return state.familyMemberIncome[index].incomeCopyLocation.isNotEmpty;
          break;
        case 'IMAGE_TYPE.bank_statement':
          DateTime now = DateTime.now();
          var formatter = DateFormat('yM');
          List<MonthBtnModel> monthBtnModel = List();
          for (var i = 0; i < 12; i++) {
            DateTime newDate = DateTime(
                now.year,
                now.month - 1 - i,
                now.day,
                now.hour,
                now.minute,
                now.second,
                now.millisecond,
                now.microsecond);
            monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
          }
          //assign location into the month location list
          for (var location
              in state.familyMemberIncome[index].incomeCopyLocation) {
            print("locaiton uploadDate " +
                formatter.format(DateTime.parse(location.stdate)));

            for (var a_monthBtnModel in monthBtnModel) {
              if (formatter.format(a_monthBtnModel.btnDate) ==
                  formatter.format(DateTime.parse(location.stdate))) {
                a_monthBtnModel.imageLocationList.add(location);
              }
            }
          }
          return monthBtnModel
              .every((month) => month.imageLocationList.isNotEmpty);
          break;
        default:
          return false;
      }
    } else {
      return false;
    }
    //return monthBtnModel.every((member) => member.every((month) => month.imageLocationList.isNotEmpty));
  }
}

class FamilyMemberNextBtnController extends NextBtnController {
  List<FamilyMemberIncome> memberList;
  Function updateMemberValidList;
  FamilyMemberNextBtnController({this.memberList, this.updateMemberValidList}) {
    validList = List.generate(memberList.length, (index) => true);
    isNotEmpty = true;
  }

  @override
  getAllValid() {
    if (memberList == null || memberList.isEmpty) {
      return true;
    } else {
      //print("${memberList.every((memb) =>memb.memNam != '' && memb.memAge != '' && memb.memGender != '' && memb.memRelation != '' && memb.memIncome != '')}");
      return memberList.every((memb) =>
          memb.memNam != '' &&
          memb.memAge != '' &&
          memb.memGender != '' &&
          memb.memRelation != '' &&
          memb.memIncome != '');
    }
  }

  @override
  updateValidStatus() {
    for (int i = 0; i < memberList.length; i++) {
      validList[i] = false;
      /*memberList[i].memNam != '' &&
          memberList[i].memAge != '' &&
          memberList[i].memGender != '' &&
          memberList[i].memRelation != '' &&
          memberList[i].memIncome != '';*/
      updateMemberValidList();
      print(
          'FamilyIncomeNextBtnController updateValidStatus:${i} - ${validList[i]}');
    }
  }
}
