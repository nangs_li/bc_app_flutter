import 'package:icanfight/psp/app/config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';


class WaveBar extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return  Container(
      color: defaultPinkColor,
        width: MediaQuery.of(context).size.width,
        height: 60,
        child:  Stack(
          children: <Widget>[
            SvgPicture.asset("assets/images/BKG_Layer4.svg", width: MediaQuery.of(context).size.width,),
            SvgPicture.asset("assets/images/BKG_Layer3.svg", width: MediaQuery.of(context).size.width,),
            SvgPicture.asset("assets/images/BKG_Layer2.svg", width: MediaQuery.of(context).size.width,),
            SvgPicture.asset("assets/images/BKG_Layer1.svg", width: MediaQuery.of(context).size.width,),
          ],
        ),
    );
  }
}