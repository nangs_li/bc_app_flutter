import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';

import 'CircularCheckbox.dart';

class MultipleCheckboxGroup extends StatefulWidget {
  const MultipleCheckboxGroup(
      {Key key,
      this.title,
      this.controller,
      this.check,
      this.uncheck,
      this.isEnable = true,
      this.txtFieldList = const [],
      this.txtFieldJsonList = const []})
      : super(key: key);
  final String title;
  final MCController controller;
  final Function check, uncheck;
  final bool isEnable;
  final List<int> txtFieldList;
  final List<Map<String, dynamic>> txtFieldJsonList;

  @override
  MultipleCheckboxGroupState createState() => MultipleCheckboxGroupState();
}

class MultipleCheckboxGroupState extends State<MultipleCheckboxGroup> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
          padding: rowPadding,
          alignment: Alignment.centerLeft,
          child: Text(widget.title,
              textAlign: TextAlign.left,
              style: customeThemeData.textTheme.bodyText1),
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.controller.options.length,
            itemBuilder: (BuildContext context, int index) {
              if (widget.txtFieldList.contains(index)) {
                final obj =
                    widget.txtFieldJsonList[widget.txtFieldList.indexOf(index)];
                return Column(
                  children: <Widget>[
                    MultipleCheckBox(
                        value: widget.controller.getValue(index),
                        isEnable: widget.isEnable,
                        radioGroup: widget.controller.options,
                        check: (data) {
                          widget.check(widget.controller.onCheck(data));
                        },
                        uncheck: (data) {
                          widget.uncheck(widget.controller.onUncheck(data));
                          obj['onChange']('');
                        },
                        index: index,
                        isMultiple: widget.controller.isMultiple),
                    widget.controller.getValue(index) ==
                            widget.controller.options[index]
                        ? Padding(
                            padding: EdgeInsets.only(top: 17),
                            child: DataTextField(
                                isValid: true,
                                hint: obj['hint'].toString(),
                                onChange: obj['onChange'],
                                keyboardType: obj['keyboardType'],
                                initValue: obj['initValue'],
                                height: obj['height'],
                                minLines: obj['minLines'],
                                maxLines: obj['maxLines'],
                                hintStyle: mcTxtFieldPlaceholderStyle,
                                style: mcTxtFieldStyle))
                        : Container()
                  ],
                );
              } else {
                return MultipleCheckBox(
                    value: widget.controller.getValue(index),
                    isEnable: widget.isEnable,
                    radioGroup: widget.controller.options,
                    check: (data) {
                      widget.check(widget.controller.onCheck(data));
                    },
                    uncheck: (data) {
                      widget.uncheck(widget.controller.onUncheck(data));
                    },
                    index: index,
                    isMultiple: widget.controller.isMultiple);
              }
            })
      ],
    );
  }
}

class MultipleCheckBox extends StatefulWidget {
  const MultipleCheckBox(
      {Key key,
      this.value,
      this.radioGroup,
      this.check,
      this.uncheck,
      this.index,
      this.isNotSpecial = true,
      this.isMultiple = false,
      this.isEnable = true,
      this.specialTxtStartIndex = -1,
      this.specialTxtEndIndex = -1})
      : super(key: key);
  final String value;
  final List<dynamic> radioGroup;
  final Function check, uncheck;
  final int index;
  final bool isNotSpecial;
  final bool isMultiple;
  final bool isEnable;
  final int specialTxtStartIndex, specialTxtEndIndex;
  @override
  MultipleCheckboxState createState() => MultipleCheckboxState();
}

class MultipleCheckboxState extends State<MultipleCheckBox> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
        onTap: () {
          if (widget.isEnable) {
            if (widget.value != widget.radioGroup[widget.index]) {
              widget.check(widget.radioGroup[widget.index]);
            } else if (widget.isMultiple) {
              widget.uncheck(widget.radioGroup[widget.index]);
            }
          }
        },
        child: Padding(
            padding: EdgeInsets.only(top: 18),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 19, right: 19),
                height: 70,
                decoration: widget.isNotSpecial
                    ? BoxDecoration(
                        boxShadow: [config_checkBoxshadow],
                        borderRadius: BorderRadius.circular(10),
                        border: widget.value == widget.radioGroup[widget.index]
                            ? Border.all(color: config_primaryColour, width: 1)
                            : Border.all(color: Colors.white, width: 1),
                        color: Colors.white)
                    : BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                child: Stack(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    SizedBox(
                        width: 26,
                        height: 26,
                        child: CircularCheckBox(
                          //dataState.gender,
                          // checkColor: config_primaryColour,
                          backgroundActiveColor: true,
                          activeColor: config_primaryColour,
                          inactiveColor: config_checkBoxBorderColour,
                          value:
                              widget.value == widget.radioGroup[widget.index],
                          onChanged: (data) {
                            // TODO
                            // setState(() {
                            if (widget.isEnable) {
                              if (data) {
                                widget.check(widget.radioGroup[widget.index]);
                              }
                            }
                            // });
                          },
                        )),
                    Padding(
                        padding: EdgeInsets.only(left: 42), //19
                        child: widget.specialTxtStartIndex == -1
                            ? Text(widget.radioGroup[widget.index],
                                textAlign: TextAlign.left,
                                softWrap: true,
                                style: customeThemeData.textTheme.caption)
                            : RichText(
                                text: TextSpan(
                                    style: customeThemeData.textTheme.caption,
                                    children: getTextList())))
                  ],
                ))));
  }

  List<TextSpan> getTextList() {
    List<TextSpan> spanList = List();
    if (widget.specialTxtStartIndex == 0) {
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index].toString().substring(
              widget.specialTxtStartIndex, widget.specialTxtEndIndex),
          style: customeThemeData.textTheme.caption
              .copyWith(color: config_primaryColour)));
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index]
              .toString()
              .substring(widget.specialTxtEndIndex)));
    } else if (widget.specialTxtEndIndex ==
        widget.radioGroup[widget.index].toString().length) {
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index]
              .toString()
              .substring(0, widget.specialTxtStartIndex - 1)));
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index].toString().substring(
              widget.specialTxtStartIndex, widget.specialTxtEndIndex),
          style: customeThemeData.textTheme.caption
              .copyWith(color: config_primaryColour)));
    } else {
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index]
              .toString()
              .substring(0, widget.specialTxtStartIndex - 1)));
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index].toString().substring(
              widget.specialTxtStartIndex, widget.specialTxtEndIndex),
          style: customeThemeData.textTheme.caption
              .copyWith(color: config_primaryColour)));
      spanList.add(TextSpan(
          text: widget.radioGroup[widget.index]
              .toString()
              .substring(widget.specialTxtEndIndex + 1)));
    }
    return spanList;
  }
}

class MCController {
  String value;
  bool isMultiple;
  List<dynamic> options;
  List<int> singleChoice;
  MCController(
      {this.value,
      this.isMultiple = false,
      this.options,
      this.singleChoice = const []});

  String getValue(int optIndex) {
    if (isMultiple && value.contains(mcSpliter)) {
      if (value.split(mcSpliter).contains(options[optIndex])) {
        return options[optIndex];
      }
    }
    return value;
  }

  String toString() {
    return value;
  }

  String onCheck(data) {
    if (isMultiple) {
      if (singleChoice.any((sc) => options[sc] == data)) {
        value = data;
      } else if (value.isNotEmpty) {
        if (singleChoice.any((sc) => options[sc] == value)) {
          //Original Choice is single
          value = data;
        } else {
          //Normal
          value += mcSpliter + data;
        }
      } else {
        value = data;
      }
    } else {
      value = data;
    }
    return value;
  }

  String onUncheck(data) {
    if (isMultiple) {
      if (singleChoice.contains((sc) => options[sc] == data)) {
        value = data;
      } else if (value.contains(mcSpliter)) {
        List<String> list = List();
        for (int i = 0; i < value.split(mcSpliter).length; i++) {
          if (value.split(mcSpliter)[i] != data) {
            list.add(value.split(mcSpliter)[i]);
          }
        }
        String str = "";
        for (int i = 0; i < list.length; i++) {
          if (i == 0) {
            str = list[i];
          } else {
            str += mcSpliter + list[i];
          }
        }
        return str;
      }
    }
    return data;
  }

  List<String> toList() {
    List<String> list = List();
    if (value.contains(mcSpliter)) {
      for (int i = 0; i < value.split(mcSpliter).length; i++) {
        list.add(value.split(mcSpliter)[i]);
      }
    } else {
      list.add(value);
    }
    return list;
  }
}
