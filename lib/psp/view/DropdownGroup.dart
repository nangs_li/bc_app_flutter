import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';

class DropdownGroup extends StatefulWidget {
  const DropdownGroup(
      {Key key, this.title, this.hint, this.items, this.value, this.onChange})
      : super(key: key);
  final String title;
  final String hint;
  final List<DropdownMenuItem> items;
  final dynamic value;
  final Function onChange;
  @override
  DropdownGroupState createState() => DropdownGroupState();
}

class DropdownGroupState extends State<DropdownGroup> {
  @override
  Widget build(BuildContext context) {
    // print('items: ${widget.items}');
    // TODO: implement build
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(children: <Widget>[
          Container(
            padding: rowPadding,
            alignment: Alignment.centerLeft,
            child: Text(widget.title,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: placeholderStyle),
          ),
          Container(
              padding: EdgeInsets.only(top: 11),
              alignment: Alignment.centerLeft,
              child: Container(
                  height: 48,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border:
                          Border.all(color: config_primaryColour, width: 1.0)),
                  child: Padding(
                      padding: EdgeInsets.only(left: 18, right: 18),
                      child: DropdownButton(
                          items: widget.items,
                          isExpanded: true,
                          elevation: 0,
                          underline: Container(),
                          hint: Text(
                            widget.hint,
                            style: dropdownPlaceholderStyle,
                            textAlign: TextAlign.center,
                          ),
                          value: widget.value,
                          onChanged: (data) => widget.onChange(data)))))
        ]));
  }
}
