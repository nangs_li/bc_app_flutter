import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:intl/intl.dart';
import 'package:icanfight/psp/app/data/dataState.dart';

enum TextFieldType {
  EnglishName,
  ChineseName,
  IDCard,
  BirthDay,
  HomePhone,
  Phone,
  Email,
  Address,
  BankNumber
}

class DataTextFieldGroup extends StatefulWidget {
  const DataTextFieldGroup({
    Key key,
    this.title,
    this.hint,
    this.initValue,
    this.keyboardType,
    this.onChange,
    // this.onFinish,
    this.focusNode,
    this.nextFocusNode,
    this.length,
    this.width,
    this.height,
    this.minLines,
    this.maxLines,
    this.isValid,
    // this.validationType = ValidType.None,
    this.isPrice = false,
    this.isEnable = true,
    this.style,
    this.type
    ///this.isNextPage = false
  }) : super(key: key);
  final String title;
  final String hint;
  final String initValue;
  final TextInputType keyboardType;
  final Function onChange;
  // final Function onFinish;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final int length;
  final double width;
  final double height;
  final int minLines;
  final int maxLines;
  final bool isValid;
  // final ValidType validationType;
  final bool isPrice;
  final bool isEnable;
  final TextStyle style;
  final TextFieldType type;
  // final bool isNextPage;
  @override
  DataTextFieldGroupState createState() => DataTextFieldGroupState();
}

class DataTextFieldGroupState extends State<DataTextFieldGroup> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        padding: rowPadding,
        alignment: Alignment.centerLeft,
        child: Text(widget.title,
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style:placeholderStyle
            //customeThemeData.textTheme.caption,
            ),
      ),
      DataTextField(
        hint: widget.hint,
        initValue: widget.initValue,
        keyboardType: widget.keyboardType,
        onChange: widget.onChange,
        // onFinish: widget.onFinish,
        focusNode: widget.focusNode,
        nextFocusNode: widget.nextFocusNode,
        length: widget.length,
        width: widget.width,
        height: widget.height,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        isValid: widget.isValid,
        isPrice: widget.isPrice,
        isEnable: widget.isEnable,
        style: widget.style,
        type: widget.type,
        // isNextPage: widget.isNextPage,
        // validationType: widget.validationType
      )
    ]);
  }
}

class DataTextField extends StatefulWidget {
  const DataTextField(
      {Key key,
      this.hint,
      this.initValue,
      this.keyboardType,
      this.onChange,
      // this.onFinish,
      this.focusNode,
      this.nextFocusNode,
      this.length,
      this.width,
      this.height,
      this.minLines,
      this.maxLines,
      this.isValid,
      this.isPrice = false,
      this.isEnable = true,
      this.isNextPage = false,
      this.hintStyle,
      // this.validationType = ValidType.None,
      this.style,
      this.type})
      : super(key: key);
  final String hint;
  final String initValue;
  final TextInputType keyboardType;
  final Function onChange;
  // final Function onFinish;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final int length;
  final double width;
  final double height;
  final int minLines;
  final int maxLines;
  final bool isValid;
  final bool isPrice;
  final bool isEnable;
  final bool isNextPage;
  final TextStyle hintStyle;
  final TextStyle style;
  final TextFieldType type;
  // final ValidType validationType;
  @override
  DataTextFieldState createState() => DataTextFieldState();
}

class DataTextFieldState extends State<DataTextField> {
  TextEditingController _textEditingController;
  bool isValid;
  bool isValidFocus;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController = TextEditingController();
    // addListener();
    isValidFocus = false;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // removeListener();
  }

  @override
  Widget build(BuildContext context) {
    List<TextInputFormatter> formatter = [];
    // print("value: ${widget.initValue}, isValidFocus: $isValidFocus, isValid: $isValid, widget.isValid: ${widget.isValid}");
    isValid = isValidFocus ? isValid : widget.isValid;
    // isValidFocus = false;
    if (widget.isPrice) {
      formatter.add(WhitelistingTextInputFormatter.digitsOnly);
      formatter.add(PriceFormatter());
    }
    if (widget.length != null) {
      formatter.add(LengthLimitingTextInputFormatter(widget.length));
    }

    if (widget.type == TextFieldType.BankNumber) {
      formatter.add(WhitelistingTextInputFormatter.digitsOnly);
    }
    /*
    if (widget.isNextPage) {
      removeListener();
    } else {
      addListener();
    }*/
    // TODO: implement build
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
    Container(
    height: widget.height ?? 48,
      width: widget.width ?? MediaQuery.of(context).size.width,
      child: TextFormField(
              enabled: widget.isEnable, //TODO
              // controller: _textEditingController,
              initialValue: widget.initValue,
              keyboardType: widget.keyboardType,
              style: widget.isPrice
                  ? dropdownStyle
                  : widget.style ??
                      textfieldStyle, //customeThemeData.textTheme.bodyText2,
              focusNode: widget.focusNode,
              onChanged: (text) => widget.onChange(text),
              onFieldSubmitted: (text) {
                if (widget.nextFocusNode != null) {
                  FocusScope.of(context).requestFocus(widget.nextFocusNode);
                }
                /*setState(() {
                  switch (widget.validationType) {
                    case ValidType.None:
                    case ValidType.Hkid:
                    case ValidType.Birth:
                      break;
                    case ValidType.Eng:
                      isValid = isEng(text);
                      break;
                    case ValidType.Chi:
                      isValid = isChi(text);
                      break;
                    case ValidType.Email:
                      isValid = isEmail(text);
                      break;
                    case ValidType.Phone:
                      isValid = isPhone(text);
                      break;
                  }
                });*/
              },
              inputFormatters: formatter,
              minLines: widget.minLines ?? 1,
              maxLines: widget.maxLines ?? 1,
              cursorColor: config_primaryColour,
              textAlignVertical: TextAlignVertical.center, //0.5
              decoration: InputDecoration(
                  contentPadding: widget.isPrice
                      ? EdgeInsets.symmetric(horizontal: 20)
                      : EdgeInsets.all(20),
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: config_primaryColour, //config_alertColour,
                          width: 1.0,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(10)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color:
                              isValid ? config_primaryColour : config_alertColour,
                          width: isValid ? 1.0 : 2.0,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(10)),
                  hintText: widget.hint,
                  hintStyle: widget.isPrice
                      ? dropdownStyle
                      : widget.hintStyle ?? txtPlaceholderStyle),
              // validator: validateEmail,
            ),

        ),
          isValid || errorMessage() == null ? Container() :  Text(errorMessage(),style: textfieldStyle.copyWith(color: config_alertColour,fontSize: 12,
              fontWeight: FontWeight.w600))
          ]) ;
  }

  String errorMessage(){
    String errorMessage;
    TextFieldType type = widget.type;
    if(widget.type == TextFieldType.EnglishName){
      errorMessage = "請輸入正確英文姓名";
    } else if (widget.type == TextFieldType.ChineseName){
      errorMessage = "請輸入正確中文姓名";
    } else if (widget.type == TextFieldType.IDCard){
      errorMessage = "請輸入正確身份證號碼";
    } else if (widget.type == TextFieldType.BirthDay){
      errorMessage = "請輸入正確出生日期";
    } else if (widget.type == TextFieldType.HomePhone){
      errorMessage = "請輸入8個位的聯絡電話";
    } else if (widget.type == TextFieldType.Phone){
      errorMessage = "請輸入8個位的聯絡電話";
    } else if (widget.type == TextFieldType.Email){
      errorMessage = "請輸入有效的電郵地址";
    } else if (widget.type == TextFieldType.Address){
      errorMessage = "請輸入郵寄地址";
    }
    return errorMessage;
  }
  /*
  void addListener() {
    if (widget.focusNode != null) {
      // if(widget.focusNode.hasListeners == false){
        widget.focusNode.addListener(focusListener);
      // }
    }
  }

  void removeListener() {
    if (widget.focusNode != null) {
      print('dispose dataTextField');
      widget.focusNode.removeListener(focusListener);
    }
  }

  void focusListener() {
    // print("Has focus A: ${widget.focusNode.hasFocus}, text: ${widget.initValue}");
    if (widget.focusNode.hasFocus == false && widget.initValue.isNotEmpty) {
      //Do checking
      setState(() {
        switch (widget.validationType) {
          case ValidType.None:
          case ValidType.Hkid:
          case ValidType.Birth:
            break;
          case ValidType.Eng:
            isValidFocus = true;
            isValid = isEng(widget.initValue);
            break;
          case ValidType.Chi:
            isValidFocus = true;
            isValid = isChi(widget.initValue);
            break;
          case ValidType.Email:
            isValidFocus = true;
            isValid = isEmail(widget.initValue);
            break;
          case ValidType.Phone:
            isValidFocus = true;
            isValid = isPhone(widget.initValue);
            break;
        }
        if (widget.onFinish != null && isValid) {
          widget.onFinish(_textEditingController.text);
        }
        //print('isValid: ${isValid}');
      });
    } else if (widget.initValue.isEmpty) {
      setState(() {
        isValidFocus = false;
      });
    }
  }
  */
}

class PriceFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    print('oldValue: ${oldValue.text}, newValue: ${newValue.text}');
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    int value = int.parse(newValue.text);
    final formatter = NumberFormat("HK\$ ###,###", "en_US");
    String newText = formatter.format(value);

    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}
