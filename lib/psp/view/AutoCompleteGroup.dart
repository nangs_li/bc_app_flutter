import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/utilities/style.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
import './CustomTypeHead.dart';

class AutoCompleteGroup extends StatefulWidget {
  const AutoCompleteGroup(
      {Key key,
      this.title,
      this.hint,
      this.initValue,
      this.keyboardType,
      this.onSelect,
      this.onChanged_edit,
      this.suggestions,
      this.focusNode,
      this.nextFocusNode,
      this.length,
      this.width,
      this.height,
      this.minLines,
      this.maxLines,
      this.isValid,
      this.isHos = false,
      this.suggestionsBoxController,
      this.textEditingController,
      this.forceRefresh = false,
      this.isEnable = true})
      : super(key: key);
  final String title;
  final String hint;
  final String initValue;
  final TextInputType keyboardType;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final int length;
  final double width;
  final double height;
  final int minLines;
  final int maxLines;
  final bool isValid;
  final Function onSelect;
  final Function onChanged_edit;
  final Function suggestions;
  final bool isHos;
  final SuggestionsBoxController suggestionsBoxController;
  final TextEditingController textEditingController;
  final bool forceRefresh;
  final bool isEnable;

  @override
  AutoCompleteGroupState createState() => AutoCompleteGroupState();
}

class AutoCompleteGroupState extends State<AutoCompleteGroup> {
  TextEditingController _controller;
  SuggestionsBoxController _suggestController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = widget.textEditingController ??
        TextEditingController(text: widget.initValue);
    _suggestController =
        widget.suggestionsBoxController ?? SuggestionsBoxController();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.forceRefresh) {
      _controller.clear();
    }
    // TODO: implement build
    return Column(children: [
      Container(
        padding: rowPadding,
        alignment: Alignment.centerLeft,
        child: Text(widget.title,
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style: widget.isHos
                ? customeThemeData.textTheme.bodyText1
                : (widget.isValid
                    ? placeholderStyle
                    : placeholderStyle.copyWith(color: config_alertColour))
            //customeThemeData.textTheme.caption,
            ),
      ),
      Container(
          height: widget.height ?? 48,
          width: widget.width ?? MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: widget.isHos ? 18 : 3), //18
          child: Stack(fit: StackFit.passthrough, children: [
            Container(
              height: widget.height ?? 48,
              width: MediaQuery.of(context).size.width,
              color: Colors.transparent,
              child: TypeAheadField(
                  suggestionsBoxVerticalOffset: widget.height != null ? -10 : 3,
                  hideOnEmpty: true,
                  isEnable: widget.isEnable,
                  textFieldConfiguration: TextFieldConfiguration(
                      maxLengthEnforced: false,
                      autofocus: false,
                      enabled: widget.isEnable,
                      controller: _controller,
                      keyboardType: widget.keyboardType,
                      focusNode: widget.focusNode,
                      minLines: widget.minLines ?? 1,
                      maxLines: widget.maxLines ?? 1,
                      cursorColor: config_primaryColour,
                      inputFormatters: widget.length != null
                          ? [LengthLimitingTextInputFormatter(widget.length)]
                          : [],
                      onSubmitted: (text) {
                        if (widget.nextFocusNode != null) {
                          FocusScope.of(context)
                              .requestFocus(widget.nextFocusNode);
                        }
                      },
                      onChanged: (text) {
                        widget.onChanged_edit(text);
                      },
                      textAlign: TextAlign.left,
                      style:
                          /*widget.isHos
                              ? customeThemeData.textTheme.bodyText2
                              : dropdownTxtFieldStyle*/
                          dropdownTxtFieldStyle, //customeThemeData.textTheme.bodyText2,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.symmetric(horizontal: 44),
                          //EdgeInsets.only(left: 44, right: 44, top: 14, bottom: 14), //left: 100
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: widget.isValid
                                      ? config_primaryColour
                                      : config_alertColour, //config_alertColour,
                                  width: widget.isValid ? 1.0 : 2.0,
                                  style: BorderStyle.solid),
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: widget.isValid
                                      ? config_primaryColour
                                      : config_alertColour,
                                  width: widget.isValid ? 1.0 : 2.0,
                                  style: BorderStyle.solid),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: widget.hint,
                          hintStyle:
                              dropdownPlaceholderStyle /*widget.isHos
                                ? dropdownPlaceholderStyle
                                : txtPlaceholderStyle,*/
                          )),
                  suggestionsCallback: (pattern) async {
                    // print('pattern: $pattern, widget.suggestions(pattern): ${widget.suggestions(pattern)}');
                    return await widget.suggestions(pattern);
                  },
                  getImmediateSuggestions: true,
                  forceUpdate: widget.forceRefresh,
                  suggestionsBoxController: _suggestController,
                  noItemsFoundBuilder: (context) {
                    return Container();
                  },
                  itemBuilder: (context, suggestion) {
                    // print('suggestion: $suggestion');
                    return widget.forceRefresh
                        ? Container()
                        : Container(
                            padding: EdgeInsets.all(8),
                            //leading: Text("${suggestion['code']}", style: txtPlaceholderStyle ),//Icon(Icons.shopping_cart),
                            child: Text(
                                "${suggestion['nameEng']}, ${suggestion['nameChi']}",
                                style: dropdownTxtFieldStyle),
                            // subtitle: Text('\$${suggestion['price']}'),
                          );
                  },
                  onSuggestionSelected: (suggestion) {
                    _controller = TextEditingController(
                        text:
                            "${suggestion['nameEng']}, ${suggestion['nameChi']}");
                    widget.onSelect(
                        "${suggestion['nameEng']}, ${suggestion['nameChi']}");
                  }),
            ),
            Positioned(
                left: 0,
                child: IconButton(
                    padding: EdgeInsets.all(0),
                    //elevation: 0,
                    color: Colors.transparent,
                    //textColor: config_primaryColour,
                    icon: Icon(
                      Icons.arrow_drop_down,
                      color: config_placeholderColour,
                    ),
                    /*Row(mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(uiData_Text['personalBankPage']['bankNameHin'],
                      style: aboutPageInfoStyle),
                  Icon(Icons.arrow_drop_down)
                ]),*/
                    onPressed: () {
                      if (widget.isEnable) {
                        Future.delayed(Duration(milliseconds: 100), () {
                          _suggestController.open();
                        });
                      }
                      //FocusScope.of(context).requestFocus(widget.focusNode);
                    })),
            _controller.text.isNotEmpty
                ? Positioned(
                    right: 0,
                    child: IconButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          if (widget.isEnable) {
                            setState(() {
                              _controller.clear();
                              _controller.text = "";
                              widget.onSelect('');
                            });
                          }
                        },
                        //elevation: 0,
                        color: Colors.transparent,
                        //textColor: config_primaryColour,
                        icon: Icon(
                          Icons.clear,
                          color: config_alertColour,
                        )),
                  )
                : Container()
          ]))
    ]);
  }
}
