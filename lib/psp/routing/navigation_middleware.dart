import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/keys.dart';
import 'package:icanfight/psp/pages/aboutPage/aboutPage.dart';
import 'package:icanfight/psp/pages/formPage/formPage.dart';
import 'package:icanfight/psp/pages/homePage/homePage.dart';
import 'package:icanfight/psp/pages/step1/drugStatus1Page/DrugStatus1Page.dart';
import 'package:icanfight/psp/pages/step1/drugStatus2Page/DrugStatus2Page.dart';
import 'package:icanfight/psp/pages/step1/drugStatus3Page/DrugStatus3Page.dart';
import 'package:icanfight/psp/pages/step1/familyIncomePage/FamilyIncomePage.dart';
import 'package:icanfight/psp/pages/step1/fundedInfoPage/FundedInfoPage.dart';
import 'package:icanfight/psp/pages/step1/personalBankPage/PersonalBankPage.dart';

import 'package:icanfight/psp/pages/step1/personalDataPage/PersonalDataPage.dart';
import 'package:icanfight/psp/pages/step1/pubOrPriHospital/PubOrPriHospital.dart';
import 'package:icanfight/psp/pages/step1/whereDrugInfoPage/WhereDrugInfoPage.dart';
import 'package:icanfight/psp/pages/step2/step2LandingPage/Step2LandingPage.dart';
import 'package:icanfight/psp/pages/step2/uploadDocListPage/UploadDocListPage.dart';
import 'package:icanfight/psp/pages/step3/AgreePage/AgreePage.dart';
import 'package:icanfight/psp/pages/step3/Step3LandingPage.dart';
import 'package:icanfight/psp/routing/routingAction.dart';

import 'package:flutter/widgets.dart';

import 'package:redux/redux.dart';

class NavigationMiddleware extends MiddlewareClass<AppState> {
/*   var _transitionsBuilder =
      (c, anim, a2, child) => FadeTransition(opacity: anim, child: child);
  var _transitionDuration = Duration(milliseconds: 500); */

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    //SystemChannels.textInput.invokeMethod('TextInput.hide');
    SingletonShareData data = SingletonShareData();
    if(data.alreadyLoadingPspPage == false && !(action is NavigateToHomePageAction)){
      data.alreadyLoadingPspPage = true;
      Keys.navKey.currentState.pushNamed("/mainPage");
     return;
    }



    if (action is NavigateToLoadingageAction) {
      Keys.navKey.currentState.pushNamed("/loading");

      /*  Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: LoadingPage())); */

      /// Navigator.push(action.context, SlideRightRoute(page: LoadingPage()));
    }

    if (action is NavigateToRegistrationAction) {
      Keys.navKey.currentState.pushNamed("/login");
      /*  Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: LoginPage())); */

      //  Navigator.push(action.context, SlideRightRoute(page: LoginPage()));
    }

    if (action is NavigateToLoginPageAction) {
      print("NavigateToLoginPageAction");
      Keys.navKey.currentState.pushNamed("/login");
      /*   Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: LoginPage())); */

      // Navigator.push(action.context, SlideRightRoute(page: LoginPage()));
    }

    if (action is NavigateToHomePageAction) {
        Keys.navKey.currentState.popAndPushNamed("/mainPage");
    }

    if (action is NavigateToBottomBarPage) {
      Keys.navKey.currentState.pushNamed("/mainPage");
    }

    if (action is NavigateToFormPageAction) {
      Keys.navKey.currentState.pushNamed("/form");
/*       Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: FormPage())); */

      /*    Navigator.push(action.context, SlideRightRoute(page: FormPage())); */

    }
    if (action is NavigateBack) {
      Keys.navKey.currentState
          .popUntil((Route<dynamic> route) => route.isFirst);
      //Keys.navKey.currentState.popUntil( ModalRoute.withName('/loading'));
      //Navigator.of(context).popUntil((route) => route.isFirst);
    }

    if (action is NavigateToAboutAction) {
      print("about");
      Keys.navKey.currentState.pushNamed("/about");

      /* Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeftWithFade,
              duration: Duration(milliseconds: 200),
              child: AboutPage()));   */

      //Navigator.push(action.context, SlideRightRoute(page: AboutPage()));

    }

    if (action is NavigateToPersonalDataPageAction) {
      print("personalData");
      Keys.navKey.currentState.pushNamed("/personalData");
      /*     Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.rightToLeft, child: PersonalDataPage())); */
      /*   Navigator.push(action.context, SlideRightRoute(page: PersonalDataPage()));
 */
    }
    if (action is NavigateToPersonalBankPageAction) {
      print("personalBank");
      Keys.navKey.currentState.pushNamed("/personalBank");
    }

    if (action is NavigateToFamilyIncomePageAction) {
      print("familyIncome");
      Keys.navKey.currentState.pushNamed("/familyIncome");
    }

    if (action is NavigateToFundedInfoPageAction) {
      print("NavigateToFundedInfoPageAction");
      Keys.navKey.currentState.pushNamed("/fundedInfo");
    }

    if (action is NavigateToPubOrPriHospitalPageAction) {
      print("NavigateToPubOrPriHospitalPageAction");
      Keys.navKey.currentState.pushNamed("/pubOrPriHospital");
    }

    if (action is NavigateToDrugStatus1PageAction) {
      print("NavigateToDrugStatus1PageAction");
      Keys.navKey.currentState.pushNamed("/drugStatus1");
    }

    if (action is NavigateToDrugStatus2PageAction) {
      print("NavigateToDrugStatus2PageAction");
      Keys.navKey.currentState.pushNamed("/drugStatus2");
    }

    if (action is NavigateToDrugStatus3PageAction) {
      print("NavigateToDrugStatus3PageAction");
      Keys.navKey.currentState.pushNamed("/drugStatus3");
    }

    if (action is NavigateToWhereDrugInfoPageAction) {
      print("NavigateToWhereDrugInfoPageAction");
      Keys.navKey.currentState.pushNamed("/whereDrugInfo");
    }

    if (action is NavigateToStep2LandingPageAction) {
      print("NavigateToStep2LandingPageAction");
      Keys.navKey.currentState.pushNamed("/step2Landing");
    }

    if (action is NavigateToUploadDocListPageAction) {
      print("NavigateToUploadDocListPageAction");
      Keys.navKey.currentState.pushNamed("/uploadDocList");
    }

    if (action is NavigateToIdDetailPageAction) {
      print("NavigateToIdDetailPageAction");
      Keys.navKey.currentState.pushNamed("/idDetail");
    }

    if (action is NavigateToInvoiceDetailPageAction) {
      print("NavigateToInvoiceDetailPageAction");
      Keys.navKey.currentState.pushNamed("/invoiceDetail");
    }

    if (action is NavigateToInvoiceInputPageAction) {
      print("NavigateToInvoiceInputPageAction");
      Keys.navKey.currentState
          .pushNamed("/invoiceInput", arguments: action.args);
    }

     if (action is NavigateToUpdateInvoiceInputPageAction) {
      print("NavigateToUpdateInvoiceInputPageAction");
      Keys.navKey.currentState
          .pushNamed("/updateInvoiceInput", arguments: action.args);
    }



    if (action is NavigateToRefLetterPageAction) {
      print("NavigateToRefLetterPageAction");
      Keys.navKey.currentState.pushNamed("/refLetter");
    }

    if (action is NavigateToMedicalReportPageAction) {
      print("NavigateToMedicalReportPageAction");
      Keys.navKey.currentState.pushNamed("/medicalReport");
    }

    if (action is NavigateToIncomePageAction) {
      print("NavigateToIncomePageAction ");
      Keys.navKey.currentState.pushNamed("/income", arguments: action.args);

      /*      Keys.navKey.currentState.pushNamed(
        "/income",
        arguments: action.userType, */

    }

    if (action is NavigateToFamIncomePageAction) {
      print("NavigateToFamIncomePageAction");
      Keys.navKey.currentState.pushNamed("/famIncome");
    }

    if (action is NavigateToTaxDetailPageAction) {
      print("NavigateToTaxDetailPageAction");

      Keys.navKey.currentState.pushNamed("/taxDetail", arguments: action.args);
      /*    Keys.navKey.currentState.pushNamed("/taxDetail"); */
    }

    if (action is NavigateToIncomeDetailPageAction) {
      print("NavigateToIncomeDetailPageAction");
      Keys.navKey.currentState
          .pushNamed("/incomeDetail", arguments: action.args);
    }

    if (action is NavigateToStep3LandingPageAction) {
      print("NavigateToStep3LandingPageAction");
      Keys.navKey.currentState.pushNamed("/step3Landing");
    }

    if (action is NavigateToAgreePageAction) {
      print("NavigateToAgreePageAction");
      Keys.navKey.currentState.pushNamed("/agree");
    }

    if (action is NavigateToStatementPageAction) {
      print("NavigateToStatementPageAction");
      Keys.navKey.currentState.pushNamed("/statement");
    }

    if (action is NavigateToFinishPageAction) {
      print("NavigateToFinishPageAction");
      Keys.navKey.currentState
          .pushNamedAndRemoveUntil("/finish", (Route<dynamic> route) => false);
    }

    if (action is NavigateToUploadBankStatementPageAction) {
      print("NavigateToUploadBankStatementPageAction");

/*  work     Keys.navKey.currentState
          .pushNamed("/uploadBankStatement", arguments: action.args); */

      Keys.navKey.currentState.pushNamedAndRemoveUntil(
          "/uploadBankStatement", ModalRoute.withName('/uploadBankStatement'),
          arguments: action.args);
    }

    if (action is NavigateToBankDataTypePageAction) {
      print("NavigateToBankDataTypePageAction");

/*       Keys.navKey.currentState.pushNamed("/bankData"); */

      Keys.navKey.currentState.pushNamed("/bankData", arguments: action.args);
    }

    if (action is NavigateToBankStatementDetailPageAction) {
      print("NavigateToBankStatementDetailPageAction");
/*       Keys.navKey.currentState
          .popAndPushNamed("/bankStatementDetail", arguments: action.args); */

      Keys.navKey.currentState.pushNamedAndRemoveUntil(
          "/bankStatementDetail", ModalRoute.withName('/uploadBankStatement'),
          arguments: action.args);
    }

    if (action is NavigateToBankBookInputPageAction) {
      print("NavigateToBankBookInputPageAction");
      Keys.navKey.currentState
          .pushNamed("/bankBookInput", arguments: action.args);

      // Navigator.popAndPushNamed(context, newRoute)
    }

    if (action is NavigateToBankBookDetailPageAction) {
      print("NavigateToBankBookDetailPageAction");
      Keys.navKey.currentState
          .pushNamed("/bankBookDetail", arguments: action.args);
    }

    if (action is NavigateToUpdateBankBookInputPageAction) {
      print("NavigateToUpdateBankBookInputPageAction");
      Keys.navKey.currentState
          .pushNamed("/updateBankBookInput", arguments: action.args);
    }

//pop -------------------------------------------------------------------------

    if (action is PopFromRegistrationAction) {
      print("PopFromRegistrationAction");
    }

    if (action is PopFromRLoadingageAction) {
      print("PopFromRLoadingageAction");
    }

    if (action is PopFromRLoginPageAction) {
      print("PopFromRLoginPageAction");
    }
    if (action is PopFromRHomePageAction) {
      print("PopFromRHomePageAction");
    }
    if (action is PopFromRFormPageAction) {
      print("PopFromRFormPageAction");

      //Navigator.push(context, PageTransition(type: PageTransitionType.leftToRight, child: DetailScreen()));
      /*   Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.leftToRightWithFade,
              duration: Duration(milliseconds: 200),
              child: AboutPage()));   */

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => AboutPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );

      /*    Navigator.push(action.context, SlideLeftRoute(page: AboutPage())); */

/* Navigator.push(action.context,
    EnterExitRoute(exitPage: action.widget, enterPage: AboutPage())); */

      //Keys.navKey.currentState.popAndPushNamed('/about');
      //Keys.navKey.currentState.popUntil(ModalRoute.withName('/about'));

    }
    if (action is PopFromRAboutAction) {
      print("PopFromRAboutAction");
      //Keys.navKey.currentState.pushNamed("/home");
      /*   Navigator.push(
          action.context,
          PageTransition(
              type: PageTransitionType.leftToRightWithFade,
              duration: Duration(milliseconds: 200),
              child: HomePage()));   */
/* 
      Keys.navKey.currentState
          .pushNamedAndRemoveUntil("/home", (Route<dynamic> route) => false); 
 
 */

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => HomePage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );

      /*   Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) => HomePage(),
        ),
      ); */
    }
    if (action is PopFromRPersonalDataPageAction) {
      print("PopFromRPersonalDataPageAction");
      // Keys.navKey.currentState.pushNamed("/form");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => FormPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }
    if (action is PopFromRPersonalBankPageAction) {
      print("PopFromRPersonalBankPageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => PersonalDataPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRFamilyIncomePageAction) {
      print("PopFromRFamilyIncomePageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => PersonalBankPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRFundedInfoPageAction) {
      print("PopFromRFundedInfoPageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => FamilyIncomePage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRPubOrPriHospitalPageAction) {
      print("PopFromRPubOrPriHospitalPageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => FundedInfoPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRDrugStatus1PageAction) {
      print("PopFromRDrugStatus1PageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => PubOrPriHospitalPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRDrugStatus2PageAction) {
      print("PopFromRDrugStatus2PageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => DrugStatus1Page(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRDrugStatus3PageAction) {
      print("PopFromRDrugStatus3PageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => DrugStatus2Page(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRWhereDrugInfoPageAction) {
      print("PopFromRWhereDrugInfoPageAction");
      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => DrugStatus3Page(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRStep2LandingPageAction) {
      print("PopFromRStep2LandingPageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => WhereDrugInfoPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRUploadDocListPageAction) {
      print("PopFromRUploadDocListPageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => Step2LandingPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRIdDetailPageAction) {
      print("PopFromRIdDetailPageAction");
    }

    if (action is PopFromRInvoiceDetailPageAction) {
      print("PopFromRInvoiceDetailPageAction");
    }

    if (action is PopFromRInvoiceInputPageAction) {
      print("PopFromRInvoiceInputPageAction");
    }

    if (action is PopFromRRefLetterPageAction) {
      print("PopFromRRefLetterPageAction");
    }

    if (action is PopFromRMedicalReportPageAction) {
      print("PopFromRMedicalReportPageAction");
    }

    if (action is PopFromRIncomePageAction) {
      print("PopFromRIncomePageAction");
    }

    if (action is PopFromRFamIncomePageAction) {
      print("PopFromRFamIncomePageAction");
    }

    if (action is PopFromRTaxDetailPageAction) {
      print("PopFromRTaxDetailPageAction");
    }

    if (action is PopFromRIncomeDetailPageAction) {
      print("PopFromRIncomeDetailPageAction");
    }

    if (action is PopFromRStep3LandingPageAction) {
      print("PopFromRStep3LandingPageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => UploadDocListPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRAgreePageAction) {
      print("PopFromRAgreePageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => Step3LandingPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRStatementPageAction) {
      print("PopFromRStatementPageAction");

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => AgreePage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    if (action is PopFromRFinishPageAction) {
      print("PopFromRFinishPageAction");
    }

    if (action is PopFromRUploadBankStatementPageAction) {
      print("PopFromRUploadBankStatementPageAction");
    }

    if (action is PopFromRBankDataTypePageAction) {
      print("PopFromRBankDataTypePageAction");
    }

    if (action is PopFromRBankStatementDetailPageAction) {
      print("PopFromRBankStatementDetailPageAction");
    }

    if (action is PopFromRBankBookInputPageAction) {
      print("PopFromRBankBookInputPageAction");
    }

    if (action is PopFromRBankBookDetailPageAction) {
      print("PopFromRBankBookDetailPageAction");
    }

    if (action is PopToUploadDocListPageAction) {
      print("PopToUploadDocListPageAction");

      //Keys.navKey.currentState.popUntil(ModalRoute.withName('/uploadDocList'));

      Navigator.pushReplacement(
        action.context,
        PageRouteBuilder(
          pageBuilder: (c, a1, a2) => UploadDocListPage(),
/*           transitionsBuilder: _transitionsBuilder,
          transitionDuration: _transitionDuration, */
        ),
      );
    }

    next(action);
  }
}
