import 'package:icanfight/psp/models/PassImageListArguments.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:flutter/material.dart';

class NavigateToRegistrationAction {
  final BuildContext context;
  NavigateToRegistrationAction(this.context);
}

class NavigateToLoadingageAction {
  final BuildContext context;
  NavigateToLoadingageAction(this.context);
}

class NavigateToLoginPageAction {
  final BuildContext context;
  NavigateToLoginPageAction(this.context);
}

class NavigateToHomePageAction {}

class NavigateToBottomBarPage {}

class NavigateToFormPageAction {
  final BuildContext context;
  NavigateToFormPageAction(this.context);
}

class NavigateBack {}

class NavigateToAboutAction {
  final BuildContext context;
  NavigateToAboutAction(this.context);
}

class NavigateToPersonalDataPageAction {
  final BuildContext context;
  NavigateToPersonalDataPageAction(this.context);
}

class NavigateToPersonalBankPageAction {}

class NavigateToFamilyIncomePageAction {}

class NavigateToFundedInfoPageAction {}

class NavigateToPubOrPriHospitalPageAction {}

class NavigateToDrugStatus1PageAction {}

class NavigateToDrugStatus2PageAction {}

class NavigateToDrugStatus3PageAction {}

class NavigateToWhereDrugInfoPageAction {}

class NavigateToStep2LandingPageAction {}

class NavigateToUploadDocListPageAction {}

class NavigateToIdDetailPageAction {}

class NavigateToInvoiceDetailPageAction {
/*   final ScreenArguments args;
  NavigateToInvoiceDetailPageAction(this.args); */
}

class NavigateToInvoiceInputPageAction {
  final PassImageListArguments args;
  NavigateToInvoiceInputPageAction(this.args);
}


class NavigateToUpdateInvoiceInputPageAction {
  final ScreenArguments args;
  NavigateToUpdateInvoiceInputPageAction(this.args);
}



class NavigateToRefLetterPageAction {}

class NavigateToMedicalReportPageAction {}

class NavigateToIncomePageAction {
  final ScreenArguments args;
  NavigateToIncomePageAction(this.args);
/*   final UserType userType;

  NavigateToIncomePageAction(this.userType); */
}

class NavigateToFamIncomePageAction {}

class NavigateToTaxDetailPageAction {
  /*  final UserType userType;
  final List<Location> imageLocationList;
  final int memberID; */
  final ScreenArguments args;

  NavigateToTaxDetailPageAction(this.args);
}

class NavigateToIncomeDetailPageAction {
  final ScreenArguments args;
  NavigateToIncomeDetailPageAction(this.args);
}

class NavigateToStep3LandingPageAction {}

class NavigateToAgreePageAction {}

class NavigateToStatementPageAction {}

class NavigateToFinishPageAction {}

class NavigateToUploadBankStatementPageAction {
  final ScreenArguments args;
  NavigateToUploadBankStatementPageAction(this.args);
}

class NavigateToBankDataTypePageAction {
  final ScreenArguments args;
  NavigateToBankDataTypePageAction(this.args);
}

class NavigateToBankStatementDetailPageAction {
  final ScreenArguments args;
  NavigateToBankStatementDetailPageAction(this.args);
}

class NavigateToBankBookInputPageAction {
  final PassImageListArguments args;
  NavigateToBankBookInputPageAction(this.args);
}

class NavigateToBankBookDetailPageAction {
  final ScreenArguments args;
  NavigateToBankBookDetailPageAction(this.args);
}

class NavigateToUpdateBankBookInputPageAction {
  final ScreenArguments args;
  NavigateToUpdateBankBookInputPageAction(this.args);
}

class PopToUploadDocListPageAction {
  final BuildContext context;
  PopToUploadDocListPageAction(this.context);
}

class PopFromRegistrationAction {}

class PopFromRLoadingageAction {}

class PopFromRLoginPageAction {}

class PopFromRHomePageAction {}

class PopFromRFormPageAction {
  final BuildContext context;
  PopFromRFormPageAction(this.context);
}

class PopFromRAboutAction {
  final BuildContext context;
  PopFromRAboutAction(this.context);
}

class PopFromRPersonalDataPageAction {
  final BuildContext context;
  PopFromRPersonalDataPageAction(this.context);
}

class PopFromRPersonalBankPageAction {
  final BuildContext context;
  PopFromRPersonalBankPageAction(this.context);
}

class PopFromRFamilyIncomePageAction {
  final BuildContext context;
  PopFromRFamilyIncomePageAction(this.context);
}

class PopFromRFundedInfoPageAction {
  final BuildContext context;
  PopFromRFundedInfoPageAction(this.context);
}

class PopFromRPubOrPriHospitalPageAction {
  final BuildContext context;
  PopFromRPubOrPriHospitalPageAction(this.context);
}

class PopFromRDrugStatus1PageAction {
  final BuildContext context;
  PopFromRDrugStatus1PageAction(this.context);
}

class PopFromRDrugStatus2PageAction {
  final BuildContext context;
  PopFromRDrugStatus2PageAction(this.context);
}

class PopFromRDrugStatus3PageAction {
  final BuildContext context;
  PopFromRDrugStatus3PageAction(this.context);
}

class PopFromRWhereDrugInfoPageAction {
  final BuildContext context;
  PopFromRWhereDrugInfoPageAction(this.context);
}

class PopFromRStep2LandingPageAction {
  final BuildContext context;
  PopFromRStep2LandingPageAction(this.context);
}

class PopFromRUploadDocListPageAction {
  final BuildContext context;
  PopFromRUploadDocListPageAction(this.context);
}

class PopFromRIdDetailPageAction {
  final BuildContext context;
  PopFromRIdDetailPageAction(this.context);
}

class PopFromRInvoiceDetailPageAction {
  final BuildContext context;
  PopFromRInvoiceDetailPageAction(this.context);
}

class PopFromRInvoiceInputPageAction {
  final BuildContext context;
  PopFromRInvoiceInputPageAction(this.context);
}

class PopFromRRefLetterPageAction {
  final BuildContext context;
  PopFromRRefLetterPageAction(this.context);
}

class PopFromRMedicalReportPageAction {
  final BuildContext context;
  PopFromRMedicalReportPageAction(this.context);
}

class PopFromRIncomePageAction {
  final BuildContext context;
  PopFromRIncomePageAction(this.context);
}

class PopFromRFamIncomePageAction {
  final BuildContext context;
  PopFromRFamIncomePageAction(this.context);
}

class PopFromRTaxDetailPageAction {
  final BuildContext context;
  PopFromRTaxDetailPageAction(this.context);
}

class PopFromRIncomeDetailPageAction {
  final BuildContext context;
  PopFromRIncomeDetailPageAction(this.context);
}

class PopFromRStep3LandingPageAction {
  final BuildContext context;
  PopFromRStep3LandingPageAction(this.context);
}

class PopFromRAgreePageAction {
  final BuildContext context;
  PopFromRAgreePageAction(this.context);
}

class PopFromRStatementPageAction {
  final BuildContext context;
  PopFromRStatementPageAction(this.context);
}

class PopFromRFinishPageAction {
  final BuildContext context;
  PopFromRFinishPageAction(this.context);
}

class PopFromRUploadBankStatementPageAction {
  final BuildContext context;
  PopFromRUploadBankStatementPageAction(this.context);
}

class PopFromRBankDataTypePageAction {
  final BuildContext context;
  PopFromRBankDataTypePageAction(this.context);
}

class PopFromRBankStatementDetailPageAction {
  final BuildContext context;
  PopFromRBankStatementDetailPageAction(this.context);
}

class PopFromRBankBookInputPageAction {
  final BuildContext context;
  PopFromRBankBookInputPageAction(this.context);
}

class PopFromRBankBookDetailPageAction {
  final BuildContext context;
  PopFromRBankBookDetailPageAction(this.context);
}
