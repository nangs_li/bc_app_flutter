import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/ValidationCtr.dart';
import 'package:icanfight/psp/models/Locale.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/CustomDateTimePicker.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/CheckBoxGroup.dart';

import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import '../../../app/theme.dart';

//comment
class PersonalDataPage extends StatefulWidget {
  @override
  _PersonalDataPageState createState() => _PersonalDataPageState();
}

class _PersonalDataPageState extends State<PersonalDataPage> {
  int current_percentage;
  String gender;
  String hkCitizen;
  DateTime birthDate;
  DateTime today;
  String marriage;
  String otherMarriage;
  String employ;
  String director;
  List<FocusNode> focusNode = List();
  FocusNode otherFocusNode = FocusNode();
  NextBtnController _nextBtnController;

  ValidationController _validationController;
  List<String> dataL = List();
  List<ValidType> validationTypeList = List();
  List<bool> validList = List();
  bool _isInitValidationCtr;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRPersonalDataPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 5; //0
    // Testing Code
    gender = '';
    hkCitizen = '';
    marriage = '';
    employ = '';
    director = '';
    birthDate = DateTime.now();
    today = DateTime.now();
    focusNode = List.generate(15, (index) => FocusNode());
    /*[
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ]; */
    validationTypeList = [
      ValidType.Eng,
      ValidType.Chi,
      ValidType.Hkid,
      ValidType.Hkid,
      ValidType.Hkid,
      ValidType.None,
      ValidType.None,
      ValidType.Birth,
      ValidType.Phone,
      ValidType.Phone,
      ValidType.Email,
      ValidType.None,
      ValidType.None,
      ValidType.None,
      ValidType.None
    ];
    validList = List.generate(15, (index) => true);
    _isInitValidationCtr = false;
    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.PersonalDataPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            /*int index = focusNode.indexWhere((focus) => focus.hasFocus == true);
            print('index: ${index}');
            //TODO: DataValidation for HKID (Special Case)
            switch (index) {
              case 2:
              case 3:
              case 4:
                dataValidation(index);
                break;
              default:
                break;
            }*/
            // print(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(widget.initValue));
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
              appBar: AppBar(
                /*      title: Text('申請程序'), */
                brightness: Brightness.dark,
                titleSpacing: 0.0,
                elevation: 0.0,
                leading: IconButton(
                  iconSize: 30.0,
                  icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                  onPressed: () => {
                    StoreProvider.of<AppState>(context)
                        .dispatch(PopFromRPersonalDataPageAction(context))
                  },
                ),
                actions: <Widget>[SaveBtnWrapper()],
              ),
              body: StoreConnector<AppState, Data_State>(
                converter: (Store<AppState> store) => store.state.dataState,
                builder: (BuildContext context, Data_State dataState) {
                  dataL = [
                    dataState.nameEng,
                    dataState.nameChinese,
                    dataState.idChar,
                    dataState.idFirstNum,
                    dataState.idLastNum,
                    dataState.gender,
                    dataState.hkCitizen,
                    dataState.birthDay,
                    dataState.contactNum,
                    dataState.mobileContactNum,
                    dataState.emailAddress,
                    dataState.homeAddress,
                    dataState.maritalStatus,
                    dataState.job,
                    dataState.isDirector
                  ];
                  _nextBtnController = NextBtnController(
                      dataList: dataL,
                      updateValid: updateValid,
                      validTypeList: validationTypeList,
                      exceptionList: [1, 8, 11]);
                  _validationController = ValidationController(
                      focusNodes: focusNode,
                      validationTypes: validationTypeList,
                      value: dataL,
                      isValid: validList,
                      exceptionList: [1, 8, 11],
                      getUpdateValue: (int index) {
                        return dataL[index];
                      },
                      onFinish: (int index, bool isValid) {
                        StoreProvider.of<AppState>(context)
                            .dispatch(SaveAppDataToLocalAction());
                        setState(() {
                          validList[index] = isValid;
                        });
                      });
                  if (_isInitValidationCtr == false) {
                    _validationController.init();
                    _isInitValidationCtr = true;
                  }

                  return Stack(children: <Widget>[
                    Container(
                        decoration:
                            BoxDecoration(color: config_normalBGColour)),
                    PrograssBar(percentage: current_percentage, step: 0),
                    Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SingleChildScrollView(
                          //EdgeInsets.all(20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(left: 23, right: 23),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(top: 33),
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            uiData_Text['personalDataPage']
                                                    ['title']
                                                .toString(),
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            style: formPageTitleStyle,
                                          ),
                                        ),
                                        //Name (ENG) - validList 0
                                        DataTextFieldGroup(
                                          type: TextFieldType.EnglishName,
                                            title:
                                                uiData_Text['personalDataPage']
                                                        ['nameEngLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['nameEngHin']
                                                    .toString(),
                                            keyboardType: TextInputType.text,
                                            initValue: dataState.nameEng,
                                            onChange: (text) {
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_NameEng_Action(
                                                          text));
                                            },
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[0],
                                            nextFocusNode: focusNode[1],
                                            isValid: validList[0],
                                            // validationType: ValidType.Eng,
                                            isEnable:
                                                dataState.isSentEmail == false),
                                        //Name (CHI) TODO: add onChange validList[1]
                                        DataTextFieldGroup(
                                            type: TextFieldType.ChineseName,
                                            title:
                                                uiData_Text['personalDataPage']
                                                        ['nameChineseLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['nameChineseHin']
                                                    .toString(),
                                            keyboardType: TextInputType.text,
                                            initValue: dataState.nameChinese,
                                            onChange: (text) {
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_NameChinese_Action(
                                                          text));
                                            },
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[1],
                                            nextFocusNode: focusNode[2],
                                            isValid: validList[1],
                                            // validationType: ValidType.Chi,
                                            isEnable:
                                                dataState.isSentEmail == false),
                                        //HKID TODO: add onChange validList 2,3,4
                                        Container(
                                          padding: rowPadding,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                              uiData_Text['personalDataPage']
                                                      ['id_charLable']
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              overflow: TextOverflow.ellipsis,
                                              style: placeholderStyle),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 3),
                                          // width: 206,
                                          child: Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 73, // 20%
                                                child: Container(
                                                  height: 48,
                                                  child: TextFormField(
                                                    inputFormatters: [
                                                      WhitelistingTextInputFormatter(RegExp("[a-zA-Z0-9]")),
                                                      LengthLimitingTextInputFormatter(
                                                          2),
                                                    ],
                                                    focusNode: focusNode[2],
                                                    keyboardType:
                                                        TextInputType.text,
                                                    initialValue:
                                                        dataState.idChar,
                                                    style: textfieldStyle,
                                                    enabled:
                                                        dataState.isSentEmail ==
                                                            false,
                                                    onChanged: (text) {
                                                      StoreProvider.of<
                                                              AppState>(context)
                                                          .dispatch(
                                                              Change_IdChar_Action(
                                                                  text));
                                                    },
                                                    onFieldSubmitted: (text) {
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              focusNode[3]);
                                                    },
                                                    cursorColor:
                                                        config_primaryColour,
                                                    textAlignVertical:
                                                        TextAlignVertical(
                                                            y: dataState.idChar
                                                                    .isNotEmpty
                                                                ? 0
                                                                : -0.5),
                                                    textAlign: TextAlign.center,
                                                    decoration: InputDecoration(
                                                      filled: true,
                                                      fillColor: Colors.white,
                                                      border: OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color:
                                                                  config_primaryColour,
                                                              width: 1.0,
                                                              style: BorderStyle
                                                                  .solid),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                      enabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: validList[
                                                                      2]
                                                                  ? config_primaryColour
                                                                  : config_alertColour,
                                                              width:
                                                                  validList[2]
                                                                      ? 1.0
                                                                      : 2.0,
                                                              style: BorderStyle
                                                                  .solid),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                      hintText: uiData_Text[
                                                                  'personalDataPage']
                                                              ['id_charHin']
                                                          .toString(),
                                                      hintStyle:
                                                          txtPlaceholderStyle,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(width: 6),
                                              SizedBox(
                                                  width: 110, //89 // 20%
                                                  child: Container(
                                                    height: 48,
                                                    child: TextFormField(
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(RegExp("[0-9]")),
                                                        LengthLimitingTextInputFormatter(
                                                            6),
                                                      ],
                                                      keyboardType:
                                                          TextInputType.number,
                                                      initialValue:
                                                          dataState.idFirstNum,
                                                      focusNode: focusNode[3],
                                                      style: textfieldStyle,
                                                      enabled: dataState
                                                              .isSentEmail ==
                                                          false,
                                                      onChanged: (text) {
                                                        StoreProvider.of<
                                                                    AppState>(
                                                                context)
                                                            .dispatch(
                                                                Change_idFirstNum_Action(
                                                                    text));
                                                      },
                                                      onFieldSubmitted: (text) {
                                                        FocusScope.of(context)
                                                            .requestFocus(
                                                                focusNode[4]);
                                                      },
                                                      cursorColor:
                                                          config_primaryColour,
                                                      textAlign:
                                                          TextAlign.center,
                                                      textAlignVertical:
                                                          TextAlignVertical(
                                                              y: dataState
                                                                      .idFirstNum
                                                                      .isNotEmpty
                                                                  ? 0
                                                                  : -0.5),
                                                      decoration: InputDecoration(
                                                          filled: true,
                                                          fillColor:
                                                              Colors.white,
                                                          border: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color:
                                                                      config_primaryColour,
                                                                  width: 1.0,
                                                                  style: BorderStyle
                                                                      .solid),
                                                              borderRadius: BorderRadius.circular(
                                                                  10)),
                                                          enabledBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: validList[3]
                                                                      ? config_primaryColour
                                                                      : config_alertColour,
                                                                  width: validList[3]
                                                                      ? 1.0
                                                                      : 2.0,
                                                                  style: BorderStyle
                                                                      .solid),
                                                              borderRadius: BorderRadius.circular(
                                                                  10)),
                                                          hintText: uiData_Text['personalDataPage']
                                                                  ['id_firstNumHin']
                                                              .toString(),
                                                          hintStyle: txtPlaceholderStyle),
                                                    ),
                                                  )),
                                              SizedBox(width: 6),
                                              SizedBox(
                                                  width: 73, // 20%
                                                  child: Container(
                                                    height: 48,
                                                    child: TextFormField(
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(RegExp("[a-zA-Z0-9]")),
                                                        LengthLimitingTextInputFormatter(
                                                            1),
                                                        // HKIDFormatter()
                                                      ],
                                                      keyboardType:
                                                          TextInputType.text,
                                                      initialValue:
                                                          dataState.idLastNum,
                                                      focusNode: focusNode[4],
                                                      style: textfieldStyle,
                                                      enabled: dataState
                                                              .isSentEmail ==
                                                          false,
                                                      onChanged: (text) {
                                                        StoreProvider.of<
                                                                    AppState>(
                                                                context)
                                                            .dispatch(
                                                                Change_idLastNum_Action(
                                                                    text));
                                                      },
                                                      cursorColor:
                                                          config_primaryColour,
                                                      textAlign:
                                                          TextAlign.center,
                                                      textAlignVertical:
                                                          TextAlignVertical(
                                                              y: dataState
                                                                      .idLastNum
                                                                      .isNotEmpty
                                                                  ? 0
                                                                  : -0.5),
                                                      decoration: InputDecoration(
                                                          filled: true,
                                                          fillColor:
                                                              Colors.white,
                                                          border: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color:
                                                                      config_primaryColour,
                                                                  width: 1.0,
                                                                  style: BorderStyle
                                                                      .solid),
                                                              borderRadius: BorderRadius.circular(
                                                                  10)),
                                                          enabledBorder: OutlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: validList[4]
                                                                      ? config_primaryColour
                                                                      : config_alertColour,
                                                                  width: validList[4]
                                                                      ? 1.0
                                                                      : 2.0,
                                                                  style: BorderStyle
                                                                      .solid),
                                                              borderRadius: BorderRadius.circular(
                                                                  10)),
                                                          hintText: uiData_Text['personalDataPage']
                                                                  ['id_lastNumHin']
                                                              .toString(),
                                                          hintStyle: txtPlaceholderStyle),
                                                    ),
                                                  )),
                                              Expanded(
                                                flex: 30,
                                                child: Row(),
                                              )
                                            ],
                                          ),
                                        ),
                                        validList[2] &&
                                            validList[3] &&
                                            validList[4] ? Container() :  Text("請輸入正確身份證號碼",style: textfieldStyle.copyWith(color: config_alertColour,fontSize: 12,
                                            fontWeight: FontWeight.w600)),
                                        //Gender TODO: update check event validList 5
                                        CheckBoxGroup(
                                          title: uiData_Text['personalDataPage']
                                                  ['genderLable']
                                              .toString(),
                                          value: dataState.gender,
                                          isValid: validList[5],
                                          labelFirst:
                                              uiData_Text['personalDataPage']
                                                      ['genderFemaleLable']
                                                  .toString(),
                                          labelSecond:
                                              uiData_Text['personalDataPage']
                                                      ['genderMaleLable']
                                                  .toString(),
                                          choiceFirst:
                                              uiData_Text['personalDataPage']
                                                      ['genderFemaleHin']
                                                  .toString(),
                                          choiceSecond:
                                              uiData_Text['personalDataPage']
                                                      ['genderMaleHin']
                                                  .toString(),
                                          isThirdChoice: false,
                                          isEnable:
                                              dataState.isSentEmail == false,
                                          check: (data) {
                                            setState(() {
                                              validList[5] = true;
                                              gender = data == true
                                                  ? uiData_Text[
                                                          'personalDataPage']
                                                      ['genderFemaleHin']
                                                  : uiData_Text[
                                                          'personalDataPage']
                                                      ['genderMaleHin'];
                                            });

                                            StoreProvider.of<AppState>(context)
                                                .dispatch(Change_gender_Action(
                                                    gender));
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction());
                                          },
                                        ),
                                        //HKCitizen TODO: update check event validList 6
                                        CheckBoxGroup(
                                          title: uiData_Text['personalDataPage']
                                                  ['hkCitizenLable']
                                              .toString(),
                                          value:
                                              dataState.hkCitizen, //hkCitizen,
                                          isValid: validList[6],
                                          labelFirst:
                                              uiData_Text['personalDataPage']
                                                      ['yesLable']
                                                  .toString(),
                                          labelSecond:
                                              uiData_Text['personalDataPage']
                                                      ['noLable']
                                                  .toString(),
                                          choiceFirst:
                                              uiData_Text['personalDataPage']
                                                      ['yesHin']
                                                  .toString(),
                                          choiceSecond:
                                              uiData_Text['personalDataPage']
                                                      ['noHin']
                                                  .toString(),
                                          isThirdChoice: false,
                                          isEnable:
                                              dataState.isSentEmail == false,
                                          check: (data) {
                                            setState(() {
                                              validList[6] = true;
                                              hkCitizen = data == true
                                                  ? uiData_Text[
                                                          'personalDataPage']
                                                      ['yesHin']
                                                  : uiData_Text[
                                                          'personalDataPage']
                                                      ['noHin'];
                                            });

                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    Change_hkCitizen_Action(
                                                        hkCitizen));
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction());
                                          },
                                        ),
                                        //Birthday validList7
                                        Container(
                                          padding: rowPadding,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                              uiData_Text['personalDataPage']
                                                      ['birthDayLable']
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              overflow: TextOverflow.ellipsis,
                                              style: validList[7]
                                                  ? placeholderStyle
                                                  : placeholderStyle.copyWith(
                                                      color:
                                                          config_alertColour)),
                                        ),
                                        Container(
                                          height: 48,
                                          width: 200, //144
                                          padding: EdgeInsets.only(top: 3),
                                          alignment: Alignment.centerLeft,
                                          child: RaisedButton(
                                              color: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(
                                                      10.0),
                                                  side: BorderSide(
                                                      color: validList[7]
                                                          ? config_primaryColour
                                                          : config_alertColour,
                                                      width: validList[7]
                                                          ? 1.0
                                                          : 2.0,
                                                      style:
                                                          BorderStyle.solid)),
                                              elevation: 0.0,
                                              padding: EdgeInsets.only(
                                                  left: 12, right: 12),
                                              onPressed: () {
                                                if (dataState.isSentEmail ==
                                                    false) {
                                                  _selectDate(
                                                      context,
                                                      dataState.birthDay != ''
                                                          ? DateTime.parse(
                                                              dataState
                                                                  .birthDay)
                                                          : DateTime(
                                                              DateTime.now().year -
                                                                  18,
                                                              DateTime.now()
                                                                  .month,
                                                              DateTime.now()
                                                                  .day));
                                                }
                                              },
                                              child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                      dataState.birthDay != ''
                                                          ? DateFormat("yyyy年M月d日").format(
                                                              DateTime.parse(dataState
                                                                  .birthDay))
                                                          : uiData_Text['personalDataPage']['birthDayHin']
                                                              .toString(),
                                                      // dataState.birthDay,
                                                      // Change_birthDay_Action(picked.toIso8601String()
                                                      style: dataState.birthDay != ''
                                                          ? textfieldStyle
                                                          : txtPlaceholderStyle))),
                                        ),
                                        validList[7] ? Container() :  Text("請輸入正確出生日期",style: textfieldStyle.copyWith(color: config_alertColour,fontSize: 12,
                                            fontWeight: FontWeight.w600)),

                                        //Contact No. (Home)
                                        DataTextFieldGroup(
                                            type: TextFieldType.HomePhone,
                                            title:
                                                uiData_Text['personalDataPage']
                                                        ['contactHomeNumLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['contactHomeNumHin']
                                                    .toString(),
                                            keyboardType: TextInputType.phone,
                                            initValue: dataState.contactNum,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_contactNum_Action(
                                                        text)),
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[8],
                                            nextFocusNode: focusNode[9],
                                            length: 8,
                                            width: 251,
                                            isValid: validList[8],
                                            // validationType: ValidType.Phone,
                                            isEnable:
                                                dataState.isSentEmail == false),

                                        //Contact No. (Mobile)
                                        DataTextFieldGroup(
                                            type: TextFieldType.Phone,
                                            title: uiData_Text[
                                                        'personalDataPage']
                                                    ['contactMobileNumLable']
                                                .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['contactMobileNumHin']
                                                    .toString(),
                                            keyboardType: TextInputType.phone,
                                            initValue:
                                                dataState.mobileContactNum,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_mobileContactNum_Action(
                                                        text)),
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[9],
                                            nextFocusNode: focusNode[10],
                                            length: 8,
                                            width: 251,
                                            isValid: validList[9],
                                            // validationType: ValidType.Phone,
                                            isEnable:
                                                dataState.isSentEmail == false),
                                        //Email
                                        DataTextFieldGroup(
                                            type: TextFieldType.Email,
                                            title:
                                                uiData_Text['personalDataPage']
                                                        ['emailLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['emailHin']
                                                    .toString(),
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            initValue: dataState.emailAddress,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_emailAddress_Action(
                                                        text)),
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[10],
                                            nextFocusNode: focusNode[11],
                                            isValid: validList[10],
                                            // validationType: ValidType.Email,
                                            isEnable:
                                                dataState.isSentEmail == false),
                                        //Address
                                        DataTextFieldGroup(
                                            type: TextFieldType.Address,
                                            title:
                                                uiData_Text['personalDataPage']
                                                        ['addressLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalDataPage']
                                                        ['addressHin']
                                                    .toString(),
                                            keyboardType:
                                                TextInputType.multiline,
                                            initValue: dataState.homeAddress,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_homeAddress_Action(
                                                        text)),
                                            /*onFinish: (text) =>
                                                StoreProvider.of<AppState>(context)
                                                    .dispatch(
                                                        SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[11],
                                            nextFocusNode: null,
                                            height: 168,
                                            minLines: 7,
                                            maxLines: 8,
                                            isValid: validList[11],
                                            isEnable:
                                                dataState.isSentEmail == false),

                                        //Other Detail
                                        Container(
                                          padding: EdgeInsets.only(top: 33),
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            uiData_Text['personalDataPage']
                                                    ['otherTitle']
                                                .toString(),
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            style: formPageTitleStyle,
                                          ),
                                        ),
                                        //Marriage
                                        CheckBoxGroup(
                                          title: uiData_Text['personalDataPage']
                                                  ['marriageLable']
                                              .toString(),
                                          value: dataState.maritalStatus,
                                          isValid: validList[12],
                                          labelFirst:
                                              uiData_Text['personalDataPage']
                                                      ['marriageSingleLable']
                                                  .toString(),
                                          labelSecond:
                                              uiData_Text['personalDataPage']
                                                      ['marriageMarryLable']
                                                  .toString(),
                                          labelThird:
                                              uiData_Text['personalDataPage']
                                                      ['marriageOtherLable']
                                                  .toString(),
                                          choiceFirst:
                                              uiData_Text['personalDataPage']
                                                      ['marriageSingleHin']
                                                  .toString(),
                                          choiceSecond:
                                              uiData_Text['personalDataPage']
                                                      ['marriageMarryHin']
                                                  .toString(),
                                          choiceThird:
                                              uiData_Text['personalDataPage']
                                                      ['marriageOtherHin']
                                                  .toString(),
                                          isThirdChoice: true,
                                          isEnable:
                                              dataState.isSentEmail == false,
                                          check: (data) {
                                            //TODO
                                            setState(() {
                                              validList[12] = true;
                                              marriage = data;
                                            });
                                            if (data !=
                                                uiData_Text['personalDataPage']
                                                        ['marriageOtherHin']
                                                    .toString()) {
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_maritalDetail_Action(
                                                          ''));
                                            }

                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    Change_maritalStatus_Action(
                                                        data));
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction());
                                          },
                                        ),
                                        dataState.maritalStatus ==
                                                uiData_Text['personalDataPage']
                                                        ['marriageOtherHin']
                                                    .toString()
                                            ? Padding(
                                                padding:
                                                    EdgeInsets.only(top: 6),
                                                child: DataTextField(
                                                    hint: uiData_Text['personalDataPage'][
                                                            'marriageOtherDetail']
                                                        .toString(),
                                                    keyboardType:
                                                        TextInputType.text,
                                                    initValue: dataState
                                                        .maritalDetail, //dataState.homeAddress,
                                                    onChange: (text) =>
                                                        StoreProvider.of<AppState>(context)
                                                            .dispatch(
                                                                Change_maritalDetail_Action(
                                                                    text)),
                                                    focusNode: otherFocusNode,
                                                    nextFocusNode: null,
                                                    isValid: true,
                                                    isEnable:
                                                        dataState.isSentEmail ==
                                                            false))
                                            : Container(),
                                        //Employment
                                        CheckBoxGroup(
                                          title: uiData_Text['personalDataPage']
                                                  ['occuLable']
                                              .toString(),
                                          value: dataState.job,
                                          isValid: validList[13],
                                          labelFirst:
                                              uiData_Text['personalDataPage']
                                                      ['occuEmployLable']
                                                  .toString(),
                                          labelSecond:
                                              uiData_Text['personalDataPage']
                                                      ['occuSelfLable']
                                                  .toString(),
                                          labelThird:
                                              uiData_Text['personalDataPage']
                                                      ['occuNoneLable']
                                                  .toString(),
                                          choiceFirst:
                                              uiData_Text['personalDataPage']
                                                      ['occuEmployHin']
                                                  .toString(),
                                          choiceSecond:
                                              uiData_Text['personalDataPage']
                                                      ['occuSelfHin']
                                                  .toString(),
                                          choiceThird:
                                              uiData_Text['personalDataPage']
                                                      ['occuNoneHin']
                                                  .toString(),
                                          isThirdChoice: true,
                                          isEnable:
                                              dataState.isSentEmail == false,
                                          check: (data) {
                                            setState(() {
                                              validList[13] = true;
                                              employ = data;
                                            });

                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    Change_job_Action(data));
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction());
                                          },
                                        ),
                                        //Director
                                        CheckBoxGroup(
                                          title: uiData_Text['personalDataPage']
                                                  ['directorLable']
                                              .toString(),
                                          value: dataState.isDirector,
                                          isValid: validList[14],
                                          labelFirst:
                                              uiData_Text['personalDataPage']
                                                      ['yesLable']
                                                  .toString(),
                                          labelSecond:
                                              uiData_Text['personalDataPage']
                                                      ['noLable']
                                                  .toString(),
                                          choiceFirst:
                                              uiData_Text['personalDataPage']
                                                      ['yesHin']
                                                  .toString(),
                                          choiceSecond:
                                              uiData_Text['personalDataPage']
                                                      ['noHin']
                                                  .toString(),
                                          isThirdChoice: false,
                                          isEnable:
                                              dataState.isSentEmail == false,
                                          check: (data) {
                                            setState(() {
                                              validList[14] = true;
                                              director = data == true
                                                  ? uiData_Text[
                                                          'personalDataPage']
                                                      ['yesHin']
                                                  : uiData_Text[
                                                          'personalDataPage']
                                                      ['noHin'];
                                            });
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    Change_isDirector_Action(
                                                        director));
                                            StoreProvider.of<AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction());
                                          },
                                        ),
                                      ])),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 38, right: 38, top: 71, bottom: 46),
                                child: NextBtnWrapper(
                                  title: StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .isSentEmail
                                      ? uiData_Text['general']['startReadBtn']
                                          .toString()
                                      : uiData_Text['general']['nextPageBtn']
                                          .toString(),
                                  clickEvent: gotoNextPage,
                                  controller: _nextBtnController,
                                ),
                                //validList: validList,
                                //dataList: dataList,
                                //checkValid: checkValid),
                              ),
                            ],
                          ),
                        )),
                  ]);
                },
              ))),
      /*
      floatingActionButton: showFab
          ? (Container(
              padding: EdgeInsets.only(bottom: 100.0),
              //margin: EdgeInsets.all(50),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: FloatingActionButton.extended(
                  backgroundColor: Colors.pink,
                  onPressed: () => {gotoNextPage()},
                  label: Text(
                    uiData_Text['general']['nextPageBtn'].toString(),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.fade,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ))
          : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
*/
    );
  }

  gotoNextPage() {
    ga.gaEvent(name:"psp_completed_personal_info_button");
    _validationController.dispose();
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToPersonalBankPageAction());
  }

  updateValid() {
    print('updateValid');
    setState(() {
      validList = _nextBtnController.validList;
      //Eng Name
      if (_nextBtnController.validList[0] == true) {
        dataValidation(0);
      }
      //Chi Name, not required
      if (_nextBtnController.validList[1] == true && dataL[1].isNotEmpty) {
        dataValidation(1);
      }
      //HKID
      dataValidation(2);
      //Birth Date
      if (_nextBtnController.validList[7]) {
        validList[7] = isBirthRangeCorrect(
            StoreProvider.of<AppState>(context).state.dataState.birthDay);
      }
      //Phone, not required
      if (_nextBtnController.validList[8] && dataL[8].isNotEmpty) {
        dataValidation(8);
      }
      if (_nextBtnController.validList[9]) {
        dataValidation(9);
      }
      //Email
      if (_nextBtnController.validList[10]) {
        dataValidation(10);
      }
      // Focus to
      List<int> textfieldIds = [0, 1, 2, 3, 4, 8, 9, 10];
      for (int i = 0; i < validList.length; i++) {
        if (validList[i] == false && textfieldIds.contains(i)) {
          // print("focust to: $i");
          FocusScope.of(context).requestFocus(focusNode[i]);
          break;
        }
      }
    });
  }

  void _selectDate(BuildContext context, DateTime dateTime) {
    DateTime today = DateTime.now();
    DateTime age18 = DateTime(today.year - 18, today.month, today.day);
    DateTime age100 = DateTime(today.year - 100, today.month, today.day);

    CustomDatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: age100,
        maxTime: age18, onChanged: (date) {
      print('change $date');
      setState(() {
        birthDate = date;
        validList[7] = isBirthRangeCorrect(date.toIso8601String());
      });
    }, onConfirm: (date) {
      print('confirm $date');
      setState(() {
        birthDate = date;
        validList[7] = isBirthRangeCorrect(date.toIso8601String());
      });
      StoreProvider.of<AppState>(context)
          .dispatch(Change_birthDay_Action(birthDate.toIso8601String()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    }, currentTime: dateTime, locale: LocaleType.zh);
  }
  /*Future<DateTime> _selectDate(BuildContext context, DateTime dateTime) async {
    DateTime today = DateTime.now();
    DateTime age18 = DateTime(today.year - 18, today.month, today.day);
    DateTime age100 = DateTime(today.year - 100, today.month, today.day);
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: dateTime, //birthDate
      firstDate: age100, //DateTime(1900, 8),
      lastDate: age18,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: config_primaryColour, //Head background
            accentColor: config_primaryColour, //selection color
            dialogBackgroundColor: Colors.white,
            buttonColor: config_primaryColour,
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != birthDate) {
      setState(() {
        // var str = "${picked.day}/${picked.month}/${picked.year}";
        birthDate = picked;
        if (birthDate.day != DateTime.now().day &&
            birthDate.month != DateTime.now().month &&
            birthDate.year != DateTime.now().year) {
          print('birthDate: ${birthDate}, isToday: true');
        } else {
          print('birthDate: ${birthDate}, isToday: false');
        }
        validList[7] = isBirthRangeCorrect(picked.toIso8601String());
      });

      StoreProvider.of<AppState>(context)
          .dispatch(Change_birthDay_Action(picked.toIso8601String()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    }
  }*/

  void dataValidation(int index) {
    switch (index) {
      case 0:
        validList[0] =
            isEng(StoreProvider.of<AppState>(context).state.dataState.nameEng);
        // print("isEng: ${isEng(StoreProvider.of<AppState>(context).state.dataState.nameEng)}");
        break;
      case 1:
        validList[1] = isChi(
            StoreProvider.of<AppState>(context).state.dataState.nameChinese);
        // print("isChi: ${isChi(StoreProvider.of<AppState>(context).state.dataState.nameChinese)}");
        break;
      case 2:
      case 3:
      case 4:
        String char =
            StoreProvider.of<AppState>(context).state.dataState.idChar;
        String firstNum =
            StoreProvider.of<AppState>(context).state.dataState.idFirstNum;
        String lastNum =
            StoreProvider.of<AppState>(context).state.dataState.idLastNum;
        if (char.isEmpty || firstNum.isEmpty || lastNum.isEmpty) {
          validList[2] = false;
          validList[3] = false;
          validList[4] = false;
        } else {
          // print("isHKID: ${isHkid(char, firstNum, lastNum)}");
          validList[2] = isHkid(char, firstNum, lastNum);
          validList[3] = isHkid(char, firstNum, lastNum);
          validList[4] = isHkid(char, firstNum, lastNum);
        }
        break;
      case 5:
      case 6:
      case 7:
        validList[7] = isBirthRangeCorrect(
            StoreProvider.of<AppState>(context).state.dataState.birthDay);
        break;
      case 8:
        // print("isPhone: ${isPhone(StoreProvider.of<AppState>(context).state.dataState.contactNum)}");
        validList[8] = isPhone(
            StoreProvider.of<AppState>(context).state.dataState.contactNum);
        // print('validList[6]: ${validList[6]}');
        break;
      case 9:
        // print("isPhone 2: ${isPhone(StoreProvider.of<AppState>(context).state.dataState.mobileContactNum)}");
        validList[9] = isPhone(StoreProvider.of<AppState>(context)
            .state
            .dataState
            .mobileContactNum);
        break;
      case 10:
        // print("isEmail: ${isEmail(StoreProvider.of<AppState>(context).state.dataState.emailAddress)}");
        validList[10] = isEmail(
            StoreProvider.of<AppState>(context).state.dataState.emailAddress);
        break;
      default:
        break;
    }
  }
}

class HKIDFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      print(true);
      return newValue;
    }

    int value = int.parse(newValue.text);

    final formatter =
        NumberFormat("(#)"); //NumberFormat.simpleCurrency(locale: "pt_Br");

    String newText = formatter.format(value);

    return newValue.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: newText.length));
  }
}
