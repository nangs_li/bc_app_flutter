import 'dart:convert';

import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/app/hospitallist.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/AutoCompleteGroup.dart';
import 'package:icanfight/psp/view/CustomTypeHead.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:icanfight/psp/view/MultipleCheckbox.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class PubOrPriHospitalPage extends StatefulWidget {
  @override
  _PubOrPriHospitalPageState createState() => _PubOrPriHospitalPageState();
}

class _PubOrPriHospitalPageState extends State<PubOrPriHospitalPage> {
  int current_percentage;
  String hosp;
  String hospName;
  bool isPubHos, isPriHos;
  FocusNode focusNode;
  NextBtnController _nextBtnController;
  MCController _mcController;
  SuggestionsBoxController suggestionsBoxController;
  // List<dynamic> hospList;
  bool forceRefresh;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRPubOrPriHospitalPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 50; //0
    hosp = '';
    hospName = '';
    isPubHos = false;
    isPriHos = false;
    forceRefresh = false;
    focusNode = FocusNode();
    // hospList = List();
    suggestionsBoxController = SuggestionsBoxController();

    Future.delayed(Duration.zero, () {
      print(
          're: ${StoreProvider.of<AppState>(context).state.dataState.isPublicHospial}');

      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.PubOrPriHospital.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

      if (StoreProvider.of<AppState>(context).state.dataState.isPublicHospial ==
          uiData_Text['pubOrPriHospitalPage']['radioGroup'][0]) {
        setState(() {
          isPubHos = true;
          isPriHos = false;
          hosp = StoreProvider.of<AppState>(context)
              .state
              .dataState
              .isPublicHospial;
        });
      } else {
        setState(() {
          isPubHos = false;
          isPriHos = true;
          hosp = StoreProvider.of<AppState>(context)
              .state
              .dataState
              .isPublicHospial;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return WillPopScope(
        onWillPop: _onWillPop,
        child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
              appBar: AppBar(
                /*      title: Text('申請程序'), */
                brightness: Brightness.dark,
                titleSpacing: 0.0,
                elevation: 0.0,
                leading: IconButton(
                  iconSize: 30.0,
                  icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                  onPressed: () => {
                    StoreProvider.of<AppState>(context).dispatch(
                        PopFromRPubOrPriHospitalPageAction(this.context))
                  },
                ),
                actions: <Widget>[SaveBtnWrapper()],
              ),
              body: StoreConnector<AppState, Data_State>(
                converter: (Store<AppState> store) => store.state.dataState,
                builder: (BuildContext context, Data_State dataState) {
                  _nextBtnController = NextBtnController(dataList: [
                    dataState.isPublicHospial,
                    dataState.treatmentLocation
                  ]);
                  _mcController = MCController(
                      value: dataState.isPublicHospial,
                      options: uiData_Text['pubOrPriHospitalPage']
                          ['radioGroup']);
                  return Stack(children: <Widget>[
                    Container(
                        decoration:
                            BoxDecoration(color: config_normalBGColour)),
                    PrograssBar(percentage: current_percentage, step: 0),
                    Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SingleChildScrollView(
                          //padding: EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(left: 23, right: 23),
                                  child: Column(children: <Widget>[
                                    //Title
                                    Container(
                                      padding: EdgeInsets.only(top: 33),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        uiData_Text['pubOrPriHospitalPage']
                                                ['title']
                                            .toString(),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style: formPageTitleStyle,
                                      ),
                                    ),
                                    //Multiple TODO: initValue & onChange
                                    MultipleCheckboxGroup(
                                      isEnable: dataState.isSentEmail == false,
                                      title: uiData_Text['pubOrPriHospitalPage']
                                              ['subtitle']
                                          .toString(),
                                      controller: _mcController,
                                      check: (data) {
                                        setState(() {
                                          print('data: ${data}');
                                          hosp = data;
                                          if (hosp ==
                                              uiData_Text[
                                                      'pubOrPriHospitalPage']
                                                  ['radioGroup'][0]) {
                                            isPubHos = true;
                                            // hospList = hospitalList['public'];
                                            print('isPubHos: true');
                                          } else {
                                            isPubHos = false;
                                            // hospList = hospitalList['private'];
                                            print('isPubHos: false');
                                          }
                                          if (suggestionsBoxController
                                                  .isEmpty() ==
                                              false) {
                                            print(
                                                'suggestionsBoxController isNotEmpty');
                                            if (suggestionsBoxController
                                                .isOpened()) {
                                              print(
                                                  'suggestionsBoxController isOpened');
                                              suggestionsBoxController.close();
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_treatmentLocation_Action(
                                                          ''));

                                              /*suggestionsBoxController.close();
                                          suggestionsBoxController.open();*/
                                              //suggestionsBoxController.suggestionsBox.onChangeMetrics();
                                              // suggestionsBoxController.suggestionsBox.widgetMounted
                                            } else {
                                              print(
                                                  'suggestionsBoxController isNotOpened');
                                            }
                                          } else {
                                            print(
                                                'suggestionsBoxController isEmpty');
                                          }
                                          forceRefresh = true;
                                          Future.delayed(
                                              Duration(milliseconds: 100), () {
                                            setState(() {
                                              forceRefresh = false;
                                              // focusNode.requestFocus();
                                            });
                                          });

                                          StoreProvider.of<AppState>(context)
                                              .dispatch(
                                                  Change_isPublicHospial_Action(
                                                      data));
                                        });
                                      },
                                    ),
                                    //DataTxt TODO: initValue & onChange
                                    hosp != ""
                                        ? AutoCompleteGroup(
                                            isEnable:
                                                dataState.isSentEmail == false,
                                            title: isPubHos
                                                ? uiData_Text[
                                                            'pubOrPriHospitalPage']
                                                        ['placePubLabel']
                                                    .toString()
                                                : uiData_Text[
                                                            'pubOrPriHospitalPage']
                                                        ['placePriLabel']
                                                    .toString(),
                                            hint: uiData_Text[
                                                        'pubOrPriHospitalPage']
                                                    ['placeHin']
                                                .toString(),
                                            keyboardType:
                                                TextInputType.multiline,
                                            initValue:
                                                dataState.treatmentLocation,
                                            suggestionsBoxController:
                                                suggestionsBoxController,
                                            onSelect: (text) {
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_treatmentLocation_Action(
                                                          text));
                                            },
                                            onChanged_edit: (text) {
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_treatmentLocation_Action(
                                                          text));
                                            },
                                            forceRefresh: forceRefresh,
                                            suggestions: (query) {
                                              print('suggestions: $isPubHos');
                                              if (isPubHos) {
                                                return hospitalList['public']
                                                    .where((s) =>
                                                        s['nameEng']
                                                            .toLowerCase()
                                                            .contains(query
                                                                .toLowerCase()) ||
                                                        s['nameChi']
                                                            .toLowerCase()
                                                            .contains(query));
                                              } else {
                                                return hospitalList['private']
                                                    .where((s) =>
                                                        s['nameEng']
                                                            .toLowerCase()
                                                            .contains(query
                                                                .toLowerCase()) ||
                                                        s['nameChi']
                                                            .toLowerCase()
                                                            .contains(query));
                                              }

                                              /*if (isPubHos) {
                                            return await Future.delayed(
                                                Duration.zero, () {
                                              return hospitalList['public']
                                                  .where((s) =>
                                                      s['nameEng']
                                                          .toLowerCase()
                                                          .contains(query
                                                              .toLowerCase()) ||
                                                      s['nameChi']
                                                          .toLowerCase()
                                                          .contains(query));
                                            });
                                          } else {
                                            return await Future.delayed(
                                                Duration.zero, () {
                                              return hospitalList['private']
                                                  .where((s) =>
                                                      s['nameEng']
                                                          .toLowerCase()
                                                          .contains(query
                                                              .toLowerCase()) ||
                                                      s['nameChi']
                                                          .toLowerCase()
                                                          .contains(query));
                                            });
                                          }*/
                                            },
                                            focusNode: focusNode,
                                            nextFocusNode: null,
                                            isValid: true,
                                            height: 80.0, //70.0
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            isHos: true,
                                          )
                                        : Container(),
                                    /*Container(
                                  padding: EdgeInsets.only(top: 16),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                      uiData_Text['pubOrPriHospitalPage']
                                          ['placePubLabel'],
                                      textAlign: TextAlign.left,
                                      style:
                                          customeThemeData.textTheme.bodyText1),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(top: 18),
                                    child: DataTextField(
                                        hint:
                                            uiData_Text['pubOrPriHospitalPage']
                                                    ['placeHin']
                                                .toString(),
                                        initValue: hospName,
                                        keyboardType: TextInputType.text,
                                        focusNode: focusNode,
                                        nextFocusNode: null,
                                        onChange: (data) {
                                          setState(() {
                                            hospName = data;
                                          });
                                        },
                                        isValid: true,
                                        height: 70))
                                        */
                                  ])),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 38, right: 38, top: 46, bottom: 46),
                                child: NextBtnWrapper(
                                    title: StoreProvider.of<AppState>(context)
                                            .state
                                            .dataState
                                            .isSentEmail
                                        ? uiData_Text['general']['startReadBtn']
                                            .toString()
                                        : uiData_Text['general']['nextPageBtn']
                                            .toString(),
                                    clickEvent: gotoNextPage,
                                    controller: _nextBtnController),
                              ),
                            ],
                          ),
                        ))
                  ]);
                },
              ),
            )));
  }

  gotoNextPage() {
    //check is public and private

    if (StoreProvider.of<AppState>(context).state.dataState.isPublicHospial ==
        "公立醫院/診所") {
      StoreProvider.of<AppState>(context)
          .dispatch(NavigateToDrugStatus1PageAction());
    } else {
      StoreProvider.of<AppState>(context)
          .dispatch(NavigateToWhereDrugInfoPageAction());
    }
  }
}
