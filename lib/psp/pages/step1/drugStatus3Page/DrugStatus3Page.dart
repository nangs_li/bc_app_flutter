import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/pages/step2/step2LandingPage/Step2LandingPage.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/MultipleCheckbox.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class DrugStatus3Page extends StatefulWidget {
  @override
  _DrugStatus3PageState createState() => _DrugStatus3PageState();
}

class _DrugStatus3PageState extends State<DrugStatus3Page> {
  int current_percentage;
  String drug;
  NextBtnController _nextBtnController;
  MCController _mcController;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRDrugStatus3PageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 80;
    drug = '';

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.DrugStatus3Page.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () => {
              StoreProvider.of<AppState>(context)
                  .dispatch(PopFromRDrugStatus3PageAction(this.context))
            },
          ),
          actions: <Widget>[SaveBtnWrapper()],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            _nextBtnController =
                NextBtnController(dataList: [dataState.treatmentStage]);
            _mcController = MCController(
                options: uiData_Text['drugStatus3Page']['radioGroup'],
                value: dataState.treatmentStage);
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 0),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 23, right: 23),
                            child: Column(children: <Widget>[
                              //Title
                              Container(
                                padding: EdgeInsets.only(top: 33),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['drugStatus3Page']['title']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              //Subtitle
                              Container(
                                padding: EdgeInsets.only(top: 6),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['drugStatus3Page']['subtitle1']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: aboutPageInfoStyle,
                                ),
                              ),
                              //Multiple TODO: initValue & onChange
                              MultipleCheckboxGroup(
                                isEnable: dataState.isSentEmail == false,
                                title: uiData_Text['drugStatus3Page']
                                        ['subtitle2']
                                    .toString(),
                                controller: _mcController,
                                check: (data) {
                                  setState(() {
                                    drug = data;
                                  });

                                  StoreProvider.of<AppState>(context).dispatch(
                                      Change_treatmentStage_Action(data));
                                },
                              ),
                            ])),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 46, bottom: 46),
                          child: NextBtnWrapper(
                            title: StoreProvider.of<AppState>(context)
                                    .state
                                    .dataState
                                    .isSentEmail
                                ? uiData_Text['general']['startReadBtn']
                                    .toString()
                                : uiData_Text['general']['nextPageBtn']
                                    .toString(),
                            clickEvent: gotoNextPage,
                            controller: _nextBtnController,
                          ),
                        ),
                      ],
                    ),
                  ))
            ]);
          },
        ),
      ),
    );
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToWhereDrugInfoPageAction());
  }
}
