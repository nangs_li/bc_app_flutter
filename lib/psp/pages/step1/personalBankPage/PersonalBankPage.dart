import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/banklist.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/ValidationCtr.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/AutoCompleteGroup.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class PersonalBankPage extends StatefulWidget {
  @override
  _PersonalBankPageState createState() => _PersonalBankPageState();
}

class _PersonalBankPageState extends State<PersonalBankPage> {
  List<FocusNode> focusNode = List();
  List<bool> validList = List();
  //List<String> txtFieldList = List();
  int current_percentage;
  NextBtnController _nextBtnController;

  ValidationController _validationController;
  List<String> dataL = List();
  List<ValidType> validationTypeList = List();
  bool _isInitValidationCtr = false;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRPersonalBankPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 25;
    print("bankList: ${bankList}");
    focusNode = [FocusNode(), FocusNode(), FocusNode()];
    validList = [true, true, true];
    validationTypeList = [ValidType.None, ValidType.None, ValidType.Eng];


    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.PersonalBankPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    SingletonShareData data = SingletonShareData();
    data.nameEng = StoreProvider.of<AppState>(context).state.dataState.nameEng;
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;
    return WillPopScope(
        onWillPop: _onWillPop,
        child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
                appBar: AppBar(
                  /*      title: Text('申請程序'), */
                  brightness: Brightness.dark,
                  titleSpacing: 0.0,
                  elevation: 0.0,
                  leading: IconButton(
                      iconSize: 30.0,
                      icon: Icon(Icons.arrow_back,
                          color: Colors.white, size: 30.0),
                      onPressed: () => {
                            StoreProvider.of<AppState>(context).dispatch(
                                PopFromRPersonalBankPageAction(context))
                          }),
                  actions: <Widget>[SaveBtnWrapper()],
                ),
                body: StoreConnector<AppState, Data_State>(
                  converter: (Store<AppState> store) => store.state.dataState,
                  builder: (BuildContext context, Data_State dataState) {
                    dataL = [
                      dataState.bankNumber,
                      dataState.bankName,
                      dataState.bankAccountName
                    ];
                    _nextBtnController = NextBtnController(
                        dataList: dataL,
                        updateValid: updateValid,
                        validTypeList: validationTypeList);
                    _validationController = ValidationController(
                        focusNodes: focusNode,
                        validationTypes: validationTypeList,
                        value: dataL,
                        isValid: validList,
                        getUpdateValue: (int index) {
                          return dataL[index];
                        },
                        onFinish: (int index, bool isValid) {
                          StoreProvider.of<AppState>(context)
                              .dispatch(SaveAppDataToLocalAction());
                          setState(() {
                            validList[index] = isValid;
                          });
                        });
                    if (_isInitValidationCtr == false) {
                      _validationController.init();
                      _isInitValidationCtr = true;
                    }
                    return Stack(children: <Widget>[
                      Container(
                          decoration:
                              BoxDecoration(color: config_normalBGColour)),
                      PrograssBar(percentage: current_percentage, step: 0),
                      Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: SingleChildScrollView(
                            //padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                    padding:
                                        EdgeInsets.only(left: 23, right: 23),
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          //Title
                                          Container(
                                            padding: EdgeInsets.only(top: 33),
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              uiData_Text['personalBankPage']
                                                      ['title']
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              overflow: TextOverflow.ellipsis,
                                              style: formPageTitleStyle,
                                            ),
                                          ),
                                          //Bank no
                                          DataTextFieldGroup(
                                           type: TextFieldType.BankNumber,
                                            title:
                                                uiData_Text['personalBankPage']
                                                        ['bankNumLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalBankPage']
                                                        ['bankNumHin']
                                                    .toString(),
                                            keyboardType: TextInputType.number,
                                            initValue: dataState.bankNumber,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_bankNumber_Action(
                                                        text)),
                                            /*onFinish: () => StoreProvider.of<
                                                    AppState>(context)
                                                .dispatch(
                                                    SaveAppDataToLocalAction()),*/
                                            focusNode: focusNode[0],
                                            nextFocusNode: focusNode[1],
                                            isValid: validList[0],
                                            isEnable:
                                                dataState.isSentEmail == false,
                                          ),

                                          //Bank Name
                                          AutoCompleteGroup(
                                            title:
                                                uiData_Text['personalBankPage']
                                                        ['bankNameLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalBankPage']
                                                        ['bankNameHin']
                                                    .toString(),
                                            keyboardType: TextInputType.text,
                                            initValue: dataState.bankName,
                                            onChanged_edit: (text) {
                                              print('onChangeEdit: $text');
                                              validList[1] = true;
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_bankName_Action(
                                                          text));
                                            },
                                            onSelect: (text) {
                                              validList[1] = true;
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      Change_bankName_Action(
                                                          text));
                                              StoreProvider.of<AppState>(
                                                      context)
                                                  .dispatch(
                                                      SaveAppDataToLocalAction());
                                            },
                                            suggestions: (query) {
                                              return bankList.where((s) =>
                                                  s['nameEng']
                                                      .toLowerCase()
                                                      .contains(query
                                                          .toLowerCase()) ||
                                                  s['nameChi']
                                                      .toLowerCase()
                                                      .contains(query));
                                            },
                                            focusNode: focusNode[1],
                                            nextFocusNode: focusNode[2],
                                            isValid: validList[1],
                                            isEnable:
                                                dataState.isSentEmail == false,
                                          ),
                                          //Bank Acc Name
                                          DataTextFieldGroup(
                                            title:
                                                uiData_Text['personalBankPage']
                                                        ['bankAccNameLable']
                                                    .toString(),
                                            hint:
                                                uiData_Text['personalBankPage']
                                                        ['bankAccNameHin']
                                                    .toString(),
                                            keyboardType: TextInputType.text,
                                            initValue:
                                                dataState.bankAccountName,
                                            onChange: (text) => StoreProvider
                                                    .of<AppState>(context)
                                                .dispatch(
                                                    Change_bankAccountName_Action(
                                                        text)),
                                            // onFinish: StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction()),
                                            focusNode: focusNode[2],
                                            nextFocusNode: null,
                                            isValid: validList[2],
                                            // validationType: ValidType.Eng,
                                            isEnable:
                                                dataState.isSentEmail == false,
                                          ),
                                        ])),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 38, right: 38, top: 46, bottom: 46),
                                  child: NextBtnWrapper(
                                    title: StoreProvider.of<AppState>(context)
                                            .state
                                            .dataState
                                            .isSentEmail
                                        ? uiData_Text['general']['startReadBtn']
                                            .toString()
                                        : uiData_Text['general']['nextPageBtn']
                                            .toString(),
                                    clickEvent: gotoNextPage,
                                    controller: _nextBtnController,
                                    //validList: validTxtField,
                                    //dataList: txtFieldList,
                                    //checkValid: checkValid
                                  ),
                                ),
                              ],
                            ),
                          ))
                    ]);
                  },
                ))));
  }

  /*checkValid() {
    setState(() {
      if (txtFieldList == null) {
      } else {
        for (int i = 0; i < txtFieldList.length; i++) {
          if (txtFieldList[i] == '') {
            validTxtField[i] = false;
          } else {
            validTxtField[i] = true;
          }
        }
      }
    });
  }*/

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToFamilyIncomePageAction());
  }

  updateValid() {
    setState(() {
      validList = _nextBtnController.validList;
      if (validList[2]) {
        validList[2] = isEng(StoreProvider.of<AppState>(context)
            .state
            .dataState
            .bankAccountName);
      }
    });
  }
}
