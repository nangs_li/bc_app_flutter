import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/MultipleCheckbox.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class WhereDrugInfoPage extends StatefulWidget {
  @override
  _WhereDrugInfoPageState createState() => _WhereDrugInfoPageState();
}

class _WhereDrugInfoPageState extends State<WhereDrugInfoPage> {
  int current_percentage;
  String drug;
  NextBtnController _nextBtnController;
  MCController _mcController;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    bool result = false;
    if (StoreProvider.of<AppState>(context).state.dataState.isPublicHospial ==
        "公立醫院/診所") {
      return StoreProvider.of<AppState>(context)
          .dispatch(PopFromRWhereDrugInfoPageAction(this.context));
    } else {
      return StoreProvider.of<AppState>(context)
          .dispatch(PopFromRDrugStatus1PageAction(this.context));
    }
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 90;
    drug = '';

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.WhereDrugInfoPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return WillPopScope(
        onWillPop: _onWillPop,
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
            appBar: AppBar(
              /*      title: Text('申請程序'), */
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0.0,
              leading: IconButton(
                iconSize: 30.0,
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                onPressed: () => {
                  if (StoreProvider.of<AppState>(context)
                          .state
                          .dataState
                          .isPublicHospial ==
                      "公立醫院/診所")
                    {
                      StoreProvider.of<AppState>(context).dispatch(
                          PopFromRWhereDrugInfoPageAction(this.context))
                    }
                  else
                    {
                      StoreProvider.of<AppState>(context)
                          .dispatch(PopFromRDrugStatus1PageAction(this.context))
                    }
                },
              ),
              actions: <Widget>[SaveBtnWrapper()],
            ),
            body: StoreConnector<AppState, Data_State>(
              converter: (Store<AppState> store) => store.state.dataState,
              builder: (BuildContext context, Data_State dataState) {
                _nextBtnController =
                    NextBtnController(dataList: [dataState.whereGetInfo]);
                _mcController = MCController(
                    value: dataState.whereGetInfo,
                    options: uiData_Text['whereDrugInfoPage']['radioGroup'],
                    isMultiple: true);
                return Stack(children: <Widget>[
                  Container(
                      decoration: BoxDecoration(color: config_normalBGColour)),
                  PrograssBar(percentage: current_percentage, step: 0),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: SingleChildScrollView(
                        //padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(left: 23, right: 23),
                                child: Column(children: <Widget>[
                                  //Title
                                  Container(
                                    padding: EdgeInsets.only(top: 33),
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      uiData_Text['whereDrugInfoPage']['title']
                                          .toString(),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: formPageTitleStyle,
                                    ),
                                  ),

                                  //Multiple TODO: initValue & onChange
                                  MultipleCheckboxGroup(
                                    isEnable: dataState.isSentEmail == false,
                                    title: uiData_Text['whereDrugInfoPage']
                                            ['subtitle']
                                        .toString(),
                                    controller: _mcController,
                                    check: (data) {
                                      /*  if (data != drug) {
                                        StoreProvider.of<AppState>(context)
                                            .dispatch(
                                                Change_whereGetInfoDetail_Action(
                                                    ""));
                                      } */

                                      setState(() {
                                        drug = data;
                                      });

                                      StoreProvider.of<AppState>(context)
                                          .dispatch(
                                              Change_whereGetInfo_Action(data));
                                    },
                                    uncheck: (data) {
                                      /*  if (data != drug) {
                                        StoreProvider.of<AppState>(context)
                                            .dispatch(
                                                Change_whereGetInfoDetail_Action(
                                                    ""));
                                      } */

                                      setState(() {
                                        drug = data;
                                      });

                                      StoreProvider.of<AppState>(context)
                                          .dispatch(
                                              Change_whereGetInfo_Action(data));
                                    },
                                    txtFieldList: [4, 5],
                                    txtFieldJsonList: [
                                      {
                                        'hint': uiData_Text['whereDrugInfoPage']
                                            ['socailPlaceHin'],
                                        'initValue':
                                            StoreProvider.of<AppState>(context)
                                                .state
                                                .dataState
                                                .whereGetInfoDetail,
                                        'onChange': (data) {
                                          print('onChange: $data');

                                          StoreProvider.of<AppState>(context)
                                              .dispatch(
                                                  Change_whereGetInfoDetail_Action(
                                                      data));
                                        },
                                        'keyboardType': TextInputType.multiline,
                                        'height': 150.0,
                                        'minLines': 9,
                                        'maxLines': 10
                                      },
                                      {
                                        'hint': uiData_Text['whereDrugInfoPage']
                                            ['otherPlaceHin'],
                                        'initValue':
                                            StoreProvider.of<AppState>(context)
                                                .state
                                                .dataState
                                                .whereGetInfoOthersDetail,
                                        'onChange': (data) {
                                          print('onChange: $data');

                                          StoreProvider.of<AppState>(context)
                                              .dispatch(
                                                  Change_whereGetInfoOthersDetail_Action(
                                                      data));
                                        },
                                        'keyboardType': TextInputType.multiline,
                                        'height': 150.0,
                                        'minLines': 9,
                                        'maxLines': 10
                                      }
                                    ],
                                  ),
                                ])),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 38, right: 38, top: 46, bottom: 46),
                              child: NextBtnWrapper(
                                  title: StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .isSentEmail
                                      ? uiData_Text['general']['startReadBtn']
                                          .toString()
                                      : uiData_Text['general']['nextPageBtn']
                                            .toString(),
                                  clickEvent: gotoNextPage,
                                  controller: _nextBtnController),
                            ),
                          ],
                        ),
                      ))
                ]);
              },
            ),
          ),
        ));
  }

  gotoNextPage() {
    ga.gaEvent(name: "psp_step1_completed_application_button");
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToStep2LandingPageAction());
  }
}
