import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';

import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/MemberCard.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PopupInfo.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class FamilyIncomePage extends StatefulWidget {
  @override
  _FamilyIncomePageState createState() => _FamilyIncomePageState();
}

class _FamilyIncomePageState extends State<FamilyIncomePage> {
  int current_percentage;
  List<DropdownMenuItem> memberNo;
  //Controllers
  FamilyMemberNextBtnController _nextBtnController;
  List<MemberCardController> memberControllers;
  List<bool> memberValidList;

  int selectedMemberNo;
  //List<FamilyMemberIncome> fmiList;
  bool isPopup;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRFamilyIncomePageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 30;
    selectedMemberNo = 0;

    memberNo = List.generate(
        7,
        (index) => DropdownMenuItem(
            value: index, child: Text(index.toString(), style: dropdownStyle)));
    isPopup = false;
    memberControllers = List();
    memberValidList = List();

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.FamilyIncomePage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

      setState(() {
        selectedMemberNo = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome
            .length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;
    int dropdownValue;

    return WillPopScope(
        onWillPop: _onWillPop,
        child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
              appBar: AppBar(
                /*      title: Text('申請程序'), */
                brightness: Brightness.dark,
                titleSpacing: 0.0,
                elevation: 0.0,
                leading: IconButton(
                  iconSize: 30.0,
                  icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                  onPressed: () => {
                    StoreProvider.of<AppState>(context)
                        .dispatch(PopFromRFamilyIncomePageAction(context))
                  },
                ),
                actions: <Widget>[SaveBtnWrapper()],
              ),
              body: StoreConnector<AppState, Data_State>(
                converter: (Store<AppState> store) => store.state.dataState,
                builder: (BuildContext context, Data_State dataState) {
                  if (_nextBtnController == null) {
                    memberValidList = List.generate(
                        dataState.familyMemberIncome.length, (index) => true);
                  }
                  memberControllers.clear();
                  for (int i = 0;
                      i < dataState.familyMemberIncome.length;
                      i++) {
                    memberControllers.add(MemberCardController(
                        data: dataState.familyMemberIncome, id: i));
                  }
                  _nextBtnController = FamilyMemberNextBtnController(
                      memberList: dataState.familyMemberIncome,
                      updateMemberValidList: updateMemberValidList);
                  return Stack(children: <Widget>[
                    Container(
                        decoration:
                            BoxDecoration(color: config_normalBGColour)),
                    PrograssBar(percentage: current_percentage, step: 0),
                    Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SingleChildScrollView(
                          //padding: EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(left: 23, right: 23),
                                  child: Column(children: <Widget>[
                                    //Title
                                    Container(
                                      padding: EdgeInsets.only(top: 33),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        uiData_Text['familyIncomePage']['title']
                                            .toString(),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style: formPageTitleStyle,
                                      ),
                                    ),
                                    //Subtitle == TODO update Member number
                                    ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minHeight: 48.0,
                                          minWidth: 5.0,
                                          maxHeight: 130.0,
                                          maxWidth:
                                              MediaQuery.of(context).size.width,
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                             Container(
                                               child: Row(
                                                 mainAxisAlignment: MainAxisAlignment.center,
                                                 crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: <Widget>[
                                                     Container(
                                                       width: SizeConfig().blockSizeHorizontal * 50,
                                                       child: Text(
                                                                uiData_Text['familyIncomePage']
                                                                        ['subtitle']
                                                                    .toString(),
                                                                style:
                                                                    customeThemeData
                                                                        .textTheme
                                                                        .bodyText1,maxLines: 3,overflow:
                                                       TextOverflow.ellipsis,
                                                              ),
                                                     ),
                                                  IconButton(
                                                    icon: Icon(
                                                              Icons.info_outline,
                                                              color:
                                                                  config_primaryColour),
                                                          onPressed: () {
                                                            setState(() {
                                                              isPopup = true;
                                                              print(
                                                                  'isPopup: $isPopup');
                                                            });
                                                            // StoreProvider.of<AppState>(context).dispatch();
                                                          },
                                                        ),
                                                  ]),
                                             ),
                                             Container(
                                                    width: SizeConfig().blockSizeHorizontal * 20,
                                                     height: 60,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        border: Border.all(
                                                            color:
                                                                config_primaryColour,
                                                            width: 1.0)),
                                                    child: Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: DropdownButton(
                                                          items: dataState
                                                                  .isSentEmail
                                                              ? null
                                                              : memberNo,
                                                          elevation: 0,
                                                          // isExpanded: false,
                                                          underline:
                                                              Container(),
                                                          value:
                                                              selectedMemberNo,
                                                          onChanged: (data) =>
                                                              setFamilyMember(
                                                                  data),
                                                          disabledHint: Text(
                                                              selectedMemberNo
                                                                  .toString(),
                                                              style:
                                                                  customeThemeData
                                                                      .textTheme
                                                                      .button),
                                                        )))
                                          ],
                                        )),
                                    //Card == TODO update info events
                                    ListView.builder(
                                        itemCount: selectedMemberNo,
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemBuilder: (context, index) {
                                          // print("fmiList[index]: ${fmiList[index]}");
                                          if (StoreProvider.of<AppState>(
                                                      context)
                                                  .state
                                                  .dataState
                                                  .familyMemberIncome
                                                  .length ==
                                              0) {
                                            return Container();
                                          } else {
                                            return MemberCard(
                                                data: StoreProvider.of<
                                                            AppState>(context)
                                                        .state
                                                        .dataState
                                                        .familyMemberIncome[
                                                    index], //dataState.familyMemberIncome
                                                index: index,
                                                isEnable:
                                                    dataState.isSentEmail ==
                                                        false,
                                                controller:
                                                    memberControllers[index],
                                                updateName: (name) {
                                                  print(
                                                      'updateName: ${index}:${name}');
                                                  updateFamilyMemberDate(index,
                                                      memNam: name);
                                                },
                                                updateAge: (age) {
                                                  updateFamilyMemberDate(index,
                                                      memAge: age);
                                                },
                                                updateGender: (gender) {
                                                  updateFamilyMemberDate(index,
                                                      memGender:
                                                          gender.toString());
                                                }, //bool true is female
                                                updateRelations: (relations) {
                                                  updateFamilyMemberDate(index,
                                                      memRelation: relations);
                                                },
                                                updateJob: (job) {
                                                  updateFamilyMemberDate(index,
                                                      memCareer: job);
                                                },
                                                updateIncome: (income) {
                                                  updateFamilyMemberDate(index,
                                                      memIncome:
                                                          income.toString());
                                                },
                                                isValid:
                                                    memberValidList[index]);
                                          }
                                        }),
                                    //Total Income
                                    Padding(
                                        padding: EdgeInsets.only(top: 48),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            //total income label
                                            Text(
                                                uiData_Text['familyIncomePage']
                                                        ['famTotalIncomeLabel']
                                                    .toString(),
                                                style: customeThemeData
                                                    .textTheme.bodyText1
                                                    .copyWith(fontSize: 18.0)),
                                            //total income val
                                            Text(getTotalIncome(),
                                                style: customeThemeData
                                                    .textTheme.bodyText1
                                                    .copyWith(fontSize: 18.0))
                                          ],
                                        ))
                                  ])),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 38, right: 38, top: 46, bottom: 46),
                                child: NextBtnWrapper(
                                  title: StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .isSentEmail
                                      ? uiData_Text['general']['startReadBtn']
                                          .toString()
                                      : uiData_Text['general']['nextPageBtn']
                                            .toString(),
                                  clickEvent: gotoNextPage,
                                  controller: _nextBtnController,
                                ),
                              ),
                            ],
                          ),
                        )),
                    PopupInfo(
                        isPop: isPopup,
                        onClose: () {
                          setState(() {
                            isPopup = false;
                          });
                        })
                  ]);
                },
              ),
            )));
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToFundedInfoPageAction());
  }

  setFamilyMember(int num) {
    print("setFamilyMember " + num.toString());
    selectedMemberNo = num;

    if (StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome
            .length !=
        num) {
      //different
      if (StoreProvider.of<AppState>(context)
              .state
              .dataState
              .familyMemberIncome
              .length >
          num) {
        //need delete familymember
        int deleteHowMany = StoreProvider.of<AppState>(context)
                .state
                .dataState
                .familyMemberIncome
                .length -
            num;

        for (int i = 0; i < deleteHowMany; i++) {
          StoreProvider.of<AppState>(context).dispatch(
              Remove_FamilyMemberIncome(StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .familyMemberIncome
                  .last));
        }
        //Edit By Anthony for UI
        //memberControllers = memberControllers.sublist(0, deleteHowMany);
      } else {
        // need add
        int addHowMany = num -
            StoreProvider.of<AppState>(context)
                .state
                .dataState
                .familyMemberIncome
                .length;

        for (int i = 0; i < addHowMany; i++) {
          StoreProvider.of<AppState>(context).dispatch(Add_FamilyMemberIncome(
              FamilyMemberIncome(
                  id: StoreProvider.of<AppState>(context)
                      .state
                      .dataState
                      .familyMemberIncome
                      .length
                      .toString(),
                  memNam: "",
                  memAge: "",
                  memGender: "",
                  memRelation: "", //""
                  memCareer: "",
                  memIncome: "",
                  incomeCopyType: "",
                  incomeCopyLocation: List<Location>())));
        }
      }

      print("current family member: " +
          StoreProvider.of<AppState>(context)
              .state
              .dataState
              .familyMemberIncome
              .length
              .toString());
    }
    setState(() {
      _nextBtnController = FamilyMemberNextBtnController(
          memberList: StoreProvider.of<AppState>(context)
              .state
              .dataState
              .familyMemberIncome,
          updateMemberValidList: updateMemberValidList);
      memberValidList = _nextBtnController.validList;
    });
  }

  getTotalIncome() {
    double total = 0.0;
    StoreProvider.of<AppState>(context)
        .state
        .dataState
        .familyMemberIncome
        .forEach((fmi) {
      print('fmi.memIncome: ${fmi.memIncome}');
      if (fmi.memIncome != '' && fmi.memIncome != null) {
        //todo error HK$
        var value =
            fmi.memIncome.substring(3).replaceAll(',', ''); //Remove HK$ and ","
        total += double.parse(value);
      }
    });
    NumberFormat numFormat = NumberFormat.simpleCurrency(name: "HK\$ ");
    return numFormat.format(total);
  }

  updateFamilyMemberDate(int member,
      {String memNam,
      String memAge,
      String memGender,
      String memRelation,
      String memCareer,
      String memIncome}) {
    print("updateFamilyMemberDate " + member.toString());

    print("memNam " + memNam.toString());

    FamilyMemberIncome temp = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[
        member]; //.copyWith(memNam:memNam, memAge:memAge, memGender:memGender, memRelation:memRelation, memCareer:memCareer, memIncome:memIncome);

    print("temp name " + temp.memNam);

/*  temp.id =  member.toString() ?? temp.id;
        temp.memNam =memNam ?? temp.memNam;
        temp.memAge = memAge ?? temp.memAge;
        temp.memGender= memGender ?? temp.memGender;
        temp.memRelation= memRelation ?? temp.memRelation;
        temp.memCareer= memCareer ?? temp.memCareer;
        temp.memIncome= memIncome ?? temp.memIncome;
        temp.incomeCopyType= temp.incomeCopyType;
        temp.incomeCopyLocation= temp.incomeCopyLocation;  */

    /*   FamilyMemberIncome temp2 =  FamilyMemberIncome(
        id: member.toString() ?? temp.id,
        memNam: memNam ?? temp.memNam,
        memAge: memAge ?? temp.memAge,
        memGender: memGender ?? temp.memGender,
        memRelation: memRelation ?? temp.memRelation,
        memCareer: memCareer ?? temp.memCareer,
        memIncome: memIncome ?? temp.memIncome,
        incomeCopyType: temp.incomeCopyType,
        incomeCopyLocation: temp.incomeCopyLocation,
      );  */

    print("temp2 name " + temp.memNam);

    StoreProvider.of<AppState>(context).dispatch(Update_FamilyMemberIncome(
        FamilyMemberIncome(
          id: member.toString() ?? temp.id,
          memNam: memNam ?? temp.memNam,
          memAge: memAge ?? temp.memAge,
          memGender: memGender ?? temp.memGender,
          memRelation: memRelation ?? temp.memRelation,
          memCareer: memCareer ?? temp.memCareer,
          memIncome: memIncome ?? temp.memIncome,
          incomeCopyType: temp.incomeCopyType,
          incomeCopyLocation: temp.incomeCopyLocation,
        ),
        member));

    print("temp name 2 " + temp.memNam);
  }

  updateMemberValidList() {
    setState(() {
      memberValidList = _nextBtnController.validList;
    });
  }
}

/* Data_State _change_FamilyMemberIncome_Reducer(
    Data_State state, Change_FamilyMemberIncome_Action action) {
  print("Change_FamilyMemberIncome_Action " +
      action.familyMemberIncome.toString());

  return state.copyWith(familyMemberIncome: action.familyMemberIncome);
} */
