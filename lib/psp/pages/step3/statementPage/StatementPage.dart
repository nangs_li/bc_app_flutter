import 'dart:convert';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/EmailCtr.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:path/path.dart' as path;

import '../../../app/theme.dart';

//comment
class StatementPage extends StatefulWidget {
  @override
  _StatementPageState createState() => _StatementPageState();
}

class _StatementPageState extends State<StatementPage> {
  int current_percentage;
  bool isSubmit;
  String temporaryEmail;
  NextBtnController _nextBtnController;
  SingletonShareData data;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRStatementPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 66;
    isSubmit = false;
    temporaryEmail = "";
    _nextBtnController = NextBtnController(dataList: []);
    data = SingletonShareData();

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.StatementPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () => {
              StoreProvider.of<AppState>(context)
                  .dispatch(PopFromRStatementPageAction(this.context))
            },
          ),
          actions: <Widget>[SaveBtnWrapper()],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 2),
              Padding(
                  padding: EdgeInsets.only(top: 20, left: 27, right: 27),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        data.smtp.receivemail.length == 0 ? Container(
                          padding: EdgeInsets.only(top: 22, bottom: 22),
                          alignment: Alignment.centerLeft,
                          child: TextField(
                            onChanged: (text) => {
                              setState(() {
                                temporaryEmail = text;
                                print(temporaryEmail);
                              })
                            },
                            decoration: InputDecoration(
                                labelText: "Email send to ", hintText: ''),
                          ),
                        ) : Container(),
                        Container(
                          padding: EdgeInsets.only(top: 28, right: 86),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['statementPage']['title'].toString(),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: formPageTitleStyle,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 22),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['statementPage']['inductionText']
                                .toString(),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: step3BodyTextStyle,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 52),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['statementPage']['content'].toString(),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: step3BodyTextStyle,
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                bottom: 142), //86+56 (padding+btn height)
                            child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: uiData_Text['statementPage']
                                        ['contentPoint']
                                    .length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.only(top: 22),
                                      child: Stack(children: <Widget>[
                                        Text('-', style: step3BodyTextStyle),
                                        Padding(
                                            padding: EdgeInsets.only(left: 24),
                                            child: Text(
                                              uiData_Text['statementPage']
                                                      ['contentPoint'][index]
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              softWrap: true,
                                              style: step3BodyTextStyle,
                                            ))
                                      ]));
                                }))
                      ],
                    ),
                  )),
              isSubmit == true
                  ? Container(
                      color: Colors.black45,
                      child: Center(child: CircularProgressIndicator()))
                  : Positioned(
                      left: 42,
                      right: 42,
                      bottom: 86,
                      child: NextBtnWrapper(
                        title: StoreProvider.of<AppState>(context)
                                .state
                                .dataState
                                .isSentEmail
                            ? uiData_Text['general']['startReadBtn'].toString()
                            : uiData_Text['statementPage']['agreeBtn']
                                .toString(),
                        clickEvent: () => submit(dataState),
                        controller: _nextBtnController,
                      ))
            ]);
          },
        ),
      ),
    );
  }

  /**
   * TODO:
   * 1. Get Case No. API
   * 2. Add Photos & csv/json and Send Email with CaseNo.
   * 3. If true, Update Case API
   */
  submit(Data_State dataState) {
    if (temporaryEmail.isNotEmpty || (data.smtp.receivemail.length > 0)) {
      print("tempory email is " + temporaryEmail);
      addPhoto(dataState);
      setState(() {
        isSubmit = true;
      });
      ga.gaEvent(name: "psp_sumbitted_application_button");
    } else {
      print("tempory email is empty");
    }
  }

  addPhoto(Data_State dataState) async {
    Directory fullPath = await getApplicationDocumentsDirectory();

    //Load files
    List<String> filePaths = List();
    //hkid
    for (int i = 0;
        i <
            StoreProvider.of<AppState>(context)
                .state
                .dataState
                .hkidLocation
                .length;
        i++) {
      String path = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .hkidLocation[i]
          .path;

      //print("id location " + path);
      filePaths.add("${fullPath.path}/$path");
    }
    //invoice
    for (int i = 0;
        i <
            StoreProvider.of<AppState>(context)
                .state
                .dataState
                .invoiceLocation
                .length;
        i++) {
      String path = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .invoiceLocation[i]
          .path;

      //print("id location " + path);
      //files.add(File("${fullPath.path}/$path"));
      filePaths.add("${fullPath.path}/$path");
    }
    //ref letter
    if (StoreProvider.of<AppState>(context).state.dataState.isPublicHospial ==
        uiData_Text['pubOrPriHospitalPage']['radioGroup'][1]) {
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .refLetterLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .refLetterLocation[i]
            .path;

        //print("id location " + path);
        filePaths.add("${fullPath.path}/$path");
      }
    }
    //medical
    if (StoreProvider.of<AppState>(context).state.dataState.isPublicHospial ==
        uiData_Text['pubOrPriHospitalPage']['radioGroup'][1]) {
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .medicalReportLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .medicalReportLocation[i]
            .path;

        //print("id location " + path);
        filePaths.add("${fullPath.path}/$path");
      }
    }
    //income copy
    for (int i = 0;
        i <
            StoreProvider.of<AppState>(context)
                .state
                .dataState
                .incomeCopyLocation
                .length;
        i++) {
      String path = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .incomeCopyLocation[i]
          .path;

      filePaths.add("${fullPath.path}/$path");
    }
    //family income
    for (int i = 0;
        i <
            StoreProvider.of<AppState>(context)
                .state
                .dataState
                .familyMemberIncome
                .length;
        i++) {
      for (int j = 0;
          j <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .familyMemberIncome[i]
                  .incomeCopyLocation
                  .length;
          j++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[i]
            .incomeCopyLocation[j]
            .path;

        filePaths.add("${fullPath.path}/$path");
      }
    }
    //getFamilyInfo(dataState);
    //Get Case
    if (dataState.isSentEmail) {
      StoreProvider.of<AppState>(context)
          .dispatch(NavigateToFinishPageAction());
    } else {
      getCase().then((body) async {
        var j = json.decode(body);
        if (j['status'] == "Success") {
          //TODO
          if (j['content'] != null) {
            String caseno = j['content']['caseno'];
            print("caseno:${caseno}");
            StoreProvider.of<AppState>(context)
                .dispatch(Change_caseNumber_Action(caseno));
            dataState = StoreProvider.of<AppState>(context).state.dataState;
            //Compress Zip & Return File
            print('Compress Zip & Return File');
            var resultFile = await compressZip(filePaths, zipQuality,
                zipWidthHeight); //Deflate.BEST_COMPRESSION
            //Send Email
            print('sendEmail');
            var result =
                await sendEmail([resultFile], dataState, temporaryEmail);
            if (result == true) {
              //if need delete all record
              /*  StoreProvider.of<AppState>(context).dispatch(cleanAllDate(context,
                    StoreProvider.of<AppState>(context).state.dataState)
                .then((value) => {
                      print("delete all file"),
                    })); */

              StoreProvider.of<AppState>(context)
                  .dispatch(Change_isSentEmail_Action(true));
              StoreProvider.of<AppState>(context)
                  .dispatch(Change_isAgreeStatement_Action(true));
              StoreProvider.of<AppState>(context)
                  .dispatch(SaveAppDataToLocalAction());
              StoreProvider.of<AppState>(context)
                  .dispatch(NavigateToFinishPageAction());
            } else {
              await openDialog(context);
              setState(() {
                isSubmit = false;
              });
            }
          } else {
            //Do nothing because there is error
            print('null data');

            await openDialog(context);

            setState(() {
              isSubmit = false;
            });
          }
        } else {
          //Do nothing because there is error
          print('err data');

          await openDialog(context);

          setState(() {
            isSubmit = false;
          });
        }
      }).catchError((err) async {
        print('err data');

        await openDialog(context);

        setState(() {
          isSubmit = false;
        });
      });
    }
    //Compress Zip & Return File
    /*print('Compress Zip & Return File');
    var resultFile = await compressZip(
        filePaths, zipQuality, zipWidthHeight); //Deflate.BEST_COMPRESSION
    //Send Email
    print('sendEmail');
    var result = await sendEmail([resultFile], dataState, temporaryEmail);
    if (result == true) {
      StoreProvider.of<AppState>(context).dispatch(cleanAllDate(
              context, StoreProvider.of<AppState>(context).state.dataState)
          .then((value) => {
                print("delete all file"),
              }));
    }*/
  }
}
