import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class AgreePage extends StatefulWidget {
  @override
  _AgreePageState createState() => _AgreePageState();
}

class _AgreePageState extends State<AgreePage> {
  int current_percentage;
  NextBtnController _nextBtnController;
  String userName = '';

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRAgreePageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 33;
    _nextBtnController = NextBtnController(dataList: []);

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context)
          .dispatch(Change_lastSavedPage_Action(PageName.AgreePage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    
     userName = StoreProvider.of<AppState>(context).state.dataState.nameChinese!=''?StoreProvider.of<AppState>(context).state.dataState.nameChinese:StoreProvider.of<AppState>(context).state.dataState.nameEng;
    
    
    

    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () => {
              StoreProvider.of<AppState>(context)
                  .dispatch(PopFromRAgreePageAction(this.context))
            },
          ),
          actions: <Widget>[SaveBtnWrapper()],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 2),
              Padding(
                  padding: EdgeInsets.only(top: 20, left: 25, right: 25),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 33), //, right: 86
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['agreePage']['title'].toString(),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: formPageTitleStyle,
                          ),
                        ),
                        Container(
                          padding: rowPadding,
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "本人" +
                                userName +
                                uiData_Text['agreePage']['inductionText']
                                    .toString(),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: step3BodyTextStyle,
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                bottom: 142), //86+56 (padding+btn height)
                            child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: uiData_Text['agreePage']
                                        ['contentPoint']
                                    .length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.only(top: 22),
                                      child: Stack(children: <Widget>[
                                        Text('-', style: step3BodyTextStyle),
                                        Padding(
                                            padding: EdgeInsets.only(left: 24),
                                            child: Text(
                                              uiData_Text['agreePage']
                                                      ['contentPoint'][index]
                                                  .toString(),
                                              textAlign: TextAlign.left,
                                              softWrap: true,
                                              style: step3BodyTextStyle,
                                            ))
                                      ]));
                                }))
                      ],
                    ),
                  )),
              Positioned(
                  left: 42,
                  right: 42,
                  bottom: 86,
                  child: NextBtnWrapper(
                    title: StoreProvider.of<AppState>(context)
                            .state
                            .dataState
                            .isSentEmail
                        ? uiData_Text['general']['startReadBtn'].toString()
                        : uiData_Text['agreePage']['agreeBtn'].toString(),
                    clickEvent: gotoNextPage,
                    controller: _nextBtnController,
                  ))
            ]);
          },
        ),
      ),
    );
  }

  addPhoto() {}

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(Change_isAgreePersonalDate_Action(true));

    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToStatementPageAction());
  }
}
