import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import '../../../app/theme.dart';

//comment
class FinishPage extends StatefulWidget {
  @override
  _FinishPageState createState() => _FinishPageState();
}

class _FinishPageState extends State<FinishPage> {
  int current_percentage;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return gotoNextPage() ?? false;
  }

  @override
  void initState() {
    super.initState();
    ga.gaSetCurrentScreen(screenName: "psp_complete_message_page");
    current_percentage = 0;

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.FinishPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, 0),
            child: AppBar(
              /*      title: Text('申請程序'), */
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0.0,
              automaticallyImplyLeading: false,
            )),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              SingleChildScrollView(
                //padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(top: 84),
                        child: Container(
                            alignment: Alignment.center,
                            width: 112,
                            height: 112,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(110),
                                border: Border.all(
                                    color: config_primaryColour, width: 5)),
                            child: Icon(
                              Icons.check,
                              size: 90,
                              color: config_primaryColour,
                            ))),
                    Container(
                      padding: EdgeInsets.only(top: 27),
                      alignment: Alignment.center,
                      child: Text(
                        uiData_Text['finishPage']['title'].toString(),
                        textAlign: TextAlign.left,
                        softWrap: true,
                        style: step3FinishTitleStyle,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 27, left: 17, right: 17),
                      alignment: Alignment.center,
                      child: Text(
                        uiData_Text['finishPage']['subtitle'].toString(),
                        textAlign: TextAlign.center,
                        softWrap: true,
                        style: step3FinishBodyStyle,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 27),
                      alignment: Alignment.centerLeft,
                      child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(
                              left: 27, right: 27, top: 20, bottom: 20),
                          child: Stack(children: [
                            Padding(
                                padding: EdgeInsets.only(left: 29),
                                child: Icon(Icons.priority_high,
                                    color: config_progressBarColour, size: 24)),
                            Padding(
                                padding: EdgeInsets.only(left: 56, right: 29),
                                child: Text(
                                  uiData_Text['finishPage']['inductionText']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  softWrap: true,
                                  style: step3FinishAlertStyle,
                                ))
                          ])),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 64, bottom: 64),
                        child: InkWell(
                            onTap: () => gotoNextPage(),
                            child: Container(
                              width: MediaQuery.of(context).size.width - 54,
                              height: 56,
                              padding: EdgeInsets.only(left: 27, right: 27),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(
                                    color: config_submitBtnColour, width: 1),
                              ),
                              child: Center(
                                  child: Text(
                                uiData_Text['finishPage']['agreeBtn']
                                    .toString(),
                                style: submitBtnTextStyle.copyWith(
                                    color: config_submitBtnColour),
                              )),
                            ))),
                  ],
                ),
              )
            ]);
          },
        ),
      ),
    );
  }

  addPhoto() {}

  gotoNextPage() {
    ga.gaEvent(name: "psp_complete_back_button");
    StoreProvider.of<AppState>(context).dispatch(NavigateToHomePageAction());
  }
}
