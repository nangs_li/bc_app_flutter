import 'package:icanfight/psp/app/app_state.dart';

import 'package:icanfight/psp/app/data/dataState.dart';

import 'package:icanfight/psp/routing/routingAction.dart';

import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
  //store.dispatch(IOPrefenceAction("state.json"));
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    /*
    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context)
          .dispatch(IOPrefenceAction('state.json'));
    });*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login Page'),
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Redux is working: ' + dataState.nameEng.toString()),
                  RaisedButton(
                    child: Text('Change page'),
                    onPressed: () => StoreProvider.of<AppState>(context)
                        .dispatch(NavigateToFormPageAction(context)),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
