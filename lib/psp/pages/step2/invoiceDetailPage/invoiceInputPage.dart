import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/CustomDateTimePicker.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:icanfight/psp/models/PassImageListArguments.dart';

import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

//comment
class InvoiceInputPage extends StatefulWidget {
  final PassImageListArguments args;
  InvoiceInputPage({Key key, this.args}) : super(key: key);

  @override
  _InvoiceInputPageState createState() => _InvoiceInputPageState();
}

class _InvoiceInputPageState extends State<InvoiceInputPage> {
  int current_percentage;
  Image idImage;
  String local_invoicePrice = "";
  DateTime date;
  List<bool> validList;
  List<FocusNode> focusNode;
  NextBtnController _nextBtnController;

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 30;
    validList = List.generate(2, (index) => true);
    date = DateTime.now();
    focusNode = List.generate(2, (index) => FocusNode());
    //TODO
    // _nextBtnController = NextBtnController(dataList: []);

    print("InvoiceInputPage");
/*     print("Usertype " + widget.args.userType.toString());
    print("memberID " + widget.args.memberID.toString());
    print("photo num " + widget.args.byteDateList.length.toString()); */

    // if not empty bytedate list
    if (widget.args.byteDateList.isNotEmpty) {
      setState(() {
        idImage =
            Image.memory(widget.args.byteDateList[0].buffer.asUint8List());
      });
    } else {
      //create an demo
      /*     initDemoByteData().then((value) => {
            print("null photo, start create a demo"),
            setState(() {
              final buffer = demoByteData[0].buffer;

              idImage = Image.memory(buffer.asUint8List(
                  demoByteData[0].offsetInBytes,
                  demoByteData[0].lengthInBytes));
            }),
          }); */
    }

    /*   readImageByteFromLocal(invoiceImage_localPath).then((value) => {
          if (value != null)
            {
              //  print("read image no null " + value.toString()),
              setState(() {
                idImage = Image.memory(value);
              })
            }
        }); */
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
            appBar: AppBar(
              /*      title: Text('申請程序'), */
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0.0,
              leading: IconButton(
                  iconSize: 30.0,
                  icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                  onPressed: () => {
                        StoreProvider.of<AppState>(context)
                            .dispatch(PopToUploadDocListPageAction(context))
                        /*    Navigator.of(context).pop(), */
                      }),
            ),
            body: StoreConnector<AppState, Data_State>(
              converter: (Store<AppState> store) => store.state.dataState,
              builder: (BuildContext context, Data_State dataState) {
                _nextBtnController = NextBtnController(
                    dataList: [date.toString(), local_invoicePrice],
                    updateValid: updateValid,
                    validTypeList: [ValidType.None, ValidType.None]);
                return Stack(children: <Widget>[
                  Container(
                      decoration: BoxDecoration(color: config_normalBGColour)),
                  PrograssBar(percentage: current_percentage, step: 1),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: SingleChildScrollView(
                        //padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            /*if (idImage != null) idImage,*/
                            Container(
                                //color: Colors.white,
                                padding: EdgeInsets.only(top: 23),
                                width: MediaQuery.of(context).size.width,
                                height: 308 *
                                    (MediaQuery.of(context).size.height / 896),
                                child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: idImage)),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 23, right: 23, top: 23),
                                child: Column(children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      uiData_Text['invoiceInputPage']
                                              ['dateLabel']
                                          .toString(),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: placeholderStyle,
                                    ),
                                  ),
                                  Container(
                                    height: 48,
                                    padding: EdgeInsets.only(top: 3),
                                    alignment: Alignment.centerLeft,
                                    child: RaisedButton(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            side: BorderSide(
                                                color: validList[0]
                                                    ? config_primaryColour
                                                    : config_alertColour,
                                                width: validList[0] ? 1.0 : 2.0,
                                                style: BorderStyle.solid)),
                                        elevation: 0.0,
                                        padding: EdgeInsets.only(
                                            left: 12, right: 12),
                                        onPressed: () => _selectDate(
                                            context, date
                                            /*dataState.birthDay != ''
                                                  ? DateTime.parse(
                                                      dataState.birthDay)
                                                  : DateTime.now()*/
                                            ),
                                        child: Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              Positioned(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Text(
                                                      //TODO
                                                      date != ''
                                                          ? DateFormat(
                                                                  "dd/MM/yyyy")
                                                              .format(date)
                                                          : uiData_Text[
                                                                      'invoiceInputPage']
                                                                  ['dateHin']
                                                              .toString(),
                                                      // dataState.birthDay,
                                                      // Change_birthDay_Action(picked.toIso8601String()
                                                      style: date != ''
                                                          ? textfieldStyle
                                                          : txtPlaceholderStyle)),
                                              Positioned(
                                                  right: 12,
                                                  child: Icon(Icons.date_range,
                                                      color:
                                                          config_placeholderColour))
                                            ])),
                                  ),
                                  DataTextFieldGroup(
                                      title: uiData_Text['invoiceInputPage']
                                              ['priceLabel']
                                          .toString(),
                                      hint: uiData_Text['invoiceInputPage']
                                              ['priceHin']
                                          .toString(),
                                      keyboardType: TextInputType.number,
                                      initValue: local_invoicePrice,
                                      onChange: (text) {
                                        setState(() {
                                          local_invoicePrice = text;
                                        });
                                      },
                                      focusNode: focusNode[1],
                                      nextFocusNode: null,
                                      isValid: validList[1],
                                      isPrice: true)
                                ])),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 38, right: 38, top: 46, bottom: 46),
                              child: NextBtnWrapper(
                                title: uiData_Text['invoiceInputPage']
                                        ['confirmBtn']
                                    .toString(),
                                clickEvent: gotoNextPage,
                                controller: _nextBtnController,
                                //validList: validTxtField,
                                //dataList: txtFieldList,
                                //checkValid: checkValid
                              ),
                            ),
                          ],
                        ),
                      )),
                ]);
              },
            ),
          )),
    );
  }

  gotoNextPage() {
    //-1 mean user
    print("date.toIso8601String(): ${date.toIso8601String()}");
    saveImageDoc(
        context,
        IMAGE_TYPE.invoice,
        widget.args.byteDateList, //demoByteData,
        date.toIso8601String(),
        date.toIso8601String(),
        () => {
              //TODO save invoice price
              StoreProvider.of<AppState>(context)
                  .dispatch(Change_InvoicePrice_Action(local_invoicePrice)),
              StoreProvider.of<AppState>(context)
                  .dispatch(SaveAppDataToLocalAction()),
              /*   StoreProvider.of<AppState>(context)
                  .dispatch(NavigateToInvoiceDetailPageAction(
                      )) */
              StoreProvider.of<AppState>(context)
                  .dispatch(NavigateToUploadDocListPageAction())
            });
  }

  void _selectDate(BuildContext context, DateTime dateTime) {
    CustomDatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1900, 8),
        maxTime: DateTime.now(), onChanged: (dateC) {
      print('change $dateC');
      setState(() {
        date = dateC;
      });
    }, onConfirm: (dateC) {
      print('confirm $dateC');
      setState(() {
        date = dateC;
      });
    }, currentTime: dateTime, locale: LocaleType.zh);
  }

  /*Future<DateTime> _selectDate(BuildContext context, DateTime dateTime) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: dateTime, //birthDate
      firstDate: DateTime(1900, 8),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: config_primaryColour, //Head background
            accentColor: config_primaryColour, //selection color
            dialogBackgroundColor: Colors.white,
            buttonColor: config_primaryColour,
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != date) {
      setState(() {
        // var str = "${picked.day}/${picked.month}/${picked.year}";
        date = picked;
        if (date.day != DateTime.now().day &&
            date.month != DateTime.now().month &&
            date.year != DateTime.now().year) {
          print('date: ${date}, isToday: true');
        } else {
          print('date: ${date}, isToday: false');
        }
      });

      //TODO
      //StoreProvider.of<AppState>(context).dispatch(Change_birthDay_Action(date.toIso8601String()));
    }
  }*/

/*     StoreProvider.of<AppState>(context)
        .dispatch(NavigateToInvoiceDetailPageAction()); */

  updateValid() {
    setState(() {
      validList = _nextBtnController.validList;
    });
  }
}
