import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/CustomDateTimePicker.dart';
import 'package:icanfight/psp/view/DataTextFieldGroup.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

//comment
class UpdateInvoiceInputPage extends StatefulWidget {
  final ScreenArguments args;
  UpdateInvoiceInputPage({Key key, this.args}) : super(key: key);

  @override
  _UpdateInvoiceInputPageState createState() => _UpdateInvoiceInputPageState();
}

class _UpdateInvoiceInputPageState extends State<UpdateInvoiceInputPage> {
  int current_percentage;
  Image idImage;
  String local_invoicePrice = "";
  DateTime date;
  List<bool> validList;
  List<FocusNode> focusNode;
  NextBtnController _nextBtnController;

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 30;
    validList = List.generate(2, (index) => true);
    // date = DateTime.now();
    focusNode = List.generate(2, (index) => FocusNode());

    print("UpdateInvoiceInputPage");

    List<Location> targetList = widget.args.imageLocationList;
    int imageList_count = targetList.length;

    if (imageList_count == 0) {
      // no income image in this list

    } else {
      for (var location in targetList) {
        String path = location.path;

        print(" location " + path);

        readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  setState(() {
                    idImage = Image.memory(value, fit: BoxFit.contain);
                    // Image.memory(widget.args.byteDateList[0].buffer.asUint8List());
                  })
                }
            });
      }
    }

    Future.delayed(Duration.zero, () {
      setState(() {
        date = DateTime.parse(StoreProvider.of<AppState>(context)
            .state
            .dataState
            .invoiceLocation[0]
            .stdate);
          print("date: ${date}");
      });

      /*local_invoicePrice =
          StoreProvider.of<AppState>(context).state.dataState.invoicePrice;*/ //.substring(3).replaceAll(',', '');

      print("local_invoicePrice " + local_invoicePrice);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Scaffold(
            appBar: AppBar(
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0.0,
              leading: IconButton(
                iconSize: 30.0,
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: StoreConnector<AppState, Data_State>(
              converter: (Store<AppState> store) => store.state.dataState,
              builder: (BuildContext context, Data_State dataState) {
                if (local_invoicePrice.isEmpty)
                  local_invoicePrice = dataState.invoicePrice;

                _nextBtnController = NextBtnController(
                    dataList: [date.toString(), local_invoicePrice],
                    updateValid: updateValid,
                    validTypeList: [ValidType.None, ValidType.None]);
                return Stack(children: <Widget>[
                  Container(
                      decoration: BoxDecoration(color: config_normalBGColour)),
                  PrograssBar(percentage: current_percentage, step: 1),
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(top: 23),
                                width: MediaQuery.of(context).size.width,
                                height: 308 *
                                    (MediaQuery.of(context).size.height / 896),
                                child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: idImage)),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 23, right: 23, top: 23),
                                child: Column(children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      uiData_Text['invoiceInputPage']
                                              ['dateLabel']
                                          .toString(),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: placeholderStyle,
                                    ),
                                  ),
                                  Container(
                                    height: 48,
                                    padding: EdgeInsets.only(top: 3),
                                    alignment: Alignment.centerLeft,
                                    child: RaisedButton(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            side: BorderSide(
                                                color: validList[0]
                                                    ? config_primaryColour
                                                    : config_alertColour,
                                                width: validList[0] ? 1.0 : 2.0,
                                                style: BorderStyle.solid)),
                                        elevation: 0.0,
                                        padding: EdgeInsets.only(
                                            left: 12, right: 12),
                                        onPressed: () =>
                                            _selectDate(context, date),
                                        child: Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              Positioned(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Text(
                                                      date != ''
                                                          ? DateFormat(
                                                                  "dd/MM/yyyy")
                                                              .format(date)
                                                          : uiData_Text[
                                                                      'invoiceInputPage']
                                                                  ['dateHin']
                                                              .toString(),
                                                      style: date != ''
                                                          ? textfieldStyle
                                                          : txtPlaceholderStyle)),
                                              Positioned(
                                                  right: 12,
                                                  child: Icon(Icons.date_range,
                                                      color:
                                                          config_placeholderColour))
                                            ])),
                                  ),
                                  DataTextFieldGroup(
                                      title: uiData_Text['invoiceInputPage']
                                              ['priceLabel']
                                          .toString(),
                                      hint: uiData_Text['invoiceInputPage']
                                              ['priceHin']
                                          .toString(),
                                      keyboardType: TextInputType.number,
                                      initValue: local_invoicePrice,
                                      onChange: (text) {
                                        setState(() {
                                          local_invoicePrice = text;
                                        });
                                      },
                                      focusNode: focusNode[1],
                                      nextFocusNode: null,
                                      isValid: validList[1],
                                      isPrice: true)
                                ])),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 38, right: 38, top: 46, bottom: 46),
                              child: NextBtnWrapper(
                                title: uiData_Text['invoiceInputPage']
                                        ['confirmBtn']
                                    .toString(),
                                clickEvent: gotoNextPage,
                                controller: _nextBtnController,
                              ),
                            ),
                          ],
                        ),
                      )),
                ]);
              },
            ),
          )),
    );
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(Update_InvoiceDate_Action(date));
    print('local_invoicePrice: $local_invoicePrice');
    StoreProvider.of<AppState>(context)
        .dispatch(Change_InvoicePrice_Action(local_invoicePrice));

    StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToUploadDocListPageAction());

/* 
    //-1 mean user
    saveImageDoc(
        context,
        IMAGE_TYPE.invoice,
        widget.args.byteDateList, 
        date.toString(),
        date.toString(),
        () => {

              StoreProvider.of<AppState>(context)
                  .dispatch(Change_InvoicePrice_Action(local_invoicePrice)),
              StoreProvider.of<AppState>(context)
                  .dispatch(SaveAppDataToLocalAction()),

              StoreProvider.of<AppState>(context)
                  .dispatch(NavigateToUploadDocListPageAction())
            }); */
  }

  void _selectDate(BuildContext context, DateTime dateTime) {
    CustomDatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1900, 8),
        maxTime: DateTime.now(), onChanged: (dateC) {
      print('change $dateC');
      setState(() {
        date = dateC;
      });
    }, onConfirm: (dateC) {
      print('confirm $dateC');
      setState(() {
        date = dateC;
      });
    }, currentTime: dateTime, locale: LocaleType.zh);
  }

  /*
  Future<DateTime> _selectDate(BuildContext context, DateTime dateTime) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: dateTime, //birthDate
      firstDate: DateTime(1900, 8),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: config_primaryColour, //Head background
            accentColor: config_primaryColour, //selection color
            dialogBackgroundColor: Colors.white,
            buttonColor: config_primaryColour,
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != date) {
      setState(() {
        // var str = "${picked.day}/${picked.month}/${picked.year}";
        date = picked;
        if (date.day != DateTime.now().day &&
            date.month != DateTime.now().month &&
            date.year != DateTime.now().year) {
          print('date: ${date}, isToday: true');
        } else {
          print('date: ${date}, isToday: false');
        }
      });
    }
  }
  */
  updateValid() {
    setState(() {
      validList = _nextBtnController.validList;
    });
  }
}
