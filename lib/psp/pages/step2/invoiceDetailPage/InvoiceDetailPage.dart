import 'package:common_utils/common_utils.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/datetime/DateTimelineInfo.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/data/dataPageAction.dart';
import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

//comment
class InvoiceDetailPage extends StatefulWidget {
/*   final ScreenArguments args;
  InvoiceDetailPage({Key key, this.args}) : super(key: key);
 */
  @override
  _InvoiceDetailPageState createState() => _InvoiceDetailPageState();
}

class _InvoiceDetailPageState extends State<InvoiceDetailPage> {
  int current_percentage;
  List<Image> imageList = List<Image>();

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 30;
    print("InvoiceDetailPage");
    /*    print("Usertype " + widget.args.userType.toString());
    print("memberID " + widget.args.memberID.toString());
    print("photo num " + widget.args.imageLocationList.length.toString());
 */
    setLocaleInfo('zh_hk', ZHTimelineInfo());
    setLocaleInfo('en_hk', ENTimelineInfo());

    Future.delayed(Duration.zero, () {
      // listDirectory();
      // int imageList_count = widget.args.imageLocationList.length;
      /*  print("familyMemberIncome length: " +
          widget.args.imageLocationList.length.toString());
       if (imageList_count == 0) {
        // no income image in this list

      } else {
        for (var location in widget.args.imageLocationList) {
          String path = location.path;

          print(" location " + path);

          readImageByteFromLocal(path).then((value) => {
                if (value != null)
                  {
                    // print("read image no null " + value.toString()),
                    setState(() {
                      imageList.add(Image.memory(value));
                    })
                  }
              });
        }
      } */

      for (var location in StoreProvider.of<AppState>(context)
          .state
          .dataState
          .invoiceLocation) {
        String path = location.path;

        print(" location " + path);

        readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  // print("read image no null " + value.toString()),
                  setState(() {
                    imageList.add(Image.memory(value));
                  })
                }
            });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          appBar: AppBar(
            /*      title: Text('申請程序'), */
            brightness: Brightness.dark,
            titleSpacing: 0.0,
            elevation: 0.0,
            leading: IconButton(
              iconSize: 30.0,
              icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
              onPressed: () => StoreProvider.of<AppState>(context)
                  .dispatch(PopToUploadDocListPageAction(context)),
            ),
          ),
          body: StoreConnector<AppState, Data_State>(
            converter: (Store<AppState> store) => store.state.dataState,
            builder: (BuildContext context, Data_State dataState) {
              return Stack(children: <Widget>[
                Container(
                    decoration: BoxDecoration(color: config_normalBGColour)),
                PrograssBar(percentage: current_percentage, step: 1),
                Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: SingleChildScrollView(
                      //padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          for (var i = 0; i < imageList.length; i++)
                            Container(
                                color: Colors.white,
                                padding: EdgeInsets.only(top: 23),
                                width: MediaQuery.of(context).size.width,
                                height: 308 *
                                    (MediaQuery.of(context).size.height / 896),
                                child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: imageList[i])),
                          Padding(
                              padding:
                                  EdgeInsets.only(left: 23, right: 23, top: 23),
                              child: Column(children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    uiData_Text['invoiceDetailPage']
                                            ['documentTypeLabel']
                                        .toString(),
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.ellipsis,
                                    style: placeholderStyle,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    uiData_Text['invoiceDetailPage']
                                            ['documentType']
                                        .toString(),
                                    textAlign: TextAlign.left,
                                    style: updateDocInfoDateStyle,
                                    softWrap: true,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 10),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    uiData_Text['invoiceDetailPage']
                                            ['uploadDateLabel']
                                        .toString(),
                                    textAlign: TextAlign.left,
                                    style: placeholderStyle,
                                    softWrap: true,
                                  ),
                                ),
                                if (dataState.invoiceLocation.isNotEmpty)
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      getAMPMStr(
                                          TimelineUtil.format(
                                              DateTime.parse(dataState
                                                      .invoiceLocation[0]
                                                      .uploadDate)
                                                  .millisecondsSinceEpoch,
                                              locale: 'zh_hk',
                                              dayFormat: DayFormat.Full),
                                          'zh_hk'),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: updateDocInfoDateStyle,
                                    ),
                                  ),
                              ])),
                        ],
                      ),
                    )),
                Positioned(
                    bottom: 0,
                    width: MediaQuery.of(context).size.width,
                    height: 110,
                    child: Container(
                        color: config_primaryColour.withAlpha(25),
                        child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkWell(
                            onTap: () {
                              if (dataState.isSentEmail == false) {
                             
                                editDate();
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.edit,
                                    color: config_nextPageBtnColour, size: 27),
                                Text(
                                    uiData_Text['invoiceDetailPage']
                                            ['editDatetn']
                                        .toString(),
                                    style: removeBtnTextStyle)
                              ],
                            )),
                          InkWell(
                            onTap: () {
                              if (dataState.isSentEmail == false) {
                                deleteIdImage();
                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.delete,
                                    color: config_nextPageBtnColour, size: 27),
                                Text(
                                    uiData_Text['invoiceDetailPage']
                                            ['deleteBtn']
                                        .toString(),
                                    style: removeBtnTextStyle)
                              ],

                            ))]))),
              ]);
            }

                        

        
          ),
        ));
  }

  editDate() {


    StoreProvider.of<AppState>(context).dispatch(
        NavigateToUpdateInvoiceInputPageAction(ScreenArguments(
            UserType.Patient,
            StoreProvider.of<AppState>(context)
          .state
          .dataState
          .invoiceLocation,
            -1)));


  }


  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToDrugStatus1PageAction());
  }

  deleteIdImage() {
    setState(() {
      imageList.clear();
    });

    for (var i in StoreProvider.of<AppState>(context)
        .state
        .dataState
        .invoiceLocation) {
      cleanLocalFile(i.path).then((value) => {
            StoreProvider.of<AppState>(context)
                .dispatch(Remove_Location_Action(i, IMAGE_TYPE.invoice, -1)),
            StoreProvider.of<AppState>(context)
                .dispatch(SaveAppDataToLocalAction()),
            open_camera(context, IMAGE_TYPE.invoice, -1, null, true)
          });
    }
  }
}
