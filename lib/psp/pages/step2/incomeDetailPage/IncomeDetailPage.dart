import 'package:common_utils/common_utils.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/models/PassImageListArguments.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/datetime/DateTimelineInfo.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';

//comment
class IncomeDetailPage extends StatefulWidget {
  final ScreenArguments args;
  IncomeDetailPage({Key key, this.args}) : super(key: key);
  @override
  _IncomeDetailPageState createState() => _IncomeDetailPageState();
}

class _IncomeDetailPageState extends State<IncomeDetailPage> {
  int current_percentage;
  List<Image> imageList = List<Image>();
  List<ByteData> byteDateList = List<ByteData>();
  String incomeDocType = "";
  String currentImageStartDate = "";

  @override
  void initState() {
    super.initState();
    current_percentage = 30;
    setLocaleInfo('zh_hk', ZHTimelineInfo());
    setLocaleInfo('en_hk', ENTimelineInfo());
    currentImageStartDate = widget.args.imageLocationList[0].stdate;
    print("currentImageStartDate: $currentImageStartDate");
    // print("currentImageStartDate: ${widget.args.imageLocationList[0].uploadDate}");
    Future.delayed(Duration.zero, () {
      if (widget.args.userType == UserType.Patient) {
        incomeDocType =
            StoreProvider.of<AppState>(context).state.dataState.incomeCopyType;
      } else {
        incomeDocType = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[widget.args.memberID]
            .incomeCopyType;
      }

      print("incomeDocType " + incomeDocType.toString());

      int imageList_count = widget.args.imageLocationList.length;
      print("familyMemberIncome length: " +
          widget.args.imageLocationList.length.toString());
      // print("widget.args.imageLocationList: ${widget.args.imageLocationList[0].stdate}");
      if (imageList_count == 0) {
        // no income image in this list

      } else {
        for (var location in widget.args.imageLocationList) {
          String path = location.path;

          print(" location " + path);

          readImageByteFromLocal(path).then((value) => {
                if (value != null)
                  {
                    byteDateList.add(ByteData.view(value.buffer)),

                    // print("read image no null " + value.toString()),
                    setState(() {
                      imageList.add(Image.memory(value));
                    })
                  }
              });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /*      title: Text('申請程序'), */
        brightness: Brightness.dark,
        titleSpacing: 0.0,
        elevation: 0.0,
        leading: IconButton(
          iconSize: 30.0,
          icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
          onPressed: () {
            Navigator.of(context).pop();
            /*  StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context)); */
          },
        ),
      ),
      body: StoreConnector<AppState, Data_State>(
        converter: (Store<AppState> store) => store.state.dataState,
        builder: (BuildContext context, Data_State dataState) {
          // print('imageList length:${imageList.length}');
          return Stack(children: <Widget>[
            Container(decoration: BoxDecoration(color: config_normalBGColour)),
            PrograssBar(percentage: current_percentage, step: 1),
            Padding(
                padding: EdgeInsets.only(top: 20),
                child: SingleChildScrollView(
                  //padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      for (var i = 0; i < imageList.length; i++)
                        Container(
                            color: incomeDocType == "IMAGE_TYPE.passbook"
                                ? Colors.transparent
                                : Colors.white,
                            padding: EdgeInsets.only(top: 23),
                            width: MediaQuery.of(context).size.width,
                            height: incomeDocType ==
                                    "IMAGE_TYPE.passbook"
                                ? MediaQuery.of(context).size.height * .5
                                : 308 *
                                    (MediaQuery.of(context).size.height / 896),
                            child: Padding(
                                padding: EdgeInsets.all(10),
                                child: imageList[i])),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 23,
                              right: 23,
                              top: incomeDocType == "IMAGE_TYPE.passbook"
                                  ? 0
                                  : 23),
                          child: Column(children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                uiData_Text['incomeDetailPage']
                                        ['documentTypeLabel']
                                    .toString(),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: placeholderStyle,
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                incomeDocType == "IMAGE_TYPE.passbook"
                                    ? getTitle(incomeDocType) +
                                        " - " +
                                        getDate(
                                            widget.args.imageLocationList[0]
                                                .stdate,
                                            widget.args.imageLocationList[0]
                                                .enddate)
                                    : getTitle(incomeDocType), //incomeDocType,
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: updateDocInfoDateStyle,
                                softWrap: true,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                uiData_Text['incomeDetailPage']
                                        ['uploadDateLabel']
                                    .toString(),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: placeholderStyle,
                              ),
                            ),
                            if (dataState.incomeCopyLocation.isNotEmpty)
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  getAMPMStr(
                                      TimelineUtil.format(
                                          DateTime.parse(widget
                                                  .args
                                                  .imageLocationList[0]
                                                  .uploadDate)
                                              .millisecondsSinceEpoch,
                                          locale: 'zh_hk',
                                          dayFormat: DayFormat.Full),
                                      'zh_hk'),
                                  //dateFormat.format(DateTime.parse(dataState.hkidLocation[0].uploadDate)),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: updateDocInfoDateStyle,
                                ),
                              ),
                          ])),
                    ],
                  ),
                )),
            Positioned(
                bottom: 0,
                width: MediaQuery.of(context).size.width,
                height: 110,
                child: Container(
                    color: config_primaryColour.withAlpha(25),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          if (incomeDocType == "IMAGE_TYPE.passbook")
                            InkWell(
                                onTap: () {
                                  if(dataState.isSentEmail==false){
                                    editDate();
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.edit,
                                        color: config_nextPageBtnColour,
                                        size: 27),
                                    Text(
                                        uiData_Text['incomeDetailPage']
                                                ['editDatetn']
                                            .toString(),
                                        style: removeBtnTextStyle)
                                  ],
                                )),
                          InkWell(
                              onTap: () {
                                if(dataState.isSentEmail==false){
                                  deleteIdImage();
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.delete,
                                      color: config_nextPageBtnColour,
                                      size: 27),
                                  Text(
                                      uiData_Text['incomeDetailPage']
                                              ['deleteBtn']
                                          .toString(),
                                      style: removeBtnTextStyle)
                                ],
                              )),
                        ])))
          ]);
        },
      ),
    );
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToDrugStatus1PageAction());
  }

  editDate() {
    print("editDate");

    /*   PassImageListArguments args;
    if (widget.args.memberID == 1) {
      args = PassImageListArguments(
          UserType.Patient, byteDateList, widget.args.memberID);
    } else {
      args = PassImageListArguments(
          UserType.FamilyMember, byteDateList, widget.args.memberID);
    } */

    StoreProvider.of<AppState>(context).dispatch(
        NavigateToUpdateBankBookInputPageAction(ScreenArguments(
            widget.args.userType,
            widget.args.imageLocationList,
            widget.args.memberID)));
  }

  deleteIdImage() {
    print("deleteIdImage");
    bool isBankStatement = false;
    var formatter = intl.DateFormat('yM');

    if (incomeDocType == IMAGE_TYPE.bank_statement.toString()) {
      isBankStatement = true;
    }
    print("current is bank statement " + isBankStatement.toString());

    setState(() {
      imageList.clear();
    });

    if (widget.args.memberID == -1) {
      print("deleteIdImage is user");
      for (var i in widget.args.imageLocationList) {
        print("start deleteIdImage " + i.path);

        bool isNotSameMonth = true;
        cleanLocalFile(i.path)
            .then((value) => {
                  StoreProvider.of<AppState>(context).dispatch(
                      Remove_Location_Action(
                          i,
                          IMAGE_TYPE.values
                              .firstWhere((e) => e.toString() == incomeDocType),
                          widget.args.memberID)),
                })
            .then((value) => {
                  if (StoreProvider.of<AppState>(context)
                      .state
                      .dataState
                      .incomeCopyLocation
                      .isEmpty)
                    {
                      StoreProvider.of<AppState>(context).dispatch(
                          Change_IncomeCopyType_Action(
                              null, widget.args.memberID)),
                      StoreProvider.of<AppState>(context)
                          .dispatch(SaveAppDataToLocalAction()),
                      if (isBankStatement)
                        {popToUploadBankStatementPage()}
                      else
                        {
                          StoreProvider.of<AppState>(context)
                              .dispatch(PopToUploadDocListPageAction(context))
                        }
                      /*    StoreProvider.of<AppState>(context)
                          .dispatch(PopToUploadDocListPageAction(context)) */
                    }
                  else
                    {
                      //  var isHaveSameMonthDOc = false,
                      StoreProvider.of<AppState>(context)
                          .dispatch(SaveAppDataToLocalAction()),

                      if (isBankStatement)
                        {
                          print("current image type is bank sheet "),
                          for (var location
                              in StoreProvider.of<AppState>(context)
                                  .state
                                  .dataState
                                  .incomeCopyLocation)
                            {
                              print("start check reminding image doc "),
                              print(" cuurnet image staret date " +
                                  formatter.format(
                                      DateTime.parse(currentImageStartDate))),
                              print("other photo start date  " +
                                  formatter
                                      .format(DateTime.parse(location.stdate))),
                              if (formatter.format(
                                      DateTime.parse(currentImageStartDate)) ==
                                  formatter
                                      .format(DateTime.parse(location.stdate)))
                                {
                                  print("still have some month doc"),
                                  isNotSameMonth = false
                                }
                            },
                          if (isNotSameMonth)
                            {
                              popToUploadBankStatementPage()
                              /*       StoreProvider.of<AppState>(context).dispatch(
                                  PopToUploadDocListPageAction(context)) */

                              /*   if (widget.args.memberID == -1)
                                {
                                  print("222 " +
                                      StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .incomeCopyType),
                                  if (StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .incomeCopyType ==
                                      "IMAGE_TYPE.bank_statement")
                                    {popToUploadBankStatementPage()}
                                  else
                                    {
                                      StoreProvider.of<AppState>(context)
                                          .dispatch(
                                              PopToUploadDocListPageAction(
                                                  context))
                                    }
                                }
                              else
                                {
                                  print("222 " +
                                      StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .familyMemberIncome[
                                              widget.args.memberID]
                                          .incomeCopyType),
                                  if (StoreProvider.of<AppState>(context)
                                          .state
                                          .dataState
                                          .familyMemberIncome[
                                              widget.args.memberID]
                                          .incomeCopyType ==
                                      "IMAGE_TYPE.bank_statement")
                                    {popToUploadBankStatementPage()}
                                  else
                                    {
                                      StoreProvider.of<AppState>(context)
                                          .dispatch(
                                              PopToUploadDocListPageAction(
                                                  context))
                                    }
                                } */
                            }
                          else
                            {Navigator.pop(context, true)}
                        }
                      else
                        {Navigator.pop(context, true)}
                    }
                });
      }
    } else {
      print(
          "deleteIdImage is family member " + widget.args.memberID.toString());

      for (var i in widget.args.imageLocationList) {
        print("start deleteIdImage " + i.path);
        bool isNotSameMonth = true;

        cleanLocalFile(i.path)
            .then((value) => {
                  StoreProvider.of<AppState>(context).dispatch(
                      Remove_Location_Action(
                          i,
                          IMAGE_TYPE.values
                              .firstWhere((e) => e.toString() == incomeDocType),
                          widget.args.memberID)),
                })
            .then((value) => {
                  if (StoreProvider.of<AppState>(context)
                      .state
                      .dataState
                      .familyMemberIncome[widget.args.memberID]
                      .incomeCopyLocation
                      .isEmpty)
                    {
                      StoreProvider.of<AppState>(context).dispatch(
                          Change_IncomeCopyType_Action(
                              null, widget.args.memberID)),
                      StoreProvider.of<AppState>(context)
                          .dispatch(SaveAppDataToLocalAction()),
                      if (isBankStatement)
                        {popToUploadBankStatementPage()}
                      else
                        {
                          StoreProvider.of<AppState>(context)
                              .dispatch(PopToUploadDocListPageAction(context))
                        }
                    }
                  else
                    {
                      print("incomeDocType 2 " + incomeDocType),
                      StoreProvider.of<AppState>(context)
                          .dispatch(SaveAppDataToLocalAction()),
                      if (incomeDocType ==
                          IMAGE_TYPE.bank_statement.toString())
                        {
                          print("current image type is bank sheet "),
                          for (var location
                              in StoreProvider.of<AppState>(context)
                                  .state
                                  .dataState
                                  .familyMemberIncome[widget.args.memberID]
                                  .incomeCopyLocation)
                            {
                              print("start check reminding image doc "),
                              print(" cuurnet image staret date " +
                                  formatter.format(
                                      DateTime.parse(currentImageStartDate))),
                              print("other photo start date  " +
                                  formatter
                                      .format(DateTime.parse(location.stdate))),
                              if (formatter.format(
                                      DateTime.parse(currentImageStartDate)) ==
                                  formatter
                                      .format(DateTime.parse(location.stdate)))
                                {
                                  print("still have some month doc"),
                                  isNotSameMonth = false
                                }
                            },
                          if (isNotSameMonth)
                            {popToUploadBankStatementPage()}
                          else
                            {Navigator.pop(context, true)}
                        }
                      else
                        {Navigator.pop(context, true)}
                    }
                });
      }
    }
/* 
      StoreProvider.of<AppState>(context)
          .dispatch(Change_IncomeCopyType_Action(null));

      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

      StoreProvider.of<AppState>(context)
          .dispatch(PopToUploadDocListPageAction()); */
    /* } else {} */

    /*   setState(() {
      idImage = null;
    }); */

    //StoreProvider.of<AppState>(context).dispatch(Remove_hkidLocation_Action());

    // cleanLocalFile(idImage_localPath);
  }

  popToUploadBankStatementPage() {
    print("popToUploadBankStatementPage");
    List<Location> targetLocationList;
    UserType targetUserType;

    if (widget.args.memberID == -1) {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .incomeCopyLocation;
      targetUserType = UserType.Patient;
    } else {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .familyMemberIncome[widget.args.memberID]
          .incomeCopyLocation;
      targetUserType = UserType.FamilyMember;
    }
    StoreProvider.of<AppState>(context).dispatch(
        NavigateToUploadBankStatementPageAction(ScreenArguments(
            targetUserType, targetLocationList, widget.args.memberID)));
  }

  String getTitle(String type) {
    switch (type) {
      case "IMAGE_TYPE.id":
        return uiData_Text['cameraPage']['id'];
        break;
      case "IMAGE_TYPE.invoice":
        return uiData_Text['cameraPage']['invoice'];
        break;
      case "IMAGE_TYPE.refletter":
        return uiData_Text['cameraPage']['refLetter'];
        break;
      case "IMAGE_TYPE.medicalreport":
        return uiData_Text['cameraPage']['medicalReport'];
        break;
      case "IMAGE_TYPE.income_tax":
        return uiData_Text['cameraPage']['incomeTax'];
        break;
      case "IMAGE_TYPE.bank_statement":
        return uiData_Text['cameraPage']['incomeBankSheet'];
        break;
      case "IMAGE_TYPE.passbook":
        return uiData_Text['cameraPage']['incomeBankBook'];
        break;
      default:
        return "";
    }
  }

  String getDate(String stdate, String enddate) {
    DateTime start = DateTime.parse(stdate);
    DateTime end = DateTime.parse(enddate);
    intl.DateFormat formatter = intl.DateFormat('MMM yyyy');
    return formatter.format(start) + " - " + formatter.format(end);
  }
}
