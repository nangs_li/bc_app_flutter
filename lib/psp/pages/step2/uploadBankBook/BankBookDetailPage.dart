import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/pages/step2/IncomeDetailPage/IncomeDetailPage.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';

//comment
class BankBookDetailPage extends StatefulWidget {
  final ScreenArguments args;
  BankBookDetailPage({Key key, this.args}) : super(key: key);

  @override
  _BankBookDetailPageState createState() => _BankBookDetailPageState();
}

class _BankBookDetailPageState extends State<BankBookDetailPage> {
  int current_percentage;
  List<GestureDetector> imageList = List<GestureDetector>();
  List<Widget> dateList = List<Widget>();
  NextBtnController _nextBtnController;
  PageController _pageController;

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 80;
    _nextBtnController = NextBtnController(dataList: []);
    _pageController = PageController(viewportFraction: 0.6);
    print("BankBookDetailPage");
    print("Usertype " + widget.args.userType.toString());
    print("memberID " + widget.args.memberID.toString());
    print("photo num " + widget.args.imageLocationList.length.toString());
/* 
    if (widget.args.imageLocationList.isNotEmpty) {
      print("path " + widget.args.imageLocationList[0].path);

      print("start date " + widget.args.imageLocationList[0].stdate);

      print("end date " + widget.args.imageLocationList[0].enddate);
    } */

    Future.delayed(Duration.zero, () {
      // listDirectory();
      _reflash(false);
    });

    /*   Future.delayed(Duration.zero, () {
      // listDirectory();

      int imageList_count = widget.args.imageLocationList.length;

      print("familyMemberIncome length: " +
          widget.args.imageLocationList.length.toString());

      if (imageList_count == 0) {
        // no income image in this list

      } else {
        for (var location in widget.args.imageLocationList) {
          String path = location.path;

          print(" location " + path);

          readImageByteFromLocal(path).then((value) => {
                if (value != null)
                  {
                    // print("read image no null " + value.toString()),
                    setState(() {
                      imageList.add(Image.memory(value));

                      GestureDetector(
                        onTap: () {}, // handle your image tap here
                        child: Image.memory(value),
                      );
                    })
                  }
              });
        }
      }
    }); */
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () => StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context)),
          ),
          actions: <Widget>[SaveBtnWrapper()],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 1),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding:
                              EdgeInsets.only(left: 23, right: 23, top: 23),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              //Title
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['bankBookDetailPage']
                                          ['documentTypeLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              //Page
                              Container(
                                padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${imageList.length}${uiData_Text['taxDetailPage']['numPageText'].toString()}",
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: updateDocPageTextStyle,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                            child: Container(
                                height: MediaQuery.of(context).size.height * .6,
                                padding: EdgeInsets.only(top: 12),
                                child: PageView.builder(
                                    controller: _pageController,
                                    scrollDirection: Axis.horizontal,
                                    physics: BouncingScrollPhysics(),
                                    itemCount: imageList.length,
                                    itemBuilder: (context, index) {
                                      return Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            imageList[index],
                                            dateList[index]
                                          ]);
                                    })
                                /*SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              child: Wrap(children: imageList),
                            )
                            */
                                )),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 38, right: 38, top: 30),
                          child: NextBtnWrapper(
                            title: uiData_Text['bankBookDetailPage']['addBtn']
                                .toString(),
                            clickEvent: () {
                              if (dataState.isSentEmail == false) {
                                addPhoto();
                              }
                            },
                            controller: _nextBtnController,
                            //validList: validTxtField,
                            //dataList: txtFieldList,
                            //checkValid: checkValid
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 26, bottom: 46),
                          child: InkWell(
                              onTap: () {
                                if (dataState.isSentEmail == false) {
                                  gotoNextPage();
                                }
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 52,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    border: Border.all(
                                        color: config_submitBtnColour,
                                        width: 1)),
                                child: Center(
                                    child: Text(
                                        uiData_Text['bankBookDetailPage']
                                            ['finishBtn'],
                                        style: customeThemeData.textTheme.button
                                            .copyWith(
                                                color:
                                                    config_submitBtnColour))),
                              )),
                        ),
                      ],
                    ),
                  ))
            ]);
          },
        ),
      ),
    );
  }

  addPhoto() {
    open_camera(
        context, IMAGE_TYPE.passbook, widget.args.memberID, null);
  }

/*   gotoNextPage() {
    StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

    Navigator.popUntil(context, ModalRoute.withName('/uploadDocList'));

  } */

  gotoNextPage() {
    StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());

    StoreProvider.of<AppState>(context)
        .dispatch(PopToUploadDocListPageAction(context));

    //Navigator.popUntil(context, ModalRoute.withName('/uploadDocList'));
    //Navigator.pop(context, true);

    /*   List<Location> targetLocationList;
    UserType targetUserType;

    if (widget.args.memberID == -1) {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .incomeCopyLocation;
      targetUserType = UserType.Patient;
    } else {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .familyMemberIncome[widget.args.memberID]
          .incomeCopyLocation;
      targetUserType = UserType.FamilyMember;
    }
    StoreProvider.of<AppState>(context).dispatch(
        NavigateToUploadBankStatementPageAction(ScreenArguments(
            targetUserType, targetLocationList, widget.args.memberID)));
  
   */
  }

  _reflash(bool isPopBack) async {
    print("_reflashing");
    List<Location> targetList;
    imageList.clear();
    dateList.clear();

    if (isPopBack) {
      if (widget.args.memberID == -1) {
        targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .incomeCopyLocation;
      } else {
        targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[widget.args.memberID]
            .incomeCopyLocation;
      }
    } else {
      targetList = widget.args.imageLocationList;
    }

    int imageList_count = targetList.length;

    print("familyMemberIncome length: " + targetList.length.toString());

    if (imageList_count == 0) {
      // no income image in this list

    } else {
      for (var location in targetList) {
        String path = location.path;

        print(" location " + path);

        await readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  setState(() {
                    List<Location> oneLocaionList = [location];

                    imageList.add(GestureDetector(
                        onTap: () {
                          print("click path " + path);

                          Navigator.of(context)
                              .push(
                                MaterialPageRoute(
                                    builder: (_) => IncomeDetailPage(
                                        args: ScreenArguments(
                                            widget.args.userType,
                                            oneLocaionList,
                                            widget.args.memberID))),
                              )
                              .then((val) => {
                                    if (val != null)
                                      {
                                        if (val) {_reflash(true)}
                                      }
                                  });
                          //.then((val) => val ? _reflash(true):()=>{});
                        }, // handle your image tap here
                        child: Container(
                            padding:
                                EdgeInsets.only(top: 10, bottom: 10, right: 10),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * .5,
                            child: Stack(children: [
                              Align(
                                alignment: Alignment.center,
                                  child:
                                      Image.memory(value, fit: BoxFit.contain)),
                              Center(
                                  child: Container(
                                      width: 120,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          color: Colors.black54),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                        SvgPicture.asset(
                                            'assets/information.svg', width: 20),
                                        Text(uiData_Text['photoNotice'],
                                            style: photoNoticeStyle)
                                      ])))
                            ]))));
                    DateTime startDate = DateTime.parse(location.stdate);
                    DateTime endDate = DateTime.parse(location.enddate);
                    DateFormat formatter = DateFormat('MMM yyyy');
                    dateList.add(Center(child: Text(
                        formatter.format(startDate) +
                            " - " +
                            formatter.format(endDate),
                        style: invoiceBtnText2Style)));
                  })
                }
            });
      }
    }
  }
}
