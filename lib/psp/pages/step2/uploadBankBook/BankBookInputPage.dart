import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/models/PassImageListArguments.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/CustomDateTimePicker.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

//pass taken photo to this page and process
class BankBookInputPage extends StatefulWidget {
  final PassImageListArguments args;
  BankBookInputPage({Key key, this.args}) : super(key: key);

  @override
  _BankBookInputPageState createState() => _BankBookInputPageState();
}

class _BankBookInputPageState extends State<BankBookInputPage> {
  int current_percentage;
  Image idImage;
  String startDate, endDate;
  NextBtnController _nextBtnController;
  List<bool> validList;
  List<FocusNode> focusNode;

  @override
  void initState() {
    super.initState();
    current_percentage = 80;
    validList = List.generate(2, (index) => true);
    focusNode = List.generate(2, (index) => FocusNode());
    startDate = '';
    endDate = '';

    print("BankBookInputPage");
    print("Usertype " + widget.args.userType.toString());
    print("memberID " + widget.args.memberID.toString());
    print("photo num " + widget.args.byteDateList.length.toString());

    // if not empty bytedate list
    if (widget.args.byteDateList.isNotEmpty) {
      setState(() {
        idImage =
            Image.memory(widget.args.byteDateList[0].buffer.asUint8List());
      });
    } else {
/*       //create an demo
      initDemoByteData().then((value) => {
            print("null photo, start create a demo"),
            setState(() {
              final buffer = demoByteData[0].buffer;

              idImage = Image.memory(buffer.asUint8List(
                  demoByteData[0].offsetInBytes,
                  demoByteData[0].lengthInBytes));
            }),
          }); */
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        /*      title: Text('申請程序'), */
        brightness: Brightness.dark,
        titleSpacing: 0.0,
        elevation: 0.0,
        leading: IconButton(
          iconSize: 30.0,
          icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
          onPressed: () {
            //Navigator.of(context).pop()
            StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context));
          },
        ),
        actions: <Widget>[SaveBtnWrapper()],
      ),
      body: StoreConnector<AppState, Data_State>(
        converter: (Store<AppState> store) => store.state.dataState,
        builder: (BuildContext context, Data_State dataState) {
          _nextBtnController =
              NextBtnController(dataList: [startDate, endDate]);

          return Stack(children: <Widget>[
            Container(decoration: BoxDecoration(color: config_normalBGColour)),
            PrograssBar(percentage: current_percentage, step: 1),
            Padding(
                padding: EdgeInsets.only(top: 20),
                child: SingleChildScrollView(
                  //padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 23, right: 23, top: 30), //23
                          child: Column(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['bankBookInputPage']['title']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              if (idImage != null)
                                SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    height: 350 * //306
                                        (MediaQuery.of(context).size.height /
                                            896),
                                    child: Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Center(child: idImage))),
                              Container(
                                padding: EdgeInsets.only(top: 10),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['bankBookInputPage']
                                          ['startDateLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: placeholderStyle,
                                ),
                              ),
                              Container(
                                height: 48,
                                padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.centerLeft,
                                child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: BorderSide(
                                            color: validList[0]
                                                ? config_primaryColour
                                                : config_alertColour,
                                            width: validList[0] ? 1.0 : 2.0,
                                            style: BorderStyle.solid)),
                                    elevation: 0.0,
                                    padding:
                                        EdgeInsets.only(left: 12, right: 12),
                                    onPressed: () => _selectDate(
                                        context,
                                        startDate != ''
                                            ? DateTime.parse(startDate)
                                            : DateTime.now(),
                                        true
                                        /*dataState.birthDay != ''
                                                  ? DateTime.parse(
                                                      dataState.birthDay)
                                                  : DateTime.now()*/
                                        ),
                                    child: Stack(
                                        alignment: Alignment.centerLeft,
                                        children: [
                                          Positioned(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Text(
                                                  //TODO
                                                  startDate != ''
                                                      ? DateFormat("dd/MM/yyyy")
                                                          .format(
                                                              DateTime.parse(
                                                                  startDate))
                                                      : uiData_Text[
                                                                  'bankBookInputPage']
                                                              ['startDateHin']
                                                          .toString(),
                                                  // dataState.birthDay,
                                                  // Change_birthDay_Action(picked.toIso8601String()
                                                  style: startDate != '' ? textfieldStyle : txtPlaceholderStyle)),
                                          Positioned(
                                              right: 12,
                                              child: Icon(Icons.date_range,
                                                  color:
                                                      config_placeholderColour))
                                        ])),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 10),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['bankBookInputPage']
                                          ['endDateLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: placeholderStyle,
                                ),
                              ),
                              Container(
                                height: 48,
                                padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.centerLeft,
                                child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: BorderSide(
                                            color: validList[1]
                                                ? config_primaryColour
                                                : config_alertColour,
                                            width: validList[1] ? 1.0 : 2.0,
                                            style: BorderStyle.solid)),
                                    elevation: 0.0,
                                    padding:
                                        EdgeInsets.only(left: 12, right: 12),
                                    onPressed: () => _selectDate(
                                        context,
                                        endDate != ''
                                            ? DateTime.parse(endDate)
                                            : DateTime.now(),
                                        false
                                        /*dataState.birthDay != ''
                                                  ? DateTime.parse(
                                                      dataState.birthDay)
                                                  : DateTime.now()*/
                                        ),
                                    child: Stack(
                                        alignment: Alignment.centerLeft,
                                        children: [
                                          Positioned(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Text(
                                                  //TODO
                                                  endDate != ''
                                                      ? DateFormat("dd/MM/yyyy")
                                                          .format(
                                                              DateTime.parse(
                                                                  endDate))
                                                      : uiData_Text[
                                                                  'bankBookInputPage']
                                                              ['endDateHin']
                                                          .toString(),
                                                  // dataState.birthDay,
                                                  // Change_birthDay_Action(picked.toIso8601String()
                                                  style: endDate != '' ? textfieldStyle : txtPlaceholderStyle)),
                                          Positioned(
                                              right: 12,
                                              child: Icon(Icons.date_range,
                                                  color:
                                                      config_placeholderColour))
                                        ])),
                              ),
                            ],
                          )),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 38, right: 38, top: 46, bottom: 46),
                        child: NextBtnWrapper(
                          title: uiData_Text['invoiceInputPage']['confirmBtn']
                              .toString(),
                          clickEvent: gotoNextPage,
                          controller: _nextBtnController,
                          //validList: validTxtField,
                          //dataList: txtFieldList,
                          //checkValid: checkValid
                        ),
                      ),
                    ],
                  ),
                ))
          ]);
        },
      ),
    );
  }

  gotoNextPage() {
    //Save photo to local and go to bankbookdetailpage

    //-1 mean user
    saveBankBookImageDoc(
        context,
        IMAGE_TYPE.passbook,
        widget.args.byteDateList,
        widget.args.memberID,
        DateTime.parse(startDate),
        DateTime.parse(endDate));
  }
  void _selectDate(BuildContext context, DateTime dateTime, bool isStartDate) {
    CustomDatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1900, 8),
        maxTime:  DateTime.now(), onChanged: (dateC) {
      print('change $dateC');
      setState(() {
        if (isStartDate) {
          startDate = dateC.toString();
        } else {
          endDate = dateC.toString();
        }
        
      });
    }, onConfirm: (dateC) {
      print('confirm $dateC');
      setState(() {
        if (isStartDate) {
          startDate = dateC.toString();
        } else {
          endDate = dateC.toString();
        }
      });
      
    }, currentTime: dateTime, locale: LocaleType.zh);
  }
  /*
  Future<DateTime> _selectDate(
      BuildContext context, DateTime dateTime, bool isStartDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: dateTime, //birthDate
      firstDate: DateTime(1900, 8),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: config_primaryColour, //Head background
            accentColor: config_primaryColour, //selection color
            dialogBackgroundColor: Colors.white,
            buttonColor: config_primaryColour,
          ),
          child: child,
        );
      },
    );

    if (picked != null && picked != dateTime) {
      setState(() {
        // var str = "${picked.day}/${picked.month}/${picked.year}";
        if (isStartDate) {
          startDate = picked.toString();
        } else {
          endDate = picked.toString();
        }
        if (dateTime.day != DateTime.now().day &&
            dateTime.month != DateTime.now().month &&
            dateTime.year != DateTime.now().year) {
          //print('date: ${dateTime}, isToday: true');
        } else {
          //print('date: ${dateTime}, isToday: false');
        }
      });

      //TODO
      //StoreProvider.of<AppState>(context).dispatch(Change_birthDay_Action(date.toIso8601String()));
    }
  }*/
}
