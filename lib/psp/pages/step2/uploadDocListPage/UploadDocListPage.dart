import 'package:camera/camera.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/pages/step2/uploadBankStatementPage/UploadBankStatementPage.dart';

import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/CameraView.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';
import '../../../controller/SaveLoadImgController.dart';

//comment
class UploadDocListPage extends StatefulWidget {
  @override
  _UploadDocListPageState createState() => _UploadDocListPageState();
}

class _UploadDocListPageState extends State<UploadDocListPage> {
  int current_percentage;
  List<ByteData> byteDate = List<ByteData>();
  NextBtnController _nextBtnController;

  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRUploadDocListPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 0;
    byteDate.add(null);

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(
          Change_lastSavedPage_Action(PageName.UploadDocListPage.toString()));
      StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction());
    });
  }

  @override
  Widget build(BuildContext context) {
    //final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          appBar: AppBar(
            /*      title: Text('申請程序'), */
            brightness: Brightness.dark,
            titleSpacing: 0.0,
            elevation: 0.0,
            leading: IconButton(
              iconSize: 30.0,
              icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
              onPressed: () => {
                StoreProvider.of<AppState>(context)
                    .dispatch(PopFromRUploadDocListPageAction(this.context))
              },
            ),
            actions: <Widget>[SaveBtnWrapper()],
          ),
          body: StoreConnector<AppState, Data_State>(
            converter: (Store<AppState> store) => store.state.dataState,
            builder: (BuildContext context, Data_State dataState) {
              // print('dataState.isPublicHospial: ${dataState.isPublicHospial}');
              _nextBtnController = UploadDocNextBtnController(
                  state: dataState); // NextBtnController(dataList: []);
              return Stack(children: <Widget>[
                Container(
                    decoration: BoxDecoration(color: config_normalBGColour)),
                SingleChildScrollView(
                  //padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(left: 23, right: 23),
                          child: Column(children: <Widget>[
                            //Title
                            Container(
                              padding: EdgeInsets.only(top: 23),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                uiData_Text['uploadDocListPage']['title']
                                    .toString(),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: formPageTitleStyle,
                              ),
                            ),
                            //Subtitle
                            Container(
                              padding: EdgeInsets.only(top: 6),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                uiData_Text['uploadDocListPage']['subtitle']
                                    .toString(),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: customeThemeData.textTheme.bodyText1,
                              ),
                            ),
                            //HKID Btn
                            Padding(
                              padding: EdgeInsets.only(top: 22),
                              child: Container(
                                  //padding: EdgeInsets.only(top: 12),
                                  width: MediaQuery.of(context).size.width,
                                  height: 70,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10.0),
                                      boxShadow: [config_boxshadow]),
                                  child: InkWell(
                                      onTap: () {
                                        open_camera(
                                            context, IMAGE_TYPE.hkid, -1, null);
                                      },
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              left: 23, right: 23),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                uiData_Text['uploadDocListPage']
                                                        ['iDPageBtn']
                                                    .toString(),
                                                style: customeThemeData
                                                    .textTheme.caption,
                                                softWrap: true,
                                                textAlign: TextAlign.left,
                                              ),
                                              dataState.hkidLocation.isNotEmpty
                                                  ? DecoratedBox(
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      40.0),
                                                          color:
                                                              config_primaryColour),
                                                      child: Padding(
                                                          padding:
                                                              EdgeInsets.all(4),
                                                          child: Icon(
                                                              Icons.check,
                                                              size: 16.0,
                                                              color: Colors
                                                                  .white)))
                                                  : Icon(
                                                      Icons.arrow_forward_ios,
                                                      size: 13.0,
                                                      color:
                                                          config_primaryColour)
                                            ],
                                          )))),
                            ),
                            //Invoice
                            Padding(
                              padding: EdgeInsets.only(top: 18),
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 132, //132
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10.0),
                                      boxShadow: [config_boxshadow]),
                                  child: InkWell(
                                      onTap: () {
                                        open_camera(context, IMAGE_TYPE.invoice,
                                            -1, null);
                                      },
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              left: 23, right: 23),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      125,
                                                  child: RichText(
                                                      text: TextSpan(
                                                          style:
                                                              invoiceBtnText2Style,
                                                          children: <TextSpan>[
                                                            TextSpan(
                                                                text: uiData_Text[
                                                                'uploadDocListPage']
                                                                [
                                                                'invoicePageBtnBeforeStart']
                                                                    .toString()),
                                                            TextSpan(
                                                          text: uiData_Text[
                                                                      'uploadDocListPage']
                                                                  [
                                                                  'invoicePageBtnStart']
                                                              .toString(),
                                                          style:
                                                              invoiceBtnTextStyle,
                                                        ),
                                                        TextSpan(
                                                            text: uiData_Text[
                                                                        'uploadDocListPage']
                                                                    [
                                                                    'invoicePageBtn']
                                                                .toString()),
                                                      ])) /*Text(
                                                    uiData_Text['uploadDocListPage']
                                                            ['invoicePageBtn']
                                                        .toString(),
                                                    style: customeThemeData
                                                        .textTheme.caption,
                                                    textAlign: TextAlign.left,
                                                    softWrap: true,
                                                    maxLines: 10,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  )*/
                                                  ),
                                              dataState.invoiceLocation
                                                      .isNotEmpty
                                                  ? DecoratedBox(
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      40.0),
                                                          color:
                                                              config_primaryColour),
                                                      child: Padding(
                                                          padding:
                                                              EdgeInsets.all(4),
                                                          child: Icon(
                                                              Icons.check,
                                                              size: 16.0,
                                                              color: Colors
                                                                  .white)))
                                                  : Icon(
                                                      Icons.arrow_forward_ios,
                                                      size: 13.0,
                                                      color:
                                                          config_primaryColour)
                                            ],
                                          )))),
                            ),
                            //Ref Letter
                            if (dataState.isPublicHospial ==
                                uiData_Text['pubOrPriHospitalPage']
                                    ['radioGroup'][1])
                              Padding(
                                padding: EdgeInsets.only(top: 18),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 70,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        boxShadow: [config_boxshadow]),
                                    child: InkWell(
                                        onTap: () {
                                          open_camera(context,
                                              IMAGE_TYPE.refletter, -1, null);
                                        },
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                left: 23, right: 23),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            .6,
                                                    child: Text(
                                                      uiData_Text['uploadDocListPage']
                                                              [
                                                              'refLetterPageBtn']
                                                          .toString(),
                                                      style: customeThemeData
                                                          .textTheme.caption,
                                                      textAlign: TextAlign.left,
                                                      softWrap: true,
                                                      maxLines: 10,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    )),
                                                dataState.refLetterLocation
                                                        .isNotEmpty
                                                    ? DecoratedBox(
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        40.0),
                                                            color:
                                                                config_primaryColour),
                                                        child: Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    4),
                                                            child: Icon(
                                                                Icons.check,
                                                                size: 16.0,
                                                                color: Colors
                                                                    .white)))
                                                    : Icon(
                                                        Icons.arrow_forward_ios,
                                                        size: 13.0,
                                                        color:
                                                            config_primaryColour)
                                              ],
                                            )))),
                              ),
                            //Medical Reports
                            if (dataState.isPublicHospial ==
                                uiData_Text['pubOrPriHospitalPage']
                                    ['radioGroup'][1])
                              Padding(
                                padding: EdgeInsets.only(top: 18),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 70,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        boxShadow: [config_boxshadow]),
                                    child: InkWell(
                                        onTap: () {
                                          open_camera(
                                              context,
                                              IMAGE_TYPE.medicalreport,
                                              -1,
                                              null);
                                        },
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                left: 23, right: 23),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            .6,
                                                    child: Text(
                                                      uiData_Text['uploadDocListPage']
                                                              [
                                                              'medicalReportPageBtn']
                                                          .toString(),
                                                      style: customeThemeData
                                                          .textTheme.caption,
                                                      textAlign: TextAlign.left,
                                                      softWrap: true,
                                                      maxLines: 10,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    )),
                                                dataState.medicalReportLocation
                                                        .isNotEmpty
                                                    ? DecoratedBox(
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        40.0),
                                                            color:
                                                                config_primaryColour),
                                                        child: Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    4),
                                                            child: Icon(
                                                                Icons.check,
                                                                size: 16.0,
                                                                color: Colors
                                                                    .white)))
                                                    : Icon(
                                                        Icons.arrow_forward_ios,
                                                        size: 13.0,
                                                        color:
                                                            config_primaryColour)
                                              ],
                                            )))),
                              ),

                            //Income
                            Padding(
                              padding: EdgeInsets.only(top: 18),
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 70,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10.0),
                                      boxShadow: [config_boxshadow]),
                                  child: InkWell(
                                      onTap: () {
                                        gotoIncomeDoc(context, -1);
                                      },
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              left: 23, right: 23),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      .6,
                                                  child: Text(
                                                    uiData_Text['uploadDocListPage']
                                                            ['incomePageBtn']
                                                        .toString(),
                                                    style: customeThemeData
                                                        .textTheme.caption,
                                                    textAlign: TextAlign.left,
                                                    softWrap: true,
                                                    maxLines: 10,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  )),
                                              checkIncomePhotos(dataState)
                                                  ? DecoratedBox(
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      40.0),
                                                          color:
                                                              config_primaryColour),
                                                      child: Padding(
                                                          padding:
                                                              EdgeInsets.all(4),
                                                          child: Icon(
                                                              Icons.check,
                                                              size: 16.0,
                                                              color: Colors
                                                                  .white)))
                                                  : Icon(
                                                      Icons.arrow_forward_ios,
                                                      size: 13.0,
                                                      color:
                                                          config_primaryColour)
                                            ],
                                          )))),
                            ),
                            //Family Member
                            if (dataState.familyMemberIncome.isNotEmpty)
                              for (int i = 0;
                                  i < dataState.familyMemberIncome.length;
                                  i++)
                                familyWidget(dataState, i)
                          ])),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 38, right: 38, top: 34, bottom: 86),
                        child: NextBtnWrapper(
                          title: StoreProvider.of<AppState>(context)
                                  .state
                                  .dataState
                                  .isSentEmail
                              ? uiData_Text['general']['startReadBtn']
                                  .toString()
                              : uiData_Text['general']['nextPageBtn']
                                  .toString(),
                          clickEvent: gotoNextPage,
                          controller: _nextBtnController,
                          /*validList: validList,
                        dataList: dataList,
                        checkValid: checkValid*/
                        ),
                      ),
                    ],
                  ),
                )
              ]);
            },
          ),
        ));
  }

  gotoNextPage() {
    print("verify all document and goto next page");
    ga.gaEvent(name: "psp_step2_completed_application_button");
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToStep3LandingPageAction());
  }

  gotoIncomeDoc(BuildContext _context, int memberID) {
    // initDemoFamilyMember(_context);

    List<Location> targetLocationList;
    UserType targetUserType;
    String incomeDocType;

    if (memberID == -1) {
      targetLocationList = StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .incomeCopyLocation;
      targetUserType = UserType.Patient;
      incomeDocType =
          StoreProvider.of<AppState>(_context).state.dataState.incomeCopyType;
    } else {
      targetLocationList = StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .familyMemberIncome[memberID]
          .incomeCopyLocation;
      targetUserType = UserType.FamilyMember;
      incomeDocType = StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .familyMemberIncome[memberID]
          .incomeCopyType;
    }

    if (targetLocationList.isEmpty) {
      StoreProvider.of<AppState>(context).dispatch(NavigateToIncomePageAction(
          ScreenArguments(targetUserType, null, memberID)));
    } else {
      switch (incomeDocType) {
        case "IMAGE_TYPE.income_tax":
          /*    StoreProvider.of<AppState>(_context).dispatch(
              NavigateToIncomeDetailPageAction(ScreenArguments(
                  targetUserType, targetLocationList, memberID)));
 */

          StoreProvider.of<AppState>(_context).dispatch(
              NavigateToTaxDetailPageAction(ScreenArguments(
                  targetUserType, targetLocationList, memberID)));

          break;

        case "IMAGE_TYPE.passbook":
          StoreProvider.of<AppState>(_context).dispatch(
              NavigateToBankBookDetailPageAction(ScreenArguments(
                  targetUserType, targetLocationList, memberID)));

          break;

        case "IMAGE_TYPE.bank_statement":
          StoreProvider.of<AppState>(context).dispatch(
              NavigateToUploadBankStatementPageAction(ScreenArguments(
                  targetUserType, targetLocationList, memberID)));
          break;
      }
    }

    /*    switch (
        StoreProvider.of<AppState>(context).state.dataState.incomeCopyType) {
      case "IMAGE_TYPE.income_tax":
        print("this is IMAGE_TYPE.income_tax doc");
        print(
            "targetLocationList size " + targetLocationList.length.toString());
        StoreProvider.of<AppState>(_context).dispatch(
            NavigateToIncomeDetailPageAction(
                ScreenArguments(targetUserType, targetLocationList, memberID)));


        break;
        case "IMAGE_TYPE.bank_statement":

        break;
      default:
        StoreProvider.of<AppState>(context).dispatch(NavigateToIncomePageAction(
            ScreenArguments(UserType.Patient, null, memberID)));
        break;
    } */

    /*             NavigateToTaxDetailPageAction(ScreenArguments(
                UserType.Patient, targetLocationList, memberID))); */
  }

  checkIncomePhotos(Data_State state) {
    //TODO for check empty
    switch (state.incomeCopyType) {
      case 'IMAGE_TYPE.income_tax':
      case 'IMAGE_TYPE.passbook':
        return state.incomeCopyLocation.isNotEmpty;
        /*state.familyMemberIncome
            .every((m) => m.incomeCopyLocation.isNotEmpty)*/
        break;
      case 'IMAGE_TYPE.bank_statement':
        DateTime now = DateTime.now();
        var formatter = DateFormat('yM');
        List<MonthBtnModel> monthBtnModel = List();
        for (var i = 0; i < 12; i++) {
          DateTime newDate = DateTime(
              now.year,
              now.month - 1 - i,
              now.day,
              now.hour,
              now.minute,
              now.second,
              now.millisecond,
              now.microsecond);
          monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
        }
        //assign location into the month location list
        for (var location in state.incomeCopyLocation) {
          print("locaiton uploadDate " +
              formatter.format(DateTime.parse(location.stdate)));

          for (var a_monthBtnModel in monthBtnModel) {
            if (formatter.format(a_monthBtnModel.btnDate) ==
                formatter.format(DateTime.parse(location.stdate))) {
              a_monthBtnModel.imageLocationList.add(location);
            }
          }
        }
        return monthBtnModel
            .every((month) => month.imageLocationList.isNotEmpty);
        break;
      default:
        return false;
    }
    ;
  }

  bool checkFamilyPhotots(Data_State state, int index) {
    print("index: ${index}");
    if (state.familyMemberIncome.isNotEmpty) {
      switch (state.familyMemberIncome[index].incomeCopyType) {
        case 'IMAGE_TYPE.income_tax':
        case 'IMAGE_TYPE.passbook':
          return state.familyMemberIncome[index].incomeCopyLocation.isNotEmpty;
          break;
        case 'IMAGE_TYPE.bank_statement':
          DateTime now = DateTime.now();
          var formatter = DateFormat('yM');
          List<MonthBtnModel> monthBtnModel = List();
          for (var i = 0; i < 12; i++) {
            DateTime newDate = DateTime(
                now.year,
                now.month - 1 - i,
                now.day,
                now.hour,
                now.minute,
                now.second,
                now.millisecond,
                now.microsecond);
            monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
          }
          //assign location into the month location list
          for (var location
              in state.familyMemberIncome[index].incomeCopyLocation) {
            print("locaiton uploadDate " +
                formatter.format(DateTime.parse(location.stdate)));

            for (var a_monthBtnModel in monthBtnModel) {
              if (formatter.format(a_monthBtnModel.btnDate) ==
                  formatter.format(DateTime.parse(location.stdate))) {
                a_monthBtnModel.imageLocationList.add(location);
              }
            }
          }
          return monthBtnModel
              .every((month) => month.imageLocationList.isNotEmpty);
          break;
        default:
          return false;
      }
    } else {
      return false;
    }
    //return monthBtnModel.every((member) => member.every((month) => month.imageLocationList.isNotEmpty));
  }

  Widget familyWidget(Data_State dataState, int index) {
    return Padding(
      padding: EdgeInsets.only(top: 18),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 70,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [config_boxshadow]),
          child: InkWell(
              onTap: () {
                gotoIncomeDoc(context, index);
              },
              child: Container(
                  padding: EdgeInsets.only(left: 23, right: 23),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                          width: MediaQuery.of(context).size.width * .6,
                          child: Text(
                            uiData_Text['uploadDocListPage']['familyMemPageBtn']
                                    .toString() +
                                dataState.familyMemberIncome[index].memNam,
                            style: customeThemeData.textTheme.caption,
                            textAlign: TextAlign.left,
                            softWrap: true,
                            maxLines: 10,
                            overflow: TextOverflow.ellipsis,
                          )),
                      checkFamilyPhotots(dataState, index)
                          ? DecoratedBox(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(40.0),
                                  color: config_primaryColour),
                              child: Padding(
                                  padding: EdgeInsets.all(4),
                                  child: Icon(Icons.check,
                                      size: 16.0, color: Colors.white)))
                          : Icon(Icons.arrow_forward_ios,
                              size: 13.0, color: config_primaryColour)
                    ],
                  )))),
    );
  }
}
