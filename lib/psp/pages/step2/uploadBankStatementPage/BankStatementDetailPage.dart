import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/pages/step2/incomeDetailPage/IncomeDetailPage.dart';

import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';
import 'UploadBankStatementPage.dart';

//comment
class BankStatementDetailPage extends StatefulWidget {
  final ScreenArguments args;
  BankStatementDetailPage({Key key, this.args}) : super(key: key);

  @override
  _BankStatementDetailPageState createState() =>
      _BankStatementDetailPageState();
}

class _BankStatementDetailPageState extends State<BankStatementDetailPage> {
  int current_percentage;
  List<GestureDetector> imageList = List<GestureDetector>();
  DateTime displayingMonth;
  NextBtnController _nextBtnController;
  PageController _pageController;

  Future<bool> _onWillPop() async {
    return gotoNextPage();
    /*   return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false; */
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 80;
    _nextBtnController = NextBtnController(dataList: []);
    _pageController = PageController(viewportFraction: 0.6);
    print("BankStatementDetailPage");
    print("member id " + widget.args.memberID.toString());
    print("widget.args: ${widget.args.imageLocationList[0].uploadDate}");
    Future.delayed(Duration.zero, () {
      // listDirectory();
      _reflash(false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () {
              gotoNextPage();
              /*      StoreProvider.of<AppState>(context)
                  .dispatch(PopToUploadDocListPageAction(context)); */
            },
          ),
          actions: <Widget>[SaveBtnWrapper()],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 1),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,

                      children: <Widget>[
                        Padding(
                          padding:
                              EdgeInsets.only(left: 23, right: 23, top: 23),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //Title
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "${DateTime.parse(widget.args.imageLocationList[0].stdate).year}年${DateTime.parse(widget.args.imageLocationList[0].stdate).month}月",
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              //Page
                              Container(
                                // padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${imageList.length}${uiData_Text['taxDetailPage']['numPageText'].toString()}",
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: updateDocPageTextStyle,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                            child: Container(
                                height:
                                    MediaQuery.of(context).size.height * .52,
                                padding: EdgeInsets.only(top: 12),
                                child: PageView.builder(
                                    controller: _pageController,
                                    scrollDirection: Axis.horizontal,
                                    physics: BouncingScrollPhysics(),
                                    itemCount: imageList.length,
                                    itemBuilder: (context, index) {
                                      return imageList[index];
                                    })
                                /*SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              child: Wrap(children: imageList),
                            )
                            */
                                )),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 38, right: 38, top: 46),
                          child: NextBtnWrapper(
                            title: uiData_Text['bankStatementDetailPage']
                                    ['addBtn']
                                .toString(),
                            clickEvent: () {
                              if (dataState.isSentEmail == false) {
                                addPhoto();
                              }
                            },
                            controller: _nextBtnController,
                            //validList: validTxtField,
                            //dataList: txtFieldList,
                            //checkValid: checkValid
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 26, bottom: 46),
                          child: InkWell(
                              onTap: () {
                                if (dataState.isSentEmail == false) {
                                  gotoNextPage();
                                }
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 52,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    border: Border.all(
                                        color: config_submitBtnColour,
                                        width: 1)),
                                child: Center(
                                    child: Text(
                                        uiData_Text['bankStatementDetailPage']
                                            ['finishBtn'],
                                        style: customeThemeData.textTheme.button
                                            .copyWith(
                                                color:
                                                    config_submitBtnColour))),
                              )),
                        ),

                        //for (var i = 0; i < imageList.length; i++) imageList[i],
                      ],
                    ),
                  ))
            ]);
          },
        ),
      ),
    );
  }

  addPhoto() {
    open_camera(context, IMAGE_TYPE.bank_statement, widget.args.memberID,
        DateTime.parse(widget.args.imageLocationList[0].stdate));
  }

  gotoNextPage() {
    //Navigator.popUntil(context, ModalRoute.withName('/uploadDocList'));
    //Navigator.pop(context, true);

    List<Location> targetLocationList;
    UserType targetUserType;

    if (widget.args.memberID == -1) {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .incomeCopyLocation;
      targetUserType = UserType.Patient;
    } else {
      targetLocationList = StoreProvider.of<AppState>(context)
          .state
          .dataState
          .familyMemberIncome[widget.args.memberID]
          .incomeCopyLocation;
      targetUserType = UserType.FamilyMember;
    }

    if (widget.args.memberID == -1) {
      if (checkIncomePhotos(
          StoreProvider.of<AppState>(context).state.dataState)) {
        StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context));
      } else {
        StoreProvider.of<AppState>(context).dispatch(
            NavigateToUploadBankStatementPageAction(ScreenArguments(
                targetUserType, targetLocationList, widget.args.memberID)));
      }
    } else {
      if (checkFamilyPhotots(
          StoreProvider.of<AppState>(context).state.dataState,
          widget.args.memberID)) {
        StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context));
      } else {
        StoreProvider.of<AppState>(context).dispatch(
            NavigateToUploadBankStatementPageAction(ScreenArguments(
                targetUserType, targetLocationList, widget.args.memberID)));
      }
    }

/* 
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToUploadBankStatementPageAction()); */

    /*    Navigator.of(context).pushNamedAndRemoveUntil(
        '/uploadBankStatement', ModalRoute.withName('/uploadDocList')); */

    // Navigator.popUntil(context, ModalRoute.withName('/uploadBankStatement'));

/*  StoreProvider.of<AppState>(context)
        .dispatch(NavigateToBankStatementDetailPageAction(context));  */

    //Navigator.pop(context);
    /*   StoreProvider.of<AppState>(context)
        .dispatch(PopToUploadDocListPageAction()); */
  }

  _reflash(bool isPopBack) async {
    print("_reflashing");
    List<Location> targetList = List<Location>();
    imageList.clear();
    var formatter = DateFormat('yM');

    if (isPopBack) {
      if (widget.args.memberID == -1) {
/*         targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .incomeCopyLocation; */

        for (var location in StoreProvider.of<AppState>(context)
            .state
            .dataState
            .incomeCopyLocation) {
          if (formatter.format(displayingMonth) ==
              formatter.format(DateTime.parse(location.stdate))) {
            targetList.add(location);
          }
        }
      } else {
        /*       targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[widget.args.memberID]
            .incomeCopyLocation; */

        for (var location in StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[widget.args.memberID]
            .incomeCopyLocation) {
          if (formatter.format(displayingMonth) ==
              formatter.format(DateTime.parse(location.stdate))) {
            targetList.add(location);
          }
        }
      }
    } else {
      targetList = widget.args.imageLocationList;
      displayingMonth = DateTime.parse(widget.args.imageLocationList[0].stdate);
    }

    int imageList_count = targetList.length;

    print("familyMemberIncome length: " + targetList.length.toString());

    if (imageList_count == 0) {
      // no income image in this list

    } else {
      for (var location in targetList) {
        String path = location.path;

        print(" location " + path);

        await readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  setState(() {
                    List<Location> oneLocaionList = [location];

                    imageList.add(GestureDetector(
                      onTap: () {
                        print("click path " + path);

                        Navigator.of(context)
                            .push(
                              MaterialPageRoute(
                                  builder: (_) => IncomeDetailPage(
                                      args: ScreenArguments(
                                          widget.args.userType,
                                          oneLocaionList,
                                          widget.args.memberID))),
                            )
                            .then((val) => {
                                  if (val != null)
                                    {
                                      if (val) {_reflash(true)}
                                    }
                                });
                        //.then((val) => val ? _reflash(true):()=>{});
                      }, // handle your image tap here
                      child: Container(
                          padding:
                              EdgeInsets.only(top: 10, bottom: 10, right: 10),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * .52,
                          child: Stack(children: [
                              Align(
                                alignment: Alignment.center,
                                  child:
                                      Image.memory(value, fit: BoxFit.contain)),
                              Center(
                                  child: Container(
                                      width: 120,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          color: Colors.black54),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                        SvgPicture.asset(
                                            'assets/information.svg', width: 20),
                                        Text(uiData_Text['photoNotice'],
                                            style: photoNoticeStyle)
                                      ])))
                            ])),
                    ));
                  })
                }
            });
      }
    }
  }

  checkIncomePhotos(Data_State state) {
    //TODO for check empty
    switch (state.incomeCopyType) {
      case 'IMAGE_TYPE.income_tax':
      case 'IMAGE_TYPE.passbook':
        return state.incomeCopyLocation.isNotEmpty;
        /*state.familyMemberIncome
            .every((m) => m.incomeCopyLocation.isNotEmpty)*/
        break;
      case 'IMAGE_TYPE.bank_statement':
        DateTime now = DateTime.now();
        var formatter = DateFormat('yM');
        List<MonthBtnModel> monthBtnModel = List();
        for (var i = 0; i < 12; i++) {
          DateTime newDate = DateTime(
              now.year,
              now.month - 1 - i,
              now.day,
              now.hour,
              now.minute,
              now.second,
              now.millisecond,
              now.microsecond);
          monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
        }
        //assign location into the month location list
        for (var location in state.incomeCopyLocation) {
          print("locaiton uploadDate " +
              formatter.format(DateTime.parse(location.stdate)));

          for (var a_monthBtnModel in monthBtnModel) {
            if (formatter.format(a_monthBtnModel.btnDate) ==
                formatter.format(DateTime.parse(location.stdate))) {
              a_monthBtnModel.imageLocationList.add(location);
            }
          }
        }
        return monthBtnModel
            .every((month) => month.imageLocationList.isNotEmpty);
        break;
      default:
        return false;
    }
    ;
  }

  bool checkFamilyPhotots(Data_State state, int index) {
    print("index: ${index}");
    if (state.familyMemberIncome.isNotEmpty) {
      switch (state.familyMemberIncome[index].incomeCopyType) {
        case 'IMAGE_TYPE.income_tax':
        case 'IMAGE_TYPE.passbook':
          return state.familyMemberIncome[index].incomeCopyLocation.isNotEmpty;
          break;
        case 'IMAGE_TYPE.bank_statement':
          DateTime now = DateTime.now();
          var formatter = DateFormat('yM');
          List<MonthBtnModel> monthBtnModel = List();
          for (var i = 0; i < 12; i++) {
            DateTime newDate = DateTime(
                now.year,
                now.month - 1 - i,
                now.day,
                now.hour,
                now.minute,
                now.second,
                now.millisecond,
                now.microsecond);
            monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
          }
          //assign location into the month location list
          for (var location
              in state.familyMemberIncome[index].incomeCopyLocation) {
            print("locaiton uploadDate " +
                formatter.format(DateTime.parse(location.stdate)));

            for (var a_monthBtnModel in monthBtnModel) {
              if (formatter.format(a_monthBtnModel.btnDate) ==
                  formatter.format(DateTime.parse(location.stdate))) {
                a_monthBtnModel.imageLocationList.add(location);
              }
            }
          }
          return monthBtnModel
              .every((month) => month.imageLocationList.isNotEmpty);
          break;
        default:
          return false;
      }
    } else {
      return false;
    }
    //return monthBtnModel.every((member) => member.every((month) => month.imageLocationList.isNotEmpty));
  }
}
