import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

class MonthBtnModel {
  final DateTime btnDate;
  final List<Location> imageLocationList;

  MonthBtnModel(this.btnDate, this.imageLocationList);
}

//comment
class UploadBankStatementPage extends StatefulWidget {
  final ScreenArguments args;
  UploadBankStatementPage({Key key, this.args}) : super(key: key);

  @override
  _UploadBankStatementPageState createState() =>
      _UploadBankStatementPageState();
}

class _UploadBankStatementPageState extends State<UploadBankStatementPage> {
  int current_percentage;
  List<Widget> buttonList = List<Widget>();
  List<MonthBtnModel> monthBtnModel = List<MonthBtnModel>();
  NextBtnController _nextBtnController;

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }
  SingletonShareData data;

  @override
  void initState() {
    super.initState();
    data = SingletonShareData();
    current_percentage = 80;
    _nextBtnController = NextBtnController(dataList: []);
    print("UploadBankStatementPage");
    print("member id " + widget.args.memberID.toString());
    print("listing all income image");

    //create 12 month list
    var now = DateTime.now();
    var formatter = DateFormat('yM');
    String formatted = formatter.format(now);
    print("formatted " + formatted);
    //TODO for check empty
    for (var i = 0; i < 12; i++) {
      DateTime newDate = DateTime(
        now.year,
        now.month - 1 - i,
      );
      monthBtnModel.add(MonthBtnModel(newDate, List<Location>()));
    }

    print(" date debug");
    //debug
    for (var del in monthBtnModel) {
      print(formatter.format(del.btnDate));
    }

    //assign location into the month location list
    for (var location in widget.args.imageLocationList) {
      print("locaiton uploadDate " +
          formatter.format(DateTime.parse(location.stdate)));

      for (var a_monthBtnModel in monthBtnModel) {
        if (formatter.format(a_monthBtnModel.btnDate) ==
            formatter.format(DateTime.parse(location.stdate))) {
          print("same month " + formatter.format(a_monthBtnModel.btnDate));
          a_monthBtnModel.imageLocationList.add(location);
        }
      }
    }

    print("after assign to list");
    for (var a_monthBtnModel in monthBtnModel) {
      print("a_monthBtnModel ");
      print(a_monthBtnModel.imageLocationList.length);
    }

    //onclick. if month location list is empty, open camera else pass location list next widget

    var newHour = 5;
    var time = DateTime.now();
    time = DateTime(1992, time.month, time.day, newHour, time.minute,
        time.second, time.millisecond, time.microsecond);

    for (var i = 0; i < monthBtnModel.length; i++) {
      //String path = location.path;

      //print(" location " + path);

      setState(() {
        buttonList.add(
          RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0)),
            onPressed: () => {goToThisMonth(context, i)},
            padding: const EdgeInsets.all(20),
            child: Text(
              i.toString(),
              style: submitBtnTextStyle,
            ),
          ),
        );
      });
    }

    /*    Future.delayed(Duration.zero, () {
      // listDirectory();

      int imageList_count = widget.args.imageLocationList.length;

      print("familyMemberIncome length: " +
          widget.args.imageLocationList.length.toString());

      if (imageList_count == 0) {
        // no income image in this list

      } else {
        for (var location in widget.args.imageLocationList) {
          String path = location.path;

          print(" location " + path);

          setState(() {
            buttonList.add(
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40.0)),
                onPressed: () => {addPhoto()},
                padding: const EdgeInsets.all(20),
                child: Text(
                  path.toString(),
                  style: submitBtnTextStyle,
                ),
              ),
            );
          });
        }
      }
    }); */
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () {
              StoreProvider.of<AppState>(context)
                  .dispatch(PopToUploadDocListPageAction(context));
            },
          ),
          actions: <Widget>[
            SaveBtnWrapper()
          ],
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 1),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 23, right: 23), //, top: 23
                            child: Column(children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 33, right: 110),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['UploadBankStatementPage']
                                          ['title']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  softWrap: true,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: monthBtnModel.length,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: EdgeInsets.only(top: 12),
                                      child: Container(
                                          //padding: EdgeInsets.only(top: 12),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 70,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              boxShadow: [config_boxshadow]),
                                          child: InkWell(
                                              onTap: () {
                                                goToThisMonth(context, index);
                                              },
                                              child: Container(
                                                  padding: EdgeInsets.only(
                                                      left: 23, right: 23),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        "${monthBtnModel[index].btnDate.year}年${monthBtnModel[index].btnDate.month}月",
                                                        style: customeThemeData
                                                            .textTheme.caption
                                                            .copyWith(
                                                                fontFamily:
                                                                    'Rawline'),
                                                        softWrap: true,
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                                      monthBtnModel[index]
                                                              .imageLocationList
                                                              .isNotEmpty
                                                          ? DecoratedBox(
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius.circular(
                                                                          40.0),
                                                                  color:
                                                                      config_primaryColour),
                                                              child: Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              4),
                                                                  child: Icon(
                                                                      Icons
                                                                          .check,
                                                                      size:
                                                                          16.0,
                                                                      color: Colors
                                                                          .white)))
                                                          : Icon(
                                                              Icons.arrow_forward_ios,
                                                              size: 13.0,
                                                              color: config_primaryColour)
                                                    ],
                                                  )))),
                                      /*Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10.0),
                                          
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(40.0)),
                                        onPressed: () =>
                                            {goToThisMonth(context, index)},
                                        padding: const EdgeInsets.all(20),
                                        child: Text(
                                          "${monthBtnModel[index].btnDate.year}年${monthBtnModel[index].btnDate.month}月",
                                          style: submitBtnTextStyle,
                                        ),
                                      )*/
                                    );
                                  }),
                              /*for (var i = 0; i < buttonList.length; i++)
                              buttonList[i],
                              */
                              FlatButton(
                                onPressed: () async {
                                  if(StoreProvider.of<AppState>(context).state.dataState.isSentEmail){
                                    // do nothing
                                  }else{
                                    await openDialog(context);
                                  }
                                },
                                child: Row(
                                  children: <Widget>[
                                      data.imageIcon(imagePath: "assets/images/refresh.svg")
                                    ,Padding(
                                      padding: EdgeInsets.only(left: SizeConfig().leftPadding),
                                      child: Text(
                                        uiData_Text['UploadBankStatementPage']['deleteAllBtn'].toString(),
                                        style: saveBtntextStyle.copyWith(color: config_progressBarColour),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 50))
                            ])),
                      ],
                    ),
                  )),
              /*      Positioned(
                  left: 42,
                  right: 42,
                  bottom: 86,
                  child: NextBtnWrapper(
                    title: uiData_Text['UploadBankStatementPage']['nextBtn']
                        .toString(),
                    clickEvent: gotoNextPage,
                    controller: _nextBtnController,
                  )) */
            ]);
          },
        ),
      ),
    );
  }

/*   addPhoto() {
    print("addPhoto");

    if (widget.args.memberID == -1) {
      StoreProvider.of<AppState>(context).dispatch(
          NavigateToBankStatementDetailPageAction(ScreenArguments(
              widget.args.userType,
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .incomeCopyLocation,
              widget.args.memberID)));
    } else {
      
      StoreProvider.of<AppState>(context).dispatch(
          NavigateToBankStatementDetailPageAction(ScreenArguments(
              widget.args.userType,
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .familyMemberIncome[widget.args.memberID]
                  .incomeCopyLocation,
              widget.args.memberID)));
    }
  } */

  goToThisMonth(BuildContext _context, int monthBtnModeCell) {
    if (monthBtnModel[monthBtnModeCell].imageLocationList.isNotEmpty) {
      StoreProvider.of<AppState>(_context).dispatch(
          NavigateToBankStatementDetailPageAction(ScreenArguments(
              widget.args.userType,
              monthBtnModel[monthBtnModeCell].imageLocationList,
              widget.args.memberID)));
    } else {
      print(
          'monthBtnModel[monthBtnModeCell].btnDate: ${monthBtnModel[monthBtnModeCell].btnDate}');
      open_camera(context, IMAGE_TYPE.bank_statement, widget.args.memberID,
          monthBtnModel[monthBtnModeCell].btnDate);
    }
  }

  gotoNextPage() {
    /*    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToUploadDocListPageAction()); */
  }

  Future<void> openDialog(BuildContext _context) async {
  return showDialog<void>(
    context: _context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {

      return CustomAlertDialog(title: uiData_Text['UploadBankStatementPage']['popupbox']['title'].toString(),
          content: uiData_Text['UploadBankStatementPage']['popupbox']['content'].toString(),
          leftButtonText: uiData_Text['UploadBankStatementPage']['popupbox']['cancel'].toString(),
          rightButtonText: uiData_Text['UploadBankStatementPage']['popupbox']['ok'].toString(),
          buildContext: context,
          rightButtonFunction: () {
            StoreProvider.of<AppState>(context).dispatch(
                Remove_All_IncomeImage_Action(widget.args.memberID));

            StoreProvider.of<AppState>(context).dispatch(
                Change_IncomeCopyType_Action(null, widget.args.memberID));

            StoreProvider.of<AppState>(context)
                .dispatch(SaveAppDataToLocalAction());

            StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context));
          });

      CupertinoAlertDialog(
        title: Text(
          uiData_Text['UploadBankStatementPage']['popupbox']['title'].toString(),
          style: formPageTitleStyle,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                uiData_Text['UploadBankStatementPage']['popupbox']['content'].toString(),
                style: formPageInstructStyle.copyWith(fontFamily: 'Rawline'),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(uiData_Text['UploadBankStatementPage']['popupbox']['cancel'].toString(),
                style: stepperTxtStyle),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
           FlatButton(
            child: Text(uiData_Text['UploadBankStatementPage']['popupbox']['ok'].toString(),
                style: stepperTxtStyle),
            onPressed: () {
              StoreProvider.of<AppState>(context).dispatch(
                    Remove_All_IncomeImage_Action(widget.args.memberID));

                StoreProvider.of<AppState>(context).dispatch(
                    Change_IncomeCopyType_Action(null, widget.args.memberID));

                StoreProvider.of<AppState>(context)
                    .dispatch(SaveAppDataToLocalAction());

                StoreProvider.of<AppState>(context)
                    .dispatch(PopToUploadDocListPageAction(context));
            },
          ),
        ],
      );
    },
  );
}
}
