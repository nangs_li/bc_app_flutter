import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/MultipleCheckbox.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

//comment
class BankDataTypePage extends StatefulWidget {
  final ScreenArguments args;
  BankDataTypePage({Key key, this.args}) : super(key: key);

  @override
  _BankDataTypePageState createState() => _BankDataTypePageState();
}

class _BankDataTypePageState extends State<BankDataTypePage> {
  int current_percentage;
  UserType cur_processWhoIncome;
  String r_title;
  String r_nameLabel;
  String r_subtitle;
  String r_uploadTaxSheetText;
  String r_uploadBankSheet;
  String selectCase;
  NextBtnController _nextBtnController;

  @override
  void initState() {
    super.initState();
    current_percentage = 20;
    print("BankDataTypePage");
    print("member id " + widget.args.memberID.toString());

    // cur_processWhoIncome = UserType.FamilyMember;
    selectCase = '';
    _nextBtnController = NextBtnController(dataList: []);

    if (widget.args.userType == UserType.Patient) {
      r_title = uiData_Text['bankDataTypePage']['patient']['title'].toString();
      r_subtitle =
          uiData_Text['bankDataTypePage']['patient']['subtitle'].toString();
      r_uploadTaxSheetText = uiData_Text['bankDataTypePage']['patient']
              ['radioGroup'][0]
          .toString();
      r_uploadBankSheet = uiData_Text['bankDataTypePage']['patient']
              ['radioGroup'][1]
          .toString();
    } else {
      r_title =
          uiData_Text['bankDataTypePage']['familyMem']['title'].toString();
      r_nameLabel =
          uiData_Text['bankDataTypePage']['familyMem']['nameLabel'].toString();
      r_subtitle =
          uiData_Text['bankDataTypePage']['familyMem']['subtitle'].toString();
      r_uploadTaxSheetText = uiData_Text['bankDataTypePage']['familyMem']
              ['radioGroup'][0]
          .toString();
      r_uploadBankSheet = uiData_Text['bankDataTypePage']['familyMem']
              ['radioGroup'][1]
          .toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return Scaffold(
      appBar: AppBar(
        /*      title: Text('申請程序'), */
        brightness: Brightness.dark,
        titleSpacing: 0.0,
        elevation: 0.0,
        leading: IconButton(
          iconSize: 30.0,
          icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[SaveBtnWrapper()],
      ),
      body: StoreConnector<AppState, Data_State>(
        converter: (Store<AppState> store) => store.state.dataState,
        builder: (BuildContext context, Data_State dataState) {
          return Stack(children: <Widget>[
            Container(decoration: BoxDecoration(color: config_normalBGColour)),
            PrograssBar(percentage: current_percentage, step: 1),
            Padding(
                padding: EdgeInsets.only(top: 20),
                child: SingleChildScrollView(
                  //padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding:
                              EdgeInsets.only(left: 23, right: 23, top: 33), 
                          child: Column(
                            children: <Widget>[
                              //Title
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  r_title,
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              //Subtitle
                              Container(
                                padding: EdgeInsets.only(top: 12, bottom: 12),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  r_subtitle,
                                  textAlign: TextAlign.left,
                                  //overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  style: customeThemeData.textTheme.bodyText1,
                                ),
                              ),
                              if (widget.args.userType == UserType.FamilyMember)
                                memberName(dataState, widget.args.memberID),
                              
                              //Checkbox
                              MultipleCheckBox(
                                isEnable: dataState.isSentEmail==false,
                                  value: selectCase,
                                  radioGroup: [
                                    r_uploadTaxSheetText,
                                    r_uploadBankSheet
                                  ],
                                  index: 0,
                                  isNotSpecial: false,
                                  check: (data) {
                                    print('data: ${data}');
                                    setState(() {
                                      selectCase = data;
                                    });
                                  }),
                              MultipleCheckBox(
                                isEnable: dataState.isSentEmail==false,
                                  value: selectCase,
                                  radioGroup: [
                                    r_uploadTaxSheetText,
                                    r_uploadBankSheet
                                  ],
                                  index: 1,
                                  isNotSpecial: false,
                                  check: (data) {
                                    print('data: ${data}');
                                    setState(() {
                                      selectCase = data;
                                    });
                                  }),
                              /*Padding(
                                padding: EdgeInsets.only(
                                    left: 15, right: 15, top: 46, bottom: 46),
                                child: NextBtnWrapper(
                                  title: uiData_Text['general']['nextPageBtn']
                                      .toString(),
                                  clickEvent: () {
                                    if (selectCase == r_uploadTaxSheetText) {
                                      gotoUploadBankStatementPage();
                                    } else if (selectCase ==
                                        r_uploadBankSheet) {
                                      gotoUploadBankBookPage();
                                    }
                                  },
                                  controller: _nextBtnController,
                                  //validList: validTxtField,
                                  //dataList: txtFieldList,
                                  //checkValid: checkValid
                                ),
                              ),*/
                            ],
                          )),
                    ],
                  ),
                )),
            Positioned(
              left: 41,
              right: 41,
              bottom: 87,
              child: NextBtnWrapper(
                title: uiData_Text['general']['nextPageBtn'].toString(),
                clickEvent: () {
                  if (selectCase == r_uploadTaxSheetText) {
                    ga.gaEvent(name:"psp_bank_statement_button");
                    gotoUploadBankStatementPage();
                  } else if (selectCase == r_uploadBankSheet) {
                    ga.gaEvent(name:"psp_bank_passbook_button");
                    gotoUploadBankBookPage();
                  }
                },
                controller: _nextBtnController,
                //validList: validTxtField,
                //dataList: txtFieldList,
                //checkValid: checkValid
              ),
            ),
          ]);
        },
      ),
    );
  }

  Widget memberName(Data_State state, int memberId) {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      alignment: Alignment.centerLeft,
      child: Text(
        r_nameLabel + " - " + state.familyMemberIncome[memberId].memNam,
        textAlign: TextAlign.left,
        overflow: TextOverflow.ellipsis,
        style: homePageSupportPlanTxtStyle.copyWith(
            color: Color.fromARGB(255, 20, 20, 20)),
      ),
    );
  }

  gotoNextPage() {
/*     StoreProvider.of<AppState>(context)
        .dispatch(NavigateToPubOrPriHospitalPageAction()); */
  }

  gotoUploadBankStatementPage() {
    /*   StoreProvider.of<AppState>(context)
        .dispatch(NavigateToUploadBankStatementPageAction()); */

    print("gotoUploadBankStatementPage");

// -1 = user
    if (widget.args.memberID == -1) {
      StoreProvider.of<AppState>(context).dispatch(
          NavigateToUploadBankStatementPageAction(ScreenArguments(
              widget.args.userType,
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .incomeCopyLocation,
              widget.args.memberID)));
    } else {
      StoreProvider.of<AppState>(context).dispatch(
          NavigateToUploadBankStatementPageAction(ScreenArguments(
              widget.args.userType,
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .familyMemberIncome[widget.args.memberID]
                  .incomeCopyLocation,
              widget.args.memberID)));
    }
  }

  gotoUploadBankBookPage() {
    print("gotoUploadBankBookPage");
    //TODO go to photo taking. Take and pass to input page
/*     StoreProvider.of<AppState>(context)
        .dispatch(NavigateToBankBookInputPageAction()); */

    open_camera(
        context, IMAGE_TYPE.passbook, widget.args.memberID, null);
  }
}
