import 'package:common_utils/common_utils.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/datetime/DateTimelineInfo.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/data/dataPageAction.dart';
import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

//comment
class MedicalReportPage extends StatefulWidget {
  @override
  _MedicalReportPageState createState() => _MedicalReportPageState();
}

class _MedicalReportPageState extends State<MedicalReportPage> {
  int current_percentage;
  List<Image> imageList = List<Image>();

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    current_percentage = 65;
    setLocaleInfo('zh_hk', ZHTimelineInfo());
    setLocaleInfo('en_hk', ENTimelineInfo());
    print("_MedicalReportPageState Page");

    Future.delayed(Duration.zero, () {
      // listDirectory();

      for (var location in StoreProvider.of<AppState>(context)
          .state
          .dataState
          .medicalReportLocation) {
        String path = location.path;

        print(" location " + path);

        readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  //  print("read image no null " + value.toString()),
                  setState(() {
                    imageList.add(Image.memory(value));
                  })
                }
            });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () {
              //Navigator.of(context).pop()
              StoreProvider.of<AppState>(context)
                  .dispatch(PopToUploadDocListPageAction(context));
            },
          ),
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 1),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        for (var i = 0; i < imageList.length; i++)
                          Container(
                              color: Colors.white,
                              padding: EdgeInsets.only(top: 23),
                              width: MediaQuery.of(context).size.width,
                              height: 308 *
                                  (MediaQuery.of(context).size.height / 896),
                              child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: imageList[i])),
                        Padding(
                            padding:
                                EdgeInsets.only(left: 23, right: 23, top: 23),
                            child: Column(children: <Widget>[
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['medicalReportPage']
                                          ['documentTypeLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: placeholderStyle,
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['medicalReportPage']
                                          ['documentType']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: updateDocInfoTextStyle,
                                  softWrap: true,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 10),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['medicalReportPage']
                                          ['uploadDateLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: placeholderStyle,
                                ),
                              ),
                              if (dataState.medicalReportLocation.isNotEmpty)
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    getAMPMStr(
                                        TimelineUtil.format(
                                            DateTime.parse(dataState
                                                    .medicalReportLocation[0]
                                                    .uploadDate)
                                                .millisecondsSinceEpoch,
                                            locale: 'zh_hk',
                                            dayFormat: DayFormat.Full),
                                        'zh_hk'),
                                    //dateFormat.format(DateTime.parse(dataState.hkidLocation[0].uploadDate)),
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.ellipsis,
                                    style: updateDocInfoDateStyle,
                                  ),
                                ),
                            ]))
                      ],
                    ),
                  )),
              Positioned(
                  bottom: 0,
                  width: MediaQuery.of(context).size.width,
                  height: 110,
                  child: InkWell(
                      onTap: () {
                        if (dataState.isSentEmail == false) {
                          deleteIdImage();
                        }
                      },
                      child: Container(
                          color: config_primaryColour.withAlpha(25),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.delete,
                                  color: config_nextPageBtnColour, size: 27),
                              Text(
                                  uiData_Text['medicalReportPage']['deleteBtn']
                                      .toString(),
                                  style: removeBtnTextStyle)
                            ],
                          ))))
            ]);
          },
        ),
      ),
    );
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToDrugStatus1PageAction());
  }

  deleteIdImage() {
    setState(() {
      imageList.clear();
    });

    for (var i in StoreProvider.of<AppState>(context)
        .state
        .dataState
        .medicalReportLocation) {
      cleanLocalFile(i.path).then((value) => {
            StoreProvider.of<AppState>(context).dispatch(
                Remove_Location_Action(i, IMAGE_TYPE.medicalreport, -1)),
            StoreProvider.of<AppState>(context)
                .dispatch(SaveAppDataToLocalAction()),
            open_camera(context, IMAGE_TYPE.medicalreport, -1, null, true)
          });
    }
  }
}
