import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/pages/step2/incomeDetailPage/IncomeDetailPage.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import '../../../app/theme.dart';
import '../../../uti/IO/app_io.dart';

//comment
class TaxDetailPage extends StatefulWidget {
/*   final UserType userType;
  final List<Location> imageLocationList;
  final int memberID; */
  final ScreenArguments args;
//final ScreenArguments args = ModalRoute.of(context).settings.arguments;
  TaxDetailPage({Key key, this.args}) : super(key: key);

  @override
  _TaxDetailPageState createState() => _TaxDetailPageState();
}

class _TaxDetailPageState extends State<TaxDetailPage> {
  int current_percentage;
  int pageno;
  ScrollController _scrollController;
  PageController _pageController;
  NextBtnController _nextBtnController;

  List<GestureDetector> imageList = List<GestureDetector>();
  // final TaxDetailPage args = ModalRoute.of(context).settings.arguments;

  Future<bool> _onWillPop() async {
    return StoreProvider.of<AppState>(context)
            .dispatch(PopToUploadDocListPageAction(context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    print("args " + widget.args.memberID.toString());

    print("TaxDetailPage ");
    print("Usertype " + widget.args.userType.toString());
    print("memberID " + widget.args.memberID.toString());

    current_percentage = 80;
    pageno = 0;
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _pageController = PageController(viewportFraction: 0.6);
    _nextBtnController = NextBtnController(dataList: []);

    Future.delayed(Duration.zero, () {
      // listDirectory();
      _reflash(false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          /*      title: Text('申請程序'), */
          brightness: Brightness.dark,
          titleSpacing: 0.0,
          elevation: 0.0,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () {
              //Navigator.of(context).pop()
              StoreProvider.of<AppState>(context)
                  .dispatch(PopToUploadDocListPageAction(context));
            },
          ),
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              PrograssBar(percentage: current_percentage, step: 1),
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    //padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      // crossAxisAlignment: CrossAxisAlignment.stretch,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding:
                              EdgeInsets.only(left: 23, right: 23, top: 23),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //Title
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  uiData_Text['taxDetailPage']
                                          ['documentTypeLabel']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: formPageTitleStyle,
                                ),
                              ),
                              //Page
                              Container(
                                padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${imageList.length}${uiData_Text['taxDetailPage']['numPageText'].toString()}",
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: updateDocPageTextStyle,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                            child: Container(
                                height:
                                    MediaQuery.of(context).size.height * .52,
                                padding: EdgeInsets.only(top: 12),
                                child: PageView.builder(
                                    controller: _pageController,
                                    scrollDirection: Axis.horizontal,
                                    physics: BouncingScrollPhysics(),
                                    itemCount: imageList.length,
                                    itemBuilder: (context, index) {
                                      return imageList[index];
                                    })
                                /*SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              child: Wrap(children: imageList),
                            )
                            */
                                )),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 38, right: 38, top: 46),
                          child: NextBtnWrapper(
                            title: uiData_Text['taxDetailPage']['addBtn']
                                .toString(),
                            clickEvent: () {
                              if (dataState.isSentEmail == false) {
                                addPhoto();
                              }
                            },
                            controller: _nextBtnController,
                            //validList: validTxtField,
                            //dataList: txtFieldList,
                            //checkValid: checkValid
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 26, bottom: 46),
                          child: InkWell(
                              onTap: () {
                                if (dataState.isSentEmail == false) {
                                  gotoNextPage();
                                }
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 52,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    border: Border.all(
                                        color: config_submitBtnColour,
                                        width: 1)),
                                child: Center(
                                    child: Text(
                                        uiData_Text['taxDetailPage']
                                            ['finishBtn'],
                                        style: customeThemeData.textTheme.button
                                            .copyWith(
                                                color:
                                                    config_submitBtnColour))),
                              )),
                        ),
                        // for (var i = 0; i < imageList.length; i++) imageList[i],
                      ],
                    ),
                  ))
            ]);
          },
        ),
      ),
    );
  }

  addPhoto() {
    /*  StoreProvider.of<AppState>(context).dispatch(SaveAppDataToLocalAction()); */
    open_camera(context, IMAGE_TYPE.income_tax, widget.args.memberID, null);
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(PopToUploadDocListPageAction(context));

/*     Navigator.popUntil(context, ModalRoute.withName('/uploadDocList')); */
    /*    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToUploadDocListPageAction()); */
  }

  _reflash(bool isPopBack) async {
    print("_reflashing");
    List<Location> targetList;
    imageList.clear();

    if (isPopBack) {
      if (widget.args.memberID == -1) {
        targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .incomeCopyLocation;
      } else {
        targetList = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .familyMemberIncome[widget.args.memberID]
            .incomeCopyLocation;
      }
    } else {
      targetList = widget.args.imageLocationList;
    }

    int imageList_count = targetList.length;

    print("familyMemberIncome length: " + targetList.length.toString());

    if (imageList_count == 0) {
      // no income image in this list

    } else {
      for (var location in targetList) {
        String path = location.path;

        print(" location " + path);

        await readImageByteFromLocal(path).then((value) => {
              if (value != null)
                {
                  setState(() {
                    List<Location> oneLocaionList = [location];

                    imageList.add(GestureDetector(
                      onTap: () {
                        print("click path " + path);
                        /*      StoreProvider.of<AppState>(context).dispatch(
              NavigateToIncomeDetailPageAction(ScreenArguments(
                  widget.args.userType, oneLocaionList , widget.args.memberID)));  */

                        Navigator.of(context)
                            .push(
                              MaterialPageRoute(
                                  builder: (_) => IncomeDetailPage(
                                      args: ScreenArguments(
                                          widget.args.userType,
                                          oneLocaionList,
                                          widget.args.memberID))),
                            )
                            .then((val) => {
                                  if (val != null)
                                    {
                                      if (val) {_reflash(true)}
                                    }
                                });
                        //.then((val) => val ? _reflash(true):()=>{});
                      }, // handle your image tap here
                      child: Container(
                          padding:
                              EdgeInsets.only(top: 10, bottom: 10, right: 10),
                          width: MediaQuery.of(context).size.width * .52,
                          height: MediaQuery.of(context).size.height * .52,
                          child: Stack(children: [
                            Align(
                              alignment: Alignment.center,
                                child:
                                    Image.memory(value, fit: BoxFit.contain)),
                            Center(
                                child: Container(
                                    width: 120,
                                      height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(40),
                                        color: Colors.black54),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(
                                              'assets/information.svg',
                                              width: 20),
                                          Text(uiData_Text['photoNotice'],
                                              style: photoNoticeStyle)
                                        ])))
                          ])),
                    ));
                  })
                }
            });
      }
    }
  }

  _scrollListener() {
    setState(() {
      pageno = (_scrollController.offset / MediaQuery.of(context).size.width)
          .round();
    });
  }
}
