import 'dart:typed_data';

import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/controller/CameraCtr.dart';
import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:icanfight/psp/models/SaveBtn.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/CheckBoxGroup.dart';
import 'package:icanfight/psp/view/MultipleCheckbox.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:icanfight/psp/view/PrograssBar.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../app/theme.dart';

class IncomePage extends StatefulWidget {
/*   final UserType userType;

  IncomePage({Key key, this.userType}) : super(key: key); */

  final ScreenArguments args;
  IncomePage({Key key, this.args}) : super(key: key);

  @override
  _IncomePageState createState() => _IncomePageState();
}

class _IncomePageState extends State<IncomePage> {
  int current_percentage;
  String selectCase;
  NextBtnController _nextBtnController;
  // UserType cur_processWhoIncome;
  String r_title;
  String r_nameLabel;
  String r_subtitle;
  String r_uploadTaxSheetText;
  String r_uploadBankSheet;
  List<ByteData> byteDate = List<ByteData>();

  @override
  void initState() {
    super.initState();
    print("IncomePage");
    print("member id" + widget.args.memberID.toString());
    byteDate.add(null);

    current_percentage = 30;
    selectCase = '';
    // cur_processWhoIncome = UserType.FamilyMember;

    if (widget.args.userType == UserType.Patient) {
      r_title = uiData_Text['incomePage']['patient']['title'].toString();
      r_subtitle = uiData_Text['incomePage']['patient']['subtitle'].toString();
      r_uploadTaxSheetText =
          uiData_Text['incomePage']['patient']['radioGroup'][0].toString();
      r_uploadBankSheet =
          uiData_Text['incomePage']['patient']['radioGroup'][1].toString();
    } else {
      r_title = uiData_Text['incomePage']['familyMem']['title'].toString();
      r_nameLabel =
          uiData_Text['incomePage']['familyMem']['nameLabel'].toString();
      r_subtitle =
          uiData_Text['incomePage']['familyMem']['subtitle'].toString();
      r_uploadTaxSheetText =
          uiData_Text['incomePage']['familyMem']['radioGroup'][0].toString();
      r_uploadBankSheet =
          uiData_Text['incomePage']['familyMem']['radioGroup'][1].toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;

    return Scaffold(
      appBar: AppBar(
        /*      title: Text('申請程序'), */
        brightness: Brightness.dark,
        titleSpacing: 0.0,
        elevation: 0.0,
        leading: IconButton(
          iconSize: 30.0,
          icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
          onPressed: () {
            //Navigator.of(context).pop()
            StoreProvider.of<AppState>(context)
                .dispatch(PopToUploadDocListPageAction(context));
          },
        ),
      ),
      body: StoreConnector<AppState, Data_State>(
        converter: (Store<AppState> store) => store.state.dataState,
        builder: (BuildContext context, Data_State dataState) {
          _nextBtnController = NextBtnController(dataList: [selectCase]);
          return Stack(children: <Widget>[
            Container(decoration: BoxDecoration(color: config_normalBGColour)),
            PrograssBar(percentage: current_percentage, step: 1),
            Padding(
                padding: EdgeInsets.only(top: 20, left: 23, right: 23),
                child: SingleChildScrollView(
                  //padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      //Title
                      Container(
                        padding: EdgeInsets.only(top: 33),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          r_title,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: formPageTitleStyle,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 23),
                          child: Text(
                            r_subtitle,
                            textAlign: TextAlign.left,
                            style: formPageInstructStyle,
                          )),

                      if (widget.args.userType == UserType.FamilyMember)
                        memberName(
                            dataState.familyMemberIncome, widget.args.memberID),
                      MultipleCheckBox(
                          value: selectCase,
                          radioGroup: [r_uploadTaxSheetText, r_uploadBankSheet],
                          index: 0,
                          isNotSpecial: false,
                          check: (data) {
                            print('data: ${data}');
                            setState(() {
                              selectCase = data;
                            });
                          }),
                      MultipleCheckBox(
                          value: selectCase,
                          radioGroup: [r_uploadTaxSheetText, r_uploadBankSheet],
                          index: 1,
                          isNotSpecial: false,
                          specialTxtEndIndex: 4,
                          specialTxtStartIndex: 0,
                          check: (data) {
                            print('data: ${data}');
                            setState(() {
                              selectCase = data;
                            });
                          }),

                      /*Padding(
                        padding: EdgeInsets.only(
                            left: 15, right: 15, top: 46, bottom: 46),
                        child: NextBtnWrapper(
                          title:
                              uiData_Text['general']['nextPageBtn'].toString(),
                          clickEvent: () {
                            if (selectCase == r_uploadTaxSheetText) {
                              open_camera(context, IMAGE_TYPE.income_tax,
                                  widget.args.memberID, null);
                            } else if (selectCase == r_uploadBankSheet) {
                              gotoUploadBankDoc();
                            }
                          },
                          controller: _nextBtnController,
                          //validList: validTxtField,
                          //dataList: txtFieldList,
                          //checkValid: checkValid
                        ),
                      ),*/
                    ],
                  ),
                )),
            Positioned(
              left: 41,
              right: 41,
              bottom: 87,
              child: NextBtnWrapper(
                title: uiData_Text['general']['nextPageBtn'].toString(),
                clickEvent: () {
                  if(dataState.isSentEmail==false){
                    if (selectCase == r_uploadTaxSheetText) {
                      ga.gaEvent(name: "psp_income_tax_button");
                      open_camera(context, IMAGE_TYPE.income_tax,
                          widget.args.memberID, null);
                    } else if (selectCase == r_uploadBankSheet) {
                      gotoUploadBankDoc();
                    }
                  }
                },
                controller: _nextBtnController,
                //validList: validTxtField,
                //dataList: txtFieldList,
                //checkValid: checkValid
              ),
            ),
          ]);
        },
      ),
    );
  }

  Widget memberName(List<FamilyMemberIncome> member, int mId) {
    return Container(
      padding: EdgeInsets.only(top: 26,bottom: 16),
      alignment: Alignment.centerLeft,
      child: Text(
        r_nameLabel + " - " + member[mId].memNam,
        textAlign: TextAlign.left,
        overflow: TextOverflow.ellipsis,
        style: homePageSupportPlanTxtStyle.copyWith(
            color: Color.fromARGB(255, 20, 20, 20)),
      ),
    );
  }

  gotoNextPage() {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToPubOrPriHospitalPageAction());
  }

/* //TODO: open camera / gallary
  gotoUploadTaxSheet() {
    // debug ------------V
    int debug_User = StoreProvider.of<AppState>(context)
        .state
        .dataState
        .familyMemberIncome
        .length;

    if (debug_User > 0) {
      print("family member bigger than 0");
    } else {
      print("no family member");
      StoreProvider.of<AppState>(context)
          .dispatch(Add_FamilyMemberIncome(FamilyMemberIncome.initial()));
    }
    // debug ------------^

    //entry point of after photo taking
    saveIncomeImageDoc(
        context, IMAGE_TYPE.income_tax, byteDate, widget.args.memberID);
  } */

  gotoUploadBankDoc() {
    print("gotoUploadBankDoc: ${widget.args.memberID}");
    StoreProvider.of<AppState>(context).dispatch(
        NavigateToBankDataTypePageAction(
            ScreenArguments(widget.args.userType, null, widget.args.memberID)));
  }
}
