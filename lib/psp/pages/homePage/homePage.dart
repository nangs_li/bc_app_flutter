import 'dart:io';
import 'package:archive/archive_io.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/models/PspHomePageData.dart';
import 'package:icanfight/psp/view/clients/waveBar.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
//Original
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';

import 'package:icanfight/psp/app/data/dataState.dart';

import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../utilities/style.dart';
import '../../app/config.dart';
import '../../app/theme.dart';
import '../../app/theme.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<File> files;
  List<Widget> list = List();

  @override
  void initState() {
    super.initState();
    //listDirectory();
    ga.gaSetCurrentScreen(screenName: "support_page");
    data.pspHomePageDataCallBack.listen((pspHomePageData) {
      setState(() {
        data.pspHomePageData = data.pspHomePageData;
      });
    });

  }

  bool checkEmpty(){
    return uiData_Text == null || uiData_Text.length == 0;
  }

  @override
  Widget build(BuildContext context) {
    //Dismiss KB
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBar(
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0,
              automaticallyImplyLeading: false,
              title: Padding(
                  padding: EdgeInsets.only(left: 24),
                  child: Text(
                    checkEmpty() ? "" : uiData_Text['homePage']['title'].toString(),
                    style: titleTxtStyle,
                    // textAlign: TextAlign.left,
                  )),
              backgroundColor: config_primaryColour,
              centerTitle: false),
          body: StoreConnector<AppState, Data_State>(
            converter: (Store<AppState> store) => store.state.dataState,
            builder: (BuildContext context, Data_State dataState) {
              return Stack(children: <Widget>[
                Container(
                    decoration: BoxDecoration(color: config_normalBGColour)),
                SingleChildScrollView(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 24),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: EdgeInsets.only(top: 42),
                              child: Text(
                                checkEmpty() ? "" : uiData_Text['homePage']['subtitleA'].toString(),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: sectionTitle,
                              ))),
                      Padding(
                          padding: EdgeInsets.only(top: 4.5),
                          child: Divider(thickness: 1, height: 1, color:Color(0xffFB729C))),
                      Padding(
                        padding: EdgeInsets.only(top: 13.5), //13.5
                        child: InkWell(
                            onTap: () {
                              print("home page");
                              ga.gaEvent(name: "psp_thumbnail");
                              this.gotoNextPage();
                            },
                            child: Container(
                                constraints: BoxConstraints.expand(height: 198),
                                child: Stack(
                                  children: <Widget>[
                                    Center(
                                        child: Image.asset(
                                          'assets/images/post2.png',
                                          fit: BoxFit.fitWidth,
                                        )), //Cover
                                    Positioned.fill(
                                        bottom: 0,
                                        left: 19,
                                        right: 19,
                                        child: Align(
                                            alignment: Alignment.bottomCenter,
                                            child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.only(
                                                    left: 16,
                                                    right: 16,
                                                    top: 18,
                                                    bottom: 18),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    border: Border.all(
                                                        width: 1.0,
                                                        color:
                                                            config_borderColour),
                                                    boxShadow: [
                                                      config_boxshadow
                                                    ],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child: Text(
                                                    checkEmpty() ? "" : uiData_Text['homePage']
                                                            ['supportPlan']
                                                        .toString(),
                                                    style: homePageSupportPlanTxtStyle))))
                                  ],
                                ))),
                      ),
                     data.pspHomePageData != null ? pspHomePageUI() : Center(child: CircularProgressIndicator()),

                      /*  Padding(
                      padding: EdgeInsets.only(top: 45),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          uiData_Text['homePage']['subtitleB'].toString(),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: subtitleTxtStyle,
                        ),
                      ),
                    ), */
                      /*  Padding(
                        padding: EdgeInsets.only(top: 4.5),
                        child: Divider(thickness: 1, height: 1)),
                    
                    TextField(
                      onChanged: (text) => StoreProvider.of<AppState>(context)
                          .dispatch(TypingAction(text)),
                      decoration: InputDecoration(
                          labelText: dataState.nameEng.toString(),
                          hintText: 'Enter a search term1'),
                    ), */

                      /* RaisedButton(
                      child: Text('Clean userData data'),
                      onPressed: () => StoreProvider.of<AppState>(context)
                          .dispatch(cleanLocalFile('userData.json')),
                    ),
                    RaisedButton(
                      child: Text('Clean appDAta data'),
                      onPressed: () => StoreProvider.of<AppState>(context)
                          .dispatch(cleanLocalFile('appData.json')),
                    ),
                    RaisedButton(
                      child: Text('Clean uidata'),
                      onPressed: () => StoreProvider.of<AppState>(context)
                          .dispatch(cleanLocalFile('uiData.json')),
                    ), */
                      /* //Zip Functions
                      Padding(
                          padding: EdgeInsets.only(top: 50, bottom: 50),
                          child: Divider(thickness: 1, height: 1)),
                      RaisedButton(
                        child: Text('Save Zip'),
                        onPressed: () => _saveZip(),
                      ),
                      RaisedButton(
                        child: Text('Load Zip'),
                        onPressed: () => _openZipFileExplorer(),
                      ),
                      //Display Zip Files
                      Padding(
                          padding: EdgeInsets.all(8),
                          child: files != null
                              ? Column(
                                  children: list,
                                )
                              : Text('No file is loaded')),
                      RaisedButton(
                        child: Text('Clean Data'),
                        onPressed: () => StoreProvider.of<AppState>(context)
                            .dispatch(
                                cleanAllDate(context, null).then((value) => {
                                      print("delete all file"),
                                    })),
                      ), */
                    ],
                  ),
                ),
                WaveBar()
              ]);
            },
          ),
        ));
  }

  gotoNextPage() {
    print("gotoNextPage");

    print("gotoNextPage lastSavedPage " +
        StoreProvider.of<AppState>(context).state.dataState.lastSavedPage);

/*     StoreProvider.of<AppState>(context)
        .dispatch(NavigateToAboutAction(context));
 */

    if (StoreProvider.of<AppState>(context).state.dataState.isSentEmail) {
      StoreProvider.of<AppState>(context)
          .dispatch(NavigateToAboutAction(context));
      //Navigator.pushReplacementNamed(context, '/home');
    } else {
      switch (
          StoreProvider.of<AppState>(context).state.dataState.lastSavedPage) {
        case "PageName.None":
          break;
        case "PageName.PersonalDataPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToPersonalDataPageAction(context));
          break;
        case "PageName.PersonalBankPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToPersonalBankPageAction());
          break;
        case "PageName.FamilyIncomePage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToFamilyIncomePageAction());
          break;
        case "PageName.FundedInfoPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToFundedInfoPageAction());
          break;
        case "PageName.PubOrPriHospital":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToPubOrPriHospitalPageAction());
          break;
        case "PageName.DrugStatus1Page":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToDrugStatus1PageAction());
          break;
        case "PageName.DrugStatus2Page":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToDrugStatus2PageAction());
          break;
        case "PageName.DrugStatus3Page":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToDrugStatus3PageAction());
          break;
        case "PageName.WhereDrugInfoPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToWhereDrugInfoPageAction());
          break;

        case "PageName.Step2LandingPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToStep2LandingPageAction());
          break;

        case "PageName.Step3LandingPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToStep3LandingPageAction());
          break;
        case "PageName.UploadDocListPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToUploadDocListPageAction());
          break;

        case "PageName.AgreePage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToAgreePageAction());
          break;
        case "PageName.FinishPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToAboutAction(context));
          break;
        case "PageName.StatementPage":
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToStatementPageAction());
          break;
        default:
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToAboutAction(context));
          break;
      }
    }
  }

  Future<void> _saveZip() async {
    try {
      var encoder = ZipFileEncoder();
      // Manually create a zip of a directory and individual files.
      Directory appDocDirectory = await getApplicationDocumentsDirectory();
      encoder.create(appDocDirectory.path + '/psp_app.zip');
      //Read Files
      List<String> filePaths = List();
      //hkid
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .hkidLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .hkidLocation[i]
            .path;

        //print("id location " + path);
        filePaths.add("${appDocDirectory.path}/$path");
        encoder.addFile(File("${appDocDirectory.path}/$path"));
      }
      //invoice
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .invoiceLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .invoiceLocation[i]
            .path;

        //print("id location " + path);
        //files.add(File("${fullPath.path}/$path"));
        filePaths.add("${appDocDirectory.path}/$path");
        encoder.addFile(File("${appDocDirectory.path}/$path"));
      }
      //ref letter
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .refLetterLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .refLetterLocation[i]
            .path;

        //print("id location " + path);
        filePaths.add("${appDocDirectory.path}/$path");
        encoder.addFile(File("${appDocDirectory.path}/$path"));
      }
      //medical
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .medicalReportLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .medicalReportLocation[i]
            .path;

        //print("id location " + path);
        filePaths.add("${appDocDirectory.path}/$path");
        encoder.addFile(File("${appDocDirectory.path}/$path"));
      }
      //income copy
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .incomeCopyLocation
                  .length;
          i++) {
        String path = StoreProvider.of<AppState>(context)
            .state
            .dataState
            .incomeCopyLocation[i]
            .path;

        filePaths.add("${appDocDirectory.path}/$path");
        encoder.addFile(File("${appDocDirectory.path}/$path"));
      }
      //family income
      for (int i = 0;
          i <
              StoreProvider.of<AppState>(context)
                  .state
                  .dataState
                  .familyMemberIncome
                  .length;
          i++) {
        for (int j = 0;
            j <
                StoreProvider.of<AppState>(context)
                    .state
                    .dataState
                    .familyMemberIncome[i]
                    .incomeCopyLocation
                    .length;
            j++) {
          String path = StoreProvider.of<AppState>(context)
              .state
              .dataState
              .familyMemberIncome[i]
              .incomeCopyLocation[j]
              .path;

          filePaths.add("${appDocDirectory.path}/$path");
          encoder.addFile(File("${appDocDirectory.path}/$path"));
        }
      }
      encoder.addFile(File("${appDocDirectory.path}/$appData_localPath"));
      //Start Zip
      encoder.close();
      await File(appDocDirectory.path + '/psp_app.zip')
          .readAsBytes()
          .then((byteData) async => {
                await Share.file('psp_app.zip', 'psp_app.zip',
                    byteData.buffer.asUint8List(), 'application/zip')
              });
    } catch (e) {
      print('error: $e');
    }
  }

//  void _openZipFileExplorer() async {
//    File zip;
//    try {
//      zip =
//          await FilePicker.getFile(type: FileType.CUSTOM, fileExtension: 'zip');
//      final bytes = zip.readAsBytesSync();
//      final archive = ZipDecoder().decodeBytes(bytes);
//      // files.clear();
//      files = [];
//      // Extract the contents of the Zip archive to disk.
//      for (final file in archive) {
//        final filename = file.name;
//        final data = file.content as List<int>;
//        print('A: ${filename}');
//        //await File(join(appDocDirectory.path,filename)).writeAsBytes(data);
//        files.add(await writeToFile(data, filename));
//        print('B: ${filename}');
//      }
//      print('D');
//    } on PlatformException catch (e) {
//      print("Unsupported operation" + e.toString());
//    }
//    if (!mounted) return;
//    print('files: $files');
//    list.clear();
//    // Display to Widget
//    files.forEach((f) {
//      print('f.path: ${f.path}');
//      switch (f.path.split('/').last.split('.').last) {
//        case 'png':
//        case 'jpg':
//        case 'jpeg':
//          {
//            list.add(Image.file(f));
//          }
//          break;
//        case 'csv':
//        case 'json':
//        case 'txt':
//          {
//            //Remark: As it needs time to  load text file from device, it needs to refresh again if add to normal List<Widget>
//            //Or you may use Future List
//            getTextFromFile(f).then((str) {
//              StoreProvider.of<AppState>(context)
//                  .dispatch(UpdateAppDataAction(Data_State.fromJson(str)));
//              setState(() {
//                list.add(Text(str));
//              });
//            });
//            //list.add(Text());
//            //TODO overwrite the app_data.json
//
//          }
//          break;
//        default:
//          {
//            //print('none');
//            list.add(Image.file(f));
//          }
//          break;
//      }
//    });
//    setState(() {});
//  }

  Future<String> getTextFromFile(File f) async {
    return await f.readAsString();
  }

  Future<File> writeToFile(List<int> data, String filename) async {
    Directory dir = await getApplicationDocumentsDirectory();
    String tempPath = dir.path;
    var filePath =
        tempPath + '/' + filename; // file_01.tmp is dump file, can be anything
    return File(filePath).writeAsBytes(data);
  }

  Column pspHomePageUI (){
    List<Widget> widgetList = List<Widget>();
    for(PageData pageData in data.pspHomePageData.data){
      if(pageData.group != null){
        widgetList.add(Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: EdgeInsets.only(top: 42),
                child: Text(
                  pageData.group,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  style: sectionTitle,
                ))
        ));
        widgetList.add(Padding(
          padding: EdgeInsets.only(top: 4.5),
          child: Divider(thickness: 1, height: 1, color:Color(0xffFB729C)),
        ));
      }
      widgetList.add(ContextRow(pageData: pageData));
    }
    
    return Column(children: widgetList);
  }
}


class ContextRow extends StatelessWidget {
  ContextRow({this.pageData });
  final PageData pageData;
  @override
  Widget build(BuildContext context) {

      return Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: const Color(0xfffb729c),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                  child: Text(pageData.districtLocation,style: TextStyle(fontFamily: 'Raleway',fontSize: 14,color: const Color(0xffffebec),),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            (pageData.image!= null) ? Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(pageData.name, style: ngoTitle),
                        Text(pageData.nameDetail, style: ngoNotes),
                        Padding(
                            padding: EdgeInsets.only(top: 20,bottom: 20, right: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                (pageData.location!= null) ? Text(pageData.location, style: ngoDetails) : Container(),
                                (pageData.phone!= null) ?Text(pageData.phone, style: ngoDetails): Container(),
                                (pageData.email!= null) ?Text(pageData.email, style: ngoDetails): Container(),
                                (pageData.website!= null) ?Text(pageData.website, style: ngoDetails): Container(),
                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                  flex: 3,
                ),
                Expanded(
                  child: Container(

                      child: CachedNetworkImage(imageUrl: pageData.image,fit: BoxFit.cover,
//                          imageBuilder: (context, imageProvider) => Container(
//                              decoration: BoxDecoration(
//                                image: DecorationImage(
//                                  image: imageProvider,
//                                  fit: BoxFit.contain,
//                                ),
//                              )),
                          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) => Icon(Icons.error))
                  ),
                  flex: 1,
                ),
              ],
            ) : Container(),
            (pageData.image!= null) ? Container() : Text(pageData.name, style: ngoTitle),
            (pageData.image!= null) ? Container() : Text(pageData.nameDetail, style: ngoNotes),
            (pageData.image!= null) ? Container() : Padding(
                padding: EdgeInsets.only(top: 20,bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    (pageData.location!= null) ? Text(pageData.location, style: ngoDetails) : Container(),
                    (pageData.phone!= null) ?Text(pageData.phone, style: ngoDetails): Container(),
                    (pageData.email!= null) ?Text(pageData.email, style: ngoDetails): Container(),
                    (pageData.website!= null) ?Text(pageData.website, style: ngoDetails): Container(),
                  ],
                )
            ),
            Divider(height:0.5, thickness: 1, color:Color(0xffB3B3BE)),
          ],
        ),
      );
  }
}

