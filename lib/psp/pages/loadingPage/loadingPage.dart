import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/userState.dart';

import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'loadingPageAction.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
  //store.dispatch(IOPrefenceAction("state.json"));
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context).dispatch(StartInitAppAction(context));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(''),
        ),
        body: StoreConnector<AppState, User_State>(
          converter: (Store<AppState> store) => store.state.userState,
          builder: (BuildContext context, User_State userState) {
            return Center(
              child: CircularProgressIndicator(),
              /*
                  Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    onChanged: (text) => StoreProvider.of<AppState>(context)
                        .dispatch(TypingAction(text)),
                    decoration: InputDecoration(
                        labelText: dataState.nameEng.toString(),
                        hintText: 'Enter a search term1'),
                  ),
                  RaisedButton(
                    child: Text('Change page'),
                    onPressed: () => StoreProvider.of<AppState>(context)
                        .dispatch(NavigateToLoginPageAction()),
                  ),

                  RaisedButton(
                    child: Text('Clean userData data'),
                    onPressed: () => StoreProvider.of<AppState>(context)
                        .dispatch(cleanLocalFile('userData.json')),
                  ),
                  RaisedButton(
                    child: Text('Clean appDAta data'),
                    onPressed: () => StoreProvider.of<AppState>(context)
                        .dispatch(cleanLocalFile('appData.json')),
                  ),
                  RaisedButton(
                    child: Text('Clean uidata'),
                    onPressed: () => StoreProvider.of<AppState>(context)
                        .dispatch(cleanLocalFile('uiData.json')),
                  ),

                ],
              ),*/
            );
          },
        ));
  }
}
