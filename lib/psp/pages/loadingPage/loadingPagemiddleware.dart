import 'dart:convert';

import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';

import 'package:icanfight/psp/app/data/dataState.dart';

import 'package:icanfight/psp/app/userState.dart';
import 'package:icanfight/psp/models/Page.dart';
import 'package:icanfight/psp/pages/homePage/homePage.dart';
import 'package:icanfight/psp/routing/routingAction.dart';

import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import '../../routing/routingAction.dart';

import 'loadingPageAction.dart';

class LoadingPageMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    //LoadUserDataFromStorageAction
    if (action is Pingme) {
      print('Pingme');
    }

    if (action is StartInitAppAction) {
      print('start LoadingPageMiddleware');

      readLocalFile(userData_localPath).then((result) {
        if (result == '') {
          print("read storage result:  result is null");
          //create default userjson
          print("create default userState json " +
              store.state.userState.toJson().toString());
          writeLocalFile(
              userData_localPath, json.encode(store.state.userState.toJson()));

          next(UpdateUserStateAction(store.state.userState));

          //save default userjson to local storage
        } else {
          print("read storage result: " + result);

          Map json3 = json.decode(result);

          print("read storage result: Map " + json3.toString());

          next(UpdateUserStateAction(User_State.fromJson(json3)));

          //return User_State.fromJson(json3);
        }

        print("isNeedConnectServer " + isNeedConnectServer.toString());

        //config uiData.json
        if (isNeedConnectServer) {
          iofetchGetString(uiData_api).then((data) {
            Map json4 = jsonDecode(data)['content'];

            print(json4);

            writeLocalFile(uiData_localPath, json.encode(json4)).then((a) {
              uiData_Text = json4;
              configUserData(store, action, next);
            });
          });
        } else {
          print("not connect server  123");

          readLocalFile(uiData_localPath).then((result) {
//            if (result == '') {
              print("load uiData.json from assets file");
              _loadAStudentAsset().then((result) {
                writeLocalFile(uiData_localPath, result).then((a) {
                  print("write uiData.json to local storage");
                  Map jsonResult = json.decode(result);

                  uiData_Text = jsonResult;
                  configUserData(store, action, next);
                });
              });
//            } else {
//              print("read uiData.json from local storage");
//
//              Map json4 = json.decode(result);
//              print(json4);
//              //print(json4.toString());
//              uiData_Text = json4;
//              configUserData(store, action, next);
//
//              /*    _loadAStudentAsset().then((result) {
//                writeLocalFile(uiData_localPath, result).then((a) {
//                  print("write uiData.json to local storage");
//                  Map jsonResult = json.decode(result);
//
//                  uiData_Text = jsonResult;
//                  configUserData(store, action, next);
//                });
//              }); */
//            }
          });
        }

        print("isNeedLogin ");

        next(action);
      });
    }

    next(action);
  }
}

gotoNextPage(NextDispatcher next, BuildContext context) {
  print("333333 gotoNextPage " +
      StoreProvider.of<AppState>(context).state.dataState.lastSavedPage);

  print("StoreProvider.of<AppState>(context).state.dataState.isSentEmail " +
      StoreProvider.of<AppState>(context)
          .state
          .dataState
          .isSentEmail
          .toString());
  if (StoreProvider.of<AppState>(context).state.dataState.isSentEmail) {
    next(NavigateToHomePageAction());
    //Navigator.pushReplacementNamed(context, '/home');
  } else {
    switch (StoreProvider.of<AppState>(context).state.dataState.lastSavedPage) {
      case "PageName.None":
        break;
      case "PageName.PersonalDataPage":
        next(NavigateToPersonalDataPageAction(context));
        break;
      case "PageName.PersonalBankPage":
        next(NavigateToPersonalBankPageAction());
        break;
      case "PageName.FamilyIncomePage":
        next(NavigateToFamilyIncomePageAction());
        break;
      case "PageName.FundedInfoPage":
        next(NavigateToFundedInfoPageAction());
        break;
      case "PageName.PubOrPriHospital":
        next(NavigateToPubOrPriHospitalPageAction());
        break;
      case "PageName.DrugStatus1Page":
        next(NavigateToDrugStatus1PageAction());
        break;
      case "PageName.DrugStatus2Page":
        next(NavigateToDrugStatus2PageAction());
        break;
      case "PageName.DrugStatus3Page":
        next(NavigateToDrugStatus3PageAction());
        break;
      case "PageName.WhereDrugInfoPage":
        next(NavigateToWhereDrugInfoPageAction());
        break;

      case "PageName.Step2LandingPage":
        next(NavigateToStep2LandingPageAction());
        break;

      case "PageName.Step3LandingPage":
        next(NavigateToStep3LandingPageAction());
        break;
      case "PageName.UploadDocListPage":
        next(NavigateToUploadDocListPageAction());
        break;

      case "PageName.AgreePage":
        next(NavigateToAgreePageAction());
        break;
      case "PageName.FinishPage":
        next(NavigateToHomePageAction());
        break;
      case "PageName.StatementPage":
        next(NavigateToStatementPageAction());
        break;
      default:
        next(NavigateToHomePageAction());
        //Navigator.pushReplacementNamed(context, '/home');
        break;
    }
  }

  //next(NavigateToStep2LandingPageAction());
  //next(NavigateToStep3LandingPageAction());
  //next(NavigateToPersonalDataPageAction(context));

  //// next(NavigateToPersonalBankPageAction());

  //next(NavigateToHomePageAction());
  //next(NavigateToPubOrPriHospitalPageAction());

  //next(NavigateToFamilyIncomePageAction());

//next(NavigateToFundedInfoPageAction());

//next(NavigateToIdDetailPageAction());
}

Future<String> _loadAStudentAsset() async {
  return await rootBundle.loadString('assets/uiData.json');
}

Future<String> _loadBankListAsset() async {
  return await rootBundle.loadString('assets/uiData.json');
}

configUserData(Store<AppState> store, dynamic action, NextDispatcher next) {
  //config userState.dart
  if (isNeedConnectServer) {
    iofetchGetString(userData_api).then((data) {
      Map json4 = jsonDecode(data)['content'];
      print('11111');

      print(json4);

      writeLocalFile(appData_localPath, json.encode(json4)).then((a) {
        next(UpdateAppDataAction(Data_State.fromMap(json4)));
        gotoNextPage(next, action.context);
      });
    });
  } else {
    readLocalFile(appData_localPath).then((result) {
      printWrapped("readLocalFile " + result);

//      if (result == '') {
        writeLocalFile(appData_localPath, (store.state.dataState.toJson()))
            .then((a) {
          next(UpdateAppDataAction(store.state.dataState));
          gotoNextPage(next, action.context);
        });
//      } else {
//        //    Map json4 = json.decode(result);
//        next(UpdateAppDataAction(Data_State.fromJson(result)));
//        gotoNextPage(next, action.context);
//      }
    });
  }
}
