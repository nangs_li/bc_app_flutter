import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/app/userState.dart';
import 'package:redux/redux.dart';
import 'loadingPageAction.dart';

final userReducer = combineReducers<User_State>([
  TypedReducer<User_State, UpdateUserStateAction>(
      _updateUserStateReducer),
  TypedReducer<User_State, StartInitAppAction>(
      _startInitAppReducer),
]);

User_State _startInitAppReducer(
    User_State state, StartInitAppAction action)
{
 return state;
}

User_State _updateUserStateReducer(
    User_State state, UpdateUserStateAction action) {
        print('start _updateUserStateActionReducer');

  return action.state;
}

