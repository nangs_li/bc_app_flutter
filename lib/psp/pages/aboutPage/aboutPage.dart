import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';

import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';

import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/clients/waveBar.dart';

import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_svg/parser.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  bool isDisplayDropDownA;
  bool isDisplayDropDownB;
  bool isDisplayDropDownC;
  bool isSubmitted;
  NextBtnController _nextBtnController;


  Future<bool> _onWillPop() async {
    print("_onWillPop");
    return StoreProvider.of<AppState>(context)
            .dispatch(NavigateToHomePageAction()) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    isDisplayDropDownA = true;
    isDisplayDropDownB = true;
    isDisplayDropDownC = true;
    isSubmitted = false;

    Future.delayed(Duration.zero, () {
      isSubmitted =
          StoreProvider.of<AppState>(context).state.dataState.isSentEmail;

      if(data.isAlreadySubmitPSPForm && !isSubmitted){
        ga.gaSetCurrentScreen(screenName:"psp_review_not_allow_page");
      } else if(isSubmitted) {
        ga.gaSetCurrentScreen(screenName:"psp_review_page");
      } else if(!isSubmitted){
        ga.gaSetCurrentScreen(screenName:"psp_introduction_page");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //Dismiss KB
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    //Next Controller



    return WillPopScope(
      onWillPop: _onWillPop, // () async => false,
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0),
            child: AppBar(
              brightness: Brightness.dark,
              titleSpacing: 0.0,
              elevation: 0.0,
              leading: IconButton(
                iconSize: 30.0,
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
                onPressed: () => {
                  StoreProvider.of<AppState>(context)
                      .dispatch(NavigateToHomePageAction())
                },
              ),
              flexibleSpace: Padding(
                  padding: EdgeInsets.only(
                      left: 24, top: Device.get().isIphoneX ? 104 : 84),
                  child: Text(
                    uiData_Text['AboutPage']['title'].toString(),
                    style: titleTxtStyle,
                    // textAlign: TextAlign.left,
                  )),
              backgroundColor: config_primaryColour,
              centerTitle: false,
              automaticallyImplyLeading: true,
            )
            /*IconButton(
                  iconSize: 25.0,
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 25.0),
                onPressed: () => Navigator.of(context).pop(),
              ) */
            ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            // print('dataState.isSentEmail: ${dataState.isSentEmail}');
            _nextBtnController = NextBtnController(
                dataList: dataState.isSentEmail ? ['true'] : ['true'],
                validTypeList: [ValidType.None]);
            return Stack(children: <Widget>[
              Container(
                  decoration: BoxDecoration(color: config_normalBGColour)),
              SingleChildScrollView(
                //padding: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 26, right: 26, top: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(top: 31, right: 19),
                        child: Text(
                          uiData_Text['AboutPage']['inductionText'].toString(),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.visible,
                          style: aboutPageBodyStyle,
                        )),
                    Padding(
                      padding: EdgeInsets.only(top: 41),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['AboutPage']['subtitleA'].toString(),
                            // textAlign: TextAlign.left,
                            // overflow: TextOverflow.ellipsis,
                            style: aboutPageSubtitleStyle,
                          )),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 15.5),
                        child: Divider(
                            thickness: 1,
                            height: 1,
                            color: config_bodyTextColour)),
                    if (isDisplayDropDownA) Dropdown_A_Widget(),
                    Padding(
                      padding: EdgeInsets.only(top: 67),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['AboutPage']['subtitleB'].toString(),
                            // textAlign: TextAlign.left,
                            // overflow: TextOverflow.ellipsis,
                            style: aboutPageSubtitleStyle,
                          )),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 15.5),
                        child: Divider(
                            thickness: 1,
                            height: 1,
                            color: config_bodyTextColour)),
                    if (isDisplayDropDownB) Dropdown_B_Widget(),
                    Padding(
                      padding: EdgeInsets.only(top: 52),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            uiData_Text['AboutPage']['subtitleC'].toString(),
                            // textAlign: TextAlign.left,
                            // overflow: TextOverflow.ellipsis,
                            style: aboutPageSubtitleStyle,
                          )),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 15.5),
                        child: Divider(
                            thickness: 1,
                            height: 1,
                            color: config_bodyTextColour)),
                    if (isDisplayDropDownC) Dropdown_C_Widget(),
                    Padding(
                        padding: rowPadding,
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                                uiData_Text['AboutPage']['info'].toString(),
                                style: aboutPageInfoStyle.copyWith(
                                    fontFamily: 'Rawline')))),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 15, right: 15, top: 71, bottom: 46),
                      child: data.isAlreadySubmitPSPForm && !isSubmitted ? Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              // color: config_nextPageBtnColour,
                              boxShadow: [config_disableBtnShadow]),
                          child: ButtonTheme(
                            minWidth: MediaQuery
                                .of(context)
                                .size
                                .width,
                            height: 56,
                            buttonColor: disableButtonColor,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0)),
                              onPressed: () {},
                              child: Text(
                                "你已提交申請",
                                style: submitBtnTextStyle.copyWith(color: disableColor),
                              ),
                            ),
                          )) : NextBtnWrapper(
                        title: isSubmitted
                            ? uiData_Text['finishPage']['title'].toString()
                            : uiData_Text['AboutPage']['startApplyBtn']
                            .toString(),
                        clickEvent: gotoNextPage, //TODO bug when isSubmitted
                        controller: _nextBtnController,
                        // dataList: [isSubmitted ? '' : 'true']
                      ),
                    ),
                  ],
                ),
              ),
              WaveBar()
            ]);
          },
        ),
      ),
      /*floatingActionButton: Container(
        padding: EdgeInsets.only(bottom: 87.0),
        //margin: EdgeInsets.all(50),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: FloatingActionButton.extended(
            backgroundColor: Colors.pink,
            onPressed: () => {gotoNextPage()},
            label: Text(
              uiData_Text['AboutPage']['startApplyBtn'].toString(),
              textAlign: TextAlign.center,
              overflow: TextOverflow.visible,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      */
    );
  }

  noneednext() {}
  gotoNextPage() {
    if(isSubmitted){
      ga.gaEvent(name: "psp_review_button");
    } else {
      ga.gaEvent(name: "psp_intro_start_application_button");
    }

    // if (!isSubmitted) {
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToFormPageAction(context));

    // }
  }

/* 
  drowdownOnClick(int dropdown) {
    switch (dropdown) {
      case 1:
        setState(() {
          isDisplayDropDownA = !isDisplayDropDownA;
          isDisplayDropDownB = false;
          isDisplayDropDownC = false;
        });

        break;
      case 2:
        setState(() {
          isDisplayDropDownA = false;
          isDisplayDropDownB = !isDisplayDropDownB;
          isDisplayDropDownC = false;
        });

        break;
      case 3:
        setState(() {
          isDisplayDropDownA = false;
          isDisplayDropDownB = false;
          isDisplayDropDownC = !isDisplayDropDownC;
        });

        break;
    }
  }
 */
  Widget Dropdown_A_Widget() {
    return Padding(
        padding: EdgeInsets.only(left: 1, right: 1),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 23.5), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        "assets/aboutPage/icon_1.svg",
                        // color: Colors.white,
                        width: 55,
                        height: 55,
                        fit: BoxFit.cover,
                        placeholderBuilder: (BuildContext context) => Container(
                            padding: const EdgeInsets.all(30.0),
                            child: const CircularProgressIndicator()),
                      ),
                      /*child: Image.asset('assets/about_cond_icon_1@3x.png',
                            width: 45, height: 45)*/
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextA'][0]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 13), //24.5
                  child:
                      Stack(alignment: Alignment.centerLeft, children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 1),
                      child: SvgPicture.asset(
                        "assets/aboutPage/icon_2.svg",
                        // color: Colors.white,
                        width: 55,
                        height: 55,
                        fit: BoxFit.fitHeight,
                        placeholderBuilder: (BuildContext context) => Container(
                            padding: const EdgeInsets.all(30.0),
                            child: const CircularProgressIndicator()),
                      ),
                      /*child: Image.asset('assets/about_cond_icon_2@3x.png',
                            width: 45, height: 45)*/
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextA'][1]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 25), //24.5
                  child:
                      Stack(alignment: Alignment.centerLeft, children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        "assets/aboutPage/icon_3.svg",
                        // color: Colors.white,
                        width: 55,
                        height: 55,
                        fit: BoxFit.fitHeight,
                        placeholderBuilder: (BuildContext context) => Container(
                            padding: const EdgeInsets.all(30.0),
                            child: const CircularProgressIndicator()),
                      ),
                      /*child: Image.asset('assets/about_cond_icon_3@3x.png',
                            width: 45, height: 45)*/
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextA'][2]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 19), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    Container(
                      child: SvgPicture.asset(
                        "assets/aboutPage/icon_4.svg",
                        // color: Colors.white,
                        width: 55,
                        height: 55,
                        fit: BoxFit.fitHeight,
                        placeholderBuilder: (BuildContext context) => Container(
                            padding: const EdgeInsets.all(30.0),
                            child: const CircularProgressIndicator()),
                      ),
                      /*child: Image.asset('assets/about_cond_icon_4@3x.png',
                            width: 45, height: 45)*/
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextA'][3]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
            ]));
  }

  Widget Dropdown_B_Widget() {
    return Padding(
        padding: EdgeInsets.only(left: 1, right: 1),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 14.5), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                  CircleWithText(text:"1" ,circleColor:config_primaryColour ,textStyle: aboutPageNumberStyle),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextB'][0]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: rowPadding, //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    CircleWithText(text:"2" ,circleColor:config_primaryColour ,textStyle: aboutPageNumberStyle),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextB'][1]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 9), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    CircleWithText(text:"3" ,circleColor:config_primaryColour ,textStyle: aboutPageNumberStyle),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextB'][2]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 30), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    CircleWithText(text:"4" ,circleColor:config_primaryColour ,textStyle: aboutPageNumberStyle),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextB'][3]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: EdgeInsets.only(top: 36), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    CircleWithText(text:"5" ,circleColor:config_primaryColour ,textStyle: aboutPageNumberStyle),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextB'][4]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
            ]));
  }

  Widget Dropdown_C_Widget() {
    return Padding(
        padding: EdgeInsets.only(left: 1, right: 1),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 14.5), //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    Positioned(
                        left: 17.5,
                        child: Text(
                          "-",
                          style: aboutPageMinStyle,
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextC'][0]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: rowPadding, //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    Positioned(
                        left: 17.5,
                        child: Text(
                          "-",
                          style: aboutPageMinStyle,
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextC'][1]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
              Padding(
                  padding: rowPadding, //24.5
                  child: Stack(alignment: Alignment.topLeft, children: <Widget>[
                    Positioned(
                        left: 17.5,
                        child: Text(
                          "-",
                          style: aboutPageMinStyle,
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        padding: EdgeInsets.only(left: 60, right: 13),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                            uiData_Text['AboutPage']['dropdownTextC'][2]
                                .toString(),
                            textAlign: TextAlign.left,
                            style: aboutPageSubbodyStyle))
                  ])),
            ]));
  }
}


class CircleWithText extends StatelessWidget {
  final String text;
  final Color circleColor;
  final TextStyle textStyle;

  CircleWithText({this.text, this.circleColor, this.textStyle});

  @override
  Widget build(BuildContext context) {
    return  Container(
        width: 34,
        height: 34,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: circleColor),
        child: Center(
            child: Text(
              text,
              style: textStyle,
              textAlign: TextAlign.center,
            )));
  }
}
