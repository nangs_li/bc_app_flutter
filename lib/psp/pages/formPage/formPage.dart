import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:icanfight/psp/view/NextBtnWrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../app/theme.dart';
import '../../view/CustomStepper.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  int _stepIndex;
  NextBtnController _nextBtnController;

  Future<bool> _onWillPop() async {
    print("_onWillPop");

    return StoreProvider.of<AppState>(context)
            .dispatch(PopFromRFormPageAction(this.context)) ??
        false;
  }

  @override
  void initState() {
    super.initState();
    ga.gaSetCurrentScreen(screenName: "psp_step1_page");
    _stepIndex = 2;
    _nextBtnController = NextBtnController(dataList: []);

    Future.delayed(Duration.zero, () {
      StoreProvider.of<AppState>(context)
          .dispatch(Change_lastSavedPage_Action("FormPage"));
    });
  }

  @override
  Widget build(BuildContext context) {
    //Dismiss KB
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          brightness: Brightness.dark,
          leading: IconButton(
            iconSize: 30.0,
            icon: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
            onPressed: () => {
              StoreProvider.of<AppState>(context)
                  .dispatch(PopFromRFormPageAction(context))
            },
          ),
          /*      title: Text('申請程序'), */
        ),
        body: StoreConnector<AppState, Data_State>(
          converter: (Store<AppState> store) => store.state.dataState,
          builder: (BuildContext context, Data_State dataState) {
            return Stack(children: <Widget>[
              Container(decoration: BoxDecoration(color: config_primaryColour)),
              Container(
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: SingleChildScrollView(
                    physics: MediaQuery.of(context).size.height <= 640
                        ? AlwaysScrollableScrollPhysics()
                        : NeverScrollableScrollPhysics(),
                    child: Padding(
                        padding: EdgeInsets.only(left: 25, right: 25),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 59),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                uiData_Text['formPage']['title'].toString(),
                                textAlign: TextAlign.left,
                                style: formPageTitleStyle,
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 23),
                                child: Text(
                                  uiData_Text['formPage']['inductionText']
                                      .toString(),
                                  textAlign: TextAlign.left,
                                  style: formPageInstructStyle,
                                )),
                            Padding(
                                padding: EdgeInsets.only(top: 0),
                                child: StepperWrapper(
                                  steps: [
                                    Step(
                                      state: StepState.indexed,
                                      isActive: true,
                                      title: Text(
                                          uiData_Text['formPage']['step1']
                                              .toString(),
                                          style: stepperTxtStyle),
                                      content: Container(),
                                    ),
                                    Step(
                                      isActive: false,
                                      title: Text(
                                          uiData_Text['formPage']['step2']
                                              .toString(),
                                          style: stepperTxtStyle),
                                      content: Container(),
                                    ),
                                    Step(
                                      isActive: false,
                                      title: Text(
                                          uiData_Text['formPage']['step3']
                                              .toString(),
                                          style: stepperTxtStyle),
                                      content: Container(),
                                    ),
                                  ],
                                  currentStep: _stepIndex,
                                  onStepTapped: (value) {
                                    print("step tapping: " + value.toString());
                                    if (StoreProvider.of<AppState>(context)
                                        .state
                                        .dataState
                                        .isSentEmail) {
                                      gotoStep(value);
                                    }
                                  },
                                  physics: NeverScrollableScrollPhysics(),
                                  controlsBuilder: (BuildContext context,
                                          {VoidCallback onStepContinue,
                                          VoidCallback onStepCancel}) =>
                                      Container(),
                                )),
                          ],
                        ))),
              ),
              displayApplyBtn()
            ]);
          },
        ),
      ),
    );
  }

  gotoNextPage() {
    ga.gaEvent(name: "psp_step1_start_application_button");
    StoreProvider.of<AppState>(context)
        .dispatch(NavigateToPersonalDataPageAction(context));
  }

  displayApplyBtn() {
    return Positioned(
        left: 41,
        right: 41,
        bottom: 87,
        child: NextBtnWrapper(
            title:
                StoreProvider.of<AppState>(context).state.dataState.isSentEmail
                    ? uiData_Text['general']['startReadBtn'].toString()
                    : uiData_Text['AboutPage']['startApplyBtn'].toString(),
            clickEvent: gotoNextPage,
            controller: _nextBtnController));
  }

  gotoStep(index) {
    print("gotoStep " + index.toString());
    switch (index) {
      case 0:
        StoreProvider.of<AppState>(context)
            .dispatch(NavigateToPersonalDataPageAction(context));

        break;

      case 1:
        StoreProvider.of<AppState>(context)
            .dispatch(NavigateToUploadDocListPageAction());
        break;

      case 2:
        StoreProvider.of<AppState>(context)
            .dispatch(NavigateToAgreePageAction());
        break;
    }
  }
}
