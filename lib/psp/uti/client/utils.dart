import 'package:icanfight/psp/view/clients/signupCheckBox.dart';
import 'package:flutter/material.dart';
import 'lang.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils {

  static setAccount(String email,String password,String accountName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setStringList("Account", [email,password,accountName]);
  }

  static Future<List<String>> getAccount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getStringList("Account");
  }

  static int mapColorFromType(String type) {
    switch (type) {
      case "1":
        return 0xFF7140BC;

      case "2":
        return 0xFFE2A93F;

      case "3":
        return 0xFF91BEC7;

      case "4":
        return 0xFFA9BD65;

      case "5":
        return 0xFF249FE0;
    }

    return 0xFFFFFFFF;
  }

  static bool isValidate(ValidationType type, String value) {

    debugPrint(value);

    switch (type) {

      case ValidationType.NONE:
        return true;

      case ValidationType.EMAIL:
        debugPrint(RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value).toString());
        return RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);

      case ValidationType.LOGIN_PASSWORD:
      // return (RegExp(".*[\w!@#\$%^&*(),.?\":{}|<>].*").hasMatch(value) && RegExp(".*[0-9].*").hasMatch(value) && RegExp("(?=.*[a-z])(?=.*[A-Z])").hasMatch(value) && value.length >= 8);
        print("0-9:${RegExp("(?=.*?[0-9])").hasMatch(value)}");
        print("a-z:${(RegExp("(?=.*[a-z])").hasMatch(value)|| RegExp("(?=.*[A-Z ])").hasMatch(value))}");
        print("value.length >= 6:${value.length >= 6}");
        return RegExp("(?=.*?[0-9])").hasMatch(value) && ((RegExp("(?=.*[a-z])").hasMatch(value)|| RegExp("(?=.*[A-Z ])").hasMatch(value)) && value.length >= 6);


      case ValidationType.PASSWORD:
        return (RegExp(".*[\w!@#\$%^&*(),.?\":{}|<>].*").hasMatch(value) && RegExp(".*[0-9].*").hasMatch(value) && RegExp("(?=.*[a-z])(?=.*[A-Z])").hasMatch(value) && value.length >= 8);

    }

    return true;


  }

  static Widget validator(ValidationType type, String value) {
    switch (type) {

      case ValidationType.NONE:
        return Container();

      case ValidationType.EMAIL:
        return Text(Lang.getString("profile_password_include"));

      case ValidationType.LOGIN_PASSWORD:
        return Container(
            margin: EdgeInsets.only(top: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(Icons.error, color: Color(0xFFFA356A)),
                Expanded(
                  child: Text(" "+Lang.getString("profile_password_include")+"\n- "+Lang.getString("profile_8_char")+"\n- "+ Lang.getString("profile_upper_lower_case") +"\n- "+ Lang.getString("profile_numberic") +"\n- "+Lang.getString("profile_special_char"),
                    style: TextStyle(
                      color: Color(0xFFFA356A),
                      fontSize: 13,
                      fontWeight: FontWeight.w600
                    ),
                  )
                )

              ],
            )
        );

      case ValidationType.PASSWORD:
        return Container(
          margin: EdgeInsets.only(top: 4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(Lang.getString("profile_password_include"), style: TextStyle(fontSize: 13, color: Color(0xFF707070), fontWeight: FontWeight.w600),),
              Row(
                children: <Widget>[
                  SignupCheckBox(value: (value.length >= 8), onChanged: (bool value) {}, checkColor: Color(0xFF7DA44C), backgroundColor: Color(0xFFF5F6FA),),
                  Expanded(child: Text(" "+Lang.getString("profile_8_char"), style: TextStyle(fontSize: 13, color: Color(0xFF707070), fontWeight: FontWeight.w600),)),
                ],
              ),
              Row(
                children: <Widget>[
                  SignupCheckBox(value: (RegExp("(?=.*[a-z])(?=.*[A-Z])").hasMatch(value)), checkColor: Color(0xFF7DA44C), backgroundColor: Color(0xFFF5F6FA), onChanged: (bool value) {

                  }),
                  Expanded(child: Text(" "+Lang.getString("profile_upper_lower_case"), style: TextStyle(fontSize: 13, color: Color(0xFF707070), fontWeight: FontWeight.w600),),)
                ],
              ),
              Row(
                children: <Widget>[
                  SignupCheckBox(value: RegExp(".*[0-9].*").hasMatch(value), checkColor: Color(0xFF7DA44C), backgroundColor: Color(0xFFF5F6FA), onChanged: (bool value) {}),
                  Expanded(child: Text(" "+Lang.getString("profile_numberic"), style: TextStyle(fontSize: 13, color: Color(0xFF707070), fontWeight: FontWeight.w600),)),
                ],
              ),
              Row(
                children: <Widget>[
                  SignupCheckBox(value: RegExp(".*[\w!@#\$%^&*(),.?\":{}|<>].*").hasMatch(value), checkColor: Color(0xFF7DA44C), backgroundColor: Color(0xFFF5F6FA), onChanged: (bool value) {}),
                  Expanded(child: Text(" "+Lang.getString("profile_special_char"), style: TextStyle(fontSize: 13, color: Color(0xFF707070), fontWeight: FontWeight.w600),)),
                ],
              ),
            ],
          ),
        );

    }

    return Text("");
  }

}

enum ValidationType {
  NONE, PASSWORD, LOGIN_PASSWORD, EMAIL,
}