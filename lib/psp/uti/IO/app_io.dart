import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:encrypt/encrypt_io.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/data/dataPageAction.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/routing/routingAction.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

import 'package:encrypt/encrypt.dart' as en;
import 'package:pointycastle/asymmetric/api.dart';

Future<String> iofetchGetString(String api) async {
  //http.Response
  final response = await http.get(api
      // 'Dynamic/Get?token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjRjNTIyMGFmODgwMzljNTNlN2IyNzAiLCJpYXQiOjE1NTk3ODY2MTF9.bceUONlTcDtgxALwJmySYj7gEFOrQyORLCHR7e5XL9JL1aJVgno9hhDmGjH5ThKH0OVtqPdOoWmX5hOSpxPMAe03BdCYzLBIRchMEx75ULbWAC-ZYpsfbJvEatWqhHIQa5YQwyuNfma8mIfFt0Ja-dm4aZo9Jn5mWMq2ij-zCs74diTrY0zEfW0iVsG24pudk_VmhYl72EUzTUulUx12v0HLwh4j05g27f2XvZEhCvbqeOK6RWv5XyUTdtu0v0qDvff5zdeYSTWwsNXQwXx5gp3iQsmYHHprgeHV_u3GTGfsbscAu4FK67H-Xx061qCsKspLxEyX3togzCTkbNbt6mahlWLyRWTnyK4oURYujwlxOBMT3_FpNVMEupVyWFWZFzFY_sD4a_nEeZbFfXYcgQ3BLh1o3WD8zWH1ZXeknDaBDl1ecWQrmmFXdzDWUVcbNIiHCl2SziZ24JOL1FGFT3zItg4qq7bzOE_pYPWXMncPM5Kt_GxHL-DeGcyhoaeyfEWGg5595BkL_g9drud9jpJct-R53KDaIl10Q2PJ2jRr3cf7fxK2FxED5Z4raDDtZHRihKeS5TOLwLnXOsndkI0r5MS5mik8D0sNKAQHn2M3xKJQ7wk77wRme_u5cKsUgCPbAs65zJXaBSAKXSzxlGAm7wqKn19-bVJCHsQHRrc&_id=5e4f507d2e34cd4b50fa879d'
      );

  print(response.body);
  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    //return IdeaData.fromJson(json.decode(response.body));

    return response.body;
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<void> cleanAllDate(BuildContext context, Data_State data_state) async {
  print("cleanAllDate:");
  /*   final path = await _localPath;
  

  
final dir = Directory(path);
    dir.deleteSync(recursive: true);

   */

  final systemTempDir = await getApplicationDocumentsDirectory();

  var lenght = 0;
  await systemTempDir
      .list(recursive: true, followLinks: false)
      .length
      .then((value) => lenght = value);
  int count = 0;

  print("lenght " + lenght.toString());
  print("count " + count.toString());
  // List directory contents, recursing into sub-directories,
  // but not following symbolic links.
  systemTempDir
      .list(recursive: true, followLinks: false)
      .listen((FileSystemEntity entity) {
    print(entity.path);

    if (File(entity.path).existsSync()) {
      entity.deleteSync(recursive: true);
      count++;

      if (count == lenght) {
//StoreProvider.of<AppState>(context).dispatch(NavigateToLoadingageAction());

        if (data_state != null) {
          StoreProvider.of<AppState>(context)
              .dispatch(UpdateAppDataAction(Data_State.initial()));
/*           StoreProvider.of<AppState>(context)
              .dispatch(Change_isSentEmail_Action(true));
          StoreProvider.of<AppState>(context)
              .dispatch(Change_isAgreeStatement_Action(true)); */
        } else {
          StoreProvider.of<AppState>(context)
              .dispatch(UpdateAppDataAction(Data_State.initial()));
        }

        StoreProvider.of<AppState>(context)
            .dispatch(SaveAppDataToLocalAction());

        if (data_state != null) {
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToFinishPageAction());
        } else {
          StoreProvider.of<AppState>(context)
              .dispatch(NavigateToLoadingageAction(context));
        }
      }
    } else {
      count++;
    }
  });

  return true;
}

Future<File> cleanLocalFile(String filename) async {
  print("writeFile: ${filename}");
  final file = await _localFile(filename);

  // Write the file
  // return file.writeAsString('');
  return file.existsSync() ? file.delete() : null;
}

Future<void> writeImageToLocalFile(String filename, ByteData _bytes) async {
  File file = await _localFile(filename);

  final path = await _localPath;
  final myDir = Directory(path + "/" + filename);

  await myDir.exists().then((isThere) {
    isThere
        ? print('exists ' + path + "/" + filename)
        : print('non-existent ' + path + "/" + filename);
  });

  ByteData bytes = _bytes;
  if (bytes == null) {
    bytes = await rootBundle.load('assets/idDemo.png');
    // bytes = await rootBundle.load('assets/homepage.png');
  }

  //ByteData bytes = await rootBundle.load(file.path);
  final buffer = bytes.buffer;

  return file.writeAsBytes(
      buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
}

Future<Uint8List> readImageByteFromLocal(String filename) async {
  //final File file = await _localFile(filename);

  File imageFile = await _localFile(filename);
  Uint8List bytes;
  await imageFile.readAsBytes().then((value) {
    bytes = Uint8List.fromList(value);
    print('reading of bytes is completed');
  }).catchError((onError) {
    print(
        'Exception Error while reading audio from path:' + onError.toString());
  });
  return bytes;
}

Future<File> writeLocalFile(String filename, String content) async {
  print("writeFile: ${filename}");
  final file = await _localFile(filename);

  // Write the file
  return file.writeAsString('$content');
}

Future<String> readLocalFile(String filename) async {
  print("readFile: ${filename}");
  try {
    final file = await _localFile(filename);

    // Read the file
    String contents = await file.readAsString();

    return contents;
  } catch (e) {
    // If encountering an error, return 0
    return '';
  }
}

void printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}

Future<File> _localFile(String filename) async {
  final path = await _localPath;
  print("_localFile: $path/$filename");
  return File('$path/$filename');
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

void listDirectory() async {
  print("--------------divider-------------");
  // Directory systemTempDir = await _localPath;
  final systemTempDir = await getApplicationDocumentsDirectory();

  // List directory contents, recursing into sub-directories,
  // but not following symbolic links.
  systemTempDir
      .list(recursive: true, followLinks: false)
      .listen((FileSystemEntity entity) {
    print(entity.path);
  });
}

dynamic dn_it() async {

  final parser = en.RSAKeyParser();
  final privateKey =
      parser.parse(await rootBundle.loadString('assets/private.pem'));
  
 

  final encrypter = en.Encrypter(en.RSA(privateKey: privateKey));
  
  final decrypted = encrypter.decrypt64(
      "f/QJegSU9JZKOvAlfT1EN664rw9FuPar1kb3LtFjae8m1yumi3KqEwzFaxDqu7uHUtW7LjWDCH9Qc6Gh1lF67EXPv4B+ugvh4ZCIozSxEFBuabqbYn4XGhQgB1mTloaJoe+fyGV3ciR5tfuPXPAm3cQmWzTRj7BnYceaqenpBhQ="); //encrypter.decrypt(encrypted);
  // TODO: string to json
  // print("decrypted: $decrypted");
  return json.decode(decrypted);
}
