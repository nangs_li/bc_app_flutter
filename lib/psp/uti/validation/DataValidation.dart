enum ValidType {
  Eng,
  Chi,
  Email,
  Phone,
  Hkid,
  Birth,
  None
}

RegExp engWordExp = RegExp(r"^.*[A-Za-z]+");
RegExp chiWordExp = RegExp(r"^.*[\p{Script=Han}]+", unicode: true);
RegExp emailExp = RegExp(
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
RegExp hkidExp = RegExp(r"^[A-Z]{1,2}[0-9]{6}[0-9A-F]{1}");
RegExp phoneExp = RegExp(r"^[0-9]{8}");

bool isEng(String str) {
  // print('fullMatch: ${engWordExp.stringMatch(str)}');
  return engWordExp.stringMatch(str.trim()) == str.trim();
}

bool isChi(String str) {
  // print('fullMatch: ${chiWordExp.stringMatch(str)}');
  return chiWordExp.stringMatch(str.trim()) == str.trim();
}

bool isEmail(String email) {
  return emailExp.hasMatch(email);
}

bool isHkid(String char, String firstNum, String lastNum) {
  /*
  space＝36; A = 10 B = 11 C = 12 D = 13 E = 14 F = 15
  G = 16 H = 17 I = 18 J = 19 K = 20 L = 21
  M = 22 N = 23 O = 24 P = 25 Q = 26 R = 27
  S = 28 T = 29 U = 30 V = 31 W = 32 X = 33
  Y = 34 Z = 35
  */
  // Step 0 - All data is enter
  if (char.isEmpty && firstNum.isEmpty && lastNum.isEmpty) {
    return false;
  }
  if (firstNum.length != 6) {
    return false;
  }
  // Step 1 - Get Sum
  int sum = 0;
  if (char.trim().length == 1) {
    if(_matchHkidChar(char.trim())==-1){
      return false;
    }
    sum = (9 * 36) +
        (_matchHkidChar(char.trim()) * 8) +
        (int.parse(firstNum[0]) * 7) +
        (int.parse(firstNum[1]) * 6) +
        (int.parse(firstNum[2]) * 5) +
        (int.parse(firstNum[3]) * 4) +
        (int.parse(firstNum[4]) * 3) +
        (int.parse(firstNum[5]) * 2);
  } else if (char.trim().length == 2) {
    if(_matchHkidChar(char.trim()[0])==-1){
      return false;
    }
    sum = (_matchHkidChar(char[0]) * 9) +
        (_matchHkidChar(char[1]) * 8) +
        (int.parse(firstNum[0]) * 7) +
        (int.parse(firstNum[1]) * 6) +
        (int.parse(firstNum[2]) * 5) +
        (int.parse(firstNum[3]) * 4) +
        (int.parse(firstNum[4]) * 3) +
        (int.parse(firstNum[5]) * 2);
  } else {
    //Error, not enter char
    return false;
  }
  // Step 2 - Get Value
  int diff = sum % 11;
  // Step 3 - Check Last Num
  if (diff > 0) {
    int checker = 11 - diff;
    int lastNumFin = _matchHkidChar(lastNum);
    if(lastNumFin!=-1){
      return lastNumFin == checker;
    }else{
      //Digit
      return int.parse(lastNum) == checker;
    }
  } else {
    return int.parse(lastNum) == diff;
  }
  // return hkidExp.hasMatch(id);
}

int _matchHkidChar(String char) {
  List<String> abc = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"
  ];
  // print('char: ${char}');
  // print('abc.indexOf(char.toUpperCase()): ${abc.indexOf(char.toUpperCase())}');
  if (abc.contains(char.toUpperCase())) {
    return abc.indexOf(char.toUpperCase()) + 10;
  } else {
    return -1;
  }
}

bool isPhone(String phoneno){
  return phoneExp.hasMatch(phoneno);
}

bool isBirthRangeCorrect(String birthdate){
  DateTime date = DateTime.parse(birthdate);
  int age = (DateTime.now().difference(date).inDays/365.25).floor();
  // print("age: $age");
  return age >= 18 && age <= 100;
}