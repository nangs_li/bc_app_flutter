import 'package:common_utils/common_utils.dart';

class ZHTimelineInfo implements TimelineInfo {
  String suffixAgo() => '前';

  String suffixAfter() => '後';

  String lessThanTenSecond() => '剛剛';

  String customYesterday() => '昨天';

  bool keepOneDay() => true;

  bool keepTwoDays() => false;

  String oneMinute(int minutes) => '$minutes分';

  String minutes(int minutes) => '$minutes分';

  String anHour(int hours) => '$hours小時';

  String hours(int hours) => '$hours小時';

  String oneDay(int days) => '$days日';

  String days(int days) => '$days日';

  DayFormat dayFormat() => DayFormat.Full;

  @override
  String lessThanOneMinute() {
    // TODO: implement lessThanOneMinute
    throw UnimplementedError();
  }

  @override
  int maxJustNowSecond() {
    // TODO: implement maxJustNowSecond
    throw UnimplementedError();
  }

  @override
  String weeks(int week) {
    // TODO: implement weeks
    throw UnimplementedError();
  }
}

class ENTimelineInfo implements TimelineInfo {
  String suffixAgo() => ' ago';

  String suffixAfter() => ' after';

  String lessThanTenSecond() => 'just now';

  String customYesterday() => 'Yesterday';

  bool keepOneDay() => true;

  bool keepTwoDays() => false;

  String oneMinute(int minutes) => 'a minute';

  String minutes(int minutes) => '$minutes minutes';

  String anHour(int hours) => 'an hour';

  String hours(int hours) => '$hours hours';

  String oneDay(int days) => 'a day';

  String days(int days) => '$days days';

  DayFormat dayFormat() => DayFormat.Full;

  @override
  String lessThanOneMinute() {
    // TODO: implement lessThanOneMinute
    throw UnimplementedError();
  }

  @override
  int maxJustNowSecond() {
    // TODO: implement maxJustNowSecond
    throw UnimplementedError();
  }

  @override
  String weeks(int week) {
    // TODO: implement weeks
    throw UnimplementedError();
  }
}

getAMPMStr(String str, String locale){
  List<String> time = str.split(' ');
  if(time.length>1){
    String hour = time[1].split(':')[0];
    String minute = time[1].split(':')[1];
    String ampm = ((int.parse(hour)/12) == 0) ? (locale=="zh_hk" ? "上午" : "AM") : (locale=="zh_hk" ? "下午" : "PM");
    return "${time[0]} ${ampm}${int.parse(hour)%12}:$minute";
  }
  return str;
}