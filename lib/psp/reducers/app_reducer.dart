
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/data/dataPageReducer.dart';

import 'package:icanfight/psp/pages/loadingPage/loadingPageReducer.dart';



AppState appReducer(AppState state, action)
{
  return AppState(
  
   userState: userReducer(state.userState, action),
   dataState: dataReducer(state.dataState, action)
  );
}