import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/uti/validation/DataValidation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ValidationController {
  List<FocusNode> focusNodes;
  List<ValidType> validationTypes;
  List<String> value;
  List<bool> isValid;
  List<int> exceptionList;
  Function onFinish;
  Function getUpdateValue;

  bool _isFocusing = false;

  ValidationController(
      {this.focusNodes,
      this.validationTypes,
      this.value,
      this.isValid,
      this.exceptionList = const [],
      this.onFinish,
      this.getUpdateValue});

  // Focus Listener
  focusListener(int index) {
    if (focusNodes[index].hasFocus == false) {
      value[index] = getUpdateValue(index);
      //if (value[index].isNotEmpty) {
      //Do checking
      if (exceptionList.contains(index) && value[index].isEmpty) {
        isValid[index] = true;
      } else {
        switch (validationTypes[index]) {
          case ValidType.None:
            break;
          case ValidType.Hkid:
            if(value[2].isNotEmpty && value[3].isNotEmpty && value[4].isNotEmpty){
              isValid[2] = isHkid(value[2], value[3], value[4]);
              isValid[3] = isHkid(value[2], value[3], value[4]);
              isValid[4] = isHkid(value[2], value[3], value[4]);
            }
            break;
          case ValidType.Birth:
            break;
          case ValidType.Eng:
            isValid[index] = isEng(value[index]);
            break;
          case ValidType.Chi:
            isValid[index] = isChi(value[index]);
            break;
          case ValidType.Email:
            isValid[index] = isEmail(value[index]);
            break;
          case ValidType.Phone:
            isValid[index] = isPhone(value[index]);
            break;
        }
      }
      if (onFinish != null) {
        onFinish(index, isValid[index]);
      }
      /*} else {
        if (exceptionList.contains(index)) {
          isValid[index] = true;
          if (onFinish != null) {
            onFinish(index, isValid[index]);
          }
        }
      }*/
      //_isFocusing = false;
    } else if (focusNodes[index].hasFocus) {
      //_isFocusing = true;
    }
    /*else if (value[index].isEmpty) {
      
    } */
  }

  init() {
    // print('init A');
    if (focusNodes != null) {
      for (int i = 0; i < focusNodes.length; i++) {
        if (focusNodes[i] != null) {
          if (focusNodes[i].hasListeners == false) {
            focusNodes[i].addListener(() => focusListener(i));
          }
        }
      }
    }
  }

  dispose() {
    if (focusNodes != null) {
      for (int i = 0; i < focusNodes.length; i++) {
        if (focusNodes[i] != null) {
          focusNodes[i].removeListener(focusListener(i));
        }
      }
    }
  }
}
