//TODO
import 'dart:async';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/data/dataState.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';

Future<bool> sendEmail(
    List<File> files, Data_State dataState, String temporaryEmail) async {
  print('sendEmail - Start: ${DateTime.now()}');

  Iterable<Attachment> toAt(Iterable<String> attachments) => (attachments ?? [])
      .map((a) => FileAttachment(File(a), contentType: 'application/zip'));
  final emailConfig = await dn_it();
  //print('email: ${emailConfig['email']}, ${emailConfig['password']}');
  //'smtp.live.com'
  SingletonShareData data = SingletonShareData();
  bool isTestingSmtp = false;

  if(data.user.email.contains("echoroaster.com")){
    isTestingSmtp = true;
  }

  final smtpServer = SmtpServer(data.smtp.smtplink,
      username: isTestingSmtp ? data.smtp.testingusername : data.smtp.username, password: isTestingSmtp ? data.smtp.testingpassword : data.smtp.password, allowInsecure: true, ignoreBadCertificate: true);
  final message = Message()
    ..from = Address(data.smtp.username, 'PSP Auto') //emailConfig['email']
    ..recipients.add(isTestingSmtp ? data.smtp.testingreceivemail : data.smtp.receivemail.length != 0 ? data.smtp.receivemail : temporaryEmail)
    // ..ccRecipients.addAll([])
    // ..bccRecipients.add(Address('bccAddress@example.com'))
    ..subject = 'Application for Roche Breast Cancer Patient Support Program (applied from mobile app)'
    //..text = 'This is the plain text.\nThis is line 2 of the text part.'
    ..html = "<p>" +
        "Case number: ${dataState.caseNumber}<br/>" +
        "English name: ${dataState.nameEng}<br/>" +
        "Chinese name: ${dataState.nameChinese}<br/>" +
        "HKID card no.: ${dataState.idChar}${dataState.idFirstNum}(${dataState.idLastNum})<br/>" +
        "Gender: ${dataState.gender} <br/>" +
        "Hong Kong permanent resident: ${dataState.hkCitizen} <br/>" +
        "Date of Birth: ${getBirthdayStr(dataState.birthDay)}<br/>" +
        "Age (Patient): ${getAge(dataState.birthDay)}<br/>" +
        "Tel. No. (Tel): ${dataState.contactNum}<br/>" +
        "Tel. No. (mobile): ${dataState.mobileContactNum}<br/>" +
        "Email address: ${dataState.emailAddress}<br/>" +
        "Address: ${dataState.homeAddress}<br/>" +
        "Marital Status (Patient): ${dataState.maritalStatus}<br/>" +
        "Free text (if Other): ${dataState.maritalDetail}<br/>" +
        "Occupation Status: ${dataState.job}<br/>" +
        "Director of any registered company: ${dataState.isDirector}<br/>" +
        "------------------------------------------------------------" +
        "<br/>" +
        "Bank Account No.: ${dataState.bankNumber}" +
        "<br/>" +
        "Bank Name: ${dataState.bankName}" +
        "<br/>" +
        "Name of Bank Account: ${dataState.bankAccountName}" +
        "<br/>" +
        "------------------------------------------------------------" +
        "<br/>" +
        "Number of working household member(s): ${dataState.familyMemberIncome.length}" +
        "<br/>" +
        getFamilyInfo(dataState) +
        "------------------------------------------------------------" +
        "<br/>" +
        "Under any social security assistance scheme : " +
        (dataState.fundedScheme != ""
            ? (dataState.fundedScheme == "沒有" ? "No" : "Yes")
            : "") +
        "<br/>" +
        "Which social security assistance scheme: ${toList(dataState.fundedScheme)}" +
        "<br/>" +
        "Free text (if Other Assistance): ${dataState.fundedSchemeDetail}" +
        "<br/>" +
        "Public or Private sector: " +
        (dataState.isPublicHospial ==
                uiData_Text['pubOrPriHospitalPage']['radioGroup'][0]
            ? "Public"
            : "") +
        (dataState.isPublicHospial ==
                uiData_Text['pubOrPriHospitalPage']['radioGroup'][1]
            ? "Privte"
            : "") +
        (dataState.isPublicHospial == "" ? "" : "") +
        "<br/>" +
        "Sector providing treatment: ${dataState.treatmentLocation}" +
        "<br/>" +
        (dataState.isPublicHospial ==
                uiData_Text['pubOrPriHospitalPage']['radioGroup'][0]
            ? ("Name of drugs to sponsor: ${dataState.treatmentDrug}" +
                "<br/>" +
                "Indication of Drug: ${toList(dataState.treatmentSituation)}" +
                "<br/>" +
                "Breast Cancer Staging: ${dataState.treatmentStage}" +
                "<br/>")
            : "") +
        "------------------------------------------------------------" +
        "<br/>" +
        "    How do you get the information of PSP?: ${toList(dataState.whereGetInfo)}" +
        "<br/>" +
        "Free text (if Social media platform): ${dataState.whereGetInfoDetail}" +
        "<br/>" +
        "Free text (if others detail): ${dataState.whereGetInfoOthersDetail}" +
        "<br/>" +
        "</p>"
    ..attachments.addAll(files.map((f) => FileAttachment(f)));
  //..attachments.addAll(toAt(files as Iterable<String>));

  try {

    // google analytics

    ga.gaEvent(name: dataState.gender == "M" ? "psp_gender_male" : "psp_gender_female");
    ga.gaEvent(name: dataState.hkCitizen == "YES" ? "psp_hkciziten_yes" : "psp_hkciziten_no");
    int birthDay = getAge(dataState.birthDay);
    if(birthDay < 30){
      ga.gaEvent(name: "psp_age_under30");
    } else if(birthDay >= 30 && birthDay <= 39){
      ga.gaEvent(name: "psp_age_30to39");
    } else if(birthDay >= 40 && birthDay <= 49){
      ga.gaEvent(name: "psp_age_40to49");
    } else if(birthDay >= 50 && birthDay <= 59){
      ga.gaEvent(name: "psp_age_50to59");
    } else if(birthDay >= 60 && birthDay <= 69){
      ga.gaEvent(name: "psp_age_60to69");
    } else if(birthDay >= 70){
      ga.gaEvent(name: "psp_age_70orabove");
    }

    if(dataState.maritalStatus == "SINGLE") {
      ga.gaEvent(name: "psp_marital_single");
    } else if(dataState.maritalStatus == "MARRIED") {
      ga.gaEvent(name: "psp_marital_married");
    } else if(dataState.maritalStatus == "OTHER") {
      ga.gaEvent(name: "psp_marital_other");
    }

    if(dataState.job == "EMPLOYED") {
      ga.gaEvent(name: "psp_occupation_employed");
    } else if(dataState.job == "SELF-EMPLOYED") {
      ga.gaEvent(name: "psp_occupation_selfemployed");
    } else if(dataState.job == "UNEMPLOYED") {
      ga.gaEvent(name: "psp_occupation_unemployed");
    }

    ga.gaEvent(name: dataState.isDirector == "YES" ? "psp_director_registed_company_yes" : "psp_director_registed_company_no");

    if (dataState.fundedScheme.contains(mcSpliter)) {
      for (int i = 0; i < dataState.fundedScheme.split(mcSpliter).length; i++) {
        String fundedScheme =  dataState.fundedScheme.split(mcSpliter)[i];
        logFundedScheme(fundedScheme: fundedScheme);
      }
    } else {
      logFundedScheme(fundedScheme: dataState.fundedScheme);
    }

    ga.gaEvent(name: dataState.isPublicHospial == uiData_Text['pubOrPriHospitalPage']['radioGroup'][0] ? "psp_public_sector" : "psp_private_sector");

    if (dataState.whereGetInfo.contains(mcSpliter)) {
      for (int i = 0; i < dataState.whereGetInfo.split(mcSpliter).length; i++) {
        String whereGetInfo =  dataState.whereGetInfo.split(mcSpliter)[i];
        logWhereGetInfo(whereGetInfo: whereGetInfo);
      }
    } else {
      logWhereGetInfo(whereGetInfo: dataState.whereGetInfo);
    }

    if(dataState.familyMemberIncome.length == 1){
      ga.gaEvent(name: "psp_hosehold_1person");
    } else if (dataState.familyMemberIncome.length == 2){
      ga.gaEvent(name: "psp_hosehold_2person");
    } else if (dataState.familyMemberIncome.length == 3){
      ga.gaEvent(name: "psp_hosehold_3person");
    } else if (dataState.familyMemberIncome.length == 4){
      ga.gaEvent(name: "psp_hosehold_4person");
    } else if (dataState.familyMemberIncome.length == 5){
      ga.gaEvent(name: "psp_hosehold_5person");
    } else if (dataState.familyMemberIncome.length == 6){
      ga.gaEvent(name: "psp_hosehold_6person");
    }


    if(dataState.treatmentDrug == "Perjeta and Herceptin IV"){
      ga.gaEvent(name: "psp_drug_Perjeta_Herceptin_IV");
    } else if (dataState.treatmentDrug == "Perjeta and Herceptin Subcutaneous"){
      ga.gaEvent(name: "psp_drug_Perjeta_Herceptin_IV_subcutaneous");
    } else if (dataState.treatmentDrug == "Kadcyla"){
      ga.gaEvent(name: "psp_drug_kadcyla");
    } else if (dataState.treatmentDrug == "Tecentriq"){
      ga.gaEvent(name: "psp_drug_tecentriq");
    }

    if (dataState.treatmentSituation.contains(mcSpliter)) {
      for (int i = 0; i < dataState.treatmentSituation.split(mcSpliter).length; i++) {
        String treatmentSituation =  dataState.treatmentSituation.split(mcSpliter)[i];
        logTreatmentSituation(treatmentSituation: treatmentSituation);
      }
    } else {
      logTreatmentSituation(treatmentSituation: dataState.treatmentSituation);
    }

    if(dataState.treatmentStage == "第一期"){
      ga.gaEvent(name: "psp_stage_1");
    } else if (dataState.treatmentStage == "第二期"){
      ga.gaEvent(name: "psp_stage_2");
    } else if (dataState.treatmentStage == "第三期"){
      ga.gaEvent(name: "psp_stage_3");
    } else if (dataState.treatmentStage == "第四期"){
      ga.gaEvent(name: "psp_stage_4");
    }




    // print('message: ${message}');
    final sendReport = await send(message, smtpServer, timeout: Duration(seconds: 15));//15
    print('Message sent: ${DateTime.now()}:  ' + sendReport.toString());
    data.alreadySubmitForm();
    return true;
  } on MailerException catch (e) {
    print('Message not sent. ${e}');
    for (var p in e.problems) {
      print('Problem: ${p.code}: ${p.msg}');
    }
    return false;
  } on TimeoutException catch(e){
    print('TimeoutException Message not sent.');
    return false;
  } on Exception catch(e){
    print('SmtpMessageValidationException Message not sent. ${e}');
    return false;
  }
}

void logTreatmentSituation ({String treatmentSituation}){
  if(treatmentSituation == "手術前治療"){
    ga.gaEvent(name: "psp_trearment_neoadjuvant_before");
  } else if (treatmentSituation == "手術後治療"){
    ga.gaEvent(name: "psp_trearment_adjuvant_after");
  } else if (treatmentSituation == "手術前及手術後治療"){
    ga.gaEvent(name: "psp_trearment_neo_before_adjuvant_after");
  } else if (treatmentSituation == "轉移性一線治療"){
    ga.gaEvent(name: "psp_trearment_metastatic_first_line");
  } else if (treatmentSituation == "轉移性二線治療"){
    ga.gaEvent(name: "psp_trearment_metastatic_second_line");
  } else if (treatmentSituation == "轉移性三線或以上治療"){
    ga.gaEvent(name: "psp_trearment_metastatic_third_line");
  }
}

void logFundedScheme ({String fundedScheme}){
  if(fundedScheme == "沒有") {
    ga.gaEvent(name: "psp_assisstance_scheme_no");
  } else if(fundedScheme == "綜合社會保障援助(綜援)計劃") {
    ga.gaEvent(name: "psp_assisstance_scheme_cssa");
  } else if(fundedScheme == "關愛基金") {
    ga.gaEvent(name: "psp_assisstance_scheme_ccf");
  } else if(fundedScheme == "撒瑪利亞基金") {
    ga.gaEvent(name: "psp_assisstance_scheme_sf");
  } else if(fundedScheme == "其他資助/津貼") {
    ga.gaEvent(name: "psp_assisstance_scheme_others");
  }
}

void logWhereGetInfo ({String whereGetInfo}){
  if(whereGetInfo == "醫生") {
    ga.gaEvent(name: "psp_marketing_physicans");
  } else if(whereGetInfo == "病人組織") {
    ga.gaEvent(name: "psp_marketing_patient_group");
  } else if(whereGetInfo == "醫院病人資源中心") {
    ga.gaEvent(name: "psp_marketing_hospitals");
  } else if(whereGetInfo == "Her2morrow 網站") {
    ga.gaEvent(name: "psp_marketing_her2morrow");
  } else if(whereGetInfo == "社交媒體") {
    ga.gaEvent(name: "psp_marketing_social_media");
  } else if(whereGetInfo == "其他") {
    ga.gaEvent(name: "psp_marketing_others");
  }
}

String getFamilyInfo(Data_State dataState) {
  String result = "";
  int income = 0;

  if (dataState.familyMemberIncome.isNotEmpty) {
    for (var i = 0; i < dataState.familyMemberIncome.length; i++) {
      result += "Working household member " +
          (i + 1).toString() +
          " - " +
          "<br/>" +
          "Name: " +
          dataState.familyMemberIncome[i].memNam +
          "<br/>" +
          "Age: " +
          dataState.familyMemberIncome[i].memAge +
          "<br/>" +
          "Gender: " +
          (dataState.familyMemberIncome[i].memGender ==
                  uiData_Text['familyIncomePage']['famGender_m']
              ? "M"
              : "") +
          (dataState.familyMemberIncome[i].memGender ==
                  uiData_Text['familyIncomePage']['famGender_f']
              ? "F"
              : "") +
          (dataState.familyMemberIncome[i].memGender == "" ? "" : "") +
          "<br/>" +
          "Relationship with Applicant: " +
          dataState.familyMemberIncome[i].memRelation +
          "<br/>" +
          "Occupation (Optional): " +
          dataState.familyMemberIncome[i].memCareer +
          "<br/>" +
          "Post-tax Income in Last Financial Year (HK\$): " +
          dataState.familyMemberIncome[i].memIncome +
          "<br/>" +
          "<br/>";
      // print('dataState.familyMemberIncome[i].memIncome: ${dataState.familyMemberIncome[i].memIncome}');
      income += int.tryParse(dataState.familyMemberIncome[i].memIncome
              .substring(3)
              .replaceAll(',', '')) ??
          0;
    }
    // print('dataState.familyMemberIncome[i].memIncome: ${income}');
    result += "Total Annual Household Income  (HK\$): " + income.toString();
    result += "<br/>";
  }

  return result;
}

String toList(String value) {
  String list = "";
  if (value.contains(mcSpliter)) {
    for (int i = 0; i < value.split(mcSpliter).length; i++) {
      if (i == 0) {
        list += value.split(mcSpliter)[i];
      } else {
        list += "," + value.split(mcSpliter)[i];
      }
    }
  } else {
    list += value;
  }
  return list.toString();
}

//GET Case API
Future<String> getCase() async {
  final response = await http.get(base_api + getCase_api,
      headers: {'Authorization': 'Bearer ${appToken}'}).timeout(const Duration(seconds: 15));

  if (response.statusCode == 200) {
    return response.body;
  } else {
    throw Exception('Failed to load post');
  }
}

//TODO Update Case API

/*Future<String> updateCase(Data_State data_state) async {

  final response = await http.put(base_api + updateCase_api,
      body: data_state, headers: {'Authorization': 'Bearer ${appToken}'});
  if (response.statusCode == 200) {
    return response.body;
  } else {
    throw Exception('Failed to load post');
  }

}*/

//TODO
Future<File> compressZip(
    List<String> paths, int quality, int targetWidth) async {
  print('Compress Zip & Return File - Start: ${DateTime.now()}');
  var encoder = ZipFileEncoder();
  // Manually create a zip of a directory and individual files.
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  encoder.create(appDocDirectory.path + '/attachment.zip');

  for (var i = 0; i < paths.length; i++) {
    //TODO need to check file type
    //Compress Image File
    if (File(paths[i]).existsSync()) {
      ImageProperties properties =
          await FlutterNativeImage.getImageProperties(paths[i]);
      if (properties.width > properties.height) {
        File f = await FlutterNativeImage.compressImage(paths[i],
            quality: quality,
            targetWidth: targetWidth,
            targetHeight:
                (properties.height * targetWidth / properties.width).round(),
                );
        //Add Files
        encoder.addFile(f);
      } else {
        File f = await FlutterNativeImage.compressImage(paths[i],
            quality: quality,
            targetWidth:
                (properties.width * targetWidth / properties.height).round(),
            targetHeight: targetWidth);
        //Add Files
        encoder.addFile(f);
      }
    }
  }
  encoder.close();
  //Save to mobile other place
  /*await File(appDocDirectory.path + '/demo.zip')
      .readAsBytes()
      .then((byteData) async => {
            await Share.file('demo.zip', 'demo.zip',
                byteData.buffer.asUint8List(), 'application/zip')
          });*/
  //Return a zip file
  print('Compress Zip & Return File - END: ${DateTime.now()}');
  return File(appDocDirectory.path + '/attachment.zip');
}

// Show Dialog
Future<void> openDialog(BuildContext _context) async {
  return showDialog<void>(
    context: _context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      return CustomAlertDialog(title: uiData_Text['submitBoxFail']['title'],
          content: uiData_Text['submitBoxFail']['content'],
          leftButtonText: null,
          rightButtonText: uiData_Text['submitBoxFail']['ok'],
          buildContext: context,
          rightButtonFunction: () {
            Navigator.of(context).pop();
          });

      CupertinoAlertDialog(
        title: Text(
          uiData_Text['submitBoxFail']['title'],
          style: formPageTitleStyle,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                uiData_Text['submitBoxFail']['content'],
                style: formPageInstructStyle.copyWith(fontFamily: 'Rawline'),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(uiData_Text['submitBoxFail']['ok'],
                style: stepperTxtStyle),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

//Email Content Method
getBirthdayStr(String str) {
  if (str.isNotEmpty) {
    var date = DateTime.parse(str);
    return DateFormat('dd/MM/yyyy').format(date);
  } else {
    return "";
  }
}

int getAge(String str) {
  if (str.isNotEmpty) {
    var date = DateTime.parse(str);
    var today = DateTime.now();
    var diff = today.difference(date);
    return (diff.inDays / 365).toInt();
  } else {
    return 0;
  }
}
