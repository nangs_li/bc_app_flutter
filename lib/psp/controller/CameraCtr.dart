import 'package:camera/camera.dart';
import 'package:icanfight/psp/app/config.dart';
import 'package:icanfight/psp/app/theme.dart';
import 'package:icanfight/psp/models/PassImageListArguments.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';
import 'package:icanfight/psp/view/CameraView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:permission_handler/permission_handler.dart';

import '../app/app_state.dart';
import '../app/data/dataPageAction.dart';
import '../app/data/dataState.dart';
import '../routing/routingAction.dart';
import 'SaveLoadImgController.dart';

List<ByteData> demoByteData = List<ByteData>();

Future<void> initDemoByteData() async {
  print("initDemoByteData");
  if (demoByteData.isNotEmpty) {
    print("initDemoByteData is not empty");
  } else {
    print("initDemoByteData add demo image into bytelist");
    ByteData bytes = await rootBundle.load('assets/idDemo.png');
    demoByteData.add(bytes);
  }
}

initDemoFamilyMember(BuildContext _context) {
  int debug_User = StoreProvider.of<AppState>(_context)
      .state
      .dataState
      .familyMemberIncome
      .length;

  if (debug_User > 0) {
    print("family member bigger than 0");
  } else {
    print("no family member");
    StoreProvider.of<AppState>(_context)
        .dispatch(Add_FamilyMemberIncome(FamilyMemberIncome.initial()));
  }
}

open_camera(BuildContext _context, IMAGE_TYPE imageType, int memberID,
    DateTime metaData,
    [bool isPushReplace = false]) async {
  bool isAllowOpenCamera = false;
  print("open_camera " + imageType.toString());
  switch (imageType) {
    case IMAGE_TYPE.hkid:
      if (StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .hkidLocation
          .isNotEmpty) {
        StoreProvider.of<AppState>(_context)
            .dispatch(NavigateToIdDetailPageAction());
      } else {
        isAllowOpenCamera = true;
      }

      break;
    case IMAGE_TYPE.invoice:
      if (StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .invoiceLocation
          .isNotEmpty) {
        StoreProvider.of<AppState>(_context)
            .dispatch(NavigateToInvoiceDetailPageAction());
      } else {
        isAllowOpenCamera = true;
      }
      break;
    case IMAGE_TYPE.refletter:
      if (StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .refLetterLocation
          .isNotEmpty) {
        StoreProvider.of<AppState>(_context)
            .dispatch(NavigateToRefLetterPageAction());
      } else {
        isAllowOpenCamera = true;
      }
      break;
    case IMAGE_TYPE.medicalreport:
      if (StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .medicalReportLocation
          .isNotEmpty) {
        StoreProvider.of<AppState>(_context)
            .dispatch(NavigateToMedicalReportPageAction());
      } else {
        isAllowOpenCamera = true;
      }
      break;
    case IMAGE_TYPE.income_tax:
      /*     bool isCorrectDocType =
          verifyIncomeDocType(_context, imageType, memberID);

      print("isCorrectDocType " + isCorrectDocType.toString());
      if (isCorrectDocType) {
        
      } else {
        //goto photo taking page
        isAllowOpenCamera = true;
      } */
      isAllowOpenCamera = true;
      break;
    case IMAGE_TYPE.bank_statement:
      isAllowOpenCamera = true;
      break;
    case IMAGE_TYPE.passbook:
      isAllowOpenCamera = true;
      break;
  }

  if (isAllowOpenCamera) {
    // WidgetsFlutterBinding.ensureInitialized();
    // TODO
    bool isAllowCamera = await requestPermission(PermissionGroup.camera);
    if (isAllowCamera == false) {
      await openDialog(_context, PermissionGroup.camera);
      // open_camera(_context, imageType, memberID, metaData);
    } else {
      final cameras = await availableCameras();

      //print("current route name " + ModalRoute.of(_context).settings.name);

      // if (ModalRoute.of(_context).settings.name == '/uploadDocList') {

      if (isPushReplace) {
        print("isPushReplace is true ");
        await Navigator.pushReplacement(
            _context,
            MaterialPageRoute(
                builder: (context) => TakePictureScreen(
                    camera: cameras,
                    imageType: imageType,
                    memberId: memberID,
                    metaData: metaData)));
      } else {
        print("isPushReplace is false ");
        await Navigator.push(
            _context,
            MaterialPageRoute(
                builder: (context) => TakePictureScreen(
                    camera: cameras,
                    imageType: imageType,
                    memberId: memberID,
                    metaData: metaData)));
      }

      /* } else {
        await Navigator.pushReplacement(
            _context,
            MaterialPageRoute(
                builder: (context) => TakePictureScreen(
                    camera: cameras,
                    imageType: imageType,
                    memberId: memberID,
                    metaData: metaData)));
      } */
    }
  }
}

//Permission
Future<void> openDialog(BuildContext _context, PermissionGroup group) async {
  return showDialog<void>(
    context: _context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      return CustomAlertDialog(title: group == PermissionGroup.camera
          ? uiData_Text['permissionBox']['camera']['title']
          : uiData_Text['permissionBox']['photo']['title'],
        content: group == PermissionGroup.camera
            ? uiData_Text['permissionBox']['camera']['content']
            : uiData_Text['permissionBox']['photo']['content'],
        leftButtonText: uiData_Text['permissionBox']['cancel'],
        rightButtonText: uiData_Text['permissionBox']['setting'],
        buildContext: context,
        rightButtonFunction: () async {
          await PermissionHandler().openAppSettings();
        });
      return CupertinoAlertDialog(
        title: Text(
          group == PermissionGroup.camera
              ? uiData_Text['permissionBox']['camera']['title']
              : uiData_Text['permissionBox']['photo']['title'],
          style: formPageTitleStyle,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                group == PermissionGroup.camera
                    ? uiData_Text['permissionBox']['camera']['content']
                    : uiData_Text['permissionBox']['photo']['content'],
                style: formPageInstructStyle,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(uiData_Text['permissionBox']['cancel'],
                style: stepperTxtStyle),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text(uiData_Text['permissionBox']['setting'],
                style: stepperTxtStyle),
            onPressed: () async {
              await PermissionHandler().openAppSettings();
            },
          ),
        ],
      );
    },
  );
}

Future<bool> requestPermission(PermissionGroup permission) async {
  final List<PermissionGroup> permissions = <PermissionGroup>[permission];
  final Map<PermissionGroup, PermissionStatus> permissionRequestResult =
      await PermissionHandler().requestPermissions(permissions);

  if (permissionRequestResult[permission] == PermissionStatus.granted) {
    return true;
  }
  return false;
}

bool verifyIncomeDocType(
    BuildContext _context, IMAGE_TYPE imageType, int memberID) {
  bool result = false;

  if (memberID == -1) {
    if (StoreProvider.of<AppState>(_context).state.dataState.incomeCopyType ==
        imageType.toString()) {
      //same income doc type
      result = true;
    } else {
      //ERROR: wrong income doc type, clean all doc record and restart
      StoreProvider.of<AppState>(_context)
          .dispatch(Change_IncomeCopyType_Action(imageType, memberID));

      for (var incomeCopy in StoreProvider.of<AppState>(_context)
          .state
          .dataState
          .incomeCopyLocation) {
        StoreProvider.of<AppState>(_context).dispatch(
            Remove_IncomeImage_Action(incomeCopy, imageType, memberID));
      }
    }
  } else {
    if (StoreProvider.of<AppState>(_context)
            .state
            .dataState
            .familyMemberIncome[memberID]
            .incomeCopyType ==
        imageType.toString()) {
      //same income doc type
      result = true;
    } else {
      FamilyMemberIncome updateFamilyMemberIncom =
          StoreProvider.of<AppState>(_context)
              .state
              .dataState
              .familyMemberIncome[memberID]
              .copyWith();

      updateFamilyMemberIncom.incomeCopyType = "";
      updateFamilyMemberIncom.incomeCopyLocation = List<Location>();

      StoreProvider.of<AppState>(_context).dispatch(
          Update_FamilyMemberIncome(updateFamilyMemberIncom, memberID));
    }
  }

  return result;
}

save_photo(BuildContext _context, IMAGE_TYPE imageType, int memberID,
    List<ByteData> byteData, DateTime metaData) async {
  //TODO if already have incomeCopyType record, go to detail page directly
  //create demo image list
  initDemoByteData();
  // initDemoFamilyMember(_context);

  switch (imageType) {
    case IMAGE_TYPE.hkid:

      //List<ByteData> byteData = await photo_take(1);

      await saveImageDoc(
          _context,
          imageType,
          byteData, //demoByteData,
          "",
          "",
          () => {
                StoreProvider.of<AppState>(_context)
                    .dispatch(SaveAppDataToLocalAction()),
                /*  StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToIdDetailPageAction()) */
                StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToUploadDocListPageAction())
              });

      break;
    case IMAGE_TYPE.invoice:

      //List<ByteData> byteData = await photo_take(1);

      PassImageListArguments args;

      args = PassImageListArguments(UserType.Patient, byteData, memberID);

      StoreProvider.of<AppState>(_context)
          .dispatch(NavigateToInvoiceInputPageAction(args));

      break;
    case IMAGE_TYPE.refletter:
      await saveImageDoc(
          _context,
          imageType,
          byteData, //demoByteData,
          "",
          "",
          () => {
                StoreProvider.of<AppState>(_context)
                    .dispatch(SaveAppDataToLocalAction()),
                /*      StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToRefLetterPageAction()) */
                StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToUploadDocListPageAction())
              });
      break;
    case IMAGE_TYPE.medicalreport:
      await saveImageDoc(
          _context,
          imageType,
          byteData, //demoByteData,
          "",
          "",
          () => {
                StoreProvider.of<AppState>(_context)
                    .dispatch(SaveAppDataToLocalAction()),
                /*     StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToMedicalReportPageAction()) */
                StoreProvider.of<AppState>(_context)
                    .dispatch(NavigateToUploadDocListPageAction())
              });

      break;
    case IMAGE_TYPE.income_tax:
      print("start save tax image to local");

      print("member " + memberID.toString());

      print("imageType " + imageType.toString());

      await saveTaxImageDoc(_context, imageType, byteData, memberID);

      break;
    case IMAGE_TYPE.bank_statement:
      //TODO call below after phototaking
      //List<ByteData> byteData = await photo_take(300);
      //TODO update incomeCopyType
      print('metaData: ${metaData}');

      await saveBankSheetImageDoc(
          _context, imageType, byteData, memberID, metaData, metaData);

      break;
    case IMAGE_TYPE.passbook:
      print("open_camera income_bankBook");
      //TODO call below after phototaking
      //List<ByteData> byteData = await photo_take(300);

      //TODO update incomeCopyType
      PassImageListArguments args;
      if (memberID == 1) {
        args = PassImageListArguments(UserType.Patient, byteData, memberID);
      } else {
        args =
            PassImageListArguments(UserType.FamilyMember, byteData, memberID);
      }

      StoreProvider.of<AppState>(_context)
          .dispatch(NavigateToBankBookInputPageAction(args));

      break;
  }
}
