import 'dart:math';

import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/models/ScreenArguments.dart';
import 'package:icanfight/psp/models/UserType.dart';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

import '../app/app_state.dart';
import '../app/data/dataPageAction.dart';
import '../app/data/dataState.dart';

import '../routing/routingAction.dart';
import '../uti/IO/app_io.dart';

String idImage_localPath = 'idImage';
String invoiceImage_localPath = 'invoiceImage';
String refLetterImage_localPath = 'refLetterImage';
String medicalReportImage_localPath = 'medicalReportImage';
String taxImage_localPath = 'taxImage';

enum IMAGE_TYPE {
  hkid,
  invoice,
  refletter,
  medicalreport,
  income_tax,
  bank_statement,
  passbook
}

Future<void> saveImageDoc(
    BuildContext _context,
    IMAGE_TYPE image_type,
    List<ByteData> dataList,
    String startTime,
    String endTime,
    Function onFinish) async {
  for (var data in dataList) {
    String fileName = generateFileName(image_type, -1);

    await writeImageToLocalFile(fileName, data).then((value) => {
          //display image
          StoreProvider.of<AppState>(_context).dispatch(Add_Location_Action(
              Location.create(fileName, startTime, endTime,
                  DateTime.now().toIso8601String()),
              image_type)),
        });
  }

  onFinish.call();
}

String generateFileName(IMAGE_TYPE image_type, int memberID) {
  String fileName = "";
    SingletonShareData data = SingletonShareData();
  String nameEng = data.nameEng ?? "";

  if (memberID == -1) {
    fileName =
    nameEng + "_applicant_" + image_type
            .toString()
            .substring(image_type.toString().indexOf(".") + 1)+"_"+
        RandomString(6);
  } else {
    fileName =
    nameEng + "_household" +
        (memberID + 1).toString() +
        "_" +image_type
            .toString()
            .substring(image_type.toString().indexOf(".") + 1)+"_"+
        RandomString(6);
  }

  print("File name ----" + fileName);

  fileName = fileName.replaceAll(RegExp(r"\s+\b|\b\s"), "");

  return fileName;
}

Future<void> saveTaxImageDoc(BuildContext _context, IMAGE_TYPE image_type,
    List<ByteData> dataList, int memberID) async {
  for (var data in dataList) {
    String fileName = generateFileName(image_type, memberID);

    await writeImageToLocalFile(fileName, data).then((value) => {
          //display image
          StoreProvider.of<AppState>(_context).dispatch(Add_IncomeImage_Action(
              Location.create(
                  fileName, "", "", DateTime.now().toIso8601String()),
              image_type,
              memberID)),

          StoreProvider.of<AppState>(_context)
              .dispatch(Change_IncomeCopyType_Action(image_type, memberID))
        });
  }

  StoreProvider.of<AppState>(_context).dispatch(SaveAppDataToLocalAction());
/*   StoreProvider.of<AppState>(_context)
      .dispatch(Change_IncomeCopyType_Action(image_type)); */

//-1 = user
  if (memberID == -1) {
    StoreProvider.of<AppState>(_context).dispatch(NavigateToTaxDetailPageAction(
        ScreenArguments(
            UserType.Patient,
            StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .incomeCopyLocation,
            memberID)));
  } else {
    StoreProvider.of<AppState>(_context).dispatch(
        /*  NavigateToBankBookDetailPageAction */
        NavigateToTaxDetailPageAction(ScreenArguments(
            UserType.FamilyMember,
            StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .familyMemberIncome[memberID]
                .incomeCopyLocation,
            memberID)));
  }
}

Future<void> saveBankBookImageDoc(BuildContext _context, IMAGE_TYPE image_type,
    List<ByteData> dataList, int memberID, DateTime start, DateTime end) async {
  for (var data in dataList) {
    String fileName = generateFileName(image_type, memberID);

    await writeImageToLocalFile(fileName, data).then((value) => {
          //display image
          StoreProvider.of<AppState>(_context).dispatch(Add_IncomeImage_Action(
              Location.create(fileName, start.toIso8601String(),
                  end.toIso8601String(), DateTime.now().toIso8601String()),
              image_type,
              memberID)),

          StoreProvider.of<AppState>(_context)
              .dispatch(Change_IncomeCopyType_Action(image_type, memberID))
        });
  }

  StoreProvider.of<AppState>(_context).dispatch(SaveAppDataToLocalAction());

//-1 = user
  if (memberID == -1) {
    StoreProvider.of<AppState>(_context).dispatch(
        NavigateToBankBookDetailPageAction(ScreenArguments(
            UserType.Patient,
            StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .incomeCopyLocation,
            memberID)));
  } else {
    StoreProvider.of<AppState>(_context).dispatch(
        NavigateToBankBookDetailPageAction(ScreenArguments(
            UserType.FamilyMember,
            StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .familyMemberIncome[memberID]
                .incomeCopyLocation,
            memberID)));
  }
}

Future<void> saveBankSheetImageDoc(BuildContext _context, IMAGE_TYPE image_type,
    List<ByteData> dataList, int memberID, DateTime start, DateTime end) async {
  for (var data in dataList) {
    String fileName = generateFileName(image_type, memberID);

    await writeImageToLocalFile(fileName, data).then((value) {
      //display image
      print('start: ${start}, end: ${end}');
      StoreProvider.of<AppState>(_context).dispatch(Add_IncomeImage_Action(
          Location.create(fileName, start.toIso8601String(),
              end.toIso8601String(), DateTime.now().toIso8601String()),
          image_type,
          memberID));

      StoreProvider.of<AppState>(_context)
          .dispatch(Change_IncomeCopyType_Action(image_type, memberID));
    });
  }

  StoreProvider.of<AppState>(_context).dispatch(SaveAppDataToLocalAction());

  var formatter = DateFormat('yM');

  List<Location> onlyDisplayAmonth = List<Location>();

//-1 = user
  if (memberID == -1) {
    for (var location in StoreProvider.of<AppState>(_context)
        .state
        .dataState
        .incomeCopyLocation) {
      if (formatter.format(start) ==
          formatter.format(DateTime.parse(location.stdate))) {
        onlyDisplayAmonth.add(location);
      }
    }

    StoreProvider.of<AppState>(_context)
        .dispatch(NavigateToBankStatementDetailPageAction(ScreenArguments(
            UserType.Patient,
            onlyDisplayAmonth
            /* StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .incomeCopyLocation */
            ,
            memberID)));
  } else {
    for (var location in StoreProvider.of<AppState>(_context)
        .state
        .dataState
        .familyMemberIncome[memberID]
        .incomeCopyLocation) {
      if (formatter.format(start) ==
          formatter.format(DateTime.parse(location.stdate))) {
        onlyDisplayAmonth.add(location);
      }
    }

    StoreProvider.of<AppState>(_context)
        .dispatch(NavigateToBankStatementDetailPageAction(ScreenArguments(
            UserType.FamilyMember,
            onlyDisplayAmonth
            /* StoreProvider.of<AppState>(_context)
                .state
                .dataState
                .familyMemberIncome[memberID]
                .incomeCopyLocation */
            ,
            memberID)));
  }
}

const chars = "abcdefghijklmnopqrstuvwxyz0123456789";

String RandomString(int strlen) {
  Random rnd = Random(DateTime.now().millisecondsSinceEpoch);
  String result = "";
  for (var i = 0; i < strlen; i++) {
    result += chars[rnd.nextInt(chars.length)];
  }
  return result;
}

/* debugGoto_BankBookInputPage(
    BuildContext context, UserType userType, int memberID) {
  print("simulate go to bankBookInputPage after photo taking");
  //create photo
/*   List<ByteData> photoTaken = List<ByteData>();
  photoTaken.add(ByteData(10)); */

  BankBookPageArguments args =
      BankBookPageArguments(userType, demoByteData, memberID);

  StoreProvider.of<AppState>(context)
      .dispatch(NavigateToBankBookInputPageAction(args));
} */
