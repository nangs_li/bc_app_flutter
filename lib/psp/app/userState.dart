import 'package:flutter/material.dart';

@immutable
class User_State {
  final String userID;
  final String userToken;

  User_State({
    @required this.userID,
    @required this.userToken,
  });

  factory User_State.initial() {
    print('initial User_State');
    return User_State(
      userID: "",
      userToken: "",
    );
  }

  User_State copyWith({String userID, String userToken}) {
    return User_State(
        userID: userID ?? this.userID, userToken: userToken ?? this.userToken);
  }

  User_State.fromJson(Map<String, dynamic> json)
      : userID = json['userID'],
        userToken = json['userToken'];

  Map<String, dynamic> toJson() => {'userID': userID, 'userToken': userToken};
}
