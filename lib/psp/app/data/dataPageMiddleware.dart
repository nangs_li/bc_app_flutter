import 'package:icanfight/psp/app/app_state.dart';

import 'package:redux/redux.dart';

import '../../uti/IO/app_io.dart';
import '../config.dart';
import 'dataPageAction.dart';

class DataMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
  
    if (action is TypingAction) {
      print('TypingAction 1 ' + action.nameEng);
      next(action.nameEng);
    }

    if (action is SaveAppDataToLocalAction) {
      print('SaveAppDataToLocalAction ');

      writeLocalFile(appData_localPath, (store.state.dataState.toJson()))
          .then((a) {
        print("save finish");
      });
    }

    next(action);
  }
}
