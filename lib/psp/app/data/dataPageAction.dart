import 'package:icanfight/psp/controller/SaveLoadImgController.dart';

import 'dataState.dart';

class UpdateAppDataAction {
  final Data_State state;
  UpdateAppDataAction(this.state);
}

class SaveAppDataToLocalAction {
  SaveAppDataToLocalAction();
}

class TypingAction {
  final String nameEng;
  TypingAction(this.nameEng);
}

//commdn
class Change_NameEng_Action {
  final String nameEng;
  Change_NameEng_Action(this.nameEng);
}

class Change_FamilyMemberIncome_Action {
  final List<FamilyMemberIncome> familyMemberIncome;
  Change_FamilyMemberIncome_Action(this.familyMemberIncome);
}

class Change_NameChinese_Action {
  final String nameChinese;
  Change_NameChinese_Action(this.nameChinese);
}

class Change_IdChar_Action {
  final String idChar;
  Change_IdChar_Action(this.idChar);
}

class Change_idFirstNum_Action {
  final String idFirstNum;
  Change_idFirstNum_Action(this.idFirstNum);
}

class Change_idLastNum_Action {
  final String idLastNum;
  Change_idLastNum_Action(this.idLastNum);
}

class Change_gender_Action {
  final String gender;
  Change_gender_Action(this.gender);
}

class Change_hkCitizen_Action {
  final String hkCitizen;
  Change_hkCitizen_Action(this.hkCitizen);
}

class Change_birthDay_Action {
  final String birthDay;
  Change_birthDay_Action(this.birthDay);
}

class Change_contactNum_Action {
  final String contactNum;
  Change_contactNum_Action(this.contactNum);
}

class Change_mobileContactNum_Action {
  final String mobileContactNum;
  Change_mobileContactNum_Action(this.mobileContactNum);
}

class Change_emailAddress_Action {
  final String emailAddress;
  Change_emailAddress_Action(this.emailAddress);
}

class Change_homeAddress_Action {
  final String homeAddress;
  Change_homeAddress_Action(this.homeAddress);
}

class Change_maritalStatus_Action {
  final String maritalStatus;
  Change_maritalStatus_Action(this.maritalStatus);
}

class Change_maritalDetail_Action {
  final String maritalDetail;
  Change_maritalDetail_Action(this.maritalDetail);
}

class Change_job_Action {
  final String job;
  Change_job_Action(this.job);
}

class Change_isDirector_Action {
  final String isDirector;
  Change_isDirector_Action(this.isDirector);
}

class Change_bankNumber_Action {
  final String bankNumber;
  Change_bankNumber_Action(this.bankNumber);
}

class Change_bankName_Action {
  final String bankName;
  Change_bankName_Action(this.bankName);
}

class Change_bankAccountName_Action {
  final String bankAccountName;
  Change_bankAccountName_Action(this.bankAccountName);
}

class Change_InvoicePrice_Action {
  final String invoicePrice;
  Change_InvoicePrice_Action(this.invoicePrice);
}

class Change_IncomeCopyType_Action {
  final IMAGE_TYPE incomeCopyType;
  final int memberID;
  Change_IncomeCopyType_Action(this.incomeCopyType, this.memberID);
}

class Change_fundedScheme_Action {
  final String fundedScheme;
  Change_fundedScheme_Action(this.fundedScheme);
}

class Change_fundedSchemeDetail_Action {
  final String fundedSchemeDetail;
  Change_fundedSchemeDetail_Action(this.fundedSchemeDetail);
}

class Change_isPublicHospial_Action {
  final String isPublicHospial;
  Change_isPublicHospial_Action(this.isPublicHospial);
}

class Change_treatmentLocation_Action {
  final String treatmentLocation;
  Change_treatmentLocation_Action(this.treatmentLocation);
}

class Change_treatmentDrug_Action {
  final String treatmentDrug;
  Change_treatmentDrug_Action(this.treatmentDrug);
}

class Change_treatmentSituation_Action {
  final String treatmentSituation;
  Change_treatmentSituation_Action(this.treatmentSituation);
}

class Change_treatmentStage_Action {
  final String treatmentStage;
  Change_treatmentStage_Action(this.treatmentStage);
}

class Change_whereGetInfo_Action {
  final String whereGetInfo;
  Change_whereGetInfo_Action(this.whereGetInfo);
}

class Change_whereGetInfoDetail_Action {
  final String whereGetInfoDetail;
  Change_whereGetInfoDetail_Action(this.whereGetInfoDetail);
}

class Change_whereGetInfoOthersDetail_Action {
  final String whereGetInfoOthersDetail;
  Change_whereGetInfoOthersDetail_Action(this.whereGetInfoOthersDetail);
}

class Change_invoicePrice_Action {
  final String invoicePrice;
  Change_invoicePrice_Action(this.invoicePrice);
}

class Change_isAgreePersonalDate_Action {
  final bool isAgreePersonalDate;
  Change_isAgreePersonalDate_Action(this.isAgreePersonalDate);
}

class Change_isAgreeStatement_Action {
  final bool isAgreeStatement;
  Change_isAgreeStatement_Action(this.isAgreeStatement);
}

class Change_isSentEmail_Action {
  final bool isSentEmail;
  Change_isSentEmail_Action(this.isSentEmail);
}

class Change_caseNumber_Action {
  final String caseNumber;
  Change_caseNumber_Action(this.caseNumber);
}

class Change_isFinishApply_Action {
  final bool isFinishApply;
  Change_isFinishApply_Action(this.isFinishApply);
}

class Change_lastSavedPage_Action {
  final String lastSavedPage;
  Change_lastSavedPage_Action(this.lastSavedPage);
}

class Change_megaData_Action {
  final String megaData;
  Change_megaData_Action(this.megaData);
}

class Add_Location_Action {
  final Location location;
  final IMAGE_TYPE imageType;
  Add_Location_Action(this.location, this.imageType);
}

class Remove_Location_Action {
  final Location location;
  final IMAGE_TYPE imageType;
  final int memberID;

  Remove_Location_Action(this.location, this.imageType, this.memberID);
}

class Add_IncomeImage_Action {
  final Location location;
  final IMAGE_TYPE imageType;
  final int memberID;
  Add_IncomeImage_Action(this.location, this.imageType, this.memberID);
}

class Remove_IncomeImage_Action {
  final Location location;
  final IMAGE_TYPE imageType;
  final int memberID;
  Remove_IncomeImage_Action(this.location, this.imageType, this.memberID);
}

class Remove_All_IncomeImage_Action {
  final int memberID;
  Remove_All_IncomeImage_Action(this.memberID);
}

class Update_InvoiceDate_Action {
  final DateTime uploadDate;

  Update_InvoiceDate_Action(this.uploadDate);
}

class Update_BankBookDate_Action {
  final Location location;
  final String st_time;
  final String end_time;
  final int memberID;
  Update_BankBookDate_Action(
      this.location, this.st_time, this.end_time, this.memberID);
}

class Add_FamilyMemberIncome {
  final FamilyMemberIncome familyMemberIncome;

  Add_FamilyMemberIncome(this.familyMemberIncome);
}

class Remove_FamilyMemberIncome {
  final FamilyMemberIncome familyMemberIncome;

  Remove_FamilyMemberIncome(this.familyMemberIncome);
}

class Update_FamilyMemberIncome {
  final FamilyMemberIncome updateFamilyMemberIncom;
  final int memberID;

  Update_FamilyMemberIncome(this.updateFamilyMemberIncom, this.memberID);
}
