// To parse this JSON data, do
//
//     final Data_State = Data_StateFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class Data_State {
  final String nameEng;
  final String nameChinese;
  final String idChar;
  final String idFirstNum;
  final String idLastNum;
  final String gender;
  final String hkCitizen;
  final String birthDay;
  final String contactNum;
  final String mobileContactNum;
  final String emailAddress;
  final String homeAddress;
  final String maritalStatus;
  final String maritalDetail;
  final String job;
  final String isDirector;
  final String bankNumber;
  final String bankName;
  final String bankAccountName;
  final List<FamilyMemberIncome> familyMemberIncome;
  final String fundedScheme;
  final String fundedSchemeDetail;
  final String isPublicHospial;
  final String treatmentLocation;
  final String treatmentDrug;
  final String treatmentSituation;
  final String treatmentStage;
  final String whereGetInfo;
  final String whereGetInfoDetail;
  final String whereGetInfoOthersDetail;
  final List<Location> hkidLocation;
  final List<Location> invoiceLocation;
  final List<Location> refLetterLocation;
  final List<Location> medicalReportLocation;
  final String invoicePrice;
  final String incomeCopyType;
  final List<Location> incomeCopyLocation;
  final bool isAgreePersonalDate;
  final bool isAgreeStatement;
  final bool isSentEmail;
  final String caseNumber;
  final bool isFinishApply;
  final String lastSavedPage;
  final String megaData;

  Data_State({
    @required this.nameEng,
    @required this.nameChinese,
    @required this.idChar,
    @required this.idFirstNum,
    @required this.idLastNum,
    @required this.gender,
    @required this.hkCitizen,
    @required this.birthDay,
    @required this.contactNum,
    @required this.mobileContactNum,
    @required this.emailAddress,
    @required this.homeAddress,
    @required this.maritalStatus,
    @required this.maritalDetail,
    @required this.job,
    @required this.isDirector,
    @required this.bankNumber,
    @required this.bankName,
    @required this.bankAccountName,
    @required this.familyMemberIncome,
    @required this.fundedScheme,
    @required this.fundedSchemeDetail,
    @required this.isPublicHospial,
    @required this.treatmentLocation,
    @required this.treatmentDrug,
    @required this.treatmentSituation,
    @required this.treatmentStage,
    @required this.whereGetInfo,
    @required this.whereGetInfoDetail,
    @required this.whereGetInfoOthersDetail,
    @required this.hkidLocation,
    @required this.invoiceLocation,
    @required this.refLetterLocation,
    @required this.medicalReportLocation,
    @required this.incomeCopyType,
    @required this.invoicePrice,
    @required this.incomeCopyLocation,
    @required this.isAgreePersonalDate,
    @required this.isAgreeStatement,
    @required this.isSentEmail,
    @required this.caseNumber,
    @required this.isFinishApply,
    @required this.lastSavedPage,
    @required this.megaData,
  });

  factory Data_State.initial() {
    return Data_State(
      nameEng: "",
      nameChinese: "",
      idChar: "",
      idFirstNum: "",
      idLastNum: "",
      gender: "",
      hkCitizen: "",
      birthDay: "",
      contactNum: "",
      mobileContactNum: "",
      emailAddress: "",
      homeAddress: "",
      maritalStatus: "",
      maritalDetail: "",
      job: "",
      isDirector: "",
      bankNumber: "",
      bankName: "",
      bankAccountName: "",
      familyMemberIncome: List<FamilyMemberIncome>(),
      fundedScheme: "",
      fundedSchemeDetail: "",
      isPublicHospial: "",
      treatmentLocation: "",
      treatmentDrug: "",
      treatmentSituation: "",
      treatmentStage: "",
      whereGetInfo: "",
      whereGetInfoDetail: "",
      whereGetInfoOthersDetail: "",
      hkidLocation: List<Location>(),
      invoiceLocation: List<Location>(),
      refLetterLocation: List<Location>(),
      medicalReportLocation: List<Location>(),
      incomeCopyType: "",
      invoicePrice: "",
      incomeCopyLocation: List<Location>(),
      isAgreePersonalDate: false,
      isAgreeStatement: false,
      isSentEmail: false,
      caseNumber: "",
      isFinishApply: false,
      lastSavedPage: "",
      megaData: "",
    );
  }

  Data_State copyWith({
    String nameEng,
    String nameChinese,
    String idChar,
    String idFirstNum,
    String idLastNum,
    String gender,
    String hkCitizen,
    String birthDay,
    String contactNum,
    String mobileContactNum,
    String emailAddress,
    String homeAddress,
    String maritalStatus,
    String maritalDetail,
    String job,
    String isDirector,
    String bankNumber,
    String bankName,
    String bankAccountName,
    List<FamilyMemberIncome> familyMemberIncome,
    String fundedScheme,
    String fundedSchemeDetail,
    String isPublicHospial,
    String treatmentLocation,
    String treatmentDrug,
    String treatmentSituation,
    String treatmentStage,
    String whereGetInfo,
    String whereGetInfoDetail,
    String whereGetInfoOthersDetail,
    List<Location> hkidLocation,
    List<Location> invoiceLocation,
    List<Location> refLetterLocation,
    List<Location> medicalReportLocation,
    String incomeCopyType,
    String invoicePrice,
    List<Location> incomeCopyLocation,
    bool isAgreePersonalDate,
    bool isAgreeStatement,
    bool isSentEmail,
    String caseNumber,
    bool isFinishApply,
    String lastSavedPage,
    String megaData,
  }) =>
      Data_State(
        nameEng: nameEng ?? this.nameEng,
        nameChinese: nameChinese ?? this.nameChinese,
        idChar: idChar ?? this.idChar,
        idFirstNum: idFirstNum ?? this.idFirstNum,
        idLastNum: idLastNum ?? this.idLastNum,
        gender: gender ?? this.gender,
        hkCitizen: hkCitizen ?? this.hkCitizen,
        birthDay: birthDay ?? this.birthDay,
        contactNum: contactNum ?? this.contactNum,
        mobileContactNum: mobileContactNum ?? this.mobileContactNum,
        emailAddress: emailAddress ?? this.emailAddress,
        homeAddress: homeAddress ?? this.homeAddress,
        maritalStatus: maritalStatus ?? this.maritalStatus,
        maritalDetail: maritalDetail ?? this.maritalDetail,
        job: job ?? this.job,
        isDirector: isDirector ?? this.isDirector,
        bankNumber: bankNumber ?? this.bankNumber,
        bankName: bankName ?? this.bankName,
        bankAccountName: bankAccountName ?? this.bankAccountName,
        familyMemberIncome: familyMemberIncome ?? this.familyMemberIncome,
        fundedScheme: fundedScheme ?? this.fundedScheme,
        fundedSchemeDetail: fundedSchemeDetail ?? this.fundedSchemeDetail,
        isPublicHospial: isPublicHospial ?? this.isPublicHospial,
        treatmentLocation: treatmentLocation ?? this.treatmentLocation,
        treatmentDrug: treatmentDrug ?? this.treatmentDrug,
        treatmentSituation: treatmentSituation ?? this.treatmentSituation,
        treatmentStage: treatmentStage ?? this.treatmentStage,
        whereGetInfo: whereGetInfo ?? this.whereGetInfo,
        whereGetInfoDetail: whereGetInfoDetail ?? this.whereGetInfoDetail,
        whereGetInfoOthersDetail:
            whereGetInfoOthersDetail ?? this.whereGetInfoOthersDetail,
        hkidLocation: hkidLocation ?? this.hkidLocation,
        invoiceLocation: invoiceLocation ?? this.invoiceLocation,
        refLetterLocation: refLetterLocation ?? this.refLetterLocation,
        medicalReportLocation:
            medicalReportLocation ?? this.medicalReportLocation,
        incomeCopyType: incomeCopyType ?? this.incomeCopyType,
        invoicePrice: invoicePrice ?? this.invoicePrice,
        incomeCopyLocation: incomeCopyLocation ?? this.incomeCopyLocation,
        isAgreePersonalDate: isAgreePersonalDate ?? this.isAgreePersonalDate,
        isAgreeStatement: isAgreeStatement ?? this.isAgreeStatement,
        isSentEmail: isSentEmail ?? this.isSentEmail,
        caseNumber: caseNumber ?? this.caseNumber,
        isFinishApply: isFinishApply ?? this.isFinishApply,
        lastSavedPage: lastSavedPage ?? this.lastSavedPage,
        megaData: megaData ?? this.megaData,
      );

  factory Data_State.fromJson(String str) =>
      Data_State.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Data_State.fromMap(Map<String, dynamic> json) => Data_State(
        nameEng: json["nameEng"] == null ? null : json["nameEng"],
        nameChinese: json["nameChinese"] == null ? null : json["nameChinese"],
        idChar: json["id_char"] == null ? null : json["id_char"],
        idFirstNum: json["id_firstNum"] == null ? null : json["id_firstNum"],
        idLastNum: json["id_lastNum"] == null ? null : json["id_lastNum"],
        gender: json["gender"] == null ? null : json["gender"],
        hkCitizen: json["hkCitizen"] == null ? null : json["hkCitizen"],
        birthDay: json["birthDay"] == null ? null : json["birthDay"],
        contactNum: json["contactNum"] == null ? null : json["contactNum"],
        mobileContactNum:
            json["mobileContactNum"] == null ? null : json["mobileContactNum"],
        emailAddress:
            json["emailAddress"] == null ? null : json["emailAddress"],
        homeAddress: json["homeAddress"] == null ? null : json["homeAddress"],
        maritalStatus:
            json["maritalStatus"] == null ? null : json["maritalStatus"],
        maritalDetail:
            json["maritalDetail"] == null ? null : json["maritalDetail"],
        job: json["job"] == null ? null : json["job"],
        isDirector: json["isDirector"] == null ? null : json["isDirector"],
        bankNumber: json["bankNumber"] == null ? null : json["bankNumber"],
        bankName: json["bankName"] == null ? null : json["bankName"],
        bankAccountName:
            json["bankAccountName"] == null ? null : json["bankAccountName"],
        familyMemberIncome: json["familyMemberIncome"] == null
            ? null
            : List<FamilyMemberIncome>.from(json["familyMemberIncome"]
                .map((x) => FamilyMemberIncome.fromMap(x))),
        fundedScheme:
            json["fundedScheme"] == null ? null : json["fundedScheme"],
        fundedSchemeDetail: json["fundedSchemeDetail"] == null
            ? null
            : json["fundedSchemeDetail"],
        isPublicHospial:
            json["isPublicHospial"] == null ? null : json["isPublicHospial"],
        treatmentLocation: json["treatmentLocation"] == null
            ? null
            : json["treatmentLocation"],
        treatmentDrug:
            json["treatmentDrug"] == null ? null : json["treatmentDrug"],
        treatmentSituation: json["treatmentSituation"] == null
            ? null
            : json["treatmentSituation"],
        treatmentStage:
            json["treatmentStage"] == null ? null : json["treatmentStage"],
        whereGetInfo:
            json["whereGetInfo"] == null ? null : json["whereGetInfo"],
        whereGetInfoDetail: json["whereGetInfoDetail"] == null
            ? null
            : json["whereGetInfoDetail"],
        whereGetInfoOthersDetail: json["whereGetInfoOthersDetail"] == null
            ? null
            : json["whereGetInfoOthersDetail"],
        hkidLocation: json["hkid_location"] == null
            ? null
            : List<Location>.from(
                json["hkid_location"].map((x) => Location.fromMap(x))),
        invoiceLocation: json["invoice_location"] == null
            ? null
            : List<Location>.from(
                json["invoice_location"].map((x) => Location.fromMap(x))),
        refLetterLocation: json["refLetter_location"] == null
            ? null
            : List<Location>.from(
                json["refLetter_location"].map((x) => Location.fromMap(x))),
        medicalReportLocation: json["medicalReport_location"] == null
            ? null
            : List<Location>.from(
                json["medicalReport_location"].map((x) => Location.fromMap(x))),
        incomeCopyType:
            json["incomeCopyType"] == null ? null : json["incomeCopyType"],
        invoicePrice:
            json["invoicePrice"] == null ? null : json["invoicePrice"],
        incomeCopyLocation: json["incomeCopy_location"] == null
            ? null
            : List<Location>.from(
                json["incomeCopy_location"].map((x) => Location.fromMap(x))),
        isAgreePersonalDate: json["isAgreePersonalDate"] == null
            ? null
            : json["isAgreePersonalDate"],
        isAgreeStatement:
            json["isAgreeStatement"] == null ? null : json["isAgreeStatement"],
        isSentEmail: json["isSentEmail"] == null ? null : json["isSentEmail"],
        caseNumber: json["caseNumber"] == null ? null : json["caseNumber"],
        isFinishApply:
            json["isFinishApply"] == null ? null : json["isFinishApply"],
        lastSavedPage:
            json["lastSavedPage"] == null ? null : json["lastSavedPage"],
        megaData: json["megaData"] == null ? null : json["megaData"],
      );

  Map<String, dynamic> toMap() => {
        "nameEng": nameEng == null ? null : nameEng,
        "nameChinese": nameChinese == null ? null : nameChinese,
        "id_char": idChar == null ? null : idChar,
        "id_firstNum": idFirstNum == null ? null : idFirstNum,
        "id_lastNum": idLastNum == null ? null : idLastNum,
        "gender": gender == null ? null : gender,
        "hkCitizen": hkCitizen == null ? null : hkCitizen,
        "birthDay": birthDay == null ? null : birthDay,
        "contactNum": contactNum == null ? null : contactNum,
        "mobileContactNum": mobileContactNum == null ? null : mobileContactNum,
        "emailAddress": emailAddress == null ? null : emailAddress,
        "homeAddress": homeAddress == null ? null : homeAddress,
        "maritalStatus": maritalStatus == null ? null : maritalStatus,
        "maritalDetail": maritalDetail == null ? null : maritalDetail,
        "job": job == null ? null : job,
        "isDirector": isDirector == null ? null : isDirector,
        "bankNumber": bankNumber == null ? null : bankNumber,
        "bankName": bankName == null ? null : bankName,
        "bankAccountName": bankAccountName == null ? null : bankAccountName,
        "familyMemberIncome": familyMemberIncome == null
            ? null
            : List<dynamic>.from(familyMemberIncome.map((x) => x.toMap())),
        "fundedScheme": fundedScheme == null ? null : fundedScheme,
        "fundedSchemeDetail":
            fundedSchemeDetail == null ? null : fundedSchemeDetail,
        "isPublicHospial": isPublicHospial == null ? null : isPublicHospial,
        "treatmentLocation":
            treatmentLocation == null ? null : treatmentLocation,
        "treatmentDrug": treatmentDrug == null ? null : treatmentDrug,
        "treatmentSituation":
            treatmentSituation == null ? null : treatmentSituation,
        "treatmentStage": treatmentStage == null ? null : treatmentStage,
        "whereGetInfo": whereGetInfo == null ? null : whereGetInfo,
        "whereGetInfoDetail":
            whereGetInfoDetail == null ? null : whereGetInfoDetail,
        "whereGetInfoOthersDetail":
            whereGetInfoOthersDetail == null ? null : whereGetInfoOthersDetail,
        "hkid_location": hkidLocation == null
            ? null
            : List<dynamic>.from(hkidLocation.map((x) => x.toMap())),
        "invoice_location": invoiceLocation == null
            ? null
            : List<dynamic>.from(invoiceLocation.map((x) => x.toMap())),
        "refLetter_location": refLetterLocation == null
            ? null
            : List<dynamic>.from(refLetterLocation.map((x) => x.toMap())),
        "medicalReport_location": medicalReportLocation == null
            ? null
            : List<dynamic>.from(medicalReportLocation.map((x) => x.toMap())),
        "incomeCopyType": incomeCopyType == null ? null : incomeCopyType,
        "invoicePrice": invoicePrice == null ? null : invoicePrice,
        "incomeCopy_location": incomeCopyLocation == null
            ? null
            : List<dynamic>.from(incomeCopyLocation.map((x) => x.toMap())),
        "isAgreePersonalDate":
            isAgreePersonalDate == null ? null : isAgreePersonalDate,
        "isAgreeStatement": isAgreeStatement == null ? null : isAgreeStatement,
        "isSentEmail": isSentEmail == null ? null : isSentEmail,
        "caseNumber": caseNumber == null ? null : caseNumber,
        "isFinishApply": isFinishApply == null ? null : isFinishApply,
        "lastSavedPage": lastSavedPage == null ? null : lastSavedPage,
        "megaData": megaData == null ? null : megaData,
      };
}

class FamilyMemberIncome {
  String id;
  String memNam;
  String memAge;
  String memGender;
  String memRelation;
  String memCareer;
  String memIncome;
  String incomeCopyType;
  List<Location> incomeCopyLocation;

  FamilyMemberIncome({
    @required this.id,
    @required this.memNam,
    @required this.memAge,
    @required this.memGender,
    @required this.memRelation,
    @required this.memCareer,
    @required this.memIncome,
    @required this.incomeCopyType,
    @required this.incomeCopyLocation,
  });

  factory FamilyMemberIncome.initial() {
    return FamilyMemberIncome(
        id: "0",
        memNam: "",
        memAge: "",
        memGender: "",
        memRelation: "",
        memCareer: "",
        memIncome: "",
        incomeCopyType: "",
        incomeCopyLocation: List<Location>());
  }

  FamilyMemberIncome copyWith({
    String memNam,
    String memAge,
    String memGender,
    String memRelation,
    String memCareer,
    String memIncome,
    String incomeCopyType,
    List<Location> incomeCopyLocation,
  }) =>
      FamilyMemberIncome(
        id: id ?? this.id,
        memNam: memNam ?? this.memNam,
        memAge: memAge ?? this.memAge,
        memGender: memGender ?? this.memGender,
        memRelation: memRelation ?? this.memRelation,
        memCareer: memCareer ?? this.memCareer,
        memIncome: memIncome ?? this.memIncome,
        incomeCopyType: incomeCopyType ?? this.incomeCopyType,
        incomeCopyLocation: incomeCopyLocation ?? this.incomeCopyLocation,
      );

  factory FamilyMemberIncome.fromJson(String str) =>
      FamilyMemberIncome.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory FamilyMemberIncome.fromMap(Map<String, dynamic> json) =>
      FamilyMemberIncome(
        id: json["id"] == null ? null : json["id"],
        memNam: json["memNam"] == null ? null : json["memNam"],
        memAge: json["memAge"] == null ? null : json["memAge"],
        memGender: json["memGender"] == null ? null : json["memGender"],
        memRelation: json["memRelation"] == null ? null : json["memRelation"],
        memCareer: json["memCareer"] == null ? null : json["memCareer"],
        memIncome: json["memIncome"] == null ? null : json["memIncome"],
        incomeCopyType:
            json["incomeCopyType"] == null ? null : json["incomeCopyType"],
        incomeCopyLocation: json["incomeCopy_location"] == null
            ? null
            : List<Location>.from(
                json["incomeCopy_location"].map((x) => Location.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "memNam": memNam == null ? null : memNam,
        "memAge": memAge == null ? null : memAge,
        "memGender": memGender == null ? null : memGender,
        "memRelation": memRelation == null ? null : memRelation,
        "memCareer": memCareer == null ? null : memCareer,
        "memIncome": memIncome == null ? null : memIncome,
        "incomeCopyType": incomeCopyType == null ? null : incomeCopyType,
        "incomeCopy_location": incomeCopyLocation == null
            ? null
            : List<dynamic>.from(incomeCopyLocation.map((x) => x.toMap())),
      };
}

class Location {
  String path;
  String stdate;
  String enddate;
  String uploadDate;

  Location({
    @required this.path,
    @required this.stdate,
    @required this.enddate,
    @required this.uploadDate,
  });

  factory Location.initial() {
    return Location(path: "", stdate: "", enddate: "", uploadDate: "");
  }

  factory Location.create(
      String _path, String _stdate, String _enddate, String _uploadDate) {
    return Location(
        path: _path,
        stdate: _stdate,
        enddate: _enddate,
        uploadDate: _uploadDate);
  }

  Location copyWith({
    String path,
    String stdate,
    String enddate,
    String uploadDate,
  }) =>
      Location(
        path: path ?? this.path,
        stdate: stdate ?? this.stdate,
        enddate: enddate ?? this.enddate,
        uploadDate: uploadDate ?? this.uploadDate,
      );

  factory Location.fromJson(String str) => Location.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Location.fromMap(Map<String, dynamic> json) => Location(
        path: json["path"] == null ? null : json["path"],
        stdate: json["stdate"] == null ? null : json["stdate"],
        enddate: json["enddate"] == null ? null : json["enddate"],
        uploadDate: json["uploadDate"] == null ? null : json["uploadDate"],
      );

  Map<String, dynamic> toMap() => {
        "path": path == null ? null : path,
        "stdate": stdate == null ? null : stdate,
        "enddate": enddate == null ? null : enddate,
        "uploadDate": uploadDate == null ? null : uploadDate,
      };
}
