import 'package:icanfight/psp/controller/SaveLoadImgController.dart';
import 'package:redux/redux.dart';

import 'dataPageAction.dart';
import 'dataState.dart';

final dataReducer = combineReducers<Data_State>([
  TypedReducer<Data_State, UpdateAppDataAction>(_updateAppDataReducer),
  TypedReducer<Data_State, TypingAction>(_typing2Reducer),
  TypedReducer<Data_State, Change_NameEng_Action>(_change_NameEng_Reducer),
  TypedReducer<Data_State, Change_NameChinese_Action>(
      _change_NameChinese_Reducer),
  TypedReducer<Data_State, SaveAppDataToLocalAction>(
      _saveAppDataToLocalReducer),
  TypedReducer<Data_State, Change_FamilyMemberIncome_Action>(
      _change_FamilyMemberIncome_Reducer),
  TypedReducer<Data_State, Change_IdChar_Action>(_change_IdChar_Action),
  TypedReducer<Data_State, Change_idFirstNum_Action>(_change_idFirstNum_Action),
  TypedReducer<Data_State, Change_idLastNum_Action>(_change_idLastNum_Action),
  TypedReducer<Data_State, Change_gender_Action>(_change_gender_Action),
  TypedReducer<Data_State, Change_hkCitizen_Action>(_change_hkCitizen_Action),
  TypedReducer<Data_State, Change_birthDay_Action>(_change_birthDay_Action),
  TypedReducer<Data_State, Change_contactNum_Action>(_change_contactNum_Action),
  TypedReducer<Data_State, Change_mobileContactNum_Action>(
      _change_mobileContactNum_Action),
  TypedReducer<Data_State, Change_emailAddress_Action>(
      _change_emailAddress_Action),
  TypedReducer<Data_State, Change_homeAddress_Action>(
      _change_homeAddress_Action),
  TypedReducer<Data_State, Change_maritalStatus_Action>(
      _change_maritalStatus_Action),
  TypedReducer<Data_State, Change_maritalDetail_Action>(
      _change_maritalDetail_Action),
  TypedReducer<Data_State, Change_job_Action>(_change_job_Action),
  TypedReducer<Data_State, Change_isDirector_Action>(_change_isDirector_Action),
  TypedReducer<Data_State, Change_bankNumber_Action>(_change_bankNumber_Action),
  TypedReducer<Data_State, Change_bankName_Action>(_change_bankName_Action),
  TypedReducer<Data_State, Change_bankAccountName_Action>(
      _change_bankAccountName_Action),
  TypedReducer<Data_State, Change_InvoicePrice_Action>(
      _change_InvoicePrice_Action),
  TypedReducer<Data_State, Add_Location_Action>(_add_Location_Action),
  TypedReducer<Data_State, Remove_Location_Action>(_remove_Location_Action),
  TypedReducer<Data_State, Add_IncomeImage_Action>(_add_IncomeImage_Action),
  TypedReducer<Data_State, Remove_IncomeImage_Action>(
      _remove_IncomeImage_Action),
  TypedReducer<Data_State, Remove_All_IncomeImage_Action>(
      _remove_all_IncomeImage_Action),
  TypedReducer<Data_State, Update_BankBookDate_Action>(
      _update_BankBookDate_Action),
  TypedReducer<Data_State, Update_InvoiceDate_Action>(
      _update_InvoiceDate_Action),
  TypedReducer<Data_State, Add_FamilyMemberIncome>(
      _add_FamilyMemberIncome_Action),
  TypedReducer<Data_State, Remove_FamilyMemberIncome>(
      _remove_FamilyMemberIncome_Action),
  TypedReducer<Data_State, Change_IncomeCopyType_Action>(
      _change_IncomeCopyType_Action),
  TypedReducer<Data_State, Update_FamilyMemberIncome>(
      _update_FamilyMemberIncome_Action),
  TypedReducer<Data_State, Change_fundedScheme_Action>(
      _change_fundedScheme_Action),
  TypedReducer<Data_State, Change_fundedSchemeDetail_Action>(
      _change_fundedSchemeDetail_Action),
  TypedReducer<Data_State, Change_isPublicHospial_Action>(
      _change_isPublicHospial_Action),
  TypedReducer<Data_State, Change_treatmentLocation_Action>(
      _change_treatmentLocation_Action),
  TypedReducer<Data_State, Change_treatmentDrug_Action>(
      _change_treatmentDrug_Action),
  TypedReducer<Data_State, Change_treatmentSituation_Action>(
      _change_treatmentSituation_Action),
  TypedReducer<Data_State, Change_treatmentStage_Action>(
      _change_treatmentStage_Action),
  TypedReducer<Data_State, Change_whereGetInfo_Action>(
      _change_whereGetInfo_Action),
  TypedReducer<Data_State, Change_whereGetInfoDetail_Action>(
      _change_whereGetInfoDetail_Action),
  TypedReducer<Data_State, Change_whereGetInfoOthersDetail_Action>(
      _change_whereGetInfoOthersDetail_Action),
  TypedReducer<Data_State, Change_invoicePrice_Action>(
      _change_invoicePrice_Action),
  TypedReducer<Data_State, Change_isAgreePersonalDate_Action>(
      _change_isAgreePersonalDate_Action),
  TypedReducer<Data_State, Change_isAgreeStatement_Action>(
      _change_isAgreeStatement_Action),
  TypedReducer<Data_State, Change_isSentEmail_Action>(
      _change_isSentEmail_Action),
  TypedReducer<Data_State, Change_caseNumber_Action>(_change_caseNumber_Action),
  TypedReducer<Data_State, Change_isFinishApply_Action>(
      _change_isFinishApply_Action),
  TypedReducer<Data_State, Change_lastSavedPage_Action>(
      _change_lastSavedPage_Action),
  TypedReducer<Data_State, Change_megaData_Action>(_change_megaData_Action)
]);

Data_State _saveAppDataToLocalReducer(
    Data_State state, SaveAppDataToLocalAction action) {
  print("_saveAppDataToLocalReducer");

  return state;
}

Data_State _updateAppDataReducer(Data_State state, UpdateAppDataAction action) {
  print("_updateAppDataReducer");

  return action.state;
}

Data_State _typing2Reducer(Data_State state, TypingAction action) {
  print("_typing2Reducer " + action.nameEng);

  return state.copyWith(nameEng: action.nameEng);
}

Data_State _change_NameEng_Reducer(
    Data_State state, Change_NameEng_Action action) {
  print("Change_NameEng_Action " + action.nameEng);

  return state.copyWith(nameEng: action.nameEng);
}

Data_State _change_NameChinese_Reducer(
    Data_State state, Change_NameChinese_Action action) {
  print("Change_NameChinese_Action " + action.nameChinese);

  return state.copyWith(nameChinese: action.nameChinese);
}

Data_State _change_IdChar_Action(
    Data_State state, Change_IdChar_Action action) {
  print("Change_IdChar_Action " + action.idChar);

  return state.copyWith(idChar: action.idChar);
}

Data_State _change_idFirstNum_Action(
    Data_State state, Change_idFirstNum_Action action) {
  print("Change_idFirstNum_Action " + action.idFirstNum);

  return state.copyWith(idFirstNum: action.idFirstNum);
}

Data_State _change_idLastNum_Action(
    Data_State state, Change_idLastNum_Action action) {
  print("Change_idLastNum_Action " + action.idLastNum);

  return state.copyWith(idLastNum: action.idLastNum);
}

Data_State _change_gender_Action(
    Data_State state, Change_gender_Action action) {
  print("Change_gender_Action " + action.gender);

  return state.copyWith(gender: action.gender);
}

Data_State _change_hkCitizen_Action(
    Data_State state, Change_hkCitizen_Action action) {
  print("Change_hkCitizen_Action " + action.hkCitizen);

  return state.copyWith(hkCitizen: action.hkCitizen);
}

Data_State _change_birthDay_Action(
    Data_State state, Change_birthDay_Action action) {
  print("Change_birthDay_Action " + action.birthDay);

  return state.copyWith(birthDay: action.birthDay);
}

Data_State _change_contactNum_Action(
    Data_State state, Change_contactNum_Action action) {
  print("Change_contactNum_Action " + action.contactNum);

  return state.copyWith(contactNum: action.contactNum);
}

Data_State _change_mobileContactNum_Action(
    Data_State state, Change_mobileContactNum_Action action) {
  print("Change_mobileContactNum_Action " + action.mobileContactNum);

  return state.copyWith(mobileContactNum: action.mobileContactNum);
}

Data_State _change_emailAddress_Action(
    Data_State state, Change_emailAddress_Action action) {
  print("Change_emailAddress_Action " + action.emailAddress);

  return state.copyWith(emailAddress: action.emailAddress);
}

Data_State _change_homeAddress_Action(
    Data_State state, Change_homeAddress_Action action) {
  print("Change_homeAddress_Action " + action.homeAddress);

  return state.copyWith(homeAddress: action.homeAddress);
}

Data_State _change_maritalStatus_Action(
    Data_State state, Change_maritalStatus_Action action) {
  print("Change_maritalStatus_Action " + action.maritalStatus);

  return state.copyWith(maritalStatus: action.maritalStatus);
}

Data_State _change_maritalDetail_Action(
    Data_State state, Change_maritalDetail_Action action) {
  print("_change_maritalDetail_Action " + action.maritalDetail);

  return state.copyWith(maritalDetail: action.maritalDetail);
}

Data_State _change_job_Action(Data_State state, Change_job_Action action) {
  print("Change_job_Action " + action.job);

  return state.copyWith(job: action.job);
}

Data_State _change_isDirector_Action(
    Data_State state, Change_isDirector_Action action) {
  print("Change_isDirector_Action " + action.isDirector);

  return state.copyWith(isDirector: action.isDirector);
}

Data_State _change_bankNumber_Action(
    Data_State state, Change_bankNumber_Action action) {
  print("Change_bankNumber_Action " + action.bankNumber);

  return state.copyWith(bankNumber: action.bankNumber);
}

Data_State _change_bankName_Action(
    Data_State state, Change_bankName_Action action) {
  print("Change_bankName_Action " + action.bankName);

  return state.copyWith(bankName: action.bankName);
}

Data_State _change_bankAccountName_Action(
    Data_State state, Change_bankAccountName_Action action) {
  print("Change_bankAccountName_Action " + action.bankAccountName);

  return state.copyWith(bankAccountName: action.bankAccountName);
}

Data_State _change_InvoicePrice_Action(
    Data_State state, Change_InvoicePrice_Action action) {
  print("_change_InvoicePrice_Action " + action.invoicePrice);

  return state.copyWith(invoicePrice: action.invoicePrice);
}

Data_State _change_FamilyMemberIncome_Reducer(
    Data_State state, Change_FamilyMemberIncome_Action action) {
  print("Change_FamilyMemberIncome_Action " +
      action.familyMemberIncome.toString());

  return state.copyWith(familyMemberIncome: action.familyMemberIncome);
}

Data_State _change_IncomeCopyType_Action(
    Data_State state, Change_IncomeCopyType_Action action) {
  print("_change_IncomeCopyType_Action + " + action.incomeCopyType.toString());

  if (action.memberID == -1) {
    if (action.incomeCopyType == null) {
      return state.copyWith(incomeCopyType: "");
    } else {
      return state.copyWith(incomeCopyType: action.incomeCopyType.toString());
    }
  } else {
    List<FamilyMemberIncome> tempFamilyMemberIncome =
        state.copyWith().familyMemberIncome;

    if (action.incomeCopyType == null) {
      tempFamilyMemberIncome[action.memberID].incomeCopyType = "";

      return state.copyWith(familyMemberIncome: tempFamilyMemberIncome);
    } else {
      tempFamilyMemberIncome[action.memberID].incomeCopyType =
          action.incomeCopyType.toString();

      return state.copyWith(familyMemberIncome: tempFamilyMemberIncome);
    }
  }
}

Data_State _add_Location_Action(Data_State state, Add_Location_Action action) {
  print("_add_Location_Action " + action.location.toString());

  switch (action.imageType) {
    case IMAGE_TYPE.hkid:
      return state.copyWith(
          hkidLocation: List.from(state.hkidLocation)..add(action.location));
      break;
    case IMAGE_TYPE.invoice:
      return state.copyWith(
          invoiceLocation: List.from(state.invoiceLocation)
            ..add(action.location));
      break;
    case IMAGE_TYPE.refletter:
      return state.copyWith(
          refLetterLocation: List.from(state.refLetterLocation)
            ..add(action.location));
      break;
    case IMAGE_TYPE.medicalreport:
      return state.copyWith(
          medicalReportLocation: List.from(state.medicalReportLocation)
            ..add(action.location));
      break;
    case IMAGE_TYPE.income_tax:
      return state.copyWith(
          incomeCopyLocation: List.from(state.incomeCopyLocation)
            ..add(action.location));
      break;
    case IMAGE_TYPE.bank_statement:
      return state.copyWith(
          incomeCopyLocation: List.from(state.incomeCopyLocation)
            ..add(action.location));
      break;
    case IMAGE_TYPE.passbook:
      return state.copyWith(
          incomeCopyLocation: List.from(state.incomeCopyLocation)
            ..add(action.location));
      break;
    default:
      return null;
      break;
  }
}

Data_State _remove_Location_Action(
    Data_State state, Remove_Location_Action action) {
  print("_remove_Location_Action " + action.location.path);
  print("_remove_Location_Action image type" + action.imageType.toString());

  if (action.memberID == -1) {
    switch (action.imageType) {
      case IMAGE_TYPE.hkid:
        return state.copyWith(
            hkidLocation: List.from(state.hkidLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.invoice:
        return state.copyWith(
            invoiceLocation: List.from(state.invoiceLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.refletter:
        return state.copyWith(
            refLetterLocation: List.from(state.refLetterLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.medicalreport:
        return state.copyWith(
            medicalReportLocation: List.from(state.medicalReportLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.income_tax:
        print("_remove_Location_Action inside IMAGE_TYPE.income_tax + " +
            action.location.path);
        return state.copyWith(
            incomeCopyLocation: List.from(state.incomeCopyLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.bank_statement:
        return state.copyWith(
            incomeCopyLocation: List.from(state.incomeCopyLocation)
              ..removeWhere((item) => item == action.location));
        break;
      case IMAGE_TYPE.passbook:
        return state.copyWith(
            incomeCopyLocation: List.from(state.incomeCopyLocation)
              ..removeWhere((item) => item == action.location));
        break;
      default:
        return null;
        break;
    }
  } else {
    print("_remove_Location_Action " + action.memberID.toString());
    List<FamilyMemberIncome> tempFamilyMemberIncome =
        state.copyWith().familyMemberIncome;

    tempFamilyMemberIncome[action.memberID].incomeCopyLocation
      ..removeWhere((item) => item == action.location);

    return state.copyWith(familyMemberIncome: tempFamilyMemberIncome);
  }
}

Data_State _add_IncomeImage_Action(
    Data_State state, Add_IncomeImage_Action action) {
  print("_add_IncomeImage_Action ");

//-1 = user
  if (action.memberID == -1) {
    return state.copyWith(
        incomeCopyLocation: List.from(state.incomeCopyLocation)
          ..add(action.location));
  } else {
    print("member " + action.memberID.toString());
    Data_State temp = state.copyWith();
    print("before add " + temp.toJson().toString());

    temp.familyMemberIncome[action.memberID].incomeCopyLocation
        .add(action.location);

    print("after add " + temp.toJson().toString());

    return temp;
  }
}

Data_State _remove_IncomeImage_Action(
    Data_State state, Remove_IncomeImage_Action action) {
  print("_remove_IncomeImage_Action ");

//-1 = user
  if (action.memberID == -1) {
    return state.copyWith(
        incomeCopyLocation: List.from(state.incomeCopyLocation)
          ..removeWhere((item) => item == action.location));
  } else {
    Data_State temp = state.copyWith();
    temp.familyMemberIncome[action.memberID].incomeCopyLocation
      ..removeWhere((item) => item == action.location);
    return temp;

    /*   familyMemberIncome: List.from(
            state.familyMemberIncome[action.memberID].incomeCopyLocation)
          ..add(action.location)); */
  }
}

Data_State _remove_all_IncomeImage_Action(
    Data_State state, Remove_All_IncomeImage_Action action) {
  print("_remove_all_IncomeImage_Action ");

  if (action.memberID == -1) {
    return state.copyWith(incomeCopyLocation: []);
  } else {
    Data_State temp = state.copyWith();

    temp.familyMemberIncome[action.memberID].incomeCopyLocation = [];

    return temp;
  }

  //return state;
}

Data_State _update_InvoiceDate_Action(
    Data_State state, Update_InvoiceDate_Action action) {
  Data_State temp = state.copyWith();

  temp.invoiceLocation[0].uploadDate = action.uploadDate.toIso8601String();
  return temp;
}

Data_State _update_BankBookDate_Action(
    Data_State state, Update_BankBookDate_Action action) {
  print("_update_BankBookDate_Action ");
  print("_update_BankBookDate_Action start time " + action.st_time);
  print("_update_BankBookDate_Action start time " + action.end_time);
//-1 = user
  if (action.memberID == -1) {
    Data_State temp = state.copyWith();

    temp.incomeCopyLocation
        .firstWhere((item) => item == action.location)
        .stdate = action.st_time;
    temp.incomeCopyLocation
        .firstWhere((item) => item == action.location)
        .enddate = action.end_time;
// Location update_location =  Location.create(tempLocation.path, action.st_time, action.end_time, tempLocation.uploadDate);

/*     List<Location> a = temp.incomeCopyLocation
      ..where((item) => item == action.location);

    return state.copyWith(
        incomeCopyLocation: List.from(state.incomeCopyLocation)
          ..add(action.location)); */

    return temp;
  } else {
/*     print("member " + action.memberID.toString());
    Data_State temp = state.copyWith();
    print("before add " + temp.toJson().toString());

    temp.familyMemberIncome[action.memberID].incomeCopyLocation
        .add(action.location);

    print("after add " + temp.toJson().toString());
 */

    Data_State temp = state.copyWith();
    temp.familyMemberIncome[action.memberID].incomeCopyLocation
        .firstWhere((item) => item == action.location)
        .stdate = action.st_time;
    temp.familyMemberIncome[action.memberID].incomeCopyLocation
        .firstWhere((item) => item == action.location)
        .enddate = action.end_time;

    return temp;
  }

/* 
   return state.copyWith(
      familyMemberIncome: state.familyMemberIncome
        ..removeAt(action.memberID)
        ..insert(action.memberID, action.updateFamilyMemberIncom));
 */
}

Data_State _add_FamilyMemberIncome_Action(
    Data_State state, Add_FamilyMemberIncome action) {
  return state.copyWith(
      familyMemberIncome: List.from(state.familyMemberIncome)
        ..add(action.familyMemberIncome));
}

Data_State _remove_FamilyMemberIncome_Action(
    Data_State state, Remove_FamilyMemberIncome action) {
  print("_remove_FamilyMemberIncome_Action");

//TODO remove image btw

  return state.copyWith(
      familyMemberIncome: List.from(state.familyMemberIncome)
        ..removeWhere((item) => item == action.familyMemberIncome));
}

Data_State _update_FamilyMemberIncome_Action(
    Data_State state, Update_FamilyMemberIncome action) {
  print("_update_FamilyMemberIncome_Action");

/* Remove_FamilyMemberIncome(state.familyMemberIncome[action.memberID]);
   Data_State temp = state.copyWith(); //[0,1,2,3,4,5] */

  //temp.familyMemberIncome.removeAt(action.memberID);  //remove 1, //[0,2,3,4,5]
  //temp.familyMemberIncome.insert(action.memberID, action.familyMemberIncome); //insert 1 //[0,1,2,3,4,5]
//TODO remove image in local storage
  return state.copyWith(
      familyMemberIncome: state.familyMemberIncome
        ..removeAt(action.memberID)
        ..insert(action.memberID, action.updateFamilyMemberIncom));
}

Data_State _change_fundedScheme_Action(
    Data_State state, Change_fundedScheme_Action action) {
  print("_change_fundedScheme_Action " + action.fundedScheme);

  return state.copyWith(fundedScheme: action.fundedScheme);
}

Data_State _change_fundedSchemeDetail_Action(
    Data_State state, Change_fundedSchemeDetail_Action action) {
  print("_change_fundedSchemeDetail_Action " + action.fundedSchemeDetail);

  return state.copyWith(fundedSchemeDetail: action.fundedSchemeDetail);
}

Data_State _change_isPublicHospial_Action(
    Data_State state, Change_isPublicHospial_Action action) {
  print("_change_isPublicHospial_Action " + action.isPublicHospial);

  return state.copyWith(isPublicHospial: action.isPublicHospial);
}

Data_State _change_treatmentLocation_Action(
    Data_State state, Change_treatmentLocation_Action action) {
  print("_change_treatmentLocation_Action " + action.treatmentLocation);

  return state.copyWith(treatmentLocation: action.treatmentLocation);
}

Data_State _change_treatmentDrug_Action(
    Data_State state, Change_treatmentDrug_Action action) {
  print("_change_treatmentDrug_Action " + action.treatmentDrug);

  return state.copyWith(treatmentDrug: action.treatmentDrug);
}

Data_State _change_treatmentSituation_Action(
    Data_State state, Change_treatmentSituation_Action action) {
  print("Change_job_Action " + action.treatmentSituation);

  return state.copyWith(treatmentSituation: action.treatmentSituation);
}

Data_State _change_treatmentStage_Action(
    Data_State state, Change_treatmentStage_Action action) {
  print("_change_treatmentStage_Action " + action.treatmentStage);

  return state.copyWith(treatmentStage: action.treatmentStage);
}

Data_State _change_whereGetInfo_Action(
    Data_State state, Change_whereGetInfo_Action action) {
  print("_change_whereGetInfo_Action " + action.whereGetInfo);

  return state.copyWith(whereGetInfo: action.whereGetInfo);
}

Data_State _change_whereGetInfoDetail_Action(
    Data_State state, Change_whereGetInfoDetail_Action action) {
  print("_change_whereGetInfoDetail_Action " + action.whereGetInfoDetail);

  return state.copyWith(whereGetInfoDetail: action.whereGetInfoDetail);
}

Data_State _change_whereGetInfoOthersDetail_Action(
    Data_State state, Change_whereGetInfoOthersDetail_Action action) {
  print("_change_whereGetInfoOthersDetail_Action " +
      action.whereGetInfoOthersDetail);

  return state.copyWith(
      whereGetInfoOthersDetail: action.whereGetInfoOthersDetail);
}

Data_State _change_invoicePrice_Action(
    Data_State state, Change_invoicePrice_Action action) {
  print("_change_invoicePrice_Action " + action.invoicePrice);

  return state.copyWith(invoicePrice: action.invoicePrice);
}

Data_State _change_isAgreePersonalDate_Action(
    Data_State state, Change_isAgreePersonalDate_Action action) {
  print("_change_isAgreePersonalDate_Action " +
      action.isAgreePersonalDate.toString());

  return state.copyWith(isAgreePersonalDate: action.isAgreePersonalDate);
}

Data_State _change_isAgreeStatement_Action(
    Data_State state, Change_isAgreeStatement_Action action) {
  print(
      "_change_isAgreeStatement_Action " + action.isAgreeStatement.toString());

  return state.copyWith(isAgreeStatement: action.isAgreeStatement);
}

Data_State _change_isSentEmail_Action(
    Data_State state, Change_isSentEmail_Action action) {
  print("_change_isSentEmail_Action " + action.isSentEmail.toString());

  return state.copyWith(isSentEmail: action.isSentEmail);
}

Data_State _change_caseNumber_Action(
    Data_State state, Change_caseNumber_Action action) {
  print("_change_caseNumber_Action " + action.caseNumber);

  return state.copyWith(caseNumber: action.caseNumber);
}

Data_State _change_isFinishApply_Action(
    Data_State state, Change_isFinishApply_Action action) {
  print("_change_isFinishApply_Action " + action.isFinishApply.toString());

  return state.copyWith(isFinishApply: action.isFinishApply);
}

Data_State _change_lastSavedPage_Action(
    Data_State state, Change_lastSavedPage_Action action) {
  print("_change_lastSavedPage_Action " + action.lastSavedPage);

  return state.copyWith(lastSavedPage: action.lastSavedPage);
}

Data_State _change_megaData_Action(
    Data_State state, Change_megaData_Action action) {
  print("_change_megaData_Action " + action.megaData);

  return state.copyWith(megaData: action.megaData);
}
