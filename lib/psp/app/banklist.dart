final List<Map<String, String>> bankList = [
  {
    "code": "003",
    "nameChi": "渣打銀行(香港)有限公司",
    "nameEng": "Standard Chartered Hong Kong"
  },
  {
    "code": "004",
    "nameChi": "香港上海滙豐銀行有限公司",
    "nameEng": "Hongkong and Shanghai Banking Corporation"
  },
  {
    "code": "009",
    "nameChi": "中國建設銀行(亞洲)股份有限公司",
    "nameEng": "China Construction Bank (Asia)"
  },
  {
    "code": "012",
    "nameChi": "中國銀行(香港)有限公司",
    "nameEng": "Bank of China (Hong Kong)"
  },
  {
    "code": "015",
    "nameChi": "東亞銀行有限公司",
    "nameEng": "The Bank of East Asia Limited"
  },
  {"code": "016", "nameChi": "星展銀行(香港)有限公司", "nameEng": "DBS Bank (Hong Kong)"},
  {
    "code": "018",
    "nameChi": "中信銀行國際有限公司",
    "nameEng": "China CITIC Bank International"
  },
  {"code": "020", "nameChi": "永隆銀行有限公司", "nameEng": "Wing Lung Bank"},
  {"code": "024", "nameChi": "恒生銀行有限公司", "nameEng": "Hang Seng Bank"},
  {
    "code": "025",
    "nameChi": "上海商業銀行有限公司",
    "nameEng": "Shanghai Commercial Bank"
  },
  {
    "code": "027",
    "nameChi": "交通銀行(香港)有限公司",
    "nameEng": "Bank of Communications (Hong Kong)"
  },
  {"code": "027", "nameChi": "東南全球銀行", "nameEng": "Southeast Global Bank"},
  {
    "code": "028",
    "nameChi": "大眾銀行(香港)有限公司",
    "nameEng": "Public Bank (Hong Kong)"
  },
  {"code": "035", "nameChi": "華僑永亨銀行有限公司", "nameEng": "OCBC Wing Hang Bank"},
  {"code": "038", "nameChi": "大有銀行有限公司", "nameEng": "Tai Yau Bank"},
  {
    "code": "039",
    "nameChi": "集友銀行有限公司",
    "nameEng": "Chiyu Banking Corporation"
  },
  {"code": "040", "nameChi": "大新銀行有限公司", "nameEng": "Dah Sing Bank"},
  {"code": "041", "nameChi": "創興銀行有限公司", "nameEng": "Chong Hing Bank"},
  {
    "code": "043",
    "nameChi": "南洋商業銀行有限公司",
    "nameEng": "Nanyang Commercial Bank"
  },
  {"code": "061", "nameChi": "大生銀行有限公司", "nameEng": "Tai Sang Bank Limited"},
  {
    "code": "072",
    "nameChi": "中國工商銀行(亞洲)有限公司",
    "nameEng": "Industrial and Commercial Bank of China (Asia)"
  },
  {
    "code": "128",
    "nameChi": "富邦銀行(香港)有限公司",
    "nameEng": "Fubon Bank (Hong Kong)"
  },
  {"code": "250", "nameChi": "花旗銀行(香港)有限公司", "nameEng": "Citibank (Hong Kong)"}
];
