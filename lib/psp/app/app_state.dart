import 'package:icanfight/psp/app/userState.dart';

import 'package:meta/meta.dart';

import 'data/dataState.dart';

@immutable
class AppState {
  User_State userState;
  Data_State dataState;

  AppState({@required this.userState, @required this.dataState});

  factory AppState.initial() {
    return AppState(
        userState: User_State.initial(), dataState: Data_State.initial());
  }
  /*
AppState copyWith({
    bool reduxSetup,
    Form_State formState,
  }){
    return AppState(
      reduxSetup: reduxSetup ?? this.reduxSetup,
      formState: formState ?? this.formState
    );
  }
  */

}
