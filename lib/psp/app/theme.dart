import 'package:flutter/material.dart';

import 'config.dart';

ThemeData customeThemeData = ThemeData(
  brightness: Brightness.light,
  primaryColor: config_primaryColour,
  accentColor: Colors.cyan[600],

  secondaryHeaderColor: config_primaryColour,
  errorColor: config_progressBarColour,
  disabledColor: config_disableBtnColour,
  dividerColor: config_primaryColour,
  highlightColor: config_progressBarColour,
  buttonColor: config_submitBtnColour,

  fontFamily: 'Raleway',

  // Define the default TextTheme. Use this to specify the default
  // text styling for headlines, titles, bodies of text, and more.

  textTheme: TextTheme(
    overline: TextStyle(
        color: config_pageTitleColour,
        fontSize: 14.0,
        fontStyle: FontStyle.normal),
    button: TextStyle(
        color: config_primaryColour,
        fontSize: 20.0,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w600),
    bodyText1: TextStyle(
        color: config_bodyTextColour,
        fontSize: 19.0,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w700),
    bodyText2: TextStyle(
        color: config_bodyTextColour,
        // fontFamily: 'Rawline',
        fontSize: 20.0,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w400),
    caption: TextStyle(
        color: config_bodyTextColour,
        fontSize: 17.0,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w500),
    subtitle1: TextStyle(
        color: config_pageTitleColour,
        fontSize: 31.0,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w500),

    /* headline1: 
    display2:,
    display1:,
    headline:,
    title: ,
    subhead:,
    body1:TextStyle(fontSize: 20, fontStyle: FontStyle.normal),
    body2:,
    caption:,
    button:,
    subtitle,
    overline: TextStyle(fontSize: 14.0, fontStyle: FontStyle.normal)

    button: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'), */
  ),
);

final TextStyle titleTxtStyle = TextStyle(
    color: Colors.white,
    fontSize: 31,
    fontWeight: FontWeight.w600,
    fontFamily: 'Raleway');
final TextStyle subtitleTxtStyle = TextStyle(
    color: config_primaryColour,
    fontWeight: FontWeight.w600,
    fontFamily: 'Raleway',
    fontSize: 16);
final TextStyle homePageSupportPlanTxtStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w700);
final TextStyle aboutPageBodyStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontFamily: 'Raleway',
    height: 1.65);
final TextStyle aboutPageSubtitleStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w700,
    fontFamily: 'NotoSansTC',
    height: 1.65);
final TextStyle aboutPageSubbodyStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontFamily: 'NotoSansTC',
    height: 1.6);
final TextStyle aboutPageNumberStyle = TextStyle(
    color: Colors.white,
    fontSize: 27.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    fontFamily: 'Raleway',
    height: 1.148);
final TextStyle aboutPageNumberInactiveStyle = TextStyle(
    color: config_primaryColour,
    fontSize: 27.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    fontFamily: 'Raleway',
    height: 1.148);
final TextStyle aboutPageMinStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 27.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w300,
    fontFamily: 'Raleway',
    height: 1.148);
final TextStyle aboutPageInfoStyle = TextStyle(
    color: config_pageTitleColour,
    fontSize: 14.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w500);
final TextStyle formPageTitleStyle = TextStyle(
    color: config_pageTitleColour,
    fontSize: 31.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w700);
final TextStyle formPageInstructStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 19.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w700,
    height: 1.15789);
final TextStyle progressBarStyle = TextStyle(
    color: config_progressBarTxtColour,
    fontSize: 14.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rawline',
    height: 1.2);
final TextStyle placeholderStyle = TextStyle(
    color: config_placeholderColour,
    fontSize: 16.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w700);
final TextStyle txtPlaceholderStyle = TextStyle(
    color: config_txtfieldPlaceholderColour,
    fontSize: 14.0,
    fontStyle: FontStyle.italic,
    fontFamily: 'Rawline',
    fontWeight: FontWeight.w500);
final TextStyle textfieldStyle = TextStyle(
    fontFamily: 'Rawline',
    color: config_bodyTextColour,
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400);

final TextStyle dropdownStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 17.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Rawline',
    fontWeight: FontWeight.w600);
final TextStyle memberTitleStyle = TextStyle(
    color: config_memberTitleColour,
    fontSize: 20,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w700);
final TextStyle defineTitleStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 22.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w600);
final TextStyle dropdownPlaceholderStyle = TextStyle(
    color: config_txtfieldPlaceholderColour, //config_dropdownPlaceholderColour,
    fontFamily: 'Raleway',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.italic);//italic
final TextStyle dropdownTxtFieldStyle = TextStyle(
    color: config_bodyTextColour,
    fontFamily: 'Raleway',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);
final TextStyle mcTxtFieldPlaceholderStyle = TextStyle(
    color: config_txtfieldPlaceholderColour,
    fontFamily: 'Raleway',
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.italic);//italic
final TextStyle mcTxtFieldStyle = TextStyle(
    color: config_bodyTextColour,
    fontFamily: 'Raleway',
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);
final TextStyle updateDocInfoTextStyle = TextStyle(
    color: config_pageTitleColour,
    fontFamily: 'Raleway',
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);
    final TextStyle updateDocInfoDateStyle = TextStyle(
    color: config_pageTitleColour,
    fontFamily: 'Rawline',
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);
final TextStyle invoiceBtnTextStyle = TextStyle(
    color: config_primaryColour,
    fontFamily: 'Rawline',
    fontSize: 16,
    fontWeight: FontWeight.w700,
    decoration: TextDecoration.underline,
    fontStyle: FontStyle.normal);
final TextStyle invoiceBtnText2Style = TextStyle(
    color: config_bodyTextColour,
    fontFamily: 'Rawline',
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);
final TextStyle updateDocPageTextStyle = TextStyle(
    color: config_nextPageBtnColour,
    fontFamily: 'Raleway',
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
final TextStyle removeBtnTextStyle = TextStyle(
    color: config_nextPageBtnColour,
    fontFamily: 'Raleway',
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal);

final TextStyle saveBtntextStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: 'Raleway',
    fontSize: 17);
final TextStyle submitBtnTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 20,
    fontWeight: FontWeight.w600,
    fontFamily: 'Raleway');
final TextStyle step3BodyTextStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontFamily: 'Raleway',
    height: 1.625);
final TextStyle step3FinishTitleStyle = TextStyle(
    color: config_primaryColour,
    fontSize: 27.0,
    fontWeight: FontWeight.w700,
    fontFamily: 'Raleway');
final TextStyle step3FinishBodyStyle = TextStyle(
    color: config_bodyTextColour,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rawline');
final TextStyle step3FinishAlertStyle = TextStyle(
    color: config_progressBarColour,
    fontSize: 19,
    fontWeight: FontWeight.w700,
    fontFamily: 'Rawline',
    height: 1.15);
final TextStyle stepperTxtStyle = TextStyle(
    color: Color.fromARGB(255, 20, 20, 20),
    fontSize: 20.0,
    fontStyle: FontStyle.normal,
    fontFamily: 'Raleway',
    fontWeight: FontWeight.w700);
final TextStyle photoNoticeStyle = TextStyle(
  color: Colors.white,
  fontSize: 14.0,
  fontStyle: FontStyle.normal,
  fontFamily: 'Raleway',
  fontWeight: FontWeight.w400
);

//Clients
final ThemeData kLightHealthTheme = _buildLightTheme();
final ThemeData kDarkGalleryTheme = _buildDarkTheme();
TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    title: base.title.copyWith(
      fontFamily: 'GoogleSans',
    ),
  );
}

ThemeData _buildDarkTheme() {
  const Color primaryColor = Color(0xFF0175c2);
  const Color secondaryColor = Color(0xFF13B9FD);
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary: primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
    accentColorBrightness: Brightness.dark,
    primaryColor: primaryColor,
    primaryColorDark: const Color(0xFF0050a0),
    primaryColorLight: secondaryColor,
    buttonColor: primaryColor,
    indicatorColor: Colors.white,
    toggleableActiveColor: const Color(0xFF6997DF),
    accentColor: secondaryColor,
    canvasColor: const Color(0xFF202124),
    scaffoldBackgroundColor: const Color(0xFF202124),
    backgroundColor: const Color(0xFF202124),
    errorColor: const Color(0xFFB00020),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
  );
  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildTextTheme(base.accentTextTheme),
  );
}

ThemeData _buildLightTheme() {
  const Color primaryColor = Color(0xFFfa6f9c);
  const Color secondaryColor = Color(0xFFfa6f9c);
  const Color lightGreenColor = Color(0xFF39C4BE);
  final ColorScheme colorScheme = const ColorScheme.light().copyWith(
    primary: primaryColor,
    secondary: secondaryColor,
  );
  final ThemeData base = ThemeData(
    cardTheme: CardTheme(
      elevation: 4,
    ),
    fontFamily: "Raleway",
    brightness: Brightness.light,
    accentColorBrightness: Brightness.dark,
    colorScheme: colorScheme,
    cursorColor: primaryColor,
    primaryColor: primaryColor,
    buttonColor: Color(0xFFE14672),
    indicatorColor: Colors.white,
    textSelectionHandleColor: primaryColor,
    toggleableActiveColor: primaryColor,
    splashColor: Colors.white24,
    splashFactory: InkRipple.splashFactory,
    accentColor: primaryColor,
    canvasColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    backgroundColor: Colors.white,
    errorColor: const Color(0xFFB00020),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
  );
  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildTextTheme(base.accentTextTheme),
  );
}
