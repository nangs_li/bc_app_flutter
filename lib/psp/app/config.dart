import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

bool isNeedConnectServer = false;
String userData_localPath = 'userData.json';
String appData_localPath = 'appData.json';
String uiData_localPath = 'uiData.json';
//String imageDirectory = "image";

String userData_api =
    '/Dynamic/Get?token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjRjNTIyMGFmODgwMzljNTNlN2IyNzAiLCJpYXQiOjE1NTk3ODY2MTF9.bceUONlTcDtgxALwJmySYj7gEFOrQyORLCHR7e5XL9JL1aJVgno9hhDmGjH5ThKH0OVtqPdOoWmX5hOSpxPMAe03BdCYzLBIRchMEx75ULbWAC-ZYpsfbJvEatWqhHIQa5YQwyuNfma8mIfFt0Ja-dm4aZo9Jn5mWMq2ij-zCs74diTrY0zEfW0iVsG24pudk_VmhYl72EUzTUulUx12v0HLwh4j05g27f2XvZEhCvbqeOK6RWv5XyUTdtu0v0qDvff5zdeYSTWwsNXQwXx5gp3iQsmYHHprgeHV_u3GTGfsbscAu4FK67H-Xx061qCsKspLxEyX3togzCTkbNbt6mahlWLyRWTnyK4oURYujwlxOBMT3_FpNVMEupVyWFWZFzFY_sD4a_nEeZbFfXYcgQ3BLh1o3WD8zWH1ZXeknDaBDl1ecWQrmmFXdzDWUVcbNIiHCl2SziZ24JOL1FGFT3zItg4qq7bzOE_pYPWXMncPM5Kt_GxHL-DeGcyhoaeyfEWGg5595BkL_g9drud9jpJct-R53KDaIl10Q2PJ2jRr3cf7fxK2FxED5Z4raDDtZHRihKeS5TOLwLnXOsndkI0r5MS5mik8D0sNKAQHn2M3xKJQ7wk77wRme_u5cKsUgCPbAs65zJXaBSAKXSzxlGAm7wqKn19-bVJCHsQHRrc&_id=5e4f507d2e34cd4b50fa879d';
String uiData_api =
    'Dynamic/Get?token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjRjNTIyMGFmODgwMzljNTNlN2IyNzAiLCJpYXQiOjE1NTk3ODY2MTF9.bceUONlTcDtgxALwJmySYj7gEFOrQyORLCHR7e5XL9JL1aJVgno9hhDmGjH5ThKH0OVtqPdOoWmX5hOSpxPMAe03BdCYzLBIRchMEx75ULbWAC-ZYpsfbJvEatWqhHIQa5YQwyuNfma8mIfFt0Ja-dm4aZo9Jn5mWMq2ij-zCs74diTrY0zEfW0iVsG24pudk_VmhYl72EUzTUulUx12v0HLwh4j05g27f2XvZEhCvbqeOK6RWv5XyUTdtu0v0qDvff5zdeYSTWwsNXQwXx5gp3iQsmYHHprgeHV_u3GTGfsbscAu4FK67H-Xx061qCsKspLxEyX3togzCTkbNbt6mahlWLyRWTnyK4oURYujwlxOBMT3_FpNVMEupVyWFWZFzFY_sD4a_nEeZbFfXYcgQ3BLh1o3WD8zWH1ZXeknDaBDl1ecWQrmmFXdzDWUVcbNIiHCl2SziZ24JOL1FGFT3zItg4qq7bzOE_pYPWXMncPM5Kt_GxHL-DeGcyhoaeyfEWGg5595BkL_g9drud9jpJct-R53KDaIl10Q2PJ2jRr3cf7fxK2FxED5Z4raDDtZHRihKeS5TOLwLnXOsndkI0r5MS5mik8D0sNKAQHn2M3xKJQ7wk77wRme_u5cKsUgCPbAs65zJXaBSAKXSzxlGAm7wqKn19-bVJCHsQHRrc&_id=5e53a964cb0db652cc53fc22';
String base_api = "http://52.221.246.164/PSPAPI";// "http://137.116.165.156:8081";
String getCase_api = "/Api/Case/GET";
String appToken = "e36fba0980484bd2f13825397ad72ae19e8f46c45bc03026da413eb269085c5d";

Map<String, dynamic> uiData_Text = {};

final Color config_primaryColour =
    const Color.fromARGB(255, 251, 114, 156); //header color,
final Color config_submitBtnColour = const Color.fromARGB(255, 255, 70, 114);
final Color config_disableBtnColour = const Color.fromARGB(255, 246, 200, 210);
final Color config_progressBarColour = const Color.fromARGB(255, 255, 54, 116);
final Color config_pageTitleColour = const Color.fromARGB(255, 243, 110, 152);
final Color config_normalBGColour = const Color.fromARGB(255, 255, 235, 236);

final Color config_bodyTextColour = const Color.fromARGB(255, 85, 85, 85);

final Color config_borderColour = const Color.fromARGB(25, 255, 70, 114);
final Color config_boxshadowColour = const Color.fromARGB(12, 74, 0, 22);
final Color config_nextPageBtnColour = const Color.fromARGB(255, 225, 70, 114);

final Color config_nextPageDisableBtnColour =
    const Color.fromARGB(255, 246, 200, 210);
final Color config_alertColour = const Color.fromARGB(255, 255, 70, 114);
final Color config_nextPageBtnShadowColour =
    const Color.fromARGB(102, 80, 15, 76);
final Color config_nextPageBtnDisableShadowColour =
    const Color.fromARGB(25, 80, 15, 76);

final Color config_progressBarTxtColour = const Color.fromARGB(255, 52, 52, 52);
final Color config_placeholderColour = const Color.fromARGB(255, 146, 145, 172);
final Color config_txtfieldPlaceholderColour = const Color.fromARGB(255, 216, 216, 226);
final Color config_checkBoxBorderColour =
    const Color.fromARGB(255, 199, 200, 201);
final Color config_memberTitleColour = const Color.fromARGB(255, 20, 20, 20);
final Color config_dropdownPlaceholderColour =
    const Color.fromARGB(255, 179, 179, 179);

final BoxShadow config_boxshadow = BoxShadow(
    color: config_boxshadowColour, offset: Offset(8, 8), blurRadius: 10.0);
final BoxShadow config_btnshadow = BoxShadow(
    color: config_nextPageBtnShadowColour,
    offset: Offset(8, 8),
    blurRadius: 30);
final BoxShadow config_disableBtnShadow = BoxShadow(
    color: config_nextPageBtnDisableShadowColour,
    offset: Offset(8, 8),
    blurRadius: 30);
final BoxShadow config_checkBoxshadow = BoxShadow(
    color: config_nextPageBtnShadowColour.withAlpha(51),
    offset: Offset(0, 3),
    blurRadius: 20);
final BoxShadow config_cardShadow = BoxShadow(
    color: config_nextPageBtnShadowColour.withAlpha(41),
    offset: Offset(0, 3),
    blurRadius: 30);
// Zip Images
final int zipQuality = 50;
final int zipWidthHeight = 800;
// MC Split
final String mcSpliter = "&A&4&";

// Clients
const Color defaultPinkColor = Color(0xFFFFEBEC);
const textFieldColor = Color(0xfffdf4f7);
const textFieldDisableColor = Color(0xffebebeb);
const defaultBoxShadow = [
  BoxShadow(
    color: Color.fromARGB(16, 80, 15, 76),
    blurRadius: 50.0,
    offset: Offset(10, 30),
  )
];
const defaultBoxDecoration = BoxDecoration(boxShadow: defaultBoxShadow);
