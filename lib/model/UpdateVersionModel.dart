class UpdateVersionModel {
  int status;
  UpdateVersionData data;

  UpdateVersionModel({this.status, this.data});

  UpdateVersionModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new UpdateVersionData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UpdateVersionData {
  String experienceAppVersion;
  String experienceAppUpdateType;
  String experienceAppUpdateUrl;
  String createdAt;
  bool checkIfNeedToUpdate;

  UpdateVersionData(
      {this.experienceAppVersion,
        this.experienceAppUpdateType,
        this.experienceAppUpdateUrl,
        this.createdAt,
        this.checkIfNeedToUpdate});

  UpdateVersionData.fromJson(Map<String, dynamic> json) {
    experienceAppVersion = json['experience_app_version'];
    experienceAppUpdateType = json['experience_app_update_type'];
    experienceAppUpdateUrl = json['experience_app_update_url'];
    createdAt = json['created_at'];
    checkIfNeedToUpdate = json['checkIfNeedToUpdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['experience_app_version'] = this.experienceAppVersion;
    data['experience_app_update_type'] = this.experienceAppUpdateType;
    data['experience_app_update_url'] = this.experienceAppUpdateUrl;
    data['created_at'] = this.createdAt;
    data['checkIfNeedToUpdate'] = this.checkIfNeedToUpdate;
    return data;
  }
}