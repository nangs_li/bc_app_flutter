class Smtp {
  final String password;
  final String username;
  final String smtplink;
  final String receivemail;
  final String testingpassword;
  final String testingusername;
  final String testingreceivemail;

  Smtp.fromJson(Map<String, dynamic> map)
      : assert(map['password'] != null),
        assert(map['username'] != null),
        assert(map['smtplink'] != null),
        password = map['password'],
        username = map['username'],
        smtplink = map['smtplink'],
        receivemail = map['receivemail'],
        testingusername = map['testingusername'],
        testingpassword = map['testingpassword'],
        testingreceivemail = map['testingreceivemail'];

}