class TNC {
  String contentEN;
  String contentCN;

  TNC({this.contentEN,this.contentCN});

  TNC.fromMap(Map snapshot,String id) :
        contentEN = snapshot['contentEN'] ?? '',
        contentCN = snapshot['contentCN'] ?? '';

  toJson() {
    return {
      "contentEN": contentEN,
      "contentCN": contentCN,

    };
  }
}