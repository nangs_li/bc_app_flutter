
import 'post.dart';

class Tag {
  final int id;
  final String link;
  final String name;

  Tag.fromJson(Map<String, dynamic> map)
    : assert(map['id'] != null),
        assert(map['link'] != null),
        assert(map['name'] != null),
        id = map['id'],
        link = map['link'],
        name = map['name'];


}