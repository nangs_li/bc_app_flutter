import 'tag.dart';

class Post {
  final int id;
  final String date;
  final PostRender title;
  final PostRender content;
  String thumbnail = "";
  final String status;
  final List<int> categories;
  final List<int> tags;
  List<Tag> tagList;
//  DocumentReference reference;

  Post.fromJson(Map<String, dynamic> map)
    : assert(map['id'] != null),
        assert(map['date'] != null),
        assert(map['title'] != null),
        assert(map['status'] != null),
        assert(map['content'] != null),
        id = map['id'],
        date = map['date'],
        title = PostRender.fromJson(map['title']),
        content = PostRender.fromJson(map['content']),
        categories = (map['categories'] as List).map((i) => i as int).toList(),
        tags = (map['tags'] as List).map((i) => i as int).toList(),
        status = map['status'];

  List<Tag> getTagList() {
    if (tagList == null) {
      return List<Tag>();
    }
    return tagList;
  }

  appendTag(Tag tag) {
    if (tagList == null) {
      tagList = List();
    }

    bool flag = true;

    tagList.forEach((Tag t) {
      if (t.id == tag.id) {
        flag = false;
      }
    });

    if (flag) {
      tagList.add(tag);
    }
  }

//  Post.fromMap(Map<String, dynamic> map, {this.reference})
//      : assert(map['date'] != null),
//        assert(map['title'] != null),
//        assert(map['thumbnail'] != null),
//        assert(map['status'] != null),
//        assert(map['content'] != null),
//        date = map['date'],
//        title = map['title'],
//        thumbnail = map['thumbnail'],
//        content = map['content'],
//        status = map['status'];

//  Post.fromSnapshot(DocumentSnapshot snapshot)
//      : this.fromMap(snapshot.data, reference: snapshot.reference);

}

class PostRender {

  String rendered;

  PostRender.fromJson(Map<String, dynamic> map)
    : rendered = map['rendered'];

}