
import 'post.dart';

class Category {
  final int id;
  final String link;
  final String name;
  final int parent;
  List<Category> subCats = List();
  List<Post> posts = List();

  Category.fromJson(Map<String, dynamic> map)
    : assert(map['id'] != null),
        assert(map['link'] != null),
        assert(map['name'] != null),
        assert(map['parent'] != null),
        id = map['id'],
        link = map['link'],
        name = map['name'],
        parent = map['parent'];

  appendSubCategory(Category category) {
    if (subCats == null) {
      subCats = List();
    }

    bool flag = true;

    subCats.forEach((Category c) {
      if (c.id == category.id) {
        flag = false;
      }
    });

    if (flag) {
      subCats.add(category);
    }
  }

  appendPost(Post post) {
    if (posts == null) {
      posts = List();
    }

    bool flag = true;

    posts.forEach((Post p) {
      if (p.id == post.id) {
        flag = false;
      }
    });

    if (flag) {
      posts.add(post);
    }
  }

}