
import 'package:flutter/material.dart';

class Event {
  final String date;
  final String endDate;
  final String endTime;
  final String title;
  final String doctor;
  final String content;
  final String type;
  final String time;
  final String alert;
  final String repeat;
  final String location;
  final String notes;
  final String eventType;

  String getTimePeriod (DateTime datetime) {

    if (date == endDate) {
      return time + "-" + endTime;
    } else {

      if((datetime.month >= DateTime.parse(date).month) && (datetime.day > DateTime.parse(date).day)) {

        if ((datetime.day < DateTime.parse(endDate).day)) {
          return "全日";
        } else {
          return "-"+endTime;
        }

      } else {
        return time+"-";
      }

    }

    return "";
  }

  int getType () {
    return int.parse(type.toString());
  }


  Map<String, dynamic> toJson() => {
    'date': date,
    'endDate': endDate,
    'endTime': endTime,
    'title': title,
    'doctor': doctor,
    'content': content,
    'type': type,
    'time': time,
    'endTime': endTime,
    'alert': alert,
    'repeat': repeat,
    'location': location,
    'notes': notes,
    'eventType': eventType,
  };

  Event.fromMap(Map<String, dynamic> map)
      : assert(map['date'] != null),
        assert(map['endDate'] != null),
        assert(map['endTime'] != null),
        assert(map['doctor'] != null),
        assert(map['title'] != null),
        assert(map['time'] != null),
        assert(map['type'] != null),
        assert(map['content'] != null),
        assert(map['alert'] != null),
        assert(map['notes'] != null),
        assert(map['location'] != null),
        assert(map['repeat'] != null),
        date = map['date'],
        endDate = map['endDate'],
        endTime = map['endTime'],
        doctor = map['doctor'],
        title = map['title'],
        content = map['content'],
        type = map['type'],
        alert = map['alert'],
        repeat = map['repeat'],
        notes = map['notes'],
        location = map['location'],
        eventType = map['eventType'] ?? "",
        time = map['time'];

}
