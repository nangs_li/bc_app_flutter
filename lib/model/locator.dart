
import 'package:get_it/get_it.dart';
import 'package:icanfight/api_provider/firebaseAPI.dart';
import 'package:icanfight/api_provider/crudModel.dart';


GetIt locator = GetIt.asNewInstance();

void setupLocator() {
  locator.registerLazySingleton(() => FireBaseAPI('BCAppDatabase'));
  locator.registerLazySingleton(() => CRUDModel()) ;
}