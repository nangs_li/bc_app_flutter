class User {
  String confirmPassword;
  String deviceId;
  String email;
  bool isFingerPrintEnable;
  String os;
  String password;
  String userName;
  String userId;

  User({this.confirmPassword, this.deviceId, this.email,this.isFingerPrintEnable,this.os,this.password,this.userName,this.userId});

  User.fromMap(Map snapshot,String id) :
        confirmPassword = snapshot['confirmPassword'] ?? '',
        deviceId = snapshot['deviceId'] ?? '',
        email = snapshot['email'] ?? '',
        isFingerPrintEnable = snapshot['isFingerPrintEnable'] ?? '',
        os = snapshot['os'] ?? '',
        password = snapshot['password'] ?? '',
        userName = snapshot['userName'] ?? '',
        userId = snapshot['userId'] ?? '';


  toJson() {
    return {
      "confirmPassword": confirmPassword,
      "deviceId": deviceId,
      "email": email,
      "isFingerPrintEnable": isFingerPrintEnable,
      "os": os,
      "password": password,
      "userName": userName,
      "userId": userId,
    };
  }
}