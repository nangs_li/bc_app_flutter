class Disclaimer {
  String id;
  String topText;
  String centerText;
  String emailTitle;
  String email;
  String phoneTitle;
  String phone;
  String taxTitle;
  String tax;
  String addressTitle;
  String address;
  String remark;
  String treatment;
  String treatmentDrug1;
  String treatmentDrug2;
  String treatmentDrug3;


  Disclaimer({this.id, this.topText, this.centerText,this.emailTitle,this.email,this.phoneTitle,this.phone,this.taxTitle,this.tax,this.addressTitle,this.address,this.remark,this.treatmentDrug1,this.treatmentDrug2,this.treatmentDrug3});

  Disclaimer.fromMap(Map snapshot,String id) :
        id = id ?? '',
        topText = snapshot['topText'] ?? '',
        centerText = snapshot['centerText'] ?? '',
        emailTitle = snapshot['emailTitle'] ?? '',
        email = snapshot['email'] ?? '',
        phoneTitle = snapshot['phoneTitle'] ?? '',
        phone = snapshot['phone'] ?? '',
        taxTitle = snapshot['taxTitle'] ?? '',
        tax = snapshot['tax'] ?? '',
        addressTitle = snapshot['addressTitle'] ?? '',
        address = snapshot['address'] ?? '',
        remark = snapshot['remark'] ?? '',
        treatment = snapshot['treatment'] ?? '',
        treatmentDrug1 = snapshot['treatmentDrug1'] ?? '',
        treatmentDrug2 = snapshot['treatmentDrug2'] ?? '',
        treatmentDrug3 = snapshot['treatmentDrug3'] ?? '';


  toJson() {
    return {
      "topText": topText,
      "centerText": centerText,
      "emailTitle": emailTitle,
      "email": email,
      "phoneTitle": phoneTitle,
      "phone": phone,
      "taxTitle": taxTitle,
      "tax": tax,
      "addressTitle": addressTitle,
      "address": address,
      "remark": remark,
      "treatment": treatment,
      "treatmentDrug1": treatmentDrug1,
      "treatmentDrug2": treatmentDrug2,
      "treatmentDrug3": treatmentDrug3,

    };
  }
}