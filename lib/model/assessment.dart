class Assessment {
  final String date;
  List<String> scores;

  Map<String, dynamic> toJson() => {
    'date': date,
    'scores': scores,
  };

  Assessment.fromMap(Map<String, dynamic> map)
      : assert(map['date'] != null),
        assert(map['scores'] != null),
        date = map['date'],
        scores = (map['scores'] as List).map((i) => i.toString()).toList();

}
