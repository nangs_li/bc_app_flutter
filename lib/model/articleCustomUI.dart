class ArticleCustomUI {
  final String title;
  final String context;
  final String imageLink;

  ArticleCustomUI.fromJson(Map<String, dynamic> map)
    : assert(map['title'] != null),
        assert(map['context'] != null),
        assert(map['imageLink'] != null),
        title = map['title'],
        context = map['context'],
        imageLink = map['imageLink'];

}