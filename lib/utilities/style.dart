import 'package:flutter/material.dart';

//const rocheBlueColor = Color(0xFF1C71D7);
//const blackBoldTitle = TextStyle(
//  color: Color(0xff020003),
//  fontSize: 16,
//  fontWeight: FontWeight.w700,
//);
//final Duration changeAnimationSearchBarDuration = Duration(milliseconds: 50);

const Color defaultPinkColor = Color(0xFFFFEBEC);
const Color lightGreenColor = Color(0xFF39C4BE);
const Color postListPinkColor = Color(0xFFF3007B);
const Color assessmentTextColor = Color(0xFF555555);
const Color purpleGreyColor = Color(0xFF75748B);
const Color defaultPinkColor2 = Color(0xFFFB729C);

const disclaimerControllerText  = TextStyle(
    color: Color(0xFF555555),
    fontSize: 16,
    height: 1.5,
    fontFamily: "Raleway-Regular",
    fontWeight: FontWeight.w500);
const disclaimerControllerSmallText  = TextStyle(
    color: Color(0xFF555555),
    fontSize: 14,
    height: 1.5,
    fontFamily: "Raleway-Regular",
    fontWeight: FontWeight.w500);

final TextStyle subtitleTxtStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w300,
    fontFamily: 'Raleway-Regular',
    fontSize: 16);

const double topPadding = 10;
const double defaultPadding = 20;
const double webViewTopPadding = 80;

const EdgeInsets rowPadding = EdgeInsets.only(top: 16,bottom:8);
const defaultBoxShadow = [BoxShadow(
  color: Color.fromARGB(16, 80, 15, 76),
  blurRadius: 50.0,
  offset: Offset(10, 30),
)];
const defaultBoxDecoration = BoxDecoration(boxShadow: defaultBoxShadow);

const textFieldColor = Color(0xfffdf4f7);
const textFieldDisableColor = Color(0xffebebeb);
const defaultBorderRadius = BorderRadius.all(Radius.circular(10));
const disableColor = Color(0xFFA0A0A0);
const disableButtonColor = Color(0xFFEBEBEB);

const sectionTitle = TextStyle(
  fontWeight: FontWeight.w600,
  fontFamily: 'Raleway-Regular',
  color:defaultPinkColor2,
  fontSize: 16,
);

const ngoTitle = TextStyle(
  fontWeight: FontWeight.w700,
  fontFamily: 'Raleway-Regular',
  color:defaultPinkColor2,
  fontSize: 22,
);

const ngoNotes = TextStyle(
  fontWeight: FontWeight.w600,
  fontFamily: 'Raleway-Regular',
  color:purpleGreyColor,
  fontSize: 14,
);

const ngoDetails = TextStyle(
  fontWeight: FontWeight.w400,
  fontFamily: 'Raleway-Regular',
  color:purpleGreyColor,
  fontSize: 16,
);