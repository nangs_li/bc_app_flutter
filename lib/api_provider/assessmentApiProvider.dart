
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:icanfight/model/assessment.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:intl/intl.dart';

class AssessmentApiProvider{

  static String _assessmentListTag = "assessments";


  Future<List<String>> getTodayAssessments() async {

    String now = DateFormat("dd-MMM-yyyy",'zh_HK').format(DateTime.now()).toString();

    List<Assessment> assessments = await getAssessments();

    List<String> list = ["-1","-1","-1","-1","-1","-1","-1"];

    assessments.forEach((Assessment ass) {
      if (ass.date == now) {
        list = ass.scores;
      }
    });

    return list;
  }

  Future<List<Assessment>> getAssessments() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> reminders = sharedPreferences.getStringList(_assessmentListTag);
//    debugPrint(reminders.length.toString());
    if (reminders == null) {
      return [Assessment.fromMap({"date": DateFormat("dd-MMM-yyyy",'zh_HK').format(DateTime.now()).toString(), "scores": ["-1","-1","-1","-1","-1","-1","-1"]})];
    }
    return reminders.map((r) => Assessment.fromMap(json.decode(r))).toList();
  }

  removeAssessment() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(_assessmentListTag);
  }

  addAssessment(int key, String value) async {

    String now = DateFormat("dd-MMM-yyyy",'zh_HK').format(DateTime.now()).toString();
    bool flag = false;


    List<Assessment> assessments = await getAssessments();
    List<Assessment> newAssessments = assessments;

    int count = 0;
    assessments.forEach((Assessment ass) {
      if (ass.date == now) {
        flag = true;
        newAssessments[count].scores[key] = value;
      }
      count++;
    });

    if (!flag) {
      Assessment assessment = Assessment.fromMap({"date": DateFormat("dd-MMM-yyyy",'zh_HK').format(DateTime.now()).toString(), "scores": ["-1","-1","-1","-1","-1","-1","-1"]});
      assessment.scores[key] = value;
      newAssessments.add(assessment);
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList(
        _assessmentListTag,
        newAssessments.map((e) {
          return json.encode(e.toJson());
        }).toList()
    );

  }

}