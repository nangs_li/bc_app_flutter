import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:icanfight/api_provider/firebaseAPI.dart';
import 'package:icanfight/model/UpdateVersionModel.dart';
import 'package:icanfight/model/category.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/smtp.dart';
import 'package:icanfight/model/tag.dart';
import 'package:icanfight/model/user.dart';
import 'package:icanfight/psp/models/PspHomePageData.dart';
import '../model/disclaimer.dart';
import '../model/tnc.dart';
import '../model/locator.dart';

class CRUDModel extends ChangeNotifier {
  FireBaseAPI _api = locator<FireBaseAPI>();

  List<Disclaimer> disclaimer;


  Future<List<Disclaimer>> fetchDatabase() async {
    var result = await _api.getDataCollection();
    print("fetchDatabase");
    disclaimer = result.documents
        .map((doc) => Disclaimer.fromMap(doc.data, doc.documentID))
        .toList();
    return disclaimer;
  }

  Stream<QuerySnapshot> fetchDatabaseAsStream() {
    return _api.streamDataCollection();
  }

  Future<List<User>> getUsers(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getUsers");
//    List<User> list = doc.data
//        .map<User>((data,i) => User.fromMap(data,i))
//        .toList() as List<dynamic>;
    return  [User.fromMap(doc.data, doc.documentID)] ;
  }

  Future<Disclaimer> getDisclaimerById(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getDisclaimerById");
    return  Disclaimer.fromMap(doc.data, doc.documentID) ;
  }

  Future<TNC> getTNCById(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getTNCById");
    return  TNC.fromMap(doc.data, doc.documentID) ;
  }

  Future<PspHomePageData> getPspHomePageData({String id}) async {
    var doc = await _api.getDocumentById(id);
    var data = doc.data['pspHomePageData'];
    print("getPspHomePageData");
    return  PspHomePageData.fromJson(json.decode(data)) ;
  }

  Future<List<Post>> getPostResponseById(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getPostResponseById");
    var post = doc.data['post'];
    var jsonString = json.decode(post);
    List<Post> list = jsonString
        .map<Post>((data) => Post.fromJson(data))
        .toList() as List<dynamic>;
//    print("postResponse:$list");
//    for(var i = 0; i < list.length; i++){
//      Post post =  list[i];
//      print("$i.postResponse:${post.tags}");
//    }
    return  list;
  }

  Future<List<Tag>> getTagResponseById(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getTagResponseById");
    var tag = doc.data['tag'];
    var jsonString = json.decode(tag);
    List<Tag> list = jsonString
        .map<Tag>((data) => Tag.fromJson(data))
        .toList();
//    print("TagResponse:$list");
//    for(var i = 0; i < list.length; i++){
//      Tag tag =  list[i];
//      print("$i.tagResponse:${tag.id}");
//    }
    return list;
  }

  Future<List<Category>> getCategoryResponseById(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getCategoryResponseById");
    var categories = doc.data['categories'];
    var jsonString = json.decode(categories);
    List<Category> list = jsonString
        .map<Category>((data) => Category.fromJson(data))
        .toList();
//    print("CategoryResponse:$list");
//    for(var i = 0; i < list.length; i++){
//      Category category =  list[i];
//      print("$i.categoryResponse:${category.id}");
//    }
    return  list;
  }

  Future<Smtp> getSmtpLink(String id) async {
    var doc = await _api.getDocumentById(id);
    print("getSmtpLink");
    Smtp smtp =  Smtp.fromJson(doc.data);
    return  smtp;
  }

  Future removeDisclaimer(String id) async{
     await _api.removeDocument(id) ;
     return ;
  }
  Future updateDisclaimer(Disclaimer data,String id) async{
    await _api.updateDocument(data.toJson(), id) ;
    return ;
  }

  Future addDisclaimer(Disclaimer data) async{
    var result  = await _api.addDocument(data.toJson()) ;

    return ;

  }

  Future<UpdateVersionModel> getVersionById({String id}) async {
    var doc = await _api.getDocumentById(id);
    print("getVersionById");
    var version = doc.data['data'];
    var jsonString = json.decode(version);
    UpdateVersionModel updateVersionModel = UpdateVersionModel.fromJson(jsonString);
    return  updateVersionModel;
  }

}
