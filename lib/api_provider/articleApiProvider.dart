import 'package:flutter/cupertino.dart';
import 'package:icanfight/model/post.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ArticleApiProvider{

  static String _articleListTag = "articles";
  static String _recordTag = "records";


  Future<List<String>> getBookmarkPost() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> reminders = sharedPreferences.getStringList(_articleListTag);
    if (reminders == null) {
      return List();
    }
    return reminders;
  }

  removeBookmarks() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(_articleListTag);
  }

  removeBookmark(Post post) async {
    List<String> posts = await getBookmarkPost();

    posts.remove(post.id.toString());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList(
        _articleListTag,
        posts
    );
  }

  bookmarkPost(Post post) async {

    List<String> posts = await getBookmarkPost();

    posts.add(post.id.toString());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setStringList(
        _articleListTag,
        posts
    );

  }

  Future<bool> hasBookmark(Post post) async {
    List<String> posts = await getBookmarkPost();
    debugPrint(posts.toString());
    return posts.indexOf(post.id.toString()) >= 0;
  }

  List<String> getRemindersList() {
    return ["-1","-1","-1","-1","-1","-1","-1","-1","-1","",""];
  }

  void setSignUpStringRecord(int index, String value) async {

    debugPrint("setSignUpStringRecord:"+(index-1).toString()+ ": " + value);

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> reminders = sharedPreferences.getStringList(_recordTag);

    if (reminders == null) {
      reminders = getRemindersList();
    }

    reminders[index-1] = value;

    sharedPreferences.setStringList(_recordTag, reminders);

  }

  Future<String> getStringRecordItem (int index) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    List<String> reminders = sharedPreferences.getStringList(_recordTag);

    if (reminders == null) {
      reminders = getRemindersList();
    }

    debugPrint(reminders[index]);

    return reminders[index];
  }

}