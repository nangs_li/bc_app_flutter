import 'package:flutter/material.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'customCheckBox.dart';

enum QuestionAnswerType {
  Enable,
  Disable,
  EnableWithTextField,
}

class CustomQuestionAnswer extends StatelessWidget {
  final String answer;
  final List choiceValues;
  final Function onTapFunction;
  final int questionIndex;
  final int answerNo;

  final Function(bool) onChangedFunction;
  final QuestionAnswerType type;
  CustomQuestionAnswer({
    Key key,
    @required this.answer,
    @required this.choiceValues,
    @required this.onTapFunction,
    @required this.onChangedFunction,
    @required this.type,
    @required this.questionIndex,
    @required this.answerNo
  })  :super(key: key);


  @override
  Widget build(BuildContext context) {
    Widget customQuestionAnswer;
    if(type == QuestionAnswerType.Enable || type == QuestionAnswerType.Disable ){
      customQuestionAnswer = GestureDetector(
        onTap: onTapFunction,
        child: Container(
          color: Colors.white,
          margin: EdgeInsets.only(top: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(left: leftPadding(), right: 20),
                      child: Text(
                        answer,
                        style: new TextStyle(color: type == QuestionAnswerType.Disable ? disableColor : Colors.black,fontSize: 16, fontWeight: FontWeight.w400),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 5,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    margin: EdgeInsets.only(right: 10, top: 0),
                    child: type == QuestionAnswerType.Disable ? Container(height: 32,width: 32): CustomCheckbox(
                      value: choiceValues[questionIndex][answerNo],
                      onChanged: onChangedFunction,
                    ),
                  ),

                ],
              ),
                textField(),
            ],
          ),
        ),
      );
    }
    return customQuestionAnswer;
  }
  double leftPadding() {
    double leftPadding = 40;
    if(questionIndex == 10){
      if(answerNo == 0 || answerNo == 1|| answerNo == 2||answerNo == 3||answerNo == 4|| answerNo == 5 || answerNo == 6 || answerNo == 7 || answerNo == 8){
        leftPadding = 80;
      }
    }
    return leftPadding;
  }

  Widget textField() {
    Widget widget = Container();
    if (type == QuestionAnswerType.Enable && choiceValues[questionIndex][answerNo] || questionIndex == 8) {
      if (questionIndex == 8 && answerNo == 0 || questionIndex == 9 && answerNo == 4 ||questionIndex == 10 && answerNo == 7 ||questionIndex == 10 && answerNo == 11) {
        String hintText;
        SingletonShareData data = SingletonShareData();
        TextEditingController controller;
        if(questionIndex == 8 && answerNo == 0){
          controller  = data.q9A1TextFieldController;
          hintText = Lang.getString("signupqa_content_9_1_textfield");
        }
        if(questionIndex == 9 && answerNo == 4){
          controller  = data.q10A5TextFieldController;
          hintText = Lang.getString("signupqa_content_10_5_textfield");
        }
        if(questionIndex == 10 && answerNo == 7){
          controller  = data.q11A8TextFieldController;
         hintText = Lang.getString("profile_drug_treatment_8_textfield");
       }
       if(questionIndex == 10 && answerNo == 11){
         controller  = data.q11A12TextFieldController;
         hintText = Lang.getString("profile_drug_treatment_12_textfield");
       }

        widget = Container( height: 160,
          padding: EdgeInsets.only(top: 10),
          margin: EdgeInsets.only(left: 40, right: 20),
          child: TextField(
            textInputAction: TextInputAction.done,
            autofocus: true,
            maxLines:5,
              controller: controller,
            keyboardType: TextInputType.multiline,
            decoration:  InputDecoration(
                enabledBorder:  OutlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide:  BorderSide(color: kLightHealthTheme.primaryColor, width: 1.0),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  )
                ),
                focusedBorder:OutlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                    borderSide:  BorderSide(color: kLightHealthTheme.primaryColor, width: 1.0),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    )
                ),
                filled: true,
                hintStyle: new TextStyle(color: kLightHealthTheme.primaryColor),
                hintText: hintText,
                fillColor: Colors.white70),
          )
        );
      }
    }
    return widget;
  }

}