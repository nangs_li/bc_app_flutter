import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';


class ReminderInputField extends StatefulWidget {

  String title;
  int maxInputLine;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  TextEditingController _textEditingController;
  bool _isMandatory;

  ReminderInputField({inputTitle: String, VoidCallback focusNodeCallBack, int maxLine = 1, FocusNode focusNode, FocusNode nextFocusNode, TextEditingController textEditingController, bool mandatory = false}) {
    title = inputTitle;
    maxInputLine = maxLine;
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _textEditingController = textEditingController;
    _isMandatory = mandatory;
  }

  @override
  _ReminderInputFieldState createState() =>
      _ReminderInputFieldState(inputTitle: title, maxLine: maxInputLine,
          focusNodeCallBack: _focusNodeCallBack, nextFocusNode: _nextFocusNode, focusNode: _focusNode,
        ted: _textEditingController, mandatory: _isMandatory
      );
}

class _ReminderInputFieldState extends
    State<ReminderInputField> {

  String title;
  int maxInputLine;
  TextEditingController textEditingController;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  bool _isMandatory;

  _ReminderInputFieldState({inputTitle: String, VoidCallback focusNodeCallBack, int maxLine = 1, FocusNode focusNode, FocusNode nextFocusNode, TextEditingController ted, bool mandatory}) {
    title = inputTitle;
    maxInputLine = maxLine;
    if (ted != null) {
      textEditingController = ted;
    } else {
      textEditingController = new TextEditingController(text: "");
    }
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _isMandatory = mandatory;
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _focusNodeCallBack();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _isMandatory ? Text("*", style: TextStyle(fontSize: 20, color: kLightHealthTheme.primaryColor)) : Text(""),
                    Text(title, style: TextStyle(color: Color(0xFF9291AC), fontWeight: FontWeight.w700, fontSize: 16)),
                  ],
                )
              )
            ],
          ),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (term) {
                  _focusNode.unfocus();
                  if (_nextFocusNode != null) {
                    FocusScope.of(context).requestFocus(_nextFocusNode);
                  }
                },
                controller: textEditingController,
                focusNode: _focusNode,
                keyboardType: TextInputType.multiline,
                maxLines: maxInputLine,
                style: TextStyle(color: Color(0xFF555555)),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFC34870)),
                      borderRadius: BorderRadius.circular(10)
                  ),
                ),
              )
          )
        ],
      ),
    );
  }

}