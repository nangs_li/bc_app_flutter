import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';
import 'customDropDown.dart';


class ReminderDropdownInputField extends StatefulWidget {

  String title;
  int maxInputLine;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  TextEditingController _textEditingController;
  List<String> _items;
  bool _isMandatory;

  ReminderDropdownInputField({inputTitle: String, VoidCallback focusNodeCallBack, int maxLine = 1, FocusNode focusNode, FocusNode nextFocusNode,
    TextEditingController textEditingController, List<String> items, bool mandatory = false}) {
    title = inputTitle;
    maxInputLine = maxLine;
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _textEditingController = textEditingController;
    _items = items;
    _isMandatory = mandatory;
  }

  @override
  _ReminderDropdownInputFieldState createState() =>
      _ReminderDropdownInputFieldState(inputTitle: title, maxLine: maxInputLine,
          focusNodeCallBack: _focusNodeCallBack, nextFocusNode: _nextFocusNode, focusNode: _focusNode,
        ted: _textEditingController, items: _items, mandatory: _isMandatory
      );
}

class _ReminderDropdownInputFieldState extends
    State<ReminderDropdownInputField> {

  String title;
  String _value;
  int maxInputLine;
  TextEditingController textEditingController;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  List<String> _items;
  bool isPop = false;
  bool _isMandatory;

  _ReminderDropdownInputFieldState({inputTitle: String, VoidCallback focusNodeCallBack, int maxLine = 1, FocusNode focusNode, FocusNode nextFocusNode,
    TextEditingController ted, List<String> items, bool mandatory}) {
    title = inputTitle;
    maxInputLine = maxLine;
    if (ted != null) {
      textEditingController = ted;
    } else {
      textEditingController = new TextEditingController(text: "");
    }
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _items = items;
    _isMandatory = mandatory;
  }

  @override
  void initState() {
    super.initState();

    if (textEditingController.text.length > 0) {
      setState(() {
        _value = textEditingController.text.toString();
      });
    }
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _focusNodeCallBack();
      }
      showDropDownPicker();
    });
  }

  showDropDownPicker () {
    setState(() {
      isPop = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _isMandatory ? Text("*", style: TextStyle(fontSize: 20, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700)) : Text(""),
                    Text(title, style: TextStyle(color: Color(0xFF9291AC), fontWeight: FontWeight.w700, fontSize: 16)),
                  ],
                )
              )
            ],
          ),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () async {
                  showDropDownPicker();
                },
                child: CustomDropdownButtonFormField(
                  onChanged: (value) {
                    setState(() {
                      _value = value;
                      isPop = false;
                    });
                    textEditingController.text = value;
                  },
                  value: _value,
//                  controller: textEditingController,
//                  enabled: false,
//                  focusNode: _focusNode,
//                  style: TextStyle(color: Color(0xFF555555)),
                  items: _items.map<CustomDropdownMenuItem<String>>((value) {
                    return CustomDropdownMenuItem<String>(
                      value: value,
                      child: Theme(
                        data: kLightHealthTheme.copyWith(
                          buttonTheme: kLightHealthTheme.buttonTheme.copyWith(
                            alignedDropdown: true,
                            padding: EdgeInsets.symmetric(horizontal:10),
                          ),
                        ),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(value, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16), textScaleFactor: 1.0,),
                        ),
                      )
                    );
                  }).toList(),
                  decoration: InputDecoration(
                    hoverColor: kLightHealthTheme.primaryColor,
                    contentPadding: EdgeInsets.symmetric(horizontal:10, vertical: 10),
                    // contentPadding: EdgeInsets.symmetric(horizontal:10, vertical: 10),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFFC34870)),
                        borderRadius: BorderRadius.circular(10)
                    ),

                  ),
                ),
              )
          )
        ],
      ),
    );
  }

}