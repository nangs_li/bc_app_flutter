import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:icanfight/utilities/style.dart';

class WebViewPage extends StatelessWidget {

  final String webViewUrl;

  WebViewPage({this.webViewUrl});

  Widget build(context) {
    print("webViewUrl:$webViewUrl");
    return  WebviewScaffold(
      url: webViewUrl,
      appBar:
      new AppBar(
        leading: Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(top: 15, left: defaultPadding),
            child:  GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                width: 40,
                height: 40,
                child: Icon(Icons.arrow_back, color: Colors.white, size: 35),
              ),
            )
        ),
        title: Padding(
          padding: EdgeInsets.only(top: 15,left: 5),
          child: Text("",
              textScaleFactor: 1.0,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.w600)),
        ),
      ),
    );

  }

}
