import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';


class SliverSpacer extends StatelessWidget {
  SliverSpacer({
    Key key,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(child: Container(color: kLightHealthTheme.primaryColor,height: 10),
    );
  }

}