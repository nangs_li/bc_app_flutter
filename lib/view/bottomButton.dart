
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
enum ButtonType {
  NormalButton,
  LoginButtonWithForgetPassword,
}

class BottomButton extends StatelessWidget {

  final RoundedRectangleBorder roundedRectangleBorder;
  final Color buttonColor;
  final String buttonText;
  final Function onPressed;
  final Color textColor;
  final ButtonType buttonType;
  final Function function;

  BottomButton({
    Key key,
   @required this.buttonText,
    @required this.onPressed,
    this.textColor = Colors.white,
    this.roundedRectangleBorder,
    this.buttonColor,
    this.buttonType,
    this.function
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(buttonType == ButtonType.LoginButtonWithForgetPassword){
      return Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.only(top: 40, bottom: 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              ButtonTheme(
                minWidth: 300,
                child: Container(
                  decoration: defaultBoxDecoration,
                  child: RaisedButton(
                      elevation: 8,
                      padding: EdgeInsets.all(12),
                      child: Text(buttonText,
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),),
                      onPressed: onPressed,
                      shape: roundedRectangleBorder != null ? roundedRectangleBorder : RoundedRectangleBorder(
                          side: BorderSide(color: kLightHealthTheme.buttonColor),
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      textColor: textColor,
                      color: buttonColor != null ? buttonColor : kLightHealthTheme.buttonColor
                  ),
                ),
              ),
          GestureDetector(
            // When the child is tapped, show a snackbar.
            onTap: function,
            // The custom button
            child:Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(Lang.getString("login_forgetpw"),
                  style: TextStyle(color: Color(0xff9291AC),fontSize: 16, fontWeight: FontWeight.w600),textAlign: TextAlign.center,),
              )
          )
            ],
          )
      );
    }
    return Container(
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.only(top: 40, bottom: 60),
        child: ButtonTheme(
          minWidth: 300,
          child: Container(
            decoration: defaultBoxDecoration,
            child: RaisedButton(
              elevation: 8,
              padding: EdgeInsets.all(12),
              child: Text(buttonText,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),),
              onPressed: onPressed,
              shape: roundedRectangleBorder != null ? roundedRectangleBorder : RoundedRectangleBorder(
                  side: BorderSide(color: kLightHealthTheme.buttonColor),
                  borderRadius: BorderRadius.all(Radius.circular(50))),
              textColor: textColor,
              color: buttonColor != null ? buttonColor : kLightHealthTheme.buttonColor
            ),
          ),
        )
    );
  }

}