import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:shimmer/shimmer.dart';


class SkeletonView extends StatelessWidget {

  final double topPadding;
  SkeletonView({this.topPadding});
  @override
  Widget build(context) {
    return Padding(
      padding: EdgeInsets.only(top:topPadding),
      child: Container(
        color: defaultPinkColor,
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: defaultPadding, vertical: defaultPadding),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              enabled: true,
              child: Column(
                children: [0, 1, 2, 3, 4, 5, 6]
                    .map((_) => Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 48.0,
                        height: 48.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(horizontal: 8.0),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: double.infinity,
                              height: 12.0,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 2.0),
                            ),
                            Container(
                              width: double.infinity,
                              height: 8.0,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 2.0),
                            ),
                            Container(
                              width: 40.0,
                              height: 8.0,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ))
                    .toList(),
              ),
            ),

          ],
        ),
      ),
    );
  }
}