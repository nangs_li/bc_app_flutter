import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';

//Added by Told
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as prefix0;
const String MIN_DATETIME = '2010-05-12 00:00';
const String MAX_DATETIME = '2021-11-25 23:55';
const String INIT_DATETIME = '2019-10-17 18:13';
// const String DATE_FORMAT = 'HH时:mm分:s';
const String DATE_FORMAT = 'MM月dd日 HH:mm';
//Above added by Told

class ReminderDateTimeInputField extends StatefulWidget {

  String title;
  String _dialogTitle;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  TextEditingController _textEditingController;
  String _initDate, _minDate;



  ReminderDateTimeInputField({dialogTitle: String, inputTitle: String, VoidCallback focusNodeCallBack, FocusNode focusNode, FocusNode nextFocusNode, TextEditingController textEditingController,
    String initDate, String minDate,
  }) {
    title = inputTitle;
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _textEditingController = textEditingController;
    _focusNode = focusNode;
    _dialogTitle = dialogTitle;
    _initDate = initDate;
    _minDate = minDate;
  }

  @override
  _ReminderDateTimeInputFieldState createState() =>
      _ReminderDateTimeInputFieldState(dialogTitle: _dialogTitle, inputTitle: title, focusNodeCallBack: _focusNodeCallBack, nextFocusNode: _nextFocusNode, focusNode: _focusNode,
          textEditingController: _textEditingController, initDate: _initDate, minDate: _minDate);
}

class _ReminderDateTimeInputFieldState extends
    State<ReminderDateTimeInputField> {

  String title;
  String _dialogTitle;
  String selectedDate = "";
  TextEditingController _textEditingController;
  VoidCallback _focusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  String startMonth;
  String startDay;
  String _initDate, _minDate;

  //Added by Told
  DateTime _dateTime;

  _ReminderDateTimeInputFieldState({dialogTitle: String, inputTitle: String, VoidCallback focusNodeCallBack, FocusNode focusNode, FocusNode nextFocusNode,
    TextEditingController textEditingController, String initDate, String minDate,}) {
    title = inputTitle;
    _focusNodeCallBack = focusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _textEditingController = textEditingController;
    _focusNode = focusNode;
    _dialogTitle = dialogTitle;
    _initDate = initDate;
    _minDate = minDate;
  }

  @override
  void initState() {
    super.initState();
    //Added by Told
    _dateTime = DateTime.parse(INIT_DATETIME);


    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        if (_focusNodeCallBack != null) {
          _focusNodeCallBack();
        }
        print('field selected');
      }
    });
  }

  void showDateTimePicker(String _title) async {

    debugPrint(_initDate);
    // DateTime date = DatePicker.showDatePicker(
      DatePicker.showDatePicker(
      context,
      minDateTime: DateTime.parse(MIN_DATETIME),//(_minDate == "") ? DateTime.parse(MIN_DATETIME): DateTime.parse(_minDate),
      maxDateTime: DateTime.parse(MAX_DATETIME),
      initialDateTime: (_initDate == "") ? DateTime.parse(INIT_DATETIME): DateTime.parse(_initDate),
      dateFormat: DATE_FORMAT,
      pickerMode: DateTimePickerMode.datetime, // show TimePicker
      pickerTheme: DateTimePickerTheme(
        title: Container(
          decoration: BoxDecoration(
            color: kLightHealthTheme.primaryColor,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          ),
          width: double.infinity,
          height: 56.0,
          alignment: Alignment.center,
          child: Text(
            _title,
            style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.w600),
          ),
        ),
        titleHeight: 56.0,
      ),
      onCancel: () {
        debugPrint('onCancel');
      },
      onChange: (dateTime, List<int> index) {
        String dum = DateFormat("yyyy-MM-dd HH:mm").parse(dateTime.toString()).toString().split(".")[0];
        _textEditingController.text = dum.substring(0,dum.length-3);
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          print('Confirm selected time: $_dateTime');
          _focusNode.unfocus();
          FocusScope.of(context).requestFocus(_nextFocusNode);
//          _textEditingController.text = _dateTime.toString();
        //       _textEditingController.text = date.year.toString() + "-" + date.month.toString().padLeft(2,'0') + "-" + date.day.toString().padLeft(2,'0') + " "
        // + ((MediaQuery.of(context).alwaysUse24HourFormat)
        //     ? time.hour.toString().padLeft(2,'0') + ":" + time.minute.toString().padLeft(2,'0')
        //     : time.hourOfPeriod.toString().padLeft(2,'0') + ":" + time.minute.toString().padLeft(2,'0') + ((time.periodOffset == 0) ? "AM" : "PM"));
        });
      },
    );
  // }
    // showDatePicker(
    //     context: context,

    //     initialDate: DateTime.now(),
    //     firstDate: DateTime(2019,8),
    //     lastDate: DateTime(2119),
    //     builder: (buildContext, widget) {
    //       return Theme(
    //           data: kLightHealthTheme.copyWith(
    //               secondaryHeaderColor: kLightHealthTheme.primaryColor,
    //               brightness: Brightness.light,
    //               splashColor: Colors.white,
    //               textTheme: TextTheme(
    //                   display1: TextStyle(color: Color(0xFF603F49)),
    //                   display2: TextStyle(color: Color(0xFF603F49)),
    //                   display3: TextStyle(color: Color(0xFF603F49)),
    //                   display4: TextStyle(color: Color(0xFF603F49)),
    //                   headline: TextStyle(color: Color(0xFF603F49)),
    //                   subhead: TextStyle(color: Color(0xFF603F49)),
    //                   title: TextStyle(color: Color(0xFF603F49)),
    //                   body1: TextStyle(color: Color(0xFF603F49)),
    //                   body2: TextStyle(color: Color(0xFF603F49)),
    //                   caption: TextStyle(color: Color(0xFF603F49))
    //               )
    //           ),
    //           child: Stack(
    //             overflow: Overflow.clip,
    //             alignment: Alignment.center,
    //             children: <Widget>[
    //               widget,
    //               Container(
    //                 padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/2+160),
    //                 child: Container(
    //                   width: MediaQuery.of(context).size.width,
    //                   decoration: new BoxDecoration(
    //                       color: kLightHealthTheme.primaryColor,
    //                       borderRadius: new BorderRadius.only(
    //                           topLeft:
    //                           const Radius.circular(8.0),
    //                           topRight: const Radius.circular(
    //                               8.0)
    //                       )
    //                   ),
    //                   margin: MediaQuery.of(context).viewInsets + const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
    //                   padding: EdgeInsets.only(left: 24, top: 30),
    //                   child: Text(_dialogTitle+" date", style: TextStyle(fontSize: 14, color: Color(0xFF953351), decoration: TextDecoration.none ),),
    //                 ),
    //               ),
    //             ],
    //           )
    //       );
    //     }
    // );
/*
    TimeOfDay time = await showTimePicker(context: context,
        initialTime: TimeOfDay.now(),
        builder: (buildContext, widget) {
          return Theme(
              data: kLightHealthTheme.copyWith(
                  secondaryHeaderColor: kLightHealthTheme.primaryColor,
                  brightness: Brightness.light,
                  splashColor: Colors.white,
                  textTheme: TextTheme(
                      display1: TextStyle(color: Color(0xFF603F49)),
                      display2: TextStyle(color: Color(0xFF603F49)),
                      display3: TextStyle(color: Color(0xFF603F49)),
                      display4: TextStyle(color: Color(0xFF603F49)),
                      headline: TextStyle(color: Color(0xFF603F49)),
                      subhead: TextStyle(color: Color(0xFF603F49)),
                      title: TextStyle(color: Color(0xFF603F49)),
                      body1: TextStyle(color: Color(0xFF603F49)),
                      body2: TextStyle(color: Color(0xFF603F49)),
                      caption: TextStyle(color: Color(0xFF603F49))
                  )
              ),
              child: Stack(
                overflow: Overflow.clip,
                alignment: Alignment.center,
                children: <Widget>[
                  widget,
                  Container(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/2+160),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                          color: kLightHealthTheme.primaryColor,
                          borderRadius: new BorderRadius.only(
                              topLeft:
                              const Radius.circular(8.0),
                              topRight: const Radius.circular(
                                  8.0)
                          )
                      ),
                      margin: MediaQuery.of(context).viewInsets + const EdgeInsets.symmetric(horizontal: 43.0, vertical: 20.0),
                      padding: EdgeInsets.only(left: 24, top: 30),
                      child: Text(_dialogTitle+" time", style: TextStyle(fontSize: 14, color: Color(0xFF953351), decoration: TextDecoration.none ),),
                    ),
                  ),
                ],
              )
          );
        }
    ).whenComplete(() {
      _focusNode.unfocus();
      FocusScope.of(context).requestFocus(_nextFocusNode);
    });
    */
    // _textEditingController.text = date.year.toString() + "-" + date.month.toString().padLeft(2,'0') + "-" + date.day.toString().padLeft(2,'0') + " "
    //     + ((MediaQuery.of(context).alwaysUse24HourFormat)
    //         ? time.hour.toString().padLeft(2,'0') + ":" + time.minute.toString().padLeft(2,'0')
    //         : time.hourOfPeriod.toString().padLeft(2,'0') + ":" + time.minute.toString().padLeft(2,'0') + ((time.periodOffset == 0) ? "AM" : "PM"));

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("*", style: TextStyle(fontSize: 20, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700)),
                    Text(title, style: TextStyle(color: Color(0xFF9291AC), fontWeight: FontWeight.w700, fontSize: 16)),
                  ],
                )
              )
            ],
          ),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () async {
                  showDateTimePicker(title);
                },
                child: TextFormField(
                  controller: _textEditingController,
                  enabled: false,
                  focusNode: _focusNode,
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: kLightHealthTheme.primaryColor),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFFC34870)),
                        borderRadius: BorderRadius.circular(10)
                    ),

                  ),
                ),
              )
          )
        ],
      ),
    );
  }

}