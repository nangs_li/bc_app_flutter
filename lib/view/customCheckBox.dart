import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:circular_check_box/circular_check_box.dart';


class CustomCheckbox extends StatelessWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;
  final Color checkColor;
  final Color backgroundColor;
  final bool tristate;
  final MaterialTapTargetSize materialTapTargetSize;
  final bool isInactiveShowCircle;

  CustomCheckbox({
    Key key,
    @required this.value,
    this.tristate = false,
    @required this.onChanged,
    this.activeColor,
    this.checkColor,
    this.backgroundColor,
    this.materialTapTargetSize,
    this.isInactiveShowCircle = false,

  })  : assert(tristate != null),
        assert(tristate || value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: SizedBox(
        width: 32,
        height:32,
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 2.0),
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color: (backgroundColor == null || this.value || this.isInactiveShowCircle) ? Color(0xFFF5F6FA) : backgroundColor,
              border: Border.all(
                width: 1.5,
                color: (backgroundColor == null || this.value || this.isInactiveShowCircle) ? Color(0xFFD0D0D0) : backgroundColor,
              ),
            ),
            child: Theme(
              data: kLightHealthTheme.copyWith(unselectedWidgetColor: Color(0x00F5F6FA)),
              child: Checkbox(
                value: value,
                tristate: tristate,
                onChanged: onChanged,
                activeColor: Color(0xFFF5F6FA),
                checkColor: (checkColor != null) ? checkColor :kLightHealthTheme.primaryColor,
                materialTapTargetSize: materialTapTargetSize,
              ),)
        ),
      ),
    );
  }

}