import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';

enum SliverAppBarType {
  LearnCategoryPage,
  SearchPage,
  AssessmentPage,
  ProfilePage,
  BookMarkPage,
  SettingPage,
  EditProfilePage,
  SetProFilePhotoPage,
  SignUpQaPage,
  TreatmentPage,
  SignInPage,
  SignUpPage,
  FullArticlePage,
  TNCPage,
  WelcomePage,
  ResetPasswordPage,
  LearnMenuPage,
  ChangePasswordPage,
  AssessmentDetailPage,
  AssessmentListingPage,
  AssessmentCompletePage,
  DisclaimerPage,
  LearnMorePage,
  CalendarPage,
  AppointmentPage

}
enum subPageType { editProfile, changePw, setProfileImage, setting, medicalRecord, drugTreatment }

class CustomSliverAppBarView extends StatelessWidget {
  // a property on this class
  final SliverAppBarType type;
  final PreferredSize bottom;
  List<Widget> widgetList;
  final String titleText;
  final bool isBack;
  final Function loadImageFunction;
  final VoidCallback backCallBack;
  // a constructor for this class
  CustomSliverAppBarView({this.type, this.bottom, this.widgetList, this.loadImageFunction,this.titleText,this.isBack,this.backCallBack});
  final SingletonShareData data = SingletonShareData();

  Widget build(context) {
    bool haveLeftButton;
    // Pass the text down to another widget
    if (type == SliverAppBarType.LearnCategoryPage ||
        type == SliverAppBarType.AssessmentPage ||
        type == SliverAppBarType.SignInPage ||
        type == SliverAppBarType.SignUpPage ||
        type == SliverAppBarType.ProfilePage ||
        type == SliverAppBarType.SignUpQaPage && !isBack
        ) {
      haveLeftButton = false;
    } else {
      haveLeftButton = true;
    }

    double appBarTopPadding = 15;
    bool centerTitleText = (widgetList == null && haveLeftButton);
    if(centerTitleText){
      widgetList = List<Widget>();
      widgetList.add(Container(margin:EdgeInsets.only(top: appBarTopPadding, left: defaultPadding), width: 40,height: 40));
    }
    Widget titleWidget = Padding(
      padding: EdgeInsets.only(top: appBarTopPadding),
      child: Text(titleText,
            textScaleFactor: 1.0,
            style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontWeight: FontWeight.w600)),

    );

//    if(type == SliverAppBarType.TNCPage){
//      titleWidget =  Text(titleText,
//            textScaleFactor: 1.0,
//            textAlign: TextAlign.center,
//            style: TextStyle(
//                color: Colors.white,
//                fontSize: 24,
//                fontWeight: FontWeight.w600));
//
//    }

    return SliverAppBar(
        automaticallyImplyLeading:haveLeftButton,
        leading: haveLeftButton ? Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(top: appBarTopPadding, left: defaultPadding),
            child: (isBack) ? GestureDetector(
              onTap: () {
                if (backCallBack == null) {
                  Navigator.pop(context);
                } else {
                  backCallBack();
                }
              },
              child: Container(
                width: 40,
                height: 40,
                child: Icon(Icons.arrow_back, color: Colors.white, size: 35),
              ),
            ) : Container()
        ) : null,
        title: centerTitleText ? Center(child: titleWidget): titleWidget,
        floating: true,
        pinned: false,
        snap: false,
        forceElevated: false,
        actions: widgetList,
        bottom: (bottom != null) ? bottom : null);

  }

}
