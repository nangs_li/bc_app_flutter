import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';

class AssessmentHeaderBar extends SliverPersistentHeaderDelegate {

  String headerTitle = "";
  Widget customWidget;

  AssessmentHeaderBar({ title: String, widget: Widget}) {
    headerTitle = title;
    customWidget = widget;
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Container(
          height: 180,
          padding: EdgeInsets.all(20),
          alignment: Alignment.center,
          color: kLightHealthTheme.primaryColor,
          child: Stack(
            children: <Widget>[
              Container(
                height: 100,
//                color: Colors.green,
                alignment: Alignment.centerLeft,
                child: Text(headerTitle, style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),
              ),
              (customWidget == null) ? Container() : customWidget
            ],
          ),
        )
      ],
    );
  }

  @override
  double get maxExtent => 180;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;

}
