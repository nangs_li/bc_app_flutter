import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/utils.dart';

class CustomInputField extends StatefulWidget {
  int iconDataCodePoint;
  String title;
  bool obscureText;
  bool isBlock;
  String initValue;
  int maxInputLines;
  VoidCallback _focusNodeCallBack;
  VoidCallback _unFocusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  ValidationType _validationType;
  TextInputAction _textInputAction;
  TextEditingController _textEditingController;
  bool _showDisableColor;
  String _preInputTitle;
  Function(String) _onChangeFunction;

  CustomInputField(
      {iconData: int,
        String preInputTitle = "",
      inputTitle: String,
      bool obscure = false,
      String hint,
      bool block = false,
      VoidCallback focusNodeCallBack, VoidCallback unFocusNodeCallBack,
      FocusNode focusNode, FocusNode nextFocusNode,
      ValidationType validationType,
      TextEditingController textEditingController,
      TextInputAction textInputAction,
      int maxLines = 1,
        bool showDisableColor = false,
        Function(String) onChangeFunction}) {
    iconDataCodePoint = iconData;
    title = inputTitle;
    obscureText = obscure;
    initValue = hint;
    isBlock = block;
    maxInputLines = maxLines;
    _focusNodeCallBack = focusNodeCallBack;
    _unFocusNodeCallBack = unFocusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _validationType = validationType;
    _textInputAction = textInputAction;
    _textEditingController = textEditingController ??  TextEditingController(text: hint);
    _showDisableColor = showDisableColor;
    _preInputTitle = preInputTitle;
    _onChangeFunction = onChangeFunction;
  }

  @override
  _CustomInputFieldState createState() => _CustomInputFieldState(
      iconData: iconDataCodePoint,
      inputTitle: title,
      obscure: obscureText,
      hint: initValue,
      block: isBlock,
      focusNodeCallBack: _focusNodeCallBack,
      unFocusNodeCallBack: _unFocusNodeCallBack,
      nextFocusNode: _nextFocusNode,
      focusNode: _focusNode,
      validationType: _validationType,
      textInputAction: _textInputAction,
      textEditingController: _textEditingController,
      maxLines: maxInputLines,
  onChangeFunction: _onChangeFunction );
}

class _CustomInputFieldState extends State<CustomInputField> {
  int iconDataCodePoint;
  String title;
  bool obscureText;
  bool isBlock;
  String initValue;
  String _value = "";
  int maxInputLines;
  VoidCallback _focusNodeCallBack;
  VoidCallback _unFocusNodeCallBack;
  FocusNode _focusNode;
  FocusNode _nextFocusNode;
  ValidationType _validationType;
  TextInputAction _textInputAction;
  TextEditingController _textEditingController;
  Function(String) _onChangeFunction;

  _CustomInputFieldState(
      {iconData: int,
      inputTitle: String,
      bool obscure = false,
      String hint,
      bool block = false,
      VoidCallback focusNodeCallBack, VoidCallback unFocusNodeCallBack,
      FocusNode focusNode, FocusNode nextFocusNode,
      ValidationType validationType,
      TextEditingController textEditingController,
      TextInputAction textInputAction = TextInputAction.next,
      int maxLines = 1,
        Function(String) onChangeFunction}) {
    iconDataCodePoint = iconData;
    title = inputTitle;
    obscureText = obscure;
    initValue = hint;
    isBlock = block;
    maxInputLines = maxLines;
    _focusNodeCallBack = focusNodeCallBack;
    _unFocusNodeCallBack = unFocusNodeCallBack;
    _nextFocusNode = nextFocusNode;
    _focusNode = focusNode;
    _validationType = validationType;
    _textInputAction = textInputAction;
    _textEditingController = textEditingController;
    _onChangeFunction = onChangeFunction;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController.addListener(() {
      setState(() {
        _value = _textEditingController.text;
      });
    });
    _focusNode.addListener(() {
//      if (_focusNode.hasFocus) {
//        _focusNodeCallBack();
//      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    BorderRadius _borderRadius = BorderRadius.circular(10);
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              iconDataCodePoint > 0
                  ? Container(
                      child: Icon(
                          IconData(iconDataCodePoint,
                              fontFamily: 'MaterialIcons'),
                          color: kLightHealthTheme.primaryColor),
                    )
                  : Container(),
              Expanded(
                  child:Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget._preInputTitle,textAlign: TextAlign.start,
                          style: TextStyle(
                              color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700, fontSize: 16)),
                      Text(title,textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Color(0xFF9291AC), fontWeight: FontWeight.w700, fontSize: 16)),
                    ],
                  ),
                )
              )

            ],
          ),
          Container(
              decoration: BoxDecoration(
                color: widget._showDisableColor
                    ? textFieldDisableColor
                    : textFieldColor,
                borderRadius: _borderRadius,

              ),
              margin: EdgeInsets.only(top: 5),
              child: TextFormField(
                controller: _textEditingController,
                keyboardType: TextInputType.multiline,
                textInputAction: _textInputAction,
                onChanged: _onChangeFunction,
                onFieldSubmitted: (term) {
                  _focusNode.unfocus();
                  if (_nextFocusNode != null) {
                    FocusScope.of(context).requestFocus(_nextFocusNode);
                  }
                  if (_unFocusNodeCallBack != null) {
                    _unFocusNodeCallBack();
                  }
                },
                maxLines: maxInputLines,
                validator: (value) {
                  return "invalid";
                },
                focusNode: _focusNode,
                enabled: !isBlock,
                obscureText: obscureText,
                style: TextStyle(color: Color(0xFF555555), fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  fillColor: isBlock ? Color(0xFFEBEBEB) : Colors.transparent,
                  contentPadding:
                      EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
                      // EdgeInsets.symmetric(horizontal:10),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: isBlock
                              ? Color(0xFFEBEBEB)
                              : kLightHealthTheme.primaryColor),
                      borderRadius: _borderRadius),
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: kLightHealthTheme.primaryColor),
                      borderRadius: _borderRadius),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFC34870)),
                      borderRadius: _borderRadius),
                ),
              )),
          (_validationType != null) ?
          Container(
            child: Utils.validator(_validationType, _value),
          )
          : Container()
        ],
      ),
    );
  }
}
