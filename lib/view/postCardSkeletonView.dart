import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:shimmer/shimmer.dart';


class PostCardSkeletonView extends StatelessWidget {

  final double topPadding;
  PostCardSkeletonView({this.topPadding});
  @override
  Widget build(context) {

    return Padding(
      padding: EdgeInsets.only(top:topPadding),
      child: Container(
        color: defaultPinkColor,
        width: double.infinity,
        padding: EdgeInsets.only(left: defaultPadding,right: defaultPadding),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              enabled: true,
              child: Column(
                children: [0]
                    .map((_) => Padding(
                  padding: const EdgeInsets.only(bottom: 110,top: defaultPadding),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                          borderRadius: defaultBorderRadius,
                          child: Container(
                            width: SizeConfig().screenWidth - defaultPadding * 2,
                            height: 200,
                            color: Colors.white,
                          )
                      ),
                    ],
                  ),
                ))
                    .toList(),
              ),
            ),

          ],
        ),
      ),
    );
  }
}