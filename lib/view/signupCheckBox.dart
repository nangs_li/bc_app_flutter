import 'package:flutter/material.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:circular_check_box/circular_check_box.dart';


class SignupCheckBox extends StatelessWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;
  final Color checkColor;
  final Color backgroundColor;
  final bool tristate;
  final MaterialTapTargetSize materialTapTargetSize;

  SignupCheckBox({
    Key key,
    @required this.value,
    this.tristate = false,
    @required this.onChanged,
    this.activeColor,
    this.checkColor,
    this.backgroundColor,
    this.materialTapTargetSize,
  })  : assert(tristate != null),
        assert(tristate || value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: SizedBox(
        width: 32,
        height: 32,
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 2.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: (backgroundColor == null || this.value)
                  ? Color(0xFFF5F6FA)
                  : backgroundColor,
            ),
            child: Theme(
              data: kLightHealthTheme.copyWith(
                  unselectedWidgetColor: Color(0x00F5F6FA)),
              child: CircularCheckBox(
                value: value,
                tristate: tristate,
                onChanged: onChanged,
                activeColor: Color(0XFF16D482),
                materialTapTargetSize: materialTapTargetSize,
              ),
            )),
      ),
    );
  }
}