import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {

  final String title;
  final String content;
  final String leftButtonText;
  final String rightButtonText;
  final Function leftButtonFunction;
  final Function rightButtonFunction;
  final BuildContext buildContext;

  CustomAlertDialog({
    Key key,
    @required this.title,
    @required this.content,
    @required this.leftButtonText,
    @required this.rightButtonText,
    @required this.rightButtonFunction,
    @required this.buildContext,
    this.leftButtonFunction
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: EdgeInsets.symmetric(
          horizontal: 20.0, vertical: 20.0),
      contentPadding: EdgeInsets.symmetric(
          horizontal: 20.0, vertical: 10.0),
      shape: RoundedRectangleBorder(
          side: BorderSide(
              color: Colors.white10),
          borderRadius: BorderRadius.all(
              Radius.circular(10))),
      title: Text(
          title,
          style: TextStyle(
              color: Color(0xFF555555),
              fontSize: 20,
              fontWeight: FontWeight.w700)),
      content: (this.content == null) ? null : Text(content,
          style: TextStyle(
              color: Color(0xFF9291AC),
              fontSize: 16,
              fontWeight: FontWeight.w500)),
      actions: <Widget>[
        (this.leftButtonText == null) ? Container() : InkWell(
          child: Container(
            child: Text(leftButtonText,
                style: TextStyle(
                  color: Color(0xFF555555),
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                )),
            margin: EdgeInsets.only(right: 20,
                top: 10,
                bottom: 10),
          ),
          onTap: () {
            (this.leftButtonFunction == null) ? Navigator.of(buildContext).pop() : this.leftButtonFunction();
          },
        ),
        InkWell(
          child: Container(
            child: Text(rightButtonText,
              style: TextStyle(
                color:(this.leftButtonText == null) ? Color(0xFF555555) : Color(0xFFE14672),
                fontWeight: FontWeight.w600,
                fontSize: 14)),
            margin: EdgeInsets.only(right: 10,
                top: 10,
                bottom: 10),
          ),
          onTap: rightButtonFunction,
        ),
      ],
    );
  }

}

