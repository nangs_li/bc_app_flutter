import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:icanfight/utilities/style.dart';

import 'package:webview_flutter/webview_flutter.dart';
import 'package:icanfight/utilities/lang.dart';
// not have customSliverAppBarView
class SupportController extends StatefulWidget {

  @override
  _SupportControllerState createState() =>
      _SupportControllerState();
}

class _SupportControllerState
    extends State<SupportController> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initModel();
  }

  initModel () async {



  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            top: false,
            child: Material(
              color: defaultPinkColor,
              child: Container(),
            ),
          ),
//      Container(
//        child: WebView(
//          javascriptMode: JavascriptMode.unrestricted,
//          initialUrl: "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/support/",
//
//          onPageFinished: (String url) {
////          setState(() {
////            embeddedUrl = null;
////          });
//          },
//        ),
//      )
          Container(
            alignment: Alignment(0, 0),
            margin: EdgeInsets.only(top: 200),
            child: Center(
              child: Column(
                children: <Widget>[
                  Image.asset("assets/images/support_cup.png", height: 240,),
                  Text(Lang.getString("support_title"), style: TextStyle(color: Color(0xFF9291AC), fontSize: 19, fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                  Text(Lang.getString("support_message"), style: TextStyle(color: Color(0xFF555555), fontSize: 16, fontWeight: FontWeight.w500, height: 2),textAlign: TextAlign.center,),
                ],
              ),
            )
          ),
        ],
      ),
    );;
  }
}