import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:icanfight/controller/assessmentController.dart';


import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'baseViewController.dart';
import 'signInController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/category.dart';
import 'bottomNavigationBarController.dart';
import 'package:intl/intl.dart';

class AssessmentCompleteController extends StatefulWidget {

  @override
  _AssessmentCompleteControllerState createState() =>
      _AssessmentCompleteControllerState();
}

class _AssessmentCompleteControllerState
    extends State<AssessmentCompleteController> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initModel();
  }

  initModel () async {

    Future.delayed(Duration(seconds: 2), () {

      Navigator.of(context).pop();
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BaseViewController(BottomNavigationBarController(index: 1)))
      );
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kLightHealthTheme.primaryColor,
      body: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: true,
                titleText: DateFormat("yyyy MMMdd日", 'zh_HK').format(
                    DateTime.now()),
                type: SliverAppBarType.AssessmentCompletePage),
                  SliverToBoxAdapter(
                    child: WaveBar(),
                  ),
              ],
            ),
            ),
          ),
          Container(
            alignment: Alignment(0, 0),
            margin: EdgeInsets.only(top: 100),
            child: Center(
              child: InkWell(
                onTap: () {
                },
                child: Container(
                    alignment: Alignment.center,
                    child: Image.asset("assets/images/CompleteAssessmentAnimation1.gif", height: 320,)
                )
              )
            ),
          ),
        ],
      ),
    );;
  }
}