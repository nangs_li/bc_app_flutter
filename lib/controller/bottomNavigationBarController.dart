import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:icanfight/controller/signInController.dart';
import 'package:icanfight/controller/supportController.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/model/UpdateVersionModel.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/pages/homePage/homePage.dart';
import 'package:icanfight/psp/pages/loadingPage/loadingPage.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'baseViewController.dart';
import 'learnCategoryController.dart';
import 'assessmentController.dart';
import 'profileController.dart';
import 'package:icanfight/utilities/lang.dart';


class BottomNavigationBarController extends StatefulWidget {

  int _selectedIdx;
  int _selectedLearnIdx;

  BottomNavigationBarController({int index = 0, int learnIdx = 0}) {
    _selectedIdx = index;
    _selectedLearnIdx = learnIdx;
  }

  @override
  _BottomNavigationBarControllerState createState() =>
      _BottomNavigationBarControllerState(index: _selectedIdx, learnIdx: _selectedLearnIdx);
}

class _BottomNavigationBarControllerState
    extends State<BottomNavigationBarController> {

  int _selectedLearnIdx;
  bool isConnected = true;

  _BottomNavigationBarControllerState({int index = 0,  int learnIdx = 0}) {
    _selectedIndex = index;
    _selectedLearnIdx = learnIdx;
  }

  final List<Widget> pages = [
    LearnCategoryController(

    ),
//    CustomCalendarController(
//
//    ),
    AssessmentController(

    ),
    ProfileController(

    ),
//    SupportController()
    LoadingPage(

    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 30), () {

      if(!data.postReady && data.isConnected){
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (buildContext) {
              return CustomAlertDialog(title: "錯誤", content: "請重新登入再試", leftButtonText: null,rightButtonText:"登出" ,buildContext: buildContext,
                  rightButtonFunction:  () {
                   data.signOut();
                   Navigator.push(
                       context,
                       MaterialPageRoute(builder: (context) => BaseViewController(SignInController())));
                  });
            }
        );
      }

    });
    data.registerCase = false;
    data.getChoiceValuesFromFireBase(data.user);
    if(data.alreadyLoadingPspPage){
      pages.removeLast();
      pages.add(HomePage());
    }
    data.checkVersionUpdate();
    data.checkVersionUpdateCallBack.listen((UpdateVersionModel updateVersionModel) {
      data.showVersionDialog(context: context,updateAppLink: updateVersionModel.data.experienceAppUpdateUrl);
    });
  }

  final PageStorageBucket bucket = PageStorageBucket();

  int _selectedIndex = 0;

  Widget _bottomNavigationBar(int selectedIndex) {
    SingletonShareData data = SingletonShareData();
    return Container(
      decoration: defaultBoxDecoration.copyWith(color: selectedIndex == 2 ? Colors.transparent : defaultPinkColor),
      child:
      ClipRRect(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(30),
            topLeft: Radius.circular(30),
          ),
          child:
          BottomNavigationBar(
            selectedFontSize: 12,
            unselectedFontSize: 12,
            type: BottomNavigationBarType.fixed,
            onTap: (int index) {
              if(!data.alreadyLogin){

                showDialog(
                    barrierDismissible: true,
                    context: context,
                    builder: (buildContext) {
                      return CustomAlertDialog(title: Lang.getString("popup_guest_title_select_disable_tabbar"), content: Lang.getString("popup_guest_content_select_disable_tabbar"),
                          leftButtonText: Lang.getString("popup_guest_no_later_select_disable_tabbar"),rightButtonText:Lang.getString(
                          "popup_guest_ok_login_select_disable_tabbar") ,buildContext: buildContext,
                          rightButtonFunction: () async {
                            SingletonShareData().signOut();
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => BaseViewController(SignInController())));
                          });
                    }
                );

              } else {
                setState(() {
                  _selectedIndex = index;
                });
                if(_selectedIndex == 0){
                  ga.gaEvent(name:"learn_navbar_button");
                } else if(_selectedIndex == 1){
                  ga.gaEvent(name: "assessment_navbar_button");
                } else if(_selectedIndex == 2){
                  ga.gaEvent(name:"profile_navbar_button");
                } else if(_selectedIndex == 3){
                  ga.gaEvent(name:"support_navbar_button" );
                }
              }
            },
            currentIndex: selectedIndex,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  activeIcon: Container(
                      child: new SvgPicture.asset("assets/images/Active_Learn.svg"),
                      height: 28),
                  icon: Container(child: new SvgPicture.asset(
                    "assets/images/Learn.svg", height: 20,),
                      alignment: Alignment.bottomCenter,
                      height: 28),
                  title: Text(Lang.getString("navigation_learn"),
                      style: TextStyle(
                          color: (selectedIndex == 0)
                              ? Color(0xFFFF5D8A)
                              : Color(
                              0xFF9291AC),
                          fontSize: 14,
                          fontWeight: FontWeight.w700))),
              BottomNavigationBarItem(
                  activeIcon: Icon(
                      Icons.assignment, size: 28, color: Color(0xFFFF5D8A)),
                  icon: Container(
                    child: Icon(Icons.assignment, color: Color(0xFF9291AC)),
                    alignment: Alignment.bottomCenter,
                    height: 28,),
                  title: Text(Lang.getString("navigation_assessment"),
                      style: TextStyle(
                          color: (selectedIndex == 1)
                              ? Color(0xFFFF5D8A)
                              : Color(
                              0xFF9291AC),
                          fontSize: 14,
                          fontWeight: FontWeight.w700))
              ),
              BottomNavigationBarItem(
                  activeIcon: Icon(
                      Icons.person, size: 28, color: Color(0xFFFF5D8A)),
                  icon: Container(
                    child: Icon(Icons.person, color: Color(0xFF9291AC)),
                    alignment: Alignment.bottomCenter,
                    height: 28,),
                  title: Text(
                    Lang.getString("navigation_profile"), style: TextStyle(
                      color: (selectedIndex == 2) ? Color(0xFFFF5D8A) : Color(
                          0xFF9291AC),
                      fontSize: 14,
                      fontWeight: FontWeight.w700),)),
              BottomNavigationBarItem(
                  activeIcon:  data.imageIcon(imagePath:"assets/images/Support_active.svg" ,height: 20,width: 20),

//                  Icon(
//                      Icons.help, size: 28, color: Color(0xFFFF5D8A)),
                  icon: Container(
                    child: data.imageIcon(imagePath:"assets/images/Support_inactive.svg" ,height: 20,width: 20),
                    alignment: Alignment.bottomCenter,
                    height: 28,),
                  title: Text(
                    Lang.getString("navigation_support"), style: TextStyle(
                      color: (selectedIndex == 3) ? Color(0xFFFF5D8A) : Color(
                          0xFF9291AC),
                      fontSize: 14,
                      fontWeight: FontWeight.w700),)),
            ],
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig(context: context);

    return Scaffold(
      bottomNavigationBar: _bottomNavigationBar(_selectedIndex),
      body: pages[_selectedIndex]
    );
  }
}

