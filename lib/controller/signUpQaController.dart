import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/api_provider/articleApiProvider.dart';
import 'package:icanfight/controller/treatmentController.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/psp/uti/client/utils.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:icanfight/view/customQuestionAnswer.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:icanfight/controller/assessmentDetailController.dart';
import 'package:icanfight/controller/disclaimerController.dart';
import 'baseViewController.dart';
import 'learnMoreController.dart';
import 'package:icanfight/utilities/lang.dart';
import 'dart:io' show Platform;

class SignUpQaController  extends StatefulWidget {

  bool _isAuth;
  bool _isStatic;

  SignUpQaController({bool isAuth = false, bool isStatic = false}) {
    _isAuth = isAuth;
    _isStatic = isStatic;
  }

  _SignUpQaControllerState createState() =>
      _SignUpQaControllerState(isAuth: _isAuth, isStatic: _isStatic);
}

class _SignUpQaControllerState extends State<SignUpQaController> {

  bool _isAuth;
  bool _isStatic;

  _SignUpQaControllerState({bool isAuth = false, bool isStatic = false}) {
    _isAuth = isAuth;
    _isStatic = isStatic;
  }

  int questionNo = 0;
  int _index = 0;
  bool isMovedOn = false;
  bool hideQ5 = false;

  ScrollController _scrollController;
  SingletonShareData data = SingletonShareData();


  List<String> titlesText = [
    Lang.getString("signupqa_title_1"),
    Lang.getString("signupqa_title_2"),
    Lang.getString("signupqa_title_3"),
    Lang.getString("signupqa_title_4"),
    Lang.getString("signupqa_title_5"),
    Lang.getString("signupqa_title_6"),
    Lang.getString("signupqa_title_7"),
    Lang.getString("signupqa_title_8"),
    Lang.getString("signupqa_title_9"),
    Lang.getString("signupqa_title_10"),
  ];

  List<List<String>> rawAnswersText = [
    [
      Lang.getString("signupqa_content_1_1"),
      Lang.getString("signupqa_content_1_2"),
      Lang.getString("signupqa_content_1_3"),
      Lang.getString("signupqa_content_1_4"),
      Lang.getString("signupqa_content_1_5"),
    ],
    [
      Lang.getString("signupqa_content_2_1"),
      Lang.getString("signupqa_content_2_2"),
      Lang.getString("signupqa_content_2_3"),
    ],
    [
      Lang.getString("signupqa_content_3_1"),
      Lang.getString("signupqa_content_3_2"),
      Lang.getString("signupqa_content_3_3"),
    ],
    [
      Lang.getString("signupqa_content_4_1"),
      Lang.getString("signupqa_content_4_2"),
      Lang.getString("signupqa_content_4_3"),
      Lang.getString("signupqa_content_4_4"),
    ],
    [
      Lang.getString("signupqa_content_5_1"),
      Lang.getString("signupqa_content_5_2"),
      Lang.getString("signupqa_content_5_3"),

    ],
    [
      Lang.getString("signupqa_content_6_1"),
      Lang.getString("signupqa_content_6_2"),
      Lang.getString("signupqa_content_6_3"),

    ],
    [
      Lang.getString("signupqa_content_7_1"),
      Lang.getString("signupqa_content_7_2"),
      Lang.getString("signupqa_content_7_3"),

    ],
    [
      Lang.getString("signupqa_content_8_1"),
      Lang.getString("signupqa_content_8_2"),
      Lang.getString("signupqa_content_8_3"),

    ],
    [
      Lang.getString("signupqa_content_9_1"),
      Lang.getString("signupqa_content_9_2"),

    ],
    [
      Lang.getString("signupqa_content_10_1"),
      Lang.getString("signupqa_content_10_2"),
      Lang.getString("signupqa_content_10_3"),
      Lang.getString("signupqa_content_10_4"),
      Lang.getString("signupqa_content_10_5"),
      Lang.getString("signupqa_content_10_6"),
      Lang.getString("signupqa_content_10_7"),

    ]

  ];

  List<List<String>> answersText = [
    [
      Lang.getString("signupqa_content_1_1"),
      Lang.getString("signupqa_content_1_2"),
      Lang.getString("signupqa_content_1_3"),
      Lang.getString("signupqa_content_1_4"),
      Lang.getString("signupqa_content_1_5"),
    ],
    [
      Lang.getString("signupqa_content_2_1"),
      Lang.getString("signupqa_content_2_2"),
      Lang.getString("signupqa_content_2_3"),
    ],
    [
      Lang.getString("signupqa_content_3_1"),
      Lang.getString("signupqa_content_3_2"),
      Lang.getString("signupqa_content_3_3"),
    ],
    [
      Lang.getString("signupqa_content_4_1"),
      Lang.getString("signupqa_content_4_2"),
      Lang.getString("signupqa_content_4_3"),
      Lang.getString("signupqa_content_4_4"),
    ],
    [
      Lang.getString("signupqa_content_5_1"),
      Lang.getString("signupqa_content_5_2"),
      Lang.getString("signupqa_content_5_3"),

    ],
    [
      Lang.getString("signupqa_content_6_1"),
      Lang.getString("signupqa_content_6_2"),
      Lang.getString("signupqa_content_6_3"),

    ],
    [
      Lang.getString("signupqa_content_7_1"),
      Lang.getString("signupqa_content_7_2"),
      Lang.getString("signupqa_content_7_3"),

    ],
    [
      Lang.getString("signupqa_content_8_1"),
      Lang.getString("signupqa_content_8_2"),
      Lang.getString("signupqa_content_8_3"),

    ],
    [
      Lang.getString("signupqa_content_9_1"),
      Lang.getString("signupqa_content_9_2"),

    ],
    [
      Lang.getString("signupqa_content_10_1"),
      Lang.getString("signupqa_content_10_2"),
      Lang.getString("signupqa_content_10_3"),
      Lang.getString("signupqa_content_10_4"),
      Lang.getString("signupqa_content_10_5"),
      Lang.getString("signupqa_content_10_6"),
      Lang.getString("signupqa_content_10_7"),

    ]

  ];

  List<String> selectedAnswersText = [
    "", "", "", "", "", "", "", "", "", ""
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaEvent(name: "signup_qna_page");
    getAssessmentLevel();
    _scrollController = ScrollController();
    SingletonShareData data = SingletonShareData();
    if(data.choiceValues == null){
      data.initSignUpQaData(forceReset: false);
    } else {
      data.backupChoiceValues = data.choiceValues.map((element)=>element).toList();
      data.backupDisableQuestionAnswer = data.disableQuestionAnswer.map((element)=>element).toList();
      data.initSignUpQaData(forceReset: true);
    }
  }

  Future navigateToDetailPage(context, index) async {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) {
          return BaseViewController(AssessmentDetailController(
              index: index
          ));
        }));
  }


  getAssessmentLevel() async {

    setState(() {
      questionNo = (0/10 * 100).round();
    });
  }

  updateCheckValue (String answer, String title, bool value) {


  }

//  hideQuestion(int index,bool hide){
//    if (hide) { //如第4期 remove Question No 4 or add back Question No 4
//      titlesText[index] = "";
//      _index += 1;
//    } else {
//      int questionNo = _index + 1;
//      titlesText[index] = Lang.getString("signupqa_title_$questionNo");
//    }
//    setState(() {
//      titlesText = titlesText;
//    });
//  }

  onTickQuestion(String answer,int answerNo) {
    // titlesText.indexOf(title) almost == _index
    List<bool> currentQuestionBoolList = [];
    List<bool> disableQuestionAnswerRow = List.filled(data.choiceValues[_index].length, false).toList();
    print("_index:$_index");
    bool isAlreadyHaveDisableQuestionAnswer = false;
    for (bool disable in data.disableQuestionAnswer[_index]) {
      if (disable) {
        isAlreadyHaveDisableQuestionAnswer = true;
      }
    }

    if (_index == 9) { // if question 10 multiple choose 1. set answer bool reverse (default false) or single choose by 1.set all to false and 2.set choose answer to true
      if (answer == Lang.getString("signupqa_content_10_7") || answer == Lang.getString("signupqa_content_10_6")){ //if choose != 已安排好接受治療，但不清楚是什麼治療 ,remove 已安排好接受治療，但不清楚是什麼治療 choose
        currentQuestionBoolList = List.filled(data.choiceValues[_index].length, false).toList();
        if(isAlreadyHaveDisableQuestionAnswer){
          currentQuestionBoolList[answerNo] = true;
        } else {
          disableQuestionAnswerRow = List.filled(data.choiceValues[_index].length, true).toList();
          disableQuestionAnswerRow[answerNo] = false;
        }
        data.choiceValues[_index] = currentQuestionBoolList; // reset choiceValues to all false list
        data.choiceValues[_index][answerNo] = !data.choiceValues[_index][answerNo]; // set false to true when select answer
      } else {
        data.choiceValues[_index][answerNo] = !data.choiceValues[_index][answerNo];
       bool disableQ11AnswerIndex5And6 = true;

        if (data.choiceValues[_index][0] ||data.choiceValues[_index][1] ||data.choiceValues[_index][2] ||data.choiceValues[_index][3]||data.choiceValues[_index][4]) {
          disableQuestionAnswerRow[5] = disableQ11AnswerIndex5And6;
          data.choiceValues[_index][5] = !disableQ11AnswerIndex5And6;
          disableQuestionAnswerRow[6] = disableQ11AnswerIndex5And6;
          data.choiceValues[_index][6] = !disableQ11AnswerIndex5And6;
        } else {
          disableQuestionAnswerRow[5] = !disableQ11AnswerIndex5And6;
          disableQuestionAnswerRow[6] = !disableQ11AnswerIndex5And6;
        }

      }

      data.disableQuestionAnswer[_index] = disableQuestionAnswerRow;
    }
    else {
      currentQuestionBoolList = List.filled(data.choiceValues[_index].length, false).toList();
    }

    if (_index == 9) { // set String into ArticleApiProvider
      String q10Value = "";

      for(int i=0;i<data.choiceValues[_index].length;i++){
        if (data.choiceValues[_index][i]) {
          q10Value += rawAnswersText[_index].indexOf(answersText[_index][i]).toString()+",";
        }
      }
      q10Value += rawAnswersText[_index].indexOf(answer).toString();
      if (answer == Lang.getString("signupqa_content_10_7")){
        ArticleApiProvider().setSignUpStringRecord(10, "6");
      } else {
        ArticleApiProvider().setSignUpStringRecord(10, q10Value);
      }

    } else {

      _scrollController.animateTo(_index * 200.0, duration: Duration(milliseconds: 300), curve: Curves.ease);
      ArticleApiProvider().setSignUpStringRecord(_index+1, answerNo.toString());
    }

    setState(() {
      data.choiceValues[_index] = data.choiceValues[_index];
      // set tick answer to list
      selectedAnswersText[_index] = answer;
      if (_index == 9||_index ==10){

      } else {
        data.choiceValues[_index] = List.filled(data.choiceValues[_index].length, false).toList();
        data.choiceValues[_index][answerNo] = !data.choiceValues[_index][answerNo]; //
      }

      // next QuestionNo(QuestionNo = index + 1)
      _index = (_index == 9) ? 9 : (_index + 1);
      questionNo = _index + 1;
//      print("answerText:$answer");
//      print("QuestionBoolList:$currentQuestionBoolList");
//      print("level:$questionNo");
//      print("newIndex:$_index");
//      print("choiceValues:$data.choiceValues");
//      print("selectedAnswersText:$selectedAnswersText");

    });

    if(_index == 3){
      if(data.choiceValues[2].indexOf(true) == 1){
        hideQ5 = true;
      } else {
        hideQ5 = false;
      }
      setState(() {
        titlesText = titlesText;
      });
    }
    if (_index == 5) { // If Q3沒有 > then 唔會問Q6
      if(data.choiceValues[2].indexOf(true) == 1){
        _index += 1;
      }
      // hideQuestion(5, data.choiceValues[2].indexOf(true) == 1);
      setState(() {
        titlesText = titlesText;
      });
    }
  }

  List<Widget> appBarRightItem() {
    List<Widget> list= List();
    if (!_isAuth) {
    list.add(Padding(
        padding: EdgeInsets.only(top: topPadding),
        child: Container(
            width: 35)
    ));
    list.add(Padding(
        padding: EdgeInsets.only(top: topPadding),
        child: Container(
            width: 35)
    ));
    }
    list.add(Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(right: defaultPadding,top: 5),
      child: Text(
        "0$questionNo/10",
        style: new TextStyle(fontSize: 20, color: Colors.white),
      ),
    ));
    return list;
  }

  Future<bool> register() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    String deviceID = await SingletonShareData().deviceId();
    bool registerSuccess = false;
    try {
      data.showLoading(context);
      final AuthResult result = await _auth.createUserWithEmailAndPassword(
        email: data.registerTextControllers[0].text,
        password: data.registerTextControllers[2].text,
      );

      String email = data.registerTextControllers[0].text;
      String password = data.registerTextControllers[2].text;
      String userName = data.registerTextControllers[4].text;
      Utils.setAccount(email, password, userName);
      print("Users:${result.user.uid}");
      await Firestore.instance.collection('BCAppDatabase')
          .document("Users")
          .setData({
        result.user.uid: {
          'userId': result.user.uid,
          'userName': userName,
          'password': "",
          'confirmPassword': "",
          'email': email,
          'os': (Platform.isIOS) ? "IOS" : "Android",
          'deviceId': deviceID,
          'isFingerPrintEnable': true,
          'alreadySubmitForm':false
        }
      }, merge: true);
      data.user = result.user;
      UserUpdateInfo updateInfo = UserUpdateInfo();
      updateInfo.displayName = userName;
      await data.user.updateProfile(updateInfo);
      await data.reloadUser();
      data.registerCase = true;
      data.hideLoading(context);
      ga.gaLogSignUp();
      print("result.additionalUserInfo:${result.additionalUserInfo}");
      registerSuccess = true;
      data.resetQ11Data();
      data.saveChoiceValuesIntoFireBase(context: context,q11: false);
      data.backupChoiceValues = null;
      data.backupDisableQuestionAnswer = null;
    } on PlatformException catch (e) {
      data.hideLoading(context);
      showDialog(
          barrierDismissible: true,
          context: context,
          builder: (buildContext) {
            return CustomAlertDialog(title: Lang.getString("login_error"), content: data.getErrorMessage(error:e,type: ErrorType.Login), leftButtonText: null,rightButtonText:Lang.getString(
                "login_close") ,buildContext: buildContext,
                rightButtonFunction:  () {
                  Navigator.of(context).pop();
                });
          }
      );
      registerSuccess = false;
    }
    return registerSuccess;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                controller: _scrollController,
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: _isAuth ? true : false,type: SliverAppBarType.SignUpQaPage,titleText: _isAuth ? Lang.getString("profile_medical_record") : Lang.getString("register_bar_title"),
                    widgetList:appBarRightItem(),

                  ),
                  SliverToBoxAdapter(
                    child: Column(
                      children: <Widget>[
                        Container(
                          color: kLightHealthTheme.primaryColor,
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Text("",
                            style: TextStyle(color: Colors.white, fontSize: 16),),
                        ),
                        WaveBar(),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                      child: Container(
                        color: defaultPinkColor,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            decoration: new BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: new BorderRadius.only(
                                    topLeft:
                                    const Radius.circular(10.0),
                                    topRight: const Radius.circular(
                                        20.0))),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                  children: titlesText.asMap().entries.map((MapEntry entry) {
                                    String title = entry.value;
                                    //print("QusetionTitle:$title");
                                    return Container(
                                        margin: EdgeInsets.only(left: 15, right: 15, bottom: 20),
                                        decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(color: Color.fromARGB(28, 255, 255, 255),
                                              blurRadius: 30.0,
                                              offset: Offset(0.0, 12.0),
                                            )
                                          ]
                                        ),
                                        child: Card(
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(borderRadius: defaultBorderRadius),
                                          color: (entry.key == 5 && hideQ5) ? disableButtonColor : Colors.white,
                                          child: Container(
                                            padding: EdgeInsets.only(top: 10, bottom: 25, left: 20, right: 10),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[  
                                                GestureDetector(
                                                  child: Container(
                                                    color: (entry.key == 5 && hideQ5) ? disableButtonColor : Colors.white,
                                                    padding: EdgeInsets.only(top: 20, bottom: 5, left: 0, right: 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Container(
                                                          height:40,
                                                          width: 30,
                                                          decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            color: (entry.key == 5 && hideQ5) ? disableColor : kLightHealthTheme.primaryColor,
                                                          ),
                                                          child: Center(child: Text((entry.key + 1).toString(), style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),textAlign: TextAlign.center,)),
                                                        ),
                                                        Container(
                                                          width: SizeConfig().blockSizeHorizontal * 61,
                                                          child: Text(title,
                                                            maxLines: 5,
                                                            style: TextStyle(fontSize: 19, fontWeight: FontWeight.w700, color: (entry.key == 5 && hideQ5) ? disableColor : Color(0xff555555)),),
                                                          margin: EdgeInsets.only(top: 8),
                                                        ),
                                                        Container(
                                                          padding: EdgeInsets.only(top: 10, right: 15),
                                                          alignment: Alignment.centerRight,
                                                          child: (_index == entry.key || entry.key == 8) ? Icon(Icons.keyboard_arrow_up, color:Color(0xFF555555),) : Icon(Icons.keyboard_arrow_down, color: (entry.key == 5 && hideQ5) ? disableButtonColor : Color(0xFF555555),),
                                                        )
                                                      ],
                                                    )
                                                  ),
                                                  onTap: () {
                                                    if(entry.key == 5 && hideQ5){
                                                      return;
                                                    }
                                                    for(int i=entry.key;i<=_index;i++) {
                                                      data.choiceValues[i] = List.filled(data.choiceValues[i].length, false).toList();
                                                    }

                                                    if (entry.key > _index) {
                                                      setState(() {
                                                        isMovedOn = true;
                                                      });
                                                      Future.delayed(Duration(seconds: 1), () {
                                                        setState(() {
                                                          isMovedOn = false;
                                                        });
                                                      });
                                                    } else {

                                                      setState(() {
                                                        _index = entry.key;
                                                      });
                                                    }

                                                  },
                                                ),
                                                (_index > entry.key) ? Container(
                                                  alignment: Alignment.centerLeft,
                                                  padding: EdgeInsets.only(top: 0, bottom: 0, left: 40, right: 20),
                                                  child: Text((entry.key == 5 && hideQ5) ? "" : selectedAnswersText[entry.key],
                                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: kLightHealthTheme.primaryColor),),
                                                ) : (
                                                    (_index == entry.key && entry.key !=3 && entry.key !=8) ?
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(builder: (context) => BaseViewController(LearnMoreController(index: entry.key))),);
                                                      },
                                                      child: Container(
                                                        alignment: Alignment.centerLeft,
                                                        padding: EdgeInsets.only(top: 0, bottom: 0, left: 40, right: 20),
                                                        child: Row(children: <Widget>[
                                                          Icon(Icons.info_outline, color: kLightHealthTheme.primaryColor),
                                                          Container(width: 5,),
                                                          Text(Lang.getString("signupqa_learn_more"),
                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: kLightHealthTheme.primaryColor),),
                                                        ],)
                                                      ),
                                                    ) :
                                                    Container()
                                                ),
                                                Column(
                                                  children: questionContext(entry: entry),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                  }).toList(),
                                ),
                                BottomButton(buttonText: Lang.getString(
                                    "submit_button"),onPressed: () async {
                                  if (questionNo == 10) {
                                    ga.gaEvent(name: "signup_submit_button");
                                  if(!data.registerCase) {
                                    data.resetQ11Data();
                                    data.saveChoiceValuesIntoFireBase(context: context,q11: false);
                                    data.backupChoiceValues = null;
                                    data.backupDisableQuestionAnswer = null;
                                    Navigator.pop(context);

                                  } else {
                                    bool registerSuccess =  await register();
                                    if(!registerSuccess){
                                      Navigator.pop(context);
                                    }

                                  if(data.isNotShowQ11()){
                                    if (questionNo == 10) {
                                      ArticleApiProvider().setSignUpStringRecord(
                                          11, "");

                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DisclaimerController(
                                                    isAuth: true,
                                                    isStatic: _isStatic)),);
                                    }
                                  } else {
                                  showDialog(
                                      barrierDismissible: true,
                                      context: context,
                                      builder: (buildContext) {
                                        return CustomAlertDialog(title: Lang.getString("signupqa_question"), content: null,
                                            leftButtonText: Lang.getString(
                                            "signupqa_question_answer1"),rightButtonText:Lang.getString(
                                            "signupqa_question_answer2") ,buildContext: buildContext,
                                            leftButtonFunction: () async {
                                            if (questionNo == 10) {
                                                  ArticleApiProvider().setSignUpStringRecord(
                                                      11, "");

                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            DisclaimerController(
                                                                isAuth: true,
                                                                isStatic: _isStatic)),);
                                              }
                                            },
                                            rightButtonFunction: () async {

                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) => BaseViewController(TreatmentController())));

                                            });
                                      }
                                  );
                                  }
                                  }
                                  }
                                },roundedRectangleBorder:RoundedRectangleBorder(side: BorderSide(color: (questionNo == 10) ? kLightHealthTheme.buttonColor : disableButtonColor), borderRadius: BorderRadius.all(Radius.circular(50)))
                                ,textColor: (questionNo == 10) ? Colors.white : Color(0xFFA0A0A0)
                                ,buttonColor: (questionNo == 10) ? kLightHealthTheme.buttonColor : disableButtonColor),
                              ],
                            ),
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          ),
          isMovedOn ? Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(top: 10, bottom: 60, left: 20, right: 20),
            child: ButtonTheme(
              minWidth: 300,
              child: Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(color: Color.fromARGB(40, 59, 0, 27),
                        blurRadius: 6.0,
                        offset: Offset(0.0, 3.0),
                      )
                    ]
                ),
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(Lang.getString("signup_fill_in"), style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              isMovedOn = false;
                            });
                          },
                          child: Text(Lang.getString("close_button"), style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: kLightHealthTheme.primaryColor),),
                        ),
                      )
                    ],
                  ),
                  onPressed: () async {

                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFAD3D5F)), borderRadius: BorderRadius.all(Radius.circular(30))),
                  disabledColor:  Color(0xFFF9EBEC),
                  textColor: Color(0xFFffffff),
                  color: Color(0xFFAD3D5F),
                ),
              ),
            ),
          ) : Container()
        ],
      ),
    );
  }

  List<Widget> questionContext({MapEntry entry}){
     Widget widget;
    // print("_index:$_index");

     List<Widget> widgetList = List<Widget>();
    List<String> answerList =  answersText[_index];

    if(entry.key == 8 ) {
      for (var i = 0; i < answersText[8].length; i++) {

        String answer = answersText[8][i];
       // print("answer:$answer");
        // print("entry.key:${entry.value}");
      //  print("entry2.key:$i");
        if (_index == entry.key) {
          widget = CustomQuestionAnswer(choiceValues: data.choiceValues,
              answer: answer,
              onTapFunction: () { },
              questionIndex: _index,
              answerNo: i,
              onChangedFunction: (bool value) {
                if (!data.disableQuestionAnswer[_index][i]) {
                  if (questionNo == 9 && value == true &&
                      answer == Lang.getString("signupqa_content_9_1")) {

                  } else {
                    _scrollController.animateTo(
                        _index * 200.0, duration: Duration(milliseconds: 300),
                        curve: Curves.ease);
                  }
                  onTickQuestion(answer, i);
                }
              },
              type: data.disableQuestionAnswer[_index][i]
                  ? QuestionAnswerType.Disable
                  : QuestionAnswerType.Enable);
        } else {
          if(widgetList.length == 0 && data.choiceValues[8][0]){
            String hintText;
            SingletonShareData data = SingletonShareData();
            TextEditingController controller;

            controller  = data.q9A1TextFieldController;
            hintText = Lang.getString("signupqa_content_9_1_textfield");
            widget = Container( height: 160,
                padding: EdgeInsets.only(top: 10),
                margin: EdgeInsets.only(left: 40, right: 20),
                child: TextField(
                  textInputAction: TextInputAction.done,
                  autofocus: true,
                  maxLines:5,
                  controller: controller,
                  keyboardType: TextInputType.multiline,
                  decoration:  InputDecoration(
                      enabledBorder:  OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                          borderSide:  BorderSide(color: kLightHealthTheme.primaryColor, width: 1.0),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          )
                      ),
                      focusedBorder:OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                          borderSide:  BorderSide(color: kLightHealthTheme.primaryColor, width: 1.0),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          )
                      ),
                      filled: true,
                      hintStyle: new TextStyle(color: kLightHealthTheme.primaryColor),
                      hintText: hintText,
                      fillColor: Colors.white70),
                )
            );
          } else {
            widget = Container();
          }

        }

        widgetList.add(widget);
      }
    } else {

     for (var i = 0; i < answerList.length; i++) {

       String answer = answerList[i];
       //print("answer:$answer");
       // print("entry.key:${entry.value}");
       //print("entry2.key:$i");
       if (_index == entry.key) {
         widget = CustomQuestionAnswer(choiceValues: data.choiceValues,
             answer: answer,
             onTapFunction: () { },
             questionIndex: _index,
             answerNo: i,
             onChangedFunction: (bool value) {
               if (!data.disableQuestionAnswer[_index][i]) {
                 if (questionNo == 9 && value == true &&
                     answer == Lang.getString("signupqa_content_9_1")) {

                 } else {
                   _scrollController.animateTo(
                       _index * 200.0, duration: Duration(milliseconds: 300),
                       curve: Curves.ease);
                 }
                 onTickQuestion(answer, i);
               }
             },
             type: data.disableQuestionAnswer[_index][i]
                 ? QuestionAnswerType.Disable
                 : QuestionAnswerType.Enable);
       } else {
         widget = Container();
       }

       widgetList.add(widget);
     }
    }

    return widgetList;
  }
}


