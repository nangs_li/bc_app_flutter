import 'package:flutter/material.dart';
import 'package:icanfight/view/bottomButton.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/utils.dart';
import 'package:icanfight/utilities/lang.dart';

class ChangePWController extends StatefulWidget {
  @override
  _ChangePWControllerState createState() => _ChangePWControllerState();
}

class _ChangePWControllerState extends State<ChangePWController> {
  List<FocusNode> _formNodes = [FocusNode(), FocusNode()];
  List<TextEditingController> _textEditingControllers = [
    TextEditingController(text: ""),TextEditingController(text: "")
  ];
  bool isButtonActive = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingControllers[1].addListener(() {

      if (_textEditingControllers[0].text != "Aa111111!") {
        setState(() {
          isButtonActive = false;
        });
        return;
      }

      if (Utils.isValidate(ValidationType.PASSWORD, _textEditingControllers[0].text) &&
          Utils.isValidate(ValidationType.PASSWORD, _textEditingControllers[1].text)
      ) {
        setState(() {
          isButtonActive = true;
        });
      } else {
        setState(() {
          isButtonActive = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SizedBox.expand(
            child: SafeArea(
              bottom: false,
              child: Material(
//                color: Color(0xFFF9EBEC),
                color: Colors.white,
                child: CustomScrollView(
                  physics: new ClampingScrollPhysics(),
                  slivers: <Widget>[
                    CustomSliverAppBarView(isBack: true,
                        titleText: Lang.getString("profile_change_password"),
                        type: SliverAppBarType.ChangePasswordPage
                    ),
                    SliverSpacer(),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                          (context, index) => Container(
                                color: kLightHealthTheme.primaryColor,
//                              height: 1000,
                                child: SingleChildScrollView(
                                  primary: false,
                                  physics: NeverScrollableScrollPhysics(),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: new BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  new BorderRadius.only(
                                                      topLeft:
                                                          const Radius.circular(
                                                              20.0),
                                                      topRight:
                                                          const Radius.circular(
                                                              20.0))),
                                          padding: EdgeInsets.only(top: 42),
//                                        height: 1000,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
//                                              height: MediaQuery.of(context)
//                                                  .size
//                                                  .height -
//                                                  108,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  padding:
                                                      EdgeInsets.only(top: 0),
//                                        height: 100,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 20,
                                                            right: 20,
                                                            top: 10,
                                                            bottom: 10),
                                                        child: CustomInputField(
                                                          textEditingController: _textEditingControllers[0],
                                                          focusNode:
                                                              _formNodes[0],
                                                          nextFocusNode:
                                                              _formNodes[1],
                                                          iconData: 0,
                                                          inputTitle:
                                                          Lang.getString("profile_current_password"),
                                                          hint: "12345678",
                                                          obscure: true,
                                                        ),
                                                      ),
                                                      (_textEditingControllers[0].text == "Aa111111!") ? Container() : Container(
                                                          margin: EdgeInsets.only(top: 0, left: 20),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Icon(Icons.error, color: Color(0xFFFA356A)),
                                                              Expanded(
                                                                  child: Text("當前密碼錯誤",
                                                                    style: TextStyle(
                                                                        color: Color(0xFFFA356A),
                                                                        fontSize: 13,
                                                                        fontWeight: FontWeight.w600
                                                                    ),
                                                                  )
                                                              )

                                                            ],
                                                          )
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 20,
                                                            right: 20,
                                                            top: 10,
                                                            bottom: 10),
                                                        child: CustomInputField(
                                                            textEditingController: _textEditingControllers[1],
                                                            focusNode:
                                                                _formNodes[1],
                                                            validationType:
                                                                ValidationType
                                                                    .PASSWORD,
                                                            iconData: 0,
                                                            inputTitle:
                                                            Lang.getString("profile_new_password"),
                                                            hint: "12345678",
                                                            obscure: true),
                                                      ),
                                                    ],
                                                  ))
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                          childCount: 1),
                    )
                  ],
                ),
              ),
            ),
          ),
          BottomButton(buttonText:Lang.getString("profile_save_change"),onPressed: () async {
            if (isButtonActive) {
              Navigator.pop(context);
            }
            //                                                          registerAction();
          },
          roundedRectangleBorder: RoundedRectangleBorder(side: BorderSide(color: isButtonActive ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB)), borderRadius: BorderRadius.all(Radius.circular(50))),
              textColor: isButtonActive ? Colors.white : Color(0xFFA0A0A0),
              buttonColor: isButtonActive ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB)
          ),
        ],
      ),
    );
  }
}
