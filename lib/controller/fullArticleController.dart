import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/model/articleCustomUI.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/tag.dart';
import 'package:html/dom.dart' as dom;
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/skeletonView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:icanfight/view/webViewPage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:icanfight/api_provider/articleApiProvider.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/parser.dart';
import 'baseViewController.dart';
enum WebViewState { onCreate,onStart,onFinish }

class FullArticleController extends StatefulWidget {

  Post post;
  List<Post> posts;
  bool _bookmarkList;

  FullArticleController({ article: Post, bool bookmarkList, List<Post> articles }) {
    post = article;
    _bookmarkList = bookmarkList;
    posts = articles;
  }

  @override
  _FullArticleControllerControllerState createState() =>
      _FullArticleControllerControllerState(article: post, bookmarkList: _bookmarkList, articles: posts);
}

class _FullArticleControllerControllerState
    extends State<FullArticleController> {

  Post post;
  List<Post> posts;
  String embeddedUrl;
  ScrollController _scrollController;
  bool scrollUp = false;
  bool multiLineScrollUp = false;
  TextEditingController _textEditingController;
  bool isBookmarked = false;
  bool showArticle = false;
  bool isButtonDisplay = true;
  bool isArticleBookmarked = false;
  double _articleFont = 1.0;
  bool _bookmarkList = false;
  WebViewState webViewState;
  SingletonShareData data;
  bool isExpanded = false;
  Post addPost;


  _FullArticleControllerControllerState({ article: Post, bool bookmarkList, List<Post> articles }) {
    post = article;
    _bookmarkList = bookmarkList;
    posts = articles;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaSetCurrentScreen(screenName:"learn_article_categories_${post.id.toString()}" );
    initModel();
    data = SingletonShareData();
    webViewState = WebViewState.onCreate;
    _scrollController = ScrollController();
    _textEditingController = TextEditingController(text: post.title.rendered);
    _scrollController.addListener(() {
//      setState(() {
//        scrollUp = (_scrollController.offset > 50);
//        multiLineScrollUp = (_textEditingController.text.length >= 50 &&
//            _scrollController.offset >  _textEditingController.text.length * 2);
//      });
    });
  }

  initModel() async {
    bool flag = await ArticleApiProvider().hasBookmark(post);
    debugPrint(flag ? "t" : "n");
    setState(() {
      isArticleBookmarked = flag;
    });

    if (post.id == 1353 || post.id == 1351 || post.id == 1348 || post.id == 1125 ) {
      setState(() {
        isButtonDisplay = false;
        embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/26/%e5%a6%b3%e9%97%9c%e5%bf%83%e8%87%aa%e5%b7%b1%e7%9a%84%e4%b9%b3%e6%88%bf%e5%81%a5%e5%ba%b7%e5%97%8e-%e8%97%a5%e7%89%a9%e6%b2%bb%e7%99%82/";
      });
    }

    if (post.id == 85) {
      setState(() {
        isButtonDisplay = false;
        embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/09/herceptin-trastuzumab/";
        // embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/herceptin-trastuzumab/";
      });
    }

    if (post.id == 200) {
      setState(() {
        isButtonDisplay = false;
        embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/08/kadcyla-trastuzumab-emtansine/";
        // embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/kadcyla-trastuzumab-emtansine/";
      });
    }

    if (post.id == 229) {
      setState(() {
        isButtonDisplay = false;
        embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/07/perjeta-pertuzumab/";
        // embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/perjeta-pertuzumab/";
      });
    }

    _textEditingController = TextEditingController(text: (embeddedUrl != null) ? "": post.title.rendered);
  }

  ArticleCustomUI getArticleCustomUI({dynamic customUI}) {
    var document = parse(customUI);
    customUI = parse(document.body.text).documentElement.text;
    var context2 = customUI.replaceAll('“', '"');
    context2 = context2.replaceAll('”', '"');
    context2 = context2.replaceAll('″', '"');
    context2 = context2.split('\n').join('');
    context2 = context2.trim();
    Map<String, dynamic> json2 = json.decode(context2);
    ArticleCustomUI articleCustomUI = ArticleCustomUI.fromJson(json2);
    return articleCustomUI;
  }

  Widget style2UI({ArticleCustomUI articleCustomUI}) {
   return Padding(
      padding: EdgeInsets.only(
          bottom: SizeConfig().blockSizeVertical * 5),
      child: Container(width: SizeConfig().blockSizeHorizontal * 40,
        height: SizeConfig().blockSizeHorizontal * 60,
        decoration: BoxDecoration(
            border: Border.all(
                width: 1.0,
                color: Colors.white
            ),
            borderRadius: BorderRadius.all(
                Radius.circular(
                    5) //                 <--- border radius here
            ),
            color: Colors.white,
            boxShadow: defaultBoxShadow
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig().blockSizeHorizontal *
                            4.65),
                    child:SizedBox(
                        width: SizeConfig().blockSizeHorizontal * 17.3,
                        height: SizeConfig().blockSizeHorizontal * 17.3,
                        child: CachedNetworkImage(
                          imageUrl: articleCustomUI.imageLink,
                          // placeholder: (context, string) {
                          //   return Image.asset("", height: 200, fit: BoxFit.contain, alignment: Alignment.topCenter,);
                          // }, height: 200, width: MediaQuery.of(context).size.width, fit: BoxFit.contain, alignment: Alignment.topCenter,),
                          placeholder: (context, string) {
                            return Center(
                                child: CircularProgressIndicator());
                          },
                        )
                    )),
                Container(
                  width: SizeConfig().blockSizeHorizontal * 35 ,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: SizeConfig().blockSizeHorizontal *
                            4.65,
                        bottom: SizeConfig().blockSizeHorizontal *
                            4.65),
                    child: Text(
                        articleCustomUI.title, maxLines: 20,
                        overflow: TextOverflow.ellipsis,
                        style: subtitleTxtStyle.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 18 * _articleFont,
                            color: Color.fromRGBO(
                                85, 85, 85, 1)),textAlign: TextAlign.center),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

    Column fetchedHtmlHere({String html}){

      print("start getting html image:${post.content.rendered}");
    print("start getting html image");

    List contextList = html.split("*****");
    List<Widget> widgetList = List<Widget>();
    bool skipStyle2UI = false;

        for (var i = 0; i < contextList.length; i++) {
         var context = contextList[i];
        if(context.contains('context')){
          ArticleCustomUI articleCustomUI = getArticleCustomUI(customUI: context);
        if(skipStyle2UI){
          skipStyle2UI = false;
        } else  if(articleCustomUI.context == "style2"){
           if( i + 1 < contextList.length){
             var context2 = contextList[i + 1];
             if(context2.contains('context')){
               ArticleCustomUI articleCustomUI2 = getArticleCustomUI(customUI: context2);
               if(articleCustomUI2.context == "style2"){
                 skipStyle2UI = true;
                 widgetList.add(Row( mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                   style2UI(articleCustomUI: articleCustomUI),
                   style2UI(articleCustomUI: articleCustomUI2)
                 ]));
               }
             } else {
               widgetList.add(Row( mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                 style2UI(articleCustomUI: articleCustomUI)
               ]));
             }
           }

          } else  if(articleCustomUI.context == "style3"){
            widgetList.add(
                Padding(
                  padding: EdgeInsets.only(
                      bottom: SizeConfig().blockSizeVertical * 5),
                  child: Container(width: SizeConfig().blockSizeHorizontal * 87,
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 1.0,
                            color: Colors.white
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(
                                5) //                 <--- border radius here
                        ),
                        color: Colors.white,
                        boxShadow: defaultBoxShadow
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig().blockSizeHorizontal * 3.6,
                              top: SizeConfig().blockSizeHorizontal * 3.6,bottom: SizeConfig().blockSizeHorizontal * 3.6),
                          child: SizedBox(
                              width: SizeConfig().blockSizeHorizontal * 12.8,
                              height: SizeConfig().blockSizeHorizontal * 12.8,
                              child: CachedNetworkImage(
                                imageUrl: articleCustomUI.imageLink,
                                // placeholder: (context, string) {
                                //   return Image.asset("", height: 200, fit: BoxFit.contain, alignment: Alignment.topCenter,);
                                // }, height: 200, width: MediaQuery.of(context).size.width, fit: BoxFit.contain, alignment: Alignment.topCenter,),
                                placeholder: (context, string) {
                                  return Center(
                                      child: CircularProgressIndicator());
                                },
                              )
                          ),
                        )
                        , Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig().blockSizeHorizontal * 3.2),
                          child: Container(
                            width: SizeConfig().blockSizeHorizontal * 55.5,
                            child:

                                     Text(
                                          articleCustomUI.title, maxLines: 20,
                                          overflow: TextOverflow.ellipsis,
                                          style: subtitleTxtStyle.copyWith(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18 * _articleFont,
                                              color: Color.fromRGBO(
                                                  85, 85, 85, 1))),

                                 ),

                          ),
                      ],
                    ),
                  ),
                ));
          } else {
            widgetList.add(
                Padding(
                  padding: EdgeInsets.only(
                      bottom: SizeConfig().blockSizeVertical * 5),
                  child: Container(width: SizeConfig().blockSizeHorizontal * 87,
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 1.0,
                            color: Colors.white
                        ),
                        borderRadius: BorderRadius.all(
                            Radius.circular(
                                5) //                 <--- border radius here
                        ),
                        color: Colors.white,
                        boxShadow: defaultBoxShadow
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig().blockSizeHorizontal * 5.6,
                              top: SizeConfig().blockSizeHorizontal * 4.53),
                          child: SizedBox(
                              width: SizeConfig().blockSizeHorizontal * 17.3,
                              height: SizeConfig().blockSizeHorizontal * 17.3,
                              child: CachedNetworkImage(
                                imageUrl: articleCustomUI.imageLink,
                                // placeholder: (context, string) {
                                //   return Image.asset("", height: 200, fit: BoxFit.contain, alignment: Alignment.topCenter,);
                                // }, height: 200, width: MediaQuery.of(context).size.width, fit: BoxFit.contain, alignment: Alignment.topCenter,),
                                placeholder: (context, string) {
                                  return Center(
                                      child: CircularProgressIndicator());
                                },
                              )
                          ),
                        )
                        , Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig().blockSizeHorizontal * 3.2),
                          child: Container(
                            width: SizeConfig().blockSizeHorizontal * 55.5,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: SizeConfig().blockSizeHorizontal *
                                          4.65,
                                      bottom: SizeConfig().blockSizeHorizontal *
                                          4.1),
                                  child: Text(
                                      articleCustomUI.title, maxLines: 20,
                                      overflow: TextOverflow.ellipsis,
                                      style: subtitleTxtStyle.copyWith(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18 * _articleFont,
                                          color: Color.fromRGBO(
                                              85, 85, 85, 1))),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      bottom: SizeConfig().blockSizeHorizontal *
                                          7.4),
                                  child: Text(
                                      articleCustomUI.context, maxLines: 20,
                                      overflow: TextOverflow.ellipsis,
                                      style: subtitleTxtStyle.copyWith(
                                          fontSize: 14 * _articleFont,
                                          color: Color.fromRGBO(
                                              85, 85, 85, 1))),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ));
          }
        } else {
          if(contextList.length > 1){
            widgetList.add(Padding(
              padding: EdgeInsets.only(bottom:SizeConfig().blockSizeVertical * 5 ),
              child: showHtml(html: context),
            ));
          } else {
            widgetList.add(showHtml(html: context));
          }

        }
      }

      if(post.id == 2087 || post.id == 2050 || post.id == 2052 || post.id == 2090 || post.id == 2057 || post.id == 2059 || post.id == 2084 || post.id == 2062 || post.id == 2054 || post.id == 2065){
        for (Post postData in data.postList) {
          if(post.id == 2087 || post.id == 2050 || post.id == 2052){
            if(postData.id == 1246){
              addPost = postData;
            }
          } else if(post.id == 2090 || post.id == 2057 || post.id == 2059){
            if(postData.id == 1253){
              addPost = postData;
            }
          } else if(post.id == 2084 || post.id == 2062 || post.id == 2054){
            if(postData.id == 1250){
              addPost = postData;
            }
          } else if(post.id == 2065){
            if(postData.id == 2381){
              addPost = postData;
            }
          }
        }
      }

  return  Column(
    mainAxisAlignment: MainAxisAlignment.start,
      children: widgetList.toList(),
    );

  }

  Widget showHtml({String html}){

    return Html(
      data: html,
      customRender: (node, children) {
        debugPrint("node:"+node.toString());
        if (node is dom.Element) {
          switch (node.localName) {
            case "figcaption":
              debugPrint("figcaption");
              return Column(
                children: children.map((c) => Center(child: c,)).toList(),
              );
          }
        }
        return Column(children: children,);
      },
      customTextStyle: (node, baseStyle) {
        if (node is dom.Element) {
          switch (node.localName) {
            case "ref":
              return baseStyle.merge(TextStyle(fontSize: 24, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700));
            case "img":
              return baseStyle.merge(TextStyle(fontSize: 24, color: kLightHealthTheme.primaryColor, backgroundColor: Colors.white, fontWeight: FontWeight.w700));
          }
        }
        return baseStyle;
      },
      defaultTextStyle: TextStyle(color: Color(0xFF6A6969), fontSize: 18.0 * _articleFont, decoration: TextDecoration.none, fontFamily: 'Raleway' ),
      onImageTap: (source) {

      },
      onLinkTap: (String url) async {
        print("debugPrintUrl:$url");
        if (url.contains(".pdf")) {
          if (await canLaunch(url)) {
            await launch(url);
          }
        } else {
         // _scrollController.animateTo(0, duration: Duration(microseconds: 100), curve: Curves.easeIn);
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) =>  BaseViewController(WebViewPage(webViewUrl:url,))),
          );


        }
      },
    );
  }

Widget showLoading(){

  if(webViewState == WebViewState.onStart){
    return SafeArea(bottom: false,child: SkeletonView(topPadding: webViewTopPadding));
  } else {
    return Container();
  }
}

//  Widget _popUpWebView() {
//
//    if (embeddedUrl == null) {
//      return Container();
//    }
//
//
//    return SafeArea(bottom: false,child:Padding(
//      padding: EdgeInsets.only(top:webViewState == WebViewState.onFinish ? 0 : 200000),
//      child: Container(
//        margin: EdgeInsets.only(top: webViewTopPadding),
//        child:  new WebviewScaffold(
//          url: "https://www.google.com",
//          appBar: new AppBar(
//            title: new Text("Widget webview"),
//          ),
//        )
//
//        WebView(
//          javascriptMode: JavascriptMode.unrestricted,
//          initialUrl: embeddedUrl,
//          onWebViewCreated: (webViewController) {
//            print("onWebViewCreated:$webViewController");
//            setState(() {
//              webViewState = WebViewState.onStart;
//            });
//          },
//          onPageFinished: (String url) {
//            print("onPageFinished:$url");
//            setState(() {
//              webViewState = WebViewState.onFinish;
//            });
//          },
//        ),
//      ),
//    )
//    );
//
//  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            top: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                controller: _scrollController,
                slivers: <Widget>[
                  CustomSliverAppBarView( isBack: true,titleText: "",type: SliverAppBarType.FullArticlePage,widgetList: <Widget>[
                    (isButtonDisplay) ?
                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.only(right: defaultPadding),
                      child: InkWell(
                        onTap: () {
                          if (_articleFont == 1.5) {
                            setState(() {
                              _articleFont = 1;
                            });
                          } else {
                            setState(() {
                              _articleFont += 0.25;
                            });
                          }
                        },
                        child: Container(
                            child:
                            Image.asset(Lang.isEng() ? "assets/images/icon_fontsize.png" : "assets/images/icon_fontsize_v2.png", width: 30)
                        ),
                      ),
                    ) : Container(),
                    isButtonDisplay ?
                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.only(right: defaultPadding),
                      child: InkWell(
                        onTap: () {
                          if (!isArticleBookmarked) {
                            setState(() {
                              isBookmarked = true;
                              isArticleBookmarked = true;
                            });
                            ArticleApiProvider().bookmarkPost(post);
                            Future.delayed(Duration(seconds: 2)).then((dynamic) {
                              setState(() {
                                isBookmarked = false;
                              });
                            });
                          } else {

                            showDialog(
                                barrierDismissible: true,
                                context: context,
                                builder: (buildContext) {
                                  return CustomAlertDialog(
                                      title: Lang.getString(
                                          "learn_bookmark_remove_message"),
                                      content: null,
                                      leftButtonText: Lang.getString(
                                          "cancel_button"),
                                      rightButtonText: Lang.getString(
                                          "remove_button"),
                                      buildContext: buildContext,
                                      rightButtonFunction: () async {
                                        ArticleApiProvider().removeBookmark(
                                            post);
                                        Navigator.of(context).pop();
                                        setState(() {
                                          isBookmarked = false;
                                          isArticleBookmarked = false;
                                        });
                                        SingletonShareData().refreshBookMarkPageCallBack
                                            .sink.add(true);
                                      });

                                }
                            );
                          }
                        },
                        child: Padding(
                          padding:  EdgeInsets.only(top:5),
                          child: Container(
                            child: Image.asset(isArticleBookmarked
                                ? "assets/images/icon_removefrombookmark.png"
                                : "assets/images/icon_addtobookmark.png",
                              width: 30),
                          ),
                        ),
                      ),
                    ) : Container(),
                  ]
                  ),
                  SliverToBoxAdapter(
                    child: Column(
                      children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width,
                            color: kLightHealthTheme.primaryColor,
                            padding: EdgeInsets.only(bottom: 10, top: 0, right: 20, left: 20),
                            child: Column(
                              children: <Widget>[
                                Scrollbar(
                                    child:  SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      reverse: true,
                                      child:TextField(readOnly: true, controller: _textEditingController, maxLines: null,
                                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 30 ), decoration: InputDecoration(labelStyle: TextStyle(color: Colors.transparent), enabledBorder: InputBorder.none,))
                            )),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: FractionalOffset(0.0, 0.0),
                                      child: Wrap(
                                        children: (post.getTagList()).map((Tag tag) {
                                          return Container(
                                            margin: EdgeInsets.only(top: 5 ),
                                            child: Container(
                                              margin: EdgeInsets.only(right: 4),
                                              padding: EdgeInsets.only(bottom: 2, left: 10, right: 10),
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(color: kLightHealthTheme.primaryColor),
                                                  borderRadius: new BorderRadius.all(Radius.circular(20))
                                              ),
                                              child: Text(tag.name, style: TextStyle(color: kLightHealthTheme.primaryColor, fontSize: 16, fontWeight: FontWeight.w400),),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            )
                        ),
                        WaveBar(),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: embeddedUrl != null ? Container() : Container(
                      alignment: Alignment(0, 0),
                      margin: EdgeInsets.only(top: 0, left: 20, right: 20, bottom: 20),
                      child: Center(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,

                            child: Column(
                              children: <Widget>[
                                fetchedHtmlHere(html: post.content.rendered),
                           addPost != null ?  Padding(
                              padding: EdgeInsets.only(top: SizeConfig().leftPadding),
                              child: isExpanded ?  fetchedHtmlHere(html: addPost.content.rendered) : MaterialButton(
                                elevation: 2.0,
                                child: Container(
                                  width: SizeConfig().blockSizeHorizontal * 65,
                                  height: SizeConfig().blockSizeHorizontal * 13,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/images/learnMore.png'),
                                        fit: BoxFit.cover),
                                  )
                                ),
                                // ),
                                onPressed: () {
                                 setState(() {
                                   isExpanded = !isExpanded;
                                 });
                                },
                              )) : Container()
                              ],
                            )

                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        //  _popUpWebView(),
          showLoading(),
          (isArticleBookmarked && isBookmarked) ? Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(top: 10, bottom: 60, left: 20, right: 20),
            child: ButtonTheme(
              minWidth: 300,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(color: Color.fromARGB(40, 59, 0, 27),
                      blurRadius: 6.0,
                      offset: Offset(0.0, 3.0),
                    )
                  ]
                ),
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(Lang.getString("learn_bookmark_message"), style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              isBookmarked = false;
                            });
                          },
                          child: Text(Lang.getString("close_button"), style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: kLightHealthTheme.primaryColor),),
                        ),
                      )
                    ],
                  ),
                  onPressed: () async {

                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(side: BorderSide(color: Color(0xFFAD3D5F)), borderRadius: BorderRadius.all(Radius.circular(30))),
                  disabledColor:  Color(0xFFF9EBEC),
                  textColor: Color(0xFFffffff),
                  color: Color(0xFFAD3D5F),
                ),
              ),
            ),
          ) : Container()
        ],
      ),
    );
  }
}
