import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customDropDown.dart' as prefix0;
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:icanfight/utilities/themes.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:icanfight/controller/assessmentListingController.dart';
import 'package:icanfight/api_provider/assessmentApiProvider.dart';
import 'package:icanfight/model/assessment.dart';

import 'baseViewController.dart';

class AssessmentController extends StatefulWidget {
  @override
  _AssessmentControllerState createState() => _AssessmentControllerState();
}

class _AssessmentControllerState extends State<AssessmentController>
    with TickerProviderStateMixin {
  Map<String, double> dataMap = new Map();
  Map<String, double> exerciseDataMap = new Map();
  List<Color> colorList = [
    Color(0xFFFB729C),
    Color(0xFFF4E0E8),
    Color(0xFFE14672),
  ];

  List _types = [
    Lang.getString("calendar_weekly")+" ("+ DateFormat("MMMdd日","zh_HK").format(DateTime.now().add(Duration(days: -7))) +" - "+ DateFormat("MMMdd日","zh_HK").format(DateTime.now()) +")",
    Lang.getString("calendar_biweekly")+" ("+ DateFormat("MMMdd日","zh_HK").format(DateTime.now().add(Duration(days: -14))) +" - "+ DateFormat("MMMdd日","zh_HK").format(DateTime.now()) +")",
    Lang.getString("calendar_monthly")+" ("+ DateFormat("MMMdd日","zh_HK").format(DateTime.now().add(Duration(days: -30))) +" - "+ DateFormat("MMMdd日","zh_HK").format(DateTime.now()) +")",
  ];
  List<prefix0.CustomDropdownMenuItem<String>> _dropDownMenuItems;
  String _selectedPeriod;
  List<List<charts.Series<LinearSales, DateTime>>> _seriesList = [];

  bool _isTodayComplete = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getStats(7);
    _dropDownMenuItems = buildAndGetDropDownMenuItems(_types);
    _selectedPeriod = _dropDownMenuItems[0].value;
    ga.gaSetCurrentScreen(screenName:"assessment_page");
  }

  handleAppLifecycleState() async {
    SystemChannels.lifecycle.setMessageHandler((msg) {

      if (msg == AppLifecycleState.resumed.toString()) {
        getStats(7);
      }

    });
  }

  getStats(int dateCount) async {

    _seriesList = [];

    List<Assessment> allAssList = await AssessmentApiProvider().getAssessments();
    initializeDateFormatting('zh_HK', null);
    for(int i=0; i<5; i++) {

      final List<LinearSales> data = [];

      for(int i=0; i<dateCount;i++) {
        data.add(new LinearSales(0, DateTime.now().subtract(Duration(days: i))));
      }

      allAssList.forEach((Assessment ass) {

        data.forEach((ls) {

          if (DateFormat("dd-MMM-yyyy",'zh_HK').format(ls.date).indexOf(ass.date) >= 0) {
            ls.score = (int.parse(ass.scores[i]) > -1) ? int.parse(ass.scores[i])+1 : 0;
          }

        });

      });

      _seriesList.add(
          [
            new charts.Series<LinearSales, DateTime>(
              id: 'Sales',
              colorFn: (_, __) => charts.Color(r: 251, g: 114, b: 156, a: 255),
              domainFn: (LinearSales sales, _) => sales.date,
              measureFn: (LinearSales sales, _) => sales.score,
              data: data,
            )
          ]
      );

    }

    double hy_more = 0;
    double hy_less = 0;
    double ex1 = 0;
    double ex2 = 0;
    double ex3 = 0;

    allAssList.forEach((Assessment ass) {

      debugPrint("scores: "+ass.scores[5]);

      if(ass.scores[6] == "1") {
        hy_more++;
      }
      if(ass.scores[6] == "0") {
        hy_less++;
      }
      if(ass.scores[5] == "0") {
        ex1++;
      }
      if(ass.scores[5] == "1") {
        ex2++;
      }
      if(ass.scores[5] == "2") {
        ex3++;
      }
    });

    dataMap.putIfAbsent(Lang.getString("assessment_hydration_more"), () => hy_more);
    dataMap.putIfAbsent(Lang.getString("assessment_hydration_less"), () => hy_less);

    exerciseDataMap.putIfAbsent(Lang.getString("assessment_exercise_1"), () => ex1);
    exerciseDataMap.putIfAbsent(Lang.getString("assessment_exercise_3"), () => ex2);
    exerciseDataMap.putIfAbsent(Lang.getString("assessment_exercise_2"), () => ex3);


    List<String> assLists = await AssessmentApiProvider().getTodayAssessments();

    int count = 0;
    assLists.forEach((value) {
      if (value != "-1") {
        count++;
      }
    });

    debugPrint("count:"+count.toString());

    if (count == 7 ) {
      setState(() {
        _isTodayComplete = true;
      });
    } else {
      setState(() {
        _isTodayComplete = false;
      });
    }

  }

  List<prefix0.CustomDropdownMenuItem<String>> buildAndGetDropDownMenuItems(List fruits) {
    List<prefix0.CustomDropdownMenuItem<String>> items = List();
    for (String fruit in fruits) {
      items.add(prefix0.CustomDropdownMenuItem(value: fruit, child: Container(padding: EdgeInsets.only(left: 4) ,child: Text(fruit),)));
    }
    return items;
  }

  @override
  void dispose() {
    super.dispose();
  }

  void changedDropDownItem(String selectedType) {
    setState(() {
      _selectedPeriod = selectedType;
    });



    getStats((_types.indexOf(selectedType)*7+7));
  }

  Future navigateToSubPage(context) async {
    ga.gaEvent(name: "assessment_start_card");
    Navigator.push(context, MaterialPageRoute(builder: (context) => BaseViewController(AssessmentListingController()))).then((val) => { getStats(7) });
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: kLightHealthTheme.primaryColor,
        child: Stack(children: <Widget>[
      SafeArea(
        child: Material(
          color: Color(0xFFF9EBEC),
          child: CustomScrollView(
            physics: new ClampingScrollPhysics(),
            slivers: <Widget>[
              CustomSliverAppBarView(isBack: true,type: SliverAppBarType.AssessmentPage, titleText: Lang.getString("assessment_title"),
                  widgetList:[
                Padding(
                    padding: EdgeInsets.only(top: topPadding),
                    child: Container(
                        width: 35)
                ),
                Padding(
                    padding: EdgeInsets.only(top: topPadding),
                    child: Container(
                        width: 35)),
                Padding(
                    padding: EdgeInsets.only(top: topPadding),
                    child: Container(
                        width: 35)),
              ]),
              SliverToBoxAdapter(
                child: WaveBar(),
              ),
              SliverToBoxAdapter(
                child:
                Container(
                    margin: EdgeInsets.only(top: 0, bottom: 10),
                    child: SingleChildScrollView(
                        child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: MediaQuery.of(context).size.width,
                                minHeight: MediaQuery.of(context).size.height),
                            child: IntrinsicHeight(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  GestureDetector(
                                      onTap: () {
                                        navigateToSubPage(context);
                                      },
                                      child: _isTodayComplete ? Container(
                                        alignment: Alignment.center,
                                        child: Stack(
                                          children: <Widget>[
                                            Container(
                                              child: SvgPicture.asset(
                                              Lang.isEng() ? 'assets/images/assessmentComplete.svg' : 'assets/images/assessmentComplete_cn.svg', width: SizeConfig().blockSizeHorizontal * 80),
                                            )
                                          ],
                                        ),
                                        height: 120,
                                        width: MediaQuery.of(context).size.width,
                                      ) : Container(
                                        alignment: Alignment.centerRight,
                                        child: Stack(
                                          children: <Widget>[
                                            Image.asset(
                                              Lang.isEng() ? 'assets/images/icon_assessment_TodayAssessment_v2.png' : 'assets/images/icon_assessment_TodayAssessment_cnt.png', height: 300,fit: BoxFit.cover),
                                          ],
                                        ),
                                        height: 270,
                                        width: MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.only(left: 20),
                                      )),
                                  Container(
                                      child: Text(
                                        Lang.getString("assessment_overview"),
                                        textAlign: TextAlign.left,
                                        style: new TextStyle(
                                            fontSize: 25.0, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w600),
                                      ),
                                      alignment: Alignment.centerLeft,
                                      margin: EdgeInsets.only(top: 20),
                                      padding: EdgeInsets.only(left: 20)),
                                  Container(
                                    color: kLightHealthTheme.primaryColor,
                                    height: 1,
                                    margin: EdgeInsets.only(top: 5, left: 20, right: 20),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20, top: 20, right: 20),
                                    alignment: Alignment.center,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
//                                        Container(
//                                          child: Text(Lang.getString("assessment_peroid"), style: TextStyle(fontSize: 16, color: Color(0xFF555555), fontWeight: FontWeight.w700),),
//                                        ),
                                        Expanded(
                                          child:Container(
                                          padding: EdgeInsets.only(left: 8, right: 8),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              border: Border.all(color: Colors.white70),
                                              borderRadius: new BorderRadius.all(Radius.circular(10))
                                          ),
                                          child: prefix0.DropdownButton(
                                            isExpanded: true,
                                            value: _selectedPeriod,
                                            items: _dropDownMenuItems,
                                            onChanged: changedDropDownItem,
                                            style: TextStyle(fontSize: 16, color: Color(0xFF555555), fontWeight: FontWeight.w600),
                                            underline: Container(),
                                            isDense: false,
                                          ),
                                        )
                                        )
                                        
                                      ],
                                    ),
                                  ),
                                  _sleep(),
                                  _energy(),
                                  _appetile(),
                                  _mood(),
                                  _social(),
                                  _exercise(),
                                  _hydration(),
                                ],
                              ),
                            )))),
              )
            ],
          ),
        ),
      ),
    ]));
  }

  Widget _sleep() {
    return
      Container(
        height: 400,
        margin: EdgeInsets.only(
            left: 20, right: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
            boxShadow: defaultBoxShadow),
        child: Column(children: <Widget>[
          Container(
              child: Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 40,
                      margin: EdgeInsets.only(left: 20),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(
                              'assets/images/Sleep.png'),
                          fit: BoxFit.fitWidth,
                        ),
                        //                    shape: BoxShape.circle,
                      )),
                  Container(
                      margin: EdgeInsets.only(left: 15),
                      child: Text(
                        Lang.getString("assessment_sleep"),
                        style: new TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Color(0xFF757575)),
                      )),
                ],
              ),
              padding: EdgeInsets.only(top: 20)),
          SimpleTimeSeriesChart(seriesList:_seriesList,seriesListIndex: 0),
        ]),
      );
  }

  Widget _energy() {
    return
      Container(
        height: 400,
        margin: EdgeInsets.only(
            left: 20, right: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
            boxShadow: defaultBoxShadow),
        child: Column(children: <Widget>[
          Container(
              child: Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 40,
                      margin: EdgeInsets.only(left: 20),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(
                              'assets/images/Energy.png'),
                          fit: BoxFit.fitWidth,
                        ),
                        //                    shape: BoxShape.circle,
                      )),
                  Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        Lang.getString("assessment_energy"),
                        style: new TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Color(0xFF757575)),
                      )),
                ],
              ),
              padding: EdgeInsets.only(top: 20)),
          SimpleTimeSeriesChart(seriesList:_seriesList,seriesListIndex: 1),
        ]),
      );
  }

  Widget _appetile() {
    return
      Container(
        height: 400,
        margin: EdgeInsets.only(
            left: 20, right: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
            boxShadow: defaultBoxShadow),
        child: Column(children: <Widget>[
          Container(
              child: Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 40,
                      margin: EdgeInsets.only(left: 20),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(
                              'assets/images/Appetite.png'),
                          fit: BoxFit.fitWidth,
                        ),
                        //                    shape: BoxShape.circle,
                      )),
                  Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        Lang.getString("assessment_appetite"),
                        style: new TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Color(0xFF757575)),
                      )),
                ],
              ),
              padding: EdgeInsets.only(top: 20)),
          SimpleTimeSeriesChart(seriesList:_seriesList,seriesListIndex: 2),
        ]),
      );
  }

  Widget _mood() {
    return
      Container(
        height: 400,
        margin: EdgeInsets.only(
            left: 20, right: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
        boxShadow: defaultBoxShadow),
        child: Column(children: <Widget>[
          Container(
              child: Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 40,
                      margin: EdgeInsets.only(left: 20),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(
                              'assets/images/Mood.png'),
                          fit: BoxFit.fitWidth,
                        ),
                        //                    shape: BoxShape.circle,
                      )),
                  Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        Lang.getString("assessment_mood"),
                        style: new TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Color(0xFF757575)),
                      )),
                ],
              ),
              padding: EdgeInsets.only(top: 20)),
          SimpleTimeSeriesChart(seriesList:_seriesList,seriesListIndex: 3),
        ]),
      );
  }

  Widget _social() {
    return
      Container(
        height: 400,
        margin: EdgeInsets.only(
            left: 20, right: 20, top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
        boxShadow: defaultBoxShadow),
        child: Column(children: <Widget>[
          Container(
              child: Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 40,
                      margin: EdgeInsets.only(left: 20),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(
                              'assets/images/Sociability.png'),
                          fit: BoxFit.fitWidth,
                        ),
                        //                    shape: BoxShape.circle,
                      )),
                  Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        Lang.getString("assessment_sociability"),
                        style: new TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Color(0xFF757575)),
                      )),
                ],
              ),
              padding: EdgeInsets.only(top: 20)),
          SimpleTimeSeriesChart(seriesList:_seriesList,seriesListIndex: 4),
        ]),
      );
  }

  Widget _hydration() {
    return
      Container(
        height: 450,
        child: (dataMap == null || dataMap.length == 0)
            ? Container()
            : Column(
          children: <Widget>[
            Container(
                child: Row(
                  children: <Widget>[
                    Container(
                        height: 60,
                        width: 25,
                        margin:
                        EdgeInsets.only(left: 20),
                        decoration: new BoxDecoration(
                          image: DecorationImage(
                            image: new AssetImage(
                                'assets/images/Hydration.png'),
                            fit: BoxFit.fitWidth,
                          ),
                          //                    shape: BoxShape.circle,
                        )),
                    Container(
                        margin:
                        EdgeInsets.only(left: 20),
                        child: Text(
                          Lang.getString("assessment_hydration"),
                          style: new TextStyle(
                              fontSize: 24,
                              fontWeight:
                              FontWeight.w800,
                              color: Color(0xFF757575)),
                        )),
                  ],
                ),
                padding: EdgeInsets.only(top: 20)),
            PieChart(
              dataMap: dataMap,
              legendFontColor: Colors.blueGrey[900],
              legendFontSize: 18.0,
              legendFontWeight: FontWeight.w500,
              animationDuration:
              Duration(milliseconds: 800),
              chartLegendSpacing: 32.0,
              chartRadius:
              MediaQuery.of(context).size.width / 2,
              showChartValuesInPercentage: true,
              showChartValues: true,
              showChartValuesOutside: false,
              chartValuesColor: Colors.white,
              colorList: colorList,
              showLegends: false,
              decimalPlaces: 0,
            ),
            Row(
              children: <Widget>[
                Container(
                    color: colorList[0],
                    width: 20,
                    height: 40,
                    margin: EdgeInsets.only(
                        left: 20, right: 20)),
                Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          ((dataMap[Lang.getString("assessment_hydration_more")] + dataMap[Lang.getString("assessment_hydration_less")]) == 0) ? "0%" : (dataMap[Lang.getString("assessment_hydration_more")] / (dataMap[Lang.getString("assessment_hydration_more")] + dataMap[Lang.getString("assessment_hydration_less")]) * 100).round().toString()+"%",
                          textAlign: TextAlign.left,
                        ),
                        Text(Lang.getString("assessment_hydration_more"), textAlign: TextAlign.left,),
                      ],
                    ))
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                    color: colorList[1],
                    width: 20,
                    height: 40,
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 10)),
                Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          ((dataMap[Lang.getString("assessment_hydration_more")] + dataMap[Lang.getString("assessment_hydration_less")]) == 0) ? "0%" : (dataMap[Lang.getString("assessment_hydration_less")] / (dataMap[Lang.getString("assessment_hydration_more")] + dataMap[Lang.getString("assessment_hydration_less")]) * 100).round().toString()+"%",
                          textAlign: TextAlign.left,
                        ),
                        Text(Lang.getString("assessment_hydration_less")),
                      ],
                    ))
              ],
            ),
          ],
        ),
        margin: EdgeInsets.only(left: 20, right: 20, top: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
        boxShadow: defaultBoxShadow),
      );
  }

  Widget _exercise() {
    return
      Container(
        height: 500,
        child: (exerciseDataMap == null || exerciseDataMap.length == 0)
            ? Container()
            : Column(
          children: <Widget>[
            Container(
                child: Row(
                  children: <Widget>[
                    Container(
                        height: 60,
                        width: 35,
                        margin:
                        EdgeInsets.only(left: 20),
                        decoration: new BoxDecoration(
                          image: DecorationImage(
                            image: new AssetImage(
                                'assets/images/Exercise.png'),
                            fit: BoxFit.fitWidth,
                          ),
                          //                    shape: BoxShape.circle,
                        )),
                    Container(
                        margin:
                        EdgeInsets.only(left: 20),
                        child: Text(
                          Lang.getString("assessment_exercise"),
                          style: new TextStyle(
                              fontSize: 24,
                              fontWeight:
                              FontWeight.w800,
                              color: Color(0xFF757575)),
                        )),
                  ],
                ),
                padding: EdgeInsets.only(top: 20)),
            PieChart(
              dataMap: exerciseDataMap,
              legendFontColor: Colors.blueGrey[900],
              legendFontSize: 18.0,
              legendFontWeight: FontWeight.w500,
              animationDuration:
              Duration(milliseconds: 800),
              chartLegendSpacing: 32.0,
              chartRadius:
              MediaQuery.of(context).size.width / 2,
              showChartValuesInPercentage: true,
              showChartValues: true,
              showChartValuesOutside: false,
              chartValuesColor: Colors.white,
              colorList: colorList,
              showLegends: false,
              decimalPlaces: 0,
            ),
            Row(
              children: <Widget>[
                Container(
                    color: colorList[0],
                    width: 20,
                    height: 40,
                    margin: EdgeInsets.only(
                        left: 20, right: 20)),
                Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")] == 0) ? "0%" : (exerciseDataMap[Lang.getString("assessment_exercise_1")] / (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")]) * 100).round().toString()+"%",
                          textAlign: TextAlign.left,
                        ),
                        Text(Lang.getString("assessment_exercise_1"), textAlign: TextAlign.left,),
                      ],
                    ))
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                    color: colorList[1],
                    width: 20,
                    height: 40,
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 10)),
                Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")] == 0) ? "0%" : (exerciseDataMap[Lang.getString("assessment_exercise_2")] / (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")]) * 100).round().toString()+"%",
                          textAlign: TextAlign.left,
                        ),
                        Text(Lang.getString("assessment_exercise_2")),
                      ],
                    ))
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                    color: colorList[2],
                    width: 20,
                    height: 40,
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 10)),
                Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")] == 0) ? "0%" : (exerciseDataMap[Lang.getString("assessment_exercise_3")] / (exerciseDataMap[Lang.getString("assessment_exercise_1")] + exerciseDataMap[Lang.getString("assessment_exercise_2")] + exerciseDataMap[Lang.getString("assessment_exercise_3")]) * 100).round().toString()+"%",
                          textAlign: TextAlign.left,
                        ),
                        Text(Lang.getString("assessment_exercise_3")),
                      ],
                    ))
              ],
            ),
          ],
        ),
        margin: EdgeInsets.only(left: 20, right: 20, top: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0)),
        boxShadow: defaultBoxShadow),
      );
  }


}

class LocalizedDateTimeFactory extends charts.LocalDateTimeFactory {

  @override
  DateFormat createDateFormat(String pattern) {
    return DateFormat(pattern, 'zh_HK');
  }

}

class LinearSales {
  int score;
  final DateTime date;

  LinearSales(this.score, this.date);
}


class SimpleTimeSeriesChart extends StatelessWidget {
  final List seriesList;
  final int seriesListIndex;
  SimpleTimeSeriesChart({this.seriesList,this.seriesListIndex});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      height: 300,
      child: (seriesList.length > 0) ? charts.TimeSeriesChart(
        seriesList[seriesListIndex],
        dateTimeFactory: LocalizedDateTimeFactory(),
        domainAxis: charts.DateTimeAxisSpec(
            tickFormatterSpec: charts.AutoDateTimeTickFormatterSpec(
                day: charts.TimeFormatterSpec(
                  format: 'MMMdd日', transitionFormat: 'MMMdd日',))),
              primaryMeasureAxis: charts.NumericAxisSpec(tickFormatterSpec: charts.BasicNumericTickFormatterSpec((num value) {
                if (value == 0) {
                  return Lang.getString("assessment_poor");
                }

                if (value == 10) {
                  return Lang.getString("assessment_good");
                }

                return '';
              })),
        animate: false,
        defaultRenderer: charts.LineRendererConfig(includePoints: true, includeLine: true),
      ) : Container(),
    );
  }


}

