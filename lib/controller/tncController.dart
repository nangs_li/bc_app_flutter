import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/bottomButton.dart';


import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'baseViewController.dart';
import 'signInController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';

import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

class TncController extends StatefulWidget {
  bool _isAuth;

  TncController({bool isAuth = false}) {
    _isAuth = isAuth;
  }

  @override
  _TncControllerState createState() => _TncControllerState(isAuth: _isAuth);
}

class _TncControllerState extends State<TncController>
    with SingleTickerProviderStateMixin {
  Animation<Offset> _offset;
  AnimationController _animationController;
  ScrollController _scrollController;
  bool _isAuth;

  _TncControllerState({bool isAuth = false}) {
    _isAuth = isAuth;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaSetCurrentScreen(screenName: "terms_n_conditions_page");
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    _offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 1.0))
        .animate(_animationController);
    _animationController.forward();

    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset > 50) {
        _animationController.reverse();
      }
    });
    SingletonShareData data = SingletonShareData();
    data.tncApiCallBack.listen((tnc) {
      setState(() {
        data.tnc = tnc;
      });
    });
//    data.getTNC();
  }

  void agreeAction() async {
    ga.gaEvent(name: "tnc_agree_button");
    if (_isAuth) {
      Navigator.pop(context);
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BaseViewController(SignInController())),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    SingletonShareData data = SingletonShareData();

    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack:true ,titleText: Lang.getString("tnc_title"),type: SliverAppBarType.TNCPage),
              SliverToBoxAdapter(
                child: WaveBar()),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment(0, 0),
            margin: EdgeInsets.only(top: 90),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Color.fromARGB(20, 255, 255, 255),
                blurRadius: 30.0,
                offset: Offset(0.0, -3.0),
              )
            ]),
            child: (data.tnc == null) ? Center(child: CircularProgressIndicator()) : SafeArea(
              bottom: false,
              child: Stack(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      child: Card(
                        color: Colors.white,
                        margin: EdgeInsets.only(bottom: 0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0),)),
                        child: SingleChildScrollView(
                          controller: _scrollController,
                          scrollDirection: Axis.vertical,
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 20, bottom: 120, left: 20, right: 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Html(
                                  data:(Lang.locale == "en") ? data.tnc.contentEN : data.tnc.contentCN,
                                  useRichText: true,
                                  customTextStyle: (node, baseStyle) {
                                    if (node is dom.Element) {
                                      switch (node.localName) {
                                        case "h1":
                                          return baseStyle.merge(TextStyle(fontSize: 24, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700));
                                        case "p":
                                          return baseStyle.merge(TextStyle(fontSize: 16, fontFamily: "PTSerif"));
                                        case "h2":
                                          return baseStyle.merge(TextStyle(fontSize: 20, fontWeight: FontWeight.w700, fontStyle: FontStyle.italic));
                                      }
                                    }
                                    return baseStyle;
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                        decoration: defaultBoxDecoration
                    ),
                    Container(
                      height: 30,
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: <Color>[
                                Color(0xFFFFFFFF),
                                Color(0x00FFFFFF)
                              ]),
                          borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    ),
                  ],
                )

            ),
          ),
          !_isAuth ? SlideTransition(
            position: _offset,
            child:  BottomButton(buttonText:Lang.getString("tnc_button"),onPressed: () async {
              agreeAction();
            }),
          ) : Container()
        ],
      ),
    );
  }
}
