import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/controller/baseViewController.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/utils.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:flutter/cupertino.dart';




class SignInController extends StatefulWidget {
  @override
  _SignInControllerState createState() => _SignInControllerState();
}

class _SignInControllerState extends State<SignInController> {
  ProgressDialog pr;
  List<FocusNode> _formNodes = [FocusNode(), FocusNode()];
  List<TextEditingController> _textEditingControllers = [
    TextEditingController(text: ""),TextEditingController(text: "")
  ];
  bool isEmailValid = true;
  bool isPwValid = true;
  FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaSetCurrentScreen(screenName: "signin_page");
    Future.delayed(const Duration(milliseconds: 500), () {
    showBiometric();
    });
  }

  void showBiometric() async {
    SingletonShareData data = SingletonShareData();
    List<String> account = await Utils.getAccount();
    if (account != null) {
      data.initBiometric(context, BiometricPage.SignInPage);
    }
  }


  void resetPassword() async {
    SingletonShareData data = SingletonShareData();

    setState(() {
      isEmailValid = _textEditingControllers[0].text.length > 0;//Utils.isValidate(ValidationType.EMAIL, _textEditingControllers[0].text););
    });

    if (_textEditingControllers[0].text.length > 0) {

      data.showLoading(context);
      try {
        await _auth.sendPasswordResetEmail(
          email: _textEditingControllers[0].text,
        );
        data.hideLoading(context);
        showDialog(
            barrierDismissible: true,
            context: context,
            builder: (buildContext) {
              return CustomAlertDialog(title: Lang.getString("login_message"), content: Lang.getString(
                  "login_forgetpw_content"), leftButtonText: null,rightButtonText:Lang.getString(
                  "login_close") ,buildContext: buildContext,
                  rightButtonFunction:  () {
                    Navigator.of(context).pop();
                  });
            }
        );

      } on PlatformException catch (e) {
        data.hideLoading(context);
        print("PlatformExceptionError:$e");
        showDialog(
            barrierDismissible: true,
            context: context,
            builder: (buildContext) {
              return CustomAlertDialog(title: Lang.getString("login_error"), content: data.getErrorMessage(error:e, type:ErrorType.ForgetPassword), leftButtonText: null,rightButtonText:Lang.getString(
                  "login_close") ,buildContext: buildContext,
                  rightButtonFunction:  () {
                    Navigator.of(context).pop();
                  });

            }
        );
      }
    }

  }

  void loginAction() async {
    ga.gaEvent(name: "signin_button");
    SingletonShareData data = SingletonShareData();

    setState(() {
      isEmailValid = _textEditingControllers[0].text.length > 0;//Utils.isValidate(ValidationType.EMAIL, _textEditingControllers[0].text);
      isPwValid = Utils.isValidate(ValidationType.LOGIN_PASSWORD, _textEditingControllers[1].text);
    });

    if (_textEditingControllers[0].text.length > 0 &&
        Utils.isValidate(ValidationType.LOGIN_PASSWORD, _textEditingControllers[1].text)) {

      data.showLoading(context);
      try {
        final AuthResult result = await _auth.signInWithEmailAndPassword(
          email: _textEditingControllers[0].text,
          password: _textEditingControllers[1].text,
        );
        data.hideLoading(context);
        data.user = result.user;
        ga.gaLogLogin(user:result.user);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BaseViewController(BottomNavigationBarController())),
        );
      } on PlatformException catch (e) {
        data.hideLoading(context);
        print("PlatformExceptionError:$e");
        showDialog(
            barrierDismissible: true,
            context: context,
            builder: (buildContext) {
              return CustomAlertDialog(title: Lang.getString("login_error"), content: data.getErrorMessage(error:e, type:ErrorType.Login), leftButtonText: null,rightButtonText:Lang.getString(
                  "login_close") ,buildContext: buildContext,
                  rightButtonFunction:  () {
                    Navigator.of(context).pop();
                  });
            }
        );
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: false,
                      type: SliverAppBarType.SignInPage,
                      titleText: Lang.getString("login_bar_title"),
                      widgetList: <Widget>[Padding(
                          padding: EdgeInsets.only(top: topPadding),
                          child: Container(
                              width: 35)
                      ),
                        Padding(
                            padding: EdgeInsets.only(top: topPadding),
                            child: Container(
                                width: 35)),
                        Container(
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.only(
                                right: defaultPadding, top: 8),
                            child: InkWell(
                              onTap: () async {
                                showDialog(
                                    barrierDismissible: true,
                                    context: context,
                                    builder: (buildContext) {
                                      return CustomAlertDialog(title: Lang.getString("popup_guest_title"), content: Lang.getString(
                                          "popup_guest_content"), leftButtonText: Lang.getString(
                                          "popup_guest_login"),rightButtonText:Lang.getString(
                                                  "popup_guest_cont") ,buildContext: buildContext,
                                      rightButtonFunction: () async {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    BaseViewController(BottomNavigationBarController())));
                                      });
                                    }
                                );
                              },
                              child: Text(Lang.getString("login_bar_skip"),
                                textScaleFactor: 1.0, style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,),),
                            )
                        ),
                      ]),
                  SliverToBoxAdapter(
                    child: WaveBar(),
                  ),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment(0, 0),
            margin: EdgeInsets.only(top: 80),
            child: Center(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      child: Card(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        color: Colors.white,
                        child: Container(
                          padding: EdgeInsets.only(top: 30, bottom: 20, left: 20, right: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(bottom: 30),
                                child: Text(Lang.getString("login_body_title"),
                                  style: TextStyle(color: Color(0xFF6A6969), fontSize: 21, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                              ),
                              CustomInputField(iconData: 0xe7fd, inputTitle: Lang.getString("inputfield_email_title"), focusNode: _formNodes[0], nextFocusNode: _formNodes[1],textInputAction: TextInputAction.next, validationType: ValidationType.NONE, textEditingController: _textEditingControllers[0],),
                              (isEmailValid) ? Container() : Row(
                                    // mainAxisAlignment: MainAxisAlignment.start,
                                    // crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(child: Icon(Icons.error, color: Color(0xFFFA356A))),
                                      Container(
                                          child: Text(" "+Lang.getString("inputfield_email_validation"),
                                            style: TextStyle(
                                                color: Color(0xFFFA356A),
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600
                                            ),
                                          ),
                                      )
                                    ],
                              ),
                              CustomInputField(iconData: 0xe897, inputTitle: Lang.getString("inputfield_password_title"), obscure: true, focusNode: _formNodes[1], textInputAction: TextInputAction.done, validationType: ValidationType.NONE, textEditingController: _textEditingControllers[1],),
                              (isPwValid) ? Container() : Container(
                                  margin: EdgeInsets.only(top: 0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(Icons.error, color: Color(0xFFFA356A)),
                                      Expanded(
//                                          child: Text(" "+Lang.getString("profile_password_include")+"\n- "+Lang.getString("profile_8_char")+"\n- "+ Lang.getString("profile_upper_lower_case") +"\n- "+ Lang.getString("profile_numberic") +"\n- "+Lang.getString("profile_special_char"),
                                        child: Text(" "+Lang.getString("profile_password_include")+"\n- "+Lang.getString("profile_8_char")+"\n- "+ Lang.getString("profile_upper_lower_case") +"\n- "+ Lang.getString("profile_numberic"),
                                        style: TextStyle(
                                                color: Color(0xFFFA356A),
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600
                                            ),
                                          )
                                      )

                                    ],
                                  )
                              ),
                              BottomButton(buttonType: ButtonType.LoginButtonWithForgetPassword,buttonText:Lang.getString("login_button"),onPressed: () async {
                                loginAction();
                              },function: () async {
                                resetPassword();
                              }),
//                              InkWell(
//                                onTap: () {
//                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPasswordController()));
//                                },
//                                child: Text("Forget password?", style: TextStyle(color: Color(0xFFA8A9BD), fontSize: 16, fontWeight: FontWeight.bold
//                                    , decoration: TextDecoration.none),),
//                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 30, bottom: 20),
                            child: ButtonTheme(
                              minWidth: 300,
                              child: RaisedButton(
                                padding: EdgeInsets.all(12),
                                child: Text(Lang.getString("login_signup_button"), style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),),
                                onPressed: () async {

                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                      BaseViewController(SignUpController()))
                                  );

                                },
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Color(0xFFE14672)),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                                disabledColor: Color(0xFFF9EBEC),
                                textColor: Color(0xFFE14672),
                                color: Color(0xFFF9EBEC),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}