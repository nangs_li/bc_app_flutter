import 'package:flutter/material.dart';
import 'package:icanfight/view/bottomButton.dart';


import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/reminderInputField.dart';
import 'package:icanfight/view/reminderDateTimeInputField.dart';
import 'package:icanfight/view/reminderDropdownInputField.dart';
import 'baseViewController.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'calendarController.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:icanfight/model/event.dart';
import 'package:icanfight/utilities/utils.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:flutter/cupertino.dart';
// not use again

//class AppointmentController extends StatefulWidget {
//
//  String _type;
//  String _title;
//  bool _isNew;
//  Event _event;
//  DateTime _selectedDate;
//
//  AppointmentController({ bool isNew, String type, String title, Event event, DateTime selectedDate}) {
//    _type = type;
//    _title = title;
//    _isNew = isNew;
//    _event = event;
//    _selectedDate = selectedDate;
//  }
//
//  @override
//  _AppointmentControllerControllerState createState() =>
//      _AppointmentControllerControllerState(title: _title, type: _type, isNew: _isNew, event: _event, selectedDate: _selectedDate);
//}
//
//class _AppointmentControllerControllerState
//    extends State<AppointmentController> {
//
//  _AppointmentControllerControllerState({ bool isNew, String type, String title, Event event, DateTime selectedDate }) {
//    _type = type;
//    _title = title;
//    _isNew = isNew;
//    _event = event;
//    _selectedDate = selectedDate;
//  }
//
//  ScrollController _scrollController;
//  List<FocusNode> _formNodes = [
//    FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode(),
//  ];
//  List<TextEditingController> _textEditingControllers = [
//    TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: ""),
//    TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: ""),
//  ];
//  String _type;
//  String _title;
//  String content;
//  bool _isNew;
//  bool isButtonLocked = false;
//  bool isButtonActive = false;
//  Event _event;
//  DateTime _selectedDate;
//  String _initEndDate;
//  TextEditingController _startDateTextController;
//  TextEditingController _endDateTextController;
//  List<String> _typeItems = [
//    Lang.getString("calendar_blood"),
//    Lang.getString("calendar_mammogram"),
//    Lang.getString("calendar_pelvic"),
//    Lang.getString("calendar_bone"),
//    Lang.getString("calendar_pet"),
//    Lang.getString("calendar_other_test"),
//    Lang.getString("calendar_surgery"),
//    Lang.getString("calendar_radio"),
//    Lang.getString("calendar_chem"),
//  ];
//  List<String> _alertItems = [
//    Lang.getString("calendar_5min_before"),
//    Lang.getString("calendar_15min_before"),
//    Lang.getString("calendar_30min_before"),
//    Lang.getString("calendar_1hour_before"),
//    Lang.getString("calendar_2hours_before"),
//    Lang.getString("calendar_1day_before"),
//    Lang.getString("calendar_2day_before"),
//    Lang.getString("calendar_1week_before"),
//  ];
//  List<int> _alertMinItems = [
//    5,
//    15,
//    30,
//    60,
//    120,
//    1440,
//    2880,
//    10080,
//  ];
//  List<String> _repeatItems = [
//    Lang.getString("calendar_never"),
//    Lang.getString("calendar_daily"),
//    Lang.getString("calendar_weekly"),
//    Lang.getString("calendar_biweekly"),
//    Lang.getString("calendar_triweekly"),
//    Lang.getString("calendar_monthly"),
//    Lang.getString("calendar_annually"),
//  ];
//
//
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
//
//  Future<void> onSelectNotification(String payload) async {
//    if (payload != null) {
//      debugPrint('notification payload: ' + payload);
//    }
//
//    await Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => BaseViewController(BottomNavigationBarController(index: 1))),
//    );
//  }
//
//  Future<void> onDidReceiveLocalNotification(
//      int id, String title, String body, String payload) async {
//    // display a dialog with the notification details, tap ok to go to another page
//    await showDialog(
//      context: context,
//      builder: (BuildContext context) =>
//          CupertinoAlertDialog(
//            title: title != null ? Text(title) : null,
//            content: body != null ? Text(body) : null,
//            actions: [
//              CupertinoDialogAction(
//                isDefaultAction: true,
//                child: Text('Ok'),
//                onPressed: () async {
//                  Navigator.of(context, rootNavigator: true).pop();
//                  await Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                      builder: (context) =>
//                      BaseViewController(BottomNavigationBarController(index: 1)),
//                    ),
//                  );
//                },
//              )
//            ],
//          ),
//    );
//  }
//
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    _scrollController = ScrollController();
//    _startDateTextController = TextEditingController(text: "");
//    _endDateTextController = TextEditingController(text: "");
//
//    if (_event != null) {
//      _selectedDate = DateTime.parse(((_event.date != null) ? _event.date : "") + " " + ((_event.time != null) ? _event.time : ""));
//      _startDateTextController.text = ((_event.date != null) ? _event.date : "") + " " + ((_event.time != null) ? _event.time : "");
//      _endDateTextController.text = ((_event.endDate != null) ? _event.endDate : "") + " " + ((_event.endTime != null) ? _event.endTime : "");
//      _textEditingControllers[0].text = _event.eventType ?? "";
//      _textEditingControllers[1].text = _event.doctor ?? "";
//      _textEditingControllers[2].text = _event.alert ?? "";
//      _textEditingControllers[3].text = _event.repeat ?? "";
//      _textEditingControllers[4].text = _event.location ?? "";
//      _textEditingControllers[5].text = _event.notes ?? "";
//    } else {
//      _textEditingControllers[2].text = _alertItems[0];
//      _textEditingControllers[3].text = _repeatItems[0];
//
//    }
//
//    if (_selectedDate != null) {
//      debugPrint(_selectedDate.toString());
//      _startDateTextController.text = _selectedDate.year.toString()+"-"+_selectedDate.month.toString().padLeft(2,"0")+"-"+_selectedDate.day.toString().padLeft(2,"0") + " " + _selectedDate.hour.toString().padLeft(2,"0")+":"+_selectedDate.minute.toString().padLeft(2,"0");
//    }
//
//    _startDateTextController.addListener(() {
//      setState(() {
//        _initEndDate = DateTime.parse(_startDateTextController.text).toString();
//        _endDateTextController.text = _startDateTextController.text;
//      });
//    });
//
//    validationRule();
//  }
//
//  void validationRule() async {
//
//    switch (_type) {
//
//      case "1":
//
//        VoidCallback function = () {
//
//          setState(() {
//            isButtonActive = (
//                _textEditingControllers[1].text.length > 0 &&
//                    _startDateTextController.text.length > 0 &&
//                    _endDateTextController.text.length > 0
//            );
//          });
//
//        };
//
//        _textEditingControllers[1].addListener(function);
//        _startDateTextController.addListener(function);
//        _endDateTextController.addListener(function);
//        break;
//
//      case "2":
//
//        VoidCallback function = () {
//
//          setState(() {
//            isButtonActive = (
//                _textEditingControllers[1].text.length > 0 &&
//                    _startDateTextController.text.length > 0 &&
//                    _endDateTextController.text.length > 0
//            );
//          });
//
//        };
//
//        _textEditingControllers[1].addListener(function);
//        _startDateTextController.addListener(function);
//        _endDateTextController.addListener(function);
//        break;
//
//      case "3":
//
//        VoidCallback function = () {
//
//          setState(() {
//            isButtonActive = (
//                _textEditingControllers[1].text.length > 0 &&
//                    _startDateTextController.text.length > 0
//            );
//          });
//
//        };
//
//        _textEditingControllers[1].addListener(function);
//        _startDateTextController.addListener(function);
//        break;
//
//      case "5":
//
//        VoidCallback function = () {
//
//          setState(() {
//            isButtonActive = (
//                _textEditingControllers[1].text.length > 0 &&
//                    _startDateTextController.text.length > 0 &&
//                    _endDateTextController.text.length > 0
//            );
//          });
//
//        };
//
//        _textEditingControllers[1].addListener(function);
//        _startDateTextController.addListener(function);
//        _endDateTextController.addListener(function);
//        break;
//
//    }
//
//  }
//
//  void createReminderAction() async {
//
//    if (!isButtonLocked) {
//      setState(() {
//        isButtonLocked = true;
//      });
//    } else {
//      return;
//    }
//
//    Event oldEvent = _event;
//
//
//    if (!_isNew) {
//      EventApiProvider().editEvent(Event.fromMap(
//          {"title": _title,
//            "content": "",
//            "type": _type,
//            "date": (_startDateTextController.text != null) ? _startDateTextController.text.split(" ")[0] : "",
//            "time": (_startDateTextController.text != null) ? _startDateTextController.text.split(" ")[1] : "" ,
//            "endDate": (_endDateTextController.text != null) ? ((_endDateTextController.text != "") ?_endDateTextController.text.split(" ")[0]: _startDateTextController.text.split(" ")[0] ) : "" ,
//            "endTime": (_endDateTextController.text != null) ? ((_endDateTextController.text != "") ?_endDateTextController.text.split(" ")[1]: _startDateTextController.text.split(" ")[1]) : "" ,
//            "doctor": _textEditingControllers[1].text,
//            "alert": _textEditingControllers[2].text,
//            "repeat": _textEditingControllers[3].text,
//            "location": _textEditingControllers[4].text,
//            "notes": _textEditingControllers[5].text,
//            "eventType": _textEditingControllers[0].text,
//          }), oldEvent);
//    } else {
//      EventApiProvider().addEvent(Event.fromMap(
//          {"title": _title,
//            "content": "",
//            "type": _type,
//            "date": (_startDateTextController.text != null) ? _startDateTextController.text.split(" ")[0] : "",
//            "time": (_startDateTextController.text != null) ? _startDateTextController.text.split(" ")[1] : "" ,
//            "endDate": (_endDateTextController.text != null) ? ((_endDateTextController.text != "") ?_endDateTextController.text.split(" ")[0]: _startDateTextController.text.split(" ")[0] ) : "" ,
//            "endTime": (_endDateTextController.text != null) ? ((_endDateTextController.text != "") ?_endDateTextController.text.split(" ")[1]: _startDateTextController.text.split(" ")[1]) : "" ,
//            "doctor": _textEditingControllers[1].text,
//            "alert": _textEditingControllers[2].text,
//            "repeat": _textEditingControllers[3].text,
//            "location": _textEditingControllers[4].text,
//            "notes": _textEditingControllers[5].text,
//            "eventType": _textEditingControllers[0].text,
//          }));
//    }
//
//    if (_textEditingControllers[2].text.length > 0) {
//      int alertIndex = _alertItems.indexOf(_textEditingControllers[2].text);
//
//      var initializationSettingsAndroid =
//      AndroidInitializationSettings('launch_icon');
//      var initializationSettingsIOS = IOSInitializationSettings(
//          onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//      var initializationSettings = InitializationSettings(
//          initializationSettingsAndroid, initializationSettingsIOS);
//      flutterLocalNotificationsPlugin.initialize(initializationSettings,
//          onSelectNotification: onSelectNotification);
//
//      var scheduledNotificationDateTime =
//      DateTime.parse(_startDateTextController.text).add(Duration(minutes: -1 * _alertMinItems[alertIndex]));
//      var androidPlatformChannelSpecifics =
//      new AndroidNotificationDetails('your other channel id',
//          'your other channel name', 'your other channel description');
//      var iOSPlatformChannelSpecifics =
//      new IOSNotificationDetails();
//      NotificationDetails platformChannelSpecifics = new NotificationDetails(
//          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//      await flutterLocalNotificationsPlugin.schedule(
//          0,
//          _title,
//          _textEditingControllers[1].text,
//          scheduledNotificationDateTime,
//          platformChannelSpecifics);
//
//    }
//
//    await Future.delayed(Duration(seconds: 1), () {
//      Navigator.pop(context);
//      Navigator.push(context,
//          MaterialPageRoute(builder: (context) {
//            return BaseViewController(BottomNavigationBarController(index: 1));
//          }));
//    });
//
//
//
//  }
//
//  VoidCallback inputFieldScrollOnFocus(double offset) => () {
////    _scrollController.animateTo(offset, duration: Duration(seconds: 1), curve: Curves.ease);
//  };
//
//  Widget _renderForm() {
//
//    switch (_type) {
//
//      case "1":
//        return Column(
//          children: <Widget>[
//            ReminderDropdownInputField(mandatory: true, inputTitle: Lang.getString("calendar_type"),focusNode: _formNodes[0],nextFocusNode: _formNodes[1],textEditingController: _textEditingControllers[1], items: _typeItems,),
////            ReminderInputField(inputTitle: "Doctor Name",focusNode: _formNodes[1],nextFocusNode: _formNodes[2],textEditingController: _textEditingControllers[1],),
//            ReminderDateTimeInputField(dialogTitle: "Start", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_startdate_time"),focusNode: _formNodes[2],nextFocusNode: _formNodes[3], textEditingController: _startDateTextController,),
//            ReminderDateTimeInputField(dialogTitle: "End", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_enddate_time"),focusNode: _formNodes[3],nextFocusNode: _formNodes[4], textEditingController: _endDateTextController,),
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_alert"),focusNode: _formNodes[4],nextFocusNode: _formNodes[5],textEditingController: _textEditingControllers[2],items: _alertItems,),
////            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_repeat"),focusNode: _formNodes[5],nextFocusNode: _formNodes[6],textEditingController: _textEditingControllers[3],items: _repeatItems,),
//            ReminderInputField(inputTitle: Lang.getString("calendar_location"),focusNodeCallBack: inputFieldScrollOnFocus(400),focusNode: _formNodes[6],nextFocusNode: _formNodes[7],textEditingController: _textEditingControllers[4],),
//            ReminderInputField(inputTitle: Lang.getString("calendar_notes"),maxLine: 5,focusNodeCallBack: inputFieldScrollOnFocus(500),focusNode: _formNodes[7],textEditingController: _textEditingControllers[5]),
//          ],
//        );
//        break;
//
//      case "2":
//        return Column(
//          children: <Widget>[
////            ReminderDropdownInputField(inputTitle: "Type",focusNode: _formNodes[0],nextFocusNode: _formNodes[1],textEditingController: _textEditingControllers[0], items: _typeItems,),
//            ReminderInputField(mandatory: true, inputTitle: Lang.getString("calendar_description"),focusNode: _formNodes[1],nextFocusNode: _formNodes[2],textEditingController: _textEditingControllers[1],),
//            ReminderDateTimeInputField(dialogTitle: "Start", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_startdate_time"),focusNode: _formNodes[2],nextFocusNode: _formNodes[3], textEditingController: _startDateTextController,),
//            ReminderDateTimeInputField(dialogTitle: "End", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_enddate_time"),focusNode: _formNodes[3],nextFocusNode: _formNodes[4], textEditingController: _endDateTextController,),
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_alert"),focusNode: _formNodes[4],nextFocusNode: _formNodes[5],textEditingController: _textEditingControllers[2],items: _alertItems,),
////            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_repeat"),focusNode: _formNodes[5],nextFocusNode: _formNodes[6],textEditingController: _textEditingControllers[3],items: _repeatItems,),
//            ReminderInputField(inputTitle: Lang.getString("calendar_location"),focusNodeCallBack: inputFieldScrollOnFocus(400),focusNode: _formNodes[6],nextFocusNode: _formNodes[7],textEditingController: _textEditingControllers[4],),
//            ReminderInputField(inputTitle: Lang.getString("calendar_notes"),maxLine: 5,focusNodeCallBack: inputFieldScrollOnFocus(500),focusNode: _formNodes[7],textEditingController: _textEditingControllers[5]),
//          ],
//        );
//        break;
//
//      case "3":
//        return Column(
//          children: <Widget>[
////            ReminderDropdownInputField(inputTitle: "Type",focusNode: _formNodes[0],nextFocusNode: _formNodes[1],textEditingController: _textEditingControllers[0], items: _typeItems,),
//            ReminderInputField(mandatory: true, inputTitle: Lang.getString("calendar_description"),focusNode: _formNodes[1],nextFocusNode: _formNodes[2],textEditingController: _textEditingControllers[1],),
//            ReminderDateTimeInputField(dialogTitle: "", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_time"),focusNode: _formNodes[2],nextFocusNode: _formNodes[3], textEditingController: _startDateTextController,),
////            ReminderDateTimeInputField(dialogTitle: "End", inputTitle: "End date & time",focusNode: _formNodes[3],nextFocusNode: _formNodes[4], textEditingController: _endDateTextController,),
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_alert"),focusNode: _formNodes[4],nextFocusNode: _formNodes[5],textEditingController: _textEditingControllers[2],items: _alertItems,),
////            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_repeat"),focusNode: _formNodes[5],nextFocusNode: _formNodes[6],textEditingController: _textEditingControllers[3],items: _repeatItems,),
////            ReminderInputField(inputTitle: "Location",focusNodeCallBack: inputFieldScrollOnFocus(400),focusNode: _formNodes[6],nextFocusNode: _formNodes[7],textEditingController: _textEditingControllers[4],),
//            ReminderInputField(inputTitle: Lang.getString("calendar_notes"),maxLine: 5,focusNodeCallBack: inputFieldScrollOnFocus(500),focusNode: _formNodes[7],textEditingController: _textEditingControllers[5]),
//          ],
//        );
//        break;
//
//      case "4":
//        return Column(
//          children: <Widget>[
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_type"),focusNode: _formNodes[0],nextFocusNode: _formNodes[1],textEditingController: _textEditingControllers[0], items: _typeItems,),
//            ReminderInputField(inputTitle: Lang.getString("calendar_doctor"),focusNode: _formNodes[1],nextFocusNode: _formNodes[2],textEditingController: _textEditingControllers[1],),
//            ReminderDateTimeInputField(dialogTitle: "Start", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_startdate_time"),focusNode: _formNodes[2],nextFocusNode: _formNodes[3], textEditingController: _startDateTextController,),
//            ReminderDateTimeInputField(dialogTitle: "End", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_enddate_time"),focusNode: _formNodes[3],nextFocusNode: _formNodes[4], textEditingController: _endDateTextController,),
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_alert"),focusNode: _formNodes[4],nextFocusNode: _formNodes[5],textEditingController: _textEditingControllers[2],items: _alertItems,),
////            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_repeat"),focusNode: _formNodes[5],nextFocusNode: _formNodes[6],textEditingController: _textEditingControllers[3],items: _repeatItems,),
//            ReminderInputField(inputTitle: Lang.getString("calendar_location"),focusNodeCallBack: inputFieldScrollOnFocus(400),focusNode: _formNodes[6],nextFocusNode: _formNodes[7],textEditingController: _textEditingControllers[4],),
//            ReminderInputField(inputTitle: Lang.getString("calendar_notes"),maxLine: 5,focusNodeCallBack: inputFieldScrollOnFocus(500),focusNode: _formNodes[7],textEditingController: _textEditingControllers[5]),
//          ],
//        );
//        break;
//
//      case "5":
//        return Column(
//          children: <Widget>[
////            ReminderDropdownInputField(inputTitle: "Type",focusNode: _formNodes[0],nextFocusNode: _formNodes[1],textEditingController: _textEditingControllers[0], items: _typeItems,),
//            ReminderInputField(mandatory: true, inputTitle: Lang.getString("calendar_description"),focusNode: _formNodes[1],nextFocusNode: _formNodes[2],textEditingController: _textEditingControllers[1],),
//            ReminderDateTimeInputField(dialogTitle: "Start", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_startdate_time"),focusNode: _formNodes[2],nextFocusNode: _formNodes[3], textEditingController: _startDateTextController,),
//            ReminderDateTimeInputField(dialogTitle: "End", initDate: _selectedDate.toString(), minDate: _selectedDate.toString(), inputTitle: Lang.getString("calendar_enddate_time"),focusNode: _formNodes[3],nextFocusNode: _formNodes[4], textEditingController: _endDateTextController,),
//            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_alert"),focusNode: _formNodes[4],nextFocusNode: _formNodes[5],textEditingController: _textEditingControllers[2],items: _alertItems,),
////            ReminderDropdownInputField(inputTitle: Lang.getString("calendar_repeat"),focusNode: _formNodes[5],nextFocusNode: _formNodes[6],textEditingController: _textEditingControllers[3],items: _repeatItems,),
////            ReminderInputField(inputTitle: "Location",focusNodeCallBack: inputFieldScrollOnFocus(400),focusNode: _formNodes[6],nextFocusNode: _formNodes[7],textEditingController: _textEditingControllers[4],),
//            ReminderInputField(inputTitle: Lang.getString("calendar_notes"),maxLine: 5,focusNodeCallBack: inputFieldScrollOnFocus(600),focusNode: _formNodes[7],textEditingController: _textEditingControllers[5]),
//          ],
//        );
//        break;
//
//    }
//
//    return Container();
//  }
//
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      color: kLightHealthTheme.primaryColor,
//      child: Stack(
//        children: <Widget>[
//          SafeArea(
//            bottom: false,
//            child: Material(
//              color: Color(0xFFF9EBEC),
//              child: CustomScrollView(
//                physics: new ClampingScrollPhysics(),
//                slivers: <Widget>[
//                  CustomSliverAppBarView(
//                      isBack: true,
//                      titleText: _isNew ? Lang.getString("calendar_new") +
//                          _title : "",
//                      type: SliverAppBarType.AppointmentPage),
//                  SliverToBoxAdapter(
//                    child: Container(
//                      color: kLightHealthTheme.primaryColor,
//                      child: SingleChildScrollView(
//                        scrollDirection: Axis.vertical,
//                        controller: _scrollController,
//                        child: Container(
//                            decoration: new BoxDecoration(
//                                color: Colors.white,
//                                borderRadius: new BorderRadius.only(
//                                    topLeft:
//                                    const Radius.circular(20.0),
//                                    topRight: const Radius.circular(
//                                        20.0))),
//                            child: Column(
//                              children: <Widget>[
//                                Container(
//                                    width: MediaQuery.of(context).size.width * 1.0,
//                                    child: Padding(
//                                      padding: EdgeInsets.all(5),
//                                      child: Card(
//                                        clipBehavior: Clip.antiAlias,
//                                        borderOnForeground: true,
//                                        elevation: 0,
//                                        shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white), borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
//                                        color: Colors.white,
//                                        child: Container(
//                                          padding: EdgeInsets.only(top: 30, bottom: 160, left: 10, right: 10),
//                                          child: Column(
//                                            mainAxisAlignment: MainAxisAlignment.center,
//                                            children: <Widget>[
//                                              _isNew ? Container() :
//                                                  Stack(
//                                                    children: <Widget>[
//                                                      Container(
//                                                        margin: EdgeInsets.only(bottom: 0),
//                                                        alignment: Alignment.centerLeft,
//                                                        color: Color(Utils.mapColorFromType(_type)),
//                                                        width: 30,
//                                                        height: 30,
//                                                      ),
//                                                      Container(
//                                                        margin: EdgeInsets.only(bottom: 20, left: 40),
//                                                        alignment: Alignment.centerLeft,
//                                                        child: Text(_title,
//                                                          style: TextStyle(color: Color(0xFF555555), fontSize: 27, fontWeight: FontWeight.w600), textAlign: TextAlign.left,),
//                                                      ),
//                                                      Container(
//                                                        alignment: Alignment.centerRight,
//                                                        child: InkWell(
//                                                          onTap: () {
//
//                                                            showDialog(
//                                                                barrierDismissible: true,
//                                                                context: context,
//                                                                builder: (buildContext) {
//                                                                  return AlertDialog(
//                                                                    titlePadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
//                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
//                                                                    shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white10), borderRadius: BorderRadius.all(Radius.circular(10))),
//                                                                    title: Text(Lang.getString("calendar_delete_reminder"), style: TextStyle(color: Color(0xFF555555), fontSize: 16, fontWeight: FontWeight.w500),),
//                                                                    actions: <Widget>[
//                                                                      InkWell(
//                                                                        child: Container(
//                                                                          child: Text(Lang.getString("cancel_button"), style: TextStyle(color: Color(0xFF555555), fontWeight: FontWeight.w500),),
//                                                                          margin: EdgeInsets.only(right: 20, top: 10, bottom: 10),
//                                                                        ),
//                                                                        onTap: () {
//                                                                          Navigator.of(buildContext).pop();
//                                                                        },
//                                                                      ),
//                                                                      InkWell(
//                                                                        child: Container(
//                                                                          child: Text(Lang.getString("delete_button"), style: TextStyle(color: Color(0xFFE14672)),),
//                                                                          margin: EdgeInsets.only(right: 10, top: 10, bottom: 10),
//                                                                        ),
//                                                                        onTap: () async {
//                                                                          EventApiProvider().removeEvent(_event);
//                                                                          Navigator.pop(context);
//                                                                          Navigator.push(context,
//                                                                              MaterialPageRoute(builder: (context) {
//                                                                                return BaseViewController(BottomNavigationBarController(index: 1));
//                                                                              }));
//                                                                        },
//                                                                      ),
//                                                                    ],
//                                                                  );
//                                                                }
//                                                            );
//                                                          },
//                                                          child: SvgPicture.asset("assets/images/icon_delete.svg", width: 20,),
//                                                        ),
//                                                      )
//                                                    ],
//                                                  ),
//                                              _renderForm()
//                                            ],
//                                          ),
//                                        ),
//                                      ),
//                                    )
//                                ),
//                                BottomButton(buttonText:_isNew ? Lang.getString("calendar_create_reminder") : Lang.getString("save_button"),onPressed: () async {
//                                  debugPrint("press");
//                                  if (isButtonActive) {
//                                    if (DateTime.parse(_startDateTextController.text).compareTo(DateTime.parse(_endDateTextController.text)) > 0) {
//
//                                      showDialog(
//                                          barrierDismissible: true,
//                                          context: context,
//                                          builder: (buildContext) {
//                                            return AlertDialog(
//                                              titlePadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
//                                              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
//                                              shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white10), borderRadius: BorderRadius.all(Radius.circular(10))),
//                                              title: Text(Lang.getString("calendar_reminder_endtime"), style: TextStyle(color: Color(0xFF555555), fontSize: 16, fontWeight: FontWeight.w500),),
//                                              actions: <Widget>[
//                                                InkWell(
//                                                  child: Container(
//                                                    child: Text(Lang.getString("ok_button"), style: TextStyle(color: Color(0xFFE14672)),),
//                                                    margin: EdgeInsets.only(right: 20, top: 10, bottom: 10),
//                                                  ),
//                                                  onTap: () async {
//                                                    setState(() {
//                                                      isButtonLocked = false;
//                                                    });
//                                                    Navigator.of(buildContext).pop();
//                                                  },
//                                                ),
//                                              ],
//                                            );
//                                          }
//                                      );
//
//                                    }  else {
//                                      createReminderAction();
//                                    }
////
//                                  }
//                                },
//                                roundedRectangleBorder: RoundedRectangleBorder(side: BorderSide(color: isButtonActive ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB)), borderRadius: BorderRadius.all(Radius.circular(50)))
//                                ,textColor: isButtonActive ? Colors.white : Color(0xFFA0A0A0),
//                                  buttonColor: isButtonActive ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB),
//                                ),
//                              ],
//                            ),
//                        ),
//                      ),
//                    ),
//                  )
//                ],
//              ),
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//}