import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/controller/signUpQaController.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/utils.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'baseViewController.dart';
import 'bottomNavigationBarController.dart';
import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:flutter/services.dart';

class SignUpController extends StatefulWidget {
  @override
  _SignUpControllerState createState() => _SignUpControllerState();
}

class _SignUpControllerState extends State<SignUpController> {
  ScrollController _scrollController;
  List<TextEditingController> _textEditingControllers = [
    TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: "")
  ];
  bool isUsernameValid = true;
  bool isEmailValid = true;
  bool isCpwValid = true;

  List<FocusNode> _formNodes = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode()
  ];
  FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaSetCurrentScreen(screenName: "signup_page");
    init();
  }

  void init() {
    _scrollController = ScrollController();
  }

  VoidCallback inputFieldScrollOnFocus(double offset) => () {
        _scrollController.animateTo(offset,
            duration: Duration(seconds: 1), curve: Curves.ease);
      };

  void registerAction() async {
    SingletonShareData data = SingletonShareData();
    ga.gaEvent(name:"signup_fill_in_info_button");
    setState(() {
      isEmailValid = Utils.isValidate(ValidationType.EMAIL, _textEditingControllers[0].text);
      isCpwValid = (_textEditingControllers[1].text == _textEditingControllers[2].text);
      isUsernameValid = _textEditingControllers[4].text.length > 0;
    });

    if (Utils.isValidate(ValidationType.EMAIL, _textEditingControllers[0].text) &&
        (_textEditingControllers[1].text == _textEditingControllers[2].text) &&
    Utils.isValidate(ValidationType.PASSWORD, _textEditingControllers[2].text) &&
        _textEditingControllers[4].text.length > 0) {
      data.registerCase = true;
      data.registerTextControllers = _textEditingControllers;
      data.showLoading(context);
      try {
        final AuthResult result = await _auth.signInWithEmailAndPassword(
          email: _textEditingControllers[0].text,
          password: "abcdefghjkil",
        );
        data.hideLoading(context);
      } on PlatformException catch (e) {
        data.hideLoading(context);
        print("PlatformExceptionError:$e");
        if (e.code == "ERROR_WRONG_PASSWORD") {
          showDialog(
              barrierDismissible: true,
              context: context,
              builder: (buildContext) {
                return CustomAlertDialog(
                    title: Lang.getString("login_error"),
                    content: "電郵已被使用",
                    leftButtonText: null,
                    rightButtonText: Lang.getString("login_close"),
                    buildContext: buildContext,
                    rightButtonFunction: () {
                      Navigator.of(context).pop();
                    });
              });
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BaseViewController(SignUpQaController())),
          );
        }
      }
    }

  }

  void initFirebase() async {
//    _handleSignIn()
//        .then((FirebaseUser user) => (user.getIdToken(refresh: true)
//        .then((IdTokenResult result) => debugPrint(result.token.toString()))))
//        .catchError((e) => debugPrint(e.toString()));
  }

//  Future<FirebaseUser> _handleSignIn() async {

//    final FirebaseUser user = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: "test_reg1@test.com", password: "a1111111")).user;
//    return user;
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          color: kLightHealthTheme.primaryColor,
          child: Stack(
            children: <Widget>[
              SafeArea(
                bottom: false,
                child: Material(
                  color: Color(0xFFF9EBEC),
                  child: CustomScrollView(
                    physics: new ClampingScrollPhysics(),
                    slivers: <Widget>[
                      CustomSliverAppBarView(isBack: false,
                          type: SliverAppBarType.SignUpPage,
                          titleText: Lang.getString("register_bar_title"),
                          widgetList: <Widget>[Padding(
                              padding: EdgeInsets.only(top: topPadding),
                              child: Container(
                                  width: 35)
                          ),
                            Padding(
                                padding: EdgeInsets.only(top: topPadding),
                                child: Container(
                                    width: 35)),
                            Container(
                                alignment: Alignment.centerRight,
                                margin: EdgeInsets.only(
                                    right: defaultPadding, top: 8),
                                child: InkWell(
                                  onTap: () async {
                                    showDialog(
                                        barrierDismissible: true,
                                        context: context,
                                        builder: (buildContext) {
                                          return CustomAlertDialog(title: Lang.getString("popup_guest_title"), content: Lang.getString(
                                              "popup_guest_signup"), leftButtonText: Lang.getString(
                                              "popup_guest_signup"),rightButtonText:Lang.getString(
                                              "popup_guest_cont") ,buildContext: buildContext,
                                              rightButtonFunction: () async {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            BaseViewController(BottomNavigationBarController())));
                                              });
                                        }
                                    );
                                  },
                                  child: Text(Lang.getString("register_bar_skip"),
                                    textScaleFactor: 1.0, style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500)),
                                )
                            )
                          ]),
                      SliverToBoxAdapter(
                        child: WaveBar(),
                      ),
                    ],
                  ),
                ),
              ),
          SafeArea(
            bottom: false,
            child:Container(
                alignment: Alignment(0, 0),
                margin: EdgeInsets.only(top: 90),
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(28, 255, 255, 255),
                    blurRadius: 30.0,
                    offset: Offset(0.0, -3.0),
                  )
                ]),
                child: Center(
                      child: Form(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 15.0),
                              child: Card(
                                margin: EdgeInsets.only(bottom: 0),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0),)),
                                color: Colors.white,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  controller: _scrollController,
                                  child: Container(
                                  padding: EdgeInsets.only(
                                      top: 50, bottom: 40, left: 20, right: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(bottom: 30),
                                        child: Text(
                                          Lang.getString("register_body_title"),
                                          style: TextStyle(
                                              color: Color(0xFF707070),
                                              fontSize: 21,
                                              fontWeight: FontWeight.w600),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      CustomInputField(
                                        showDisableColor: true,
                                        iconData: 0xe7fd,
                                        inputTitle: Lang.getString("inputfield_user_role_title"),
                                        hint: Lang.getString("inputfield_user_role_hint"),
                                        block: true,
                                        focusNode: _formNodes[0],
                                        textInputAction: TextInputAction.next,
                                      ),
                                      CustomInputField(
                                          onChangeFunction: (string){
                                            setState(() {
                                              isUsernameValid = _textEditingControllers[4].text.length > 0;
                                            });
                                          },
                                          iconData: 0xe7fd,
                                          preInputTitle: "*",
                                          inputTitle: Lang.getString("inputfield_username_title"),
                                          focusNode: _formNodes[1],
                                          nextFocusNode: _formNodes[2],
                                          textEditingController: _textEditingControllers[4],
                                          textInputAction:
                                              TextInputAction.next),
                                      (isUsernameValid) ? Container() : Container(
                                          margin: EdgeInsets.only(top: 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(Icons.error, color: Color(0xFFFA356A)),
                                              Expanded(
                                                  child: Text(" "+Lang.getString("inputfield_username_validation"),
                                                    style: TextStyle(
                                                        color: Color(0xFFFA356A),
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600
                                                    ),
                                                  )
                                              )

                                            ],
                                          )
                                      ),
                                      CustomInputField(
                                        iconData: 0xe897,
                                        preInputTitle: "*",
                                        inputTitle: Lang.getString("inputfield_password_title"),
                                        obscure: true,
                                        focusNodeCallBack:
                                            inputFieldScrollOnFocus(300),
                                        focusNode: _formNodes[2],
                                        nextFocusNode: _formNodes[3],
                                        textInputAction: TextInputAction.next,
                                        validationType: ValidationType.PASSWORD,
                                        textEditingController: _textEditingControllers[2],
                                      ),
                                      CustomInputField(
                                          onChangeFunction: (string){
                                            setState(() {
                                              isCpwValid = (_textEditingControllers[1].text == _textEditingControllers[2].text);
                                            });
                                          },
                                          iconData: 0xe897,
                                          preInputTitle: "*",
                                          inputTitle: Lang.getString("inputfield_confirm_password_title"),
                                          obscure: true,
                                          focusNodeCallBack:
                                              inputFieldScrollOnFocus(400),
                                          focusNode: _formNodes[3],
                                          nextFocusNode: _formNodes[4],
                                          textEditingController: _textEditingControllers[1],
                                          textInputAction:
                                              TextInputAction.next),
                                      (isCpwValid) ? Container() : Container(
                                          margin: EdgeInsets.only(top: 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(Icons.error, color: Color(0xFFFA356A)),
                                              Expanded(
                                                  child: Text(" "+Lang.getString("inputfield_cpw_validation"),
                                                    style: TextStyle(
                                                        color: Color(0xFFFA356A),
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600
                                                    ),
                                                  )
                                              )

                                            ],
                                          )
                                      ),
                                      CustomInputField(
                                        onChangeFunction: (string){
                                          setState(() {
                                            isEmailValid = Utils.isValidate(ValidationType.EMAIL, _textEditingControllers[0].text);
                                          });
                                        },
                                        iconData: 0xe0be,
                                        preInputTitle: "*",
                                        inputTitle: Lang.getString("inputfield_email_title"),
                                        focusNodeCallBack:
                                            inputFieldScrollOnFocus(500),
                                        focusNode: _formNodes[4],
                                        textEditingController: _textEditingControllers[0],
                                        textInputAction: TextInputAction.done,
                                        unFocusNodeCallBack:
                                            inputFieldScrollOnFocus(0),
                                      ),
                                      (isEmailValid) ? Container() : Container(
                                          margin: EdgeInsets.only(top: 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(Icons.error, color: Color(0xFFFA356A)),
                                              Expanded(
                                                  child: Text(" "+Lang.getString("inputfield_email_validation"),
                                                    style: TextStyle(
                                                        color: Color(0xFFFA356A),
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w600
                                                    ),
                                                  )
                                              )

                                            ],
                                          )
                                      ),
                                      BottomButton(buttonText:Lang.getString("register_button"),onPressed: () async {
                                        registerAction();
                                      },
                                          roundedRectangleBorder: RoundedRectangleBorder(side: BorderSide(color: isEmailValid &&  isCpwValid && isUsernameValid ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB)), borderRadius: BorderRadius.all(Radius.circular(50))),
                                      textColor: isEmailValid &&  isCpwValid && isUsernameValid   ? Colors.white : Color(0xFFA0A0A0),
                                      buttonColor: isEmailValid &&  isCpwValid && isUsernameValid ? kLightHealthTheme.buttonColor : Color(0xFFEBEBEB)),
                                      Container(
                                        height: 100,
                                      )

                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                            Container(
                              height: 30,
                              margin: EdgeInsets.symmetric(horizontal: 15.0),
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: <Color>[
                                        Color(0xFFFFFFFF),
                                        Color(0x00FFFFFF)
                                      ]),
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
                            ),
                          ],
                      ),
                    )
                ),
              )
          ),

            ],
          ),
        ));
  }
}

