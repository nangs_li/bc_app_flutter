import 'package:flutter/material.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:icanfight/controller/assessmentDetailController.dart';
import 'package:icanfight/view/customCheckBox.dart';
import 'package:intl/intl.dart';


import 'package:icanfight/api_provider/assessmentApiProvider.dart';
import 'assessmentCompleteController.dart';
import 'package:icanfight/utilities/lang.dart';

class AssessmentListingController extends StatefulWidget {
  _AssessmentListingControllerState createState() =>
      _AssessmentListingControllerState();
}

class _AssessmentListingControllerState
    extends State<AssessmentListingController> {
  int level = 0;

  List choiceValues = [false, false, false, false, false, false, false];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAssessmentLevel();
  }

  Future navigateToDetailPage(context, index) async {
    ga.gaEvent(name: "assessment_start_button");
    Navigator.pop(context);
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) {
        return AssessmentDetailController(index: index);
      }),
      (Route<dynamic> route) => true, /**/
    );
  }

  getAssessmentLevel() async {
    List<String> assLists = await AssessmentApiProvider().getTodayAssessments();
    int count = 0;
    int index = 0;
    assLists.forEach((value) {
      debugPrint(value);
      if (value != "-1") {
        count++;
        choiceValues[index] = true;
      }
      ;
      index++;
    });
    setState(() {
      level = (count / 7 * 100).round();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView( isBack: true,titleText: DateFormat("yyyy年MMMdd日", 'zh_HK')
                      .format(DateTime.now()),type: SliverAppBarType.AssessmentListingPage,widgetList: <Widget>[ Container(
                    alignment: Alignment.centerRight,
                    margin: EdgeInsets.only(right: 20,top: 4),
                    child: CircularPercentIndicator(
                      radius: 50.0,
                      lineWidth: 2.0,
                      percent: level / 100,
                      center: new Text(
                        "$level%",
                        textScaleFactor: 1.0,
                        style: new TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      progressColor: Colors.white,
                    ),
                  )]),
                  SliverSpacer(),
                  SliverToBoxAdapter(
                      child: Container(
                    color: kLightHealthTheme.primaryColor,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Container(
                        padding: EdgeInsets.only(top: 20),
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.only(
                                topLeft: const Radius.circular(20.0),
                                topRight: const Radius.circular(20.0))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 0);
                                },
                                child: Container(
                                    height: 61,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 60,
                                                width: 40,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Sleep.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 20),
                                                child: Text(
                                                  Lang.getString("assessment_sleep"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[0]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 1);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 40,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Energy.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 20),
                                                child: Text(
                                                  Lang.getString("assessment_energy"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[1]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 2);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 35,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Appetite.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 25),
                                                child: Text(
                                                  Lang.getString("assessment_appetite"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[2]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 3);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 35,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Mood.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 25),
                                                child: Text(
                                                  Lang.getString("assessment_mood"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[3]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 4);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 35,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Sociability.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 25),
                                                child: Text(
                                                  Lang.getString("assessment_sociability"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[4]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 5);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 35,
                                                margin:
                                                    EdgeInsets.only(left: 40),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Exercise.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 25),
                                                child: Text(
                                                  Lang.getString("assessment_exercise"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[5]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            GestureDetector(
                                onTap: () {
                                  navigateToDetailPage(context, 6);
                                },
                                child: Container(
                                    height: 77,
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                                height: 76,
                                                width: 25,
                                                margin:
                                                    EdgeInsets.only(left: 45),
                                                decoration: new BoxDecoration(
                                                  image: DecorationImage(
                                                    image: new AssetImage(
                                                        'assets/images/Hydration.png'),
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                  //                    shape: BoxShape.circle,
                                                )),
                                            Container(
                                                margin:
                                                    EdgeInsets.only(left: 30),
                                                child: Text(
                                                  Lang.getString("assessment_hydration"),
                                                  style: new TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Color(0xFF707070)),
                                                )),
                                            Spacer(),
                                            choiceValues[6]
                                                ? Container(
                                                    alignment:
                                                        Alignment.topRight,
                                                    margin: EdgeInsets.only(
                                                        right: 28, top: 5),
                                                    child: CustomCheckbox(
                                                      value: true,
                                                      onChanged:
                                                          (bool value) {},
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Container(
                                          height: 1,
                                          color: Color(0xFFE6E6E6),
                                        )
                                      ],
                                    ))),
                            BottomButton(buttonText:Lang.getString("assessment_start"),onPressed: () async { navigateToDetailPage(context, 0); }),

                          ],
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
