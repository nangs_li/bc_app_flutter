import 'dart:io';

import 'package:flutter/material.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customAlertDialog.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'baseViewController.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'signInController.dart';
import 'tncController.dart';
import 'disclaimerController.dart';
import 'package:icanfight/utilities/lang.dart';

class SettingController extends StatefulWidget {
  @override
  _SettingControllerState createState() => _SettingControllerState();
}

class _SettingControllerState extends State<SettingController> {

  String version = "";
  SingletonShareData data;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = SingletonShareData();
    initModel();
    data.setVersion().then((versionString) {
      setState(() {
        version = versionString;
      });
    });
  }

  initModel() async {

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            top: true,
            bottom: false,
            child: Material(
              color: Colors.white,
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                CustomSliverAppBarView(isBack: true,type: SliverAppBarType.SettingPage,titleText: Lang.getString("profile_setting"),
                          ),
                  SliverSpacer(),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (context, index) => Container(
                              color: kLightHealthTheme.primaryColor,
//                              height: 1000,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: new BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: new BorderRadius.only(
                                                topLeft:
                                                    const Radius.circular(20.0),
                                                topRight: const Radius.circular(
                                                    20.0))),
                                        padding: EdgeInsets.only(top: 0),
//                                        height: 1000,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
//                                            Row(
//                                              children: <Widget>[
//                                                Container(
////                                                color: Colors.black,
//                                                  // height: 37,
//                                                  width: MediaQuery.of(context)
//                                                          .size
//                                                          .width *
//                                                      0.5,
//                                                  padding:
//                                                      EdgeInsets.only(left: 20),
//                                                  child: Text(Lang.getString("profile_general"),
//                                                      textAlign: TextAlign.left,
//                                                      style: new TextStyle(
//                                                          color:
//                                                              Color(0xFFF36E98),
//                                                          fontSize: 19,
//                                                          fontWeight:
//                                                              FontWeight.bold)),
//                                                ),
//                                              ],
//                                            ),
//                                            Container(
//                                                height: 1,
//                                                margin: EdgeInsets.only(top: 5.0),
//                                                width: MediaQuery.of(context)
//                                                    .size
//                                                    .width,
//                                                color: Color(0xFFE6E6E6)),
//                                            Row(
//                                              children: <Widget>[
//                                                Container(
//                                                  margin: EdgeInsets.only(left: 15,right: 10),
//                                                  padding: EdgeInsets.only(top: 10),
//                                                  child: Container(
//                                                    child: Icon(Icons.language, color: Color(0xFFFB729C), size:30),
//                                                  ),
//                                                ),
//                                                Container(
//                                                  // height: 30,
//                                                  margin:EdgeInsets.only(top: 10),
//                                                  // width: (MediaQuery.of(context).size.width -54) *0.9,
//                                                  alignment:Alignment.centerLeft,
//                                                  child: Text(
//                                                    Lang.getString("profile_lang"),
//                                                    style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color:Color(0xFFA8A9BD)),
//                                                  ),
//                                                ),
//                                              ],
//                                            ),
//                                            InkWell(
//                                              child: Container(
////                                                height: 30,
//                                                alignment: Alignment.centerLeft,
//                                                margin: EdgeInsets.only(left: 55, bottom: 10),
//                                                child: Text(
//                                                  (Lang.locale == "tc") ? "English" : "繁體中文",
//                                                  style: new TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Color(0xFF555555)),
//                                                ),
//                                              ),
//                                              onTap: () {
//                                                Lang.switchLang();
//                                                Navigator.pop(context);
//
//                                                Navigator.push(
//                                                  context,
//                                                  MaterialPageRoute(builder: (context) => SettingController(),));
//                                              },
//                                            ),
//                                            Container(
//                                                height: 1,
//                                                width: MediaQuery.of(context).size.width,color: Color(0xFFE6E6E6)),
//                                            Row(
//                                              children: <Widget>[
//                                                Container(
//                                                  margin: EdgeInsets.only(left: 15, right: 10),
//                                                  padding: EdgeInsets.only(top: 10),
//                                                  child: Container(
//                                                    child: Icon(Icons.notifications, color: kLightHealthTheme.primaryColor, size: 30,)
//                                                  ),
//                                                ),
//                                                Container(
//                                                  // height: 30,
//                                                  margin:EdgeInsets.only(top: 10),
//                                                  // width: (MediaQuery.of(context).size.width -54) *0.9,
//                                                  alignment:Alignment.centerLeft,
//                                                  child: Text(
//                                                    Lang.getString("profile_notification"),
//                                                    style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color:Color(0xFFA8A9BD)),
//                                                  ),
//                                                ),
//                                              ],
//                                            ),
//                                            Container(
//                                              // height: 30,
//                                              alignment: Alignment.centerLeft,
//                                              margin: EdgeInsets.only(left: 55, right: 10, bottom: 10),
//                                              child: Text(
//                                                "On",
//                                                style: new TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Color(0xFF555555)),
//                                              ),
//                                            ),
//                                            Container(
//                                                height: 1,
//                                                width: MediaQuery.of(context).size.width,
//                                                color: Color(0xFFE6E6E6)),
                                            Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                                width: MediaQuery.of(context).size.width,
                                                padding:EdgeInsets.only(top: defaultPadding),
//                                        height: 100,
                                                child: Column(
                                                  children: <Widget>[
                                                    Container(
//                                                color: Colors.black,
                                                      // height: 37,
                                                      padding: EdgeInsets.only(left: 20),
                                                      alignment:Alignment.centerLeft,
                                                      child: Text(Lang.getString("profile_other_setting"),
                                                          textAlign:TextAlign.left,
                                                          style: new TextStyle(color: Color(0xFFF36E98),fontSize: 19,fontWeight:FontWeight.bold)),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        margin: EdgeInsets.symmetric(vertical: 5.0),
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),
                                                    Row(
                                                      children: <Widget>[
                                                        InkWell(
                                                          child: Container(
                                                            height: 55,
                                                            width: (MediaQuery.of(context).size.width) *0.9 -20,
                                                            margin:EdgeInsets.only(left: 20),
                                                            alignment: Alignment.centerLeft,
                                                            child: Text(
                                                              Lang.getString("profile_disclaimer"),
                                                              style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                            ),
                                                          ),
                                                          onTap: () {

                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(builder: (context) => BaseViewController(TncController(isAuth: true,))));
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),
                                                    Row(
                                                      children: <Widget>[
                                                        InkWell(
                                                          child: Container(
                                                            height: 55,
                                                            width: (MediaQuery.of(context).size.width) *0.9 -20,
                                                            margin:EdgeInsets.only(left: 20),
                                                            alignment: Alignment.centerLeft,
                                                            child: Text(
                                                              Lang.getString("profile_privacy"),
                                                              style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                            ),
                                                          ),
                                                          onTap: () {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(builder: (context) => DisclaimerController(isAuth: true, isStatic: true, isFirst: true,)),);
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),
                                                    Row(
                                                      children: <Widget>[
                                                        InkWell(
                                                          child: Container(
                                                            height: 55,
                                                            width: (MediaQuery.of(context).size.width) *0.9 -20,
                                                            margin:EdgeInsets.only(left: 20),
                                                            alignment: Alignment.centerLeft,
                                                            child: Text(
                                                              version,
                                                              style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                            ),
                                                          ),
                                                          onTap: () {

                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),
                                                    Container(
//                                                      height: 55,
                                                      child: Column(
                                                        children: <Widget>[
                                                          InkWell(
                                                            onTap: () {

                                                              showDialog(
                                                                  barrierDismissible: true,
                                                                  context: context,
                                                                  builder: (buildContext) {
                                                                    return CustomAlertDialog(title: Lang.getString("profile_sure"), content: null, leftButtonText: Lang.getString(
                                                                        "profile_stay"),rightButtonText:Lang.getString(
                                                                        "profile_logout") ,buildContext: buildContext,
                                                                        rightButtonFunction: () async {
                                                                          SingletonShareData().signOut();
                                                                          Navigator.push(
                                                                              context,
                                                                              MaterialPageRoute(builder: (context) => BaseViewController(SignInController())));
                                                                        });
                                                                  }
                                                              );
                                                            },
                                                            child: Container(
                                                                height: 55,
                                                                width: (MediaQuery.of(context).size.width),
                                                                margin: EdgeInsets.only(left: 20),
                                                                alignment: Alignment.centerLeft,
                                                                child: Text(
                                                                  Lang.getString("profile_logout"),
                                                                  style: new TextStyle(fontSize:16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                                )),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),
                                                  ],
                                                ))
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                        childCount: 1),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
