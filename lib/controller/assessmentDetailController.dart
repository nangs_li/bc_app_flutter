import 'package:flutter/material.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:icanfight/view/customCheckBox.dart';

import 'package:intl/intl.dart';
import 'assessmentListingController.dart';
import 'assessmentCompleteController.dart';
import 'package:icanfight/utilities/lang.dart';

import 'package:icanfight/api_provider/assessmentApiProvider.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'baseViewController.dart';

class AssessmentDetailController extends StatefulWidget {
  final int index;

  AssessmentDetailController({Key key, @required this.index}) : super(key: key);

  _AssessmentDetailControllerState createState() =>
      _AssessmentDetailControllerState(index: index);
}

class _AssessmentDetailControllerState extends State<AssessmentDetailController>
    with TickerProviderStateMixin {
  final int index;
  int level = 0;
  int _count = 0;
  int nextCount = 0;
  bool _firstTimeFlag = true;
  Animation<Offset> _offset;
  AnimationController _animationController;
  ScrollController _scrollController;

  _AssessmentDetailControllerState({Key key, @required this.index});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAssessmentLevel();
    if (index == 3) {
      setState(() {
        _value = 1.0;
        _selectedValue = 4;
      });
    }

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 10), reverseDuration: Duration(microseconds: 500));

    _offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 2.0))
        .animate(_animationController);
    _animationController.forward();

  }

  getAssessmentLevel() async {
    List<String> assLists = await AssessmentApiProvider().getTodayAssessments();
    int count = 0;
    assLists.forEach((value) {
//      debugPrint(value);
        if (value != "-1") {
          count++;
        }
    });
    setState(() {
      level = (count/7 * 100).round();
      _count = count;
      nextCount = count;
    });

    if (assLists[index] != "-1") {
      setState(() {
        _firstTimeFlag = false;
        choiceValues[int.parse(assLists[index])] = true;
        _value = int.parse(assLists[index])/(answers[index].length-1);
        _selectedValue = int.parse(assLists[index]);
      });
    }
  }

  List questions = [
    Lang.getString("assessment_sleep_q"),
    Lang.getString("assessment_energy_q"),
    Lang.getString("assessment_appetite_q"),
    Lang.getString("assessment_mood_q"),
    Lang.getString("assessment_sociability_q"),
    Lang.getString("assessment_exercise_q"),
    Lang.getString("assessment_hydration_q"),
  ];

  List<List> answers = [
    [
      Lang.getString("assessment_sleep_5"),
      Lang.getString("assessment_sleep_4"),
      Lang.getString("assessment_sleep_3"),
      Lang.getString("assessment_sleep_2"),
      Lang.getString("assessment_sleep_1"),
    ],
    [
      Lang.getString("assessment_energy_5"),
      Lang.getString("assessment_energy_4"),
      Lang.getString("assessment_energy_3"),
      Lang.getString("assessment_energy_2"),
      Lang.getString("assessment_energy_1"),
    ],
    [
      Lang.getString("assessment_appetite_5"),
      Lang.getString("assessment_appetite_4"),
      Lang.getString("assessment_appetite_3"),
      Lang.getString("assessment_appetite_2"),
      Lang.getString("assessment_appetite_1"),
    ],
    [
      "assets/images/Icon_Angry_L.svg",
      "assets/images/Icon_Sad_L.svg",
      "assets/images/Icon_Neutral_L.svg",
      "assets/images/Icon_Happy_L.svg",
      "assets/images/Icon_Awesome_L.svg",
    ],
    [
      Lang.getString("assessment_sociability_5"),
      Lang.getString("assessment_sociability_4"),
      Lang.getString("assessment_sociability_3"),
      Lang.getString("assessment_sociability_2"),
      Lang.getString("assessment_sociability_1"),
    ],
    [
      Lang.getString("assessment_exercise_3"),
      Lang.getString("assessment_exercise_2"),
      Lang.getString("assessment_exercise_1"),
    ],
    [
      Lang.getString("assessment_hydration_2"),
      Lang.getString("assessment_hydration_1"),
    ]
  ];

  List _images = [
    'Sleep.png',
    'Energy.png',
    'Appetite.png',
    'Mood.png',
    'Sociability.png',
    'Exercise.png',
    'Hydration.png'
  ];

  List choiceValues = [false, false, false, false, false];

  double _value = 0.5;
  int _selectedValue = 0;

  void setValue(double value) async {

    List<String> assLists = await AssessmentApiProvider().getTodayAssessments();

    if (assLists[index] == "-1") {
      setState(() {
        nextCount = _count+1;
      });
    }

    setState(() {
      _firstTimeFlag = false;
      _value = value;
      _selectedValue = (value * (answers[index].length - 0.1)).floor();
      choiceValues = [false, false, false, false, false];
      choiceValues[(value * (answers[index].length - 0.1) ).floor()] = true;
    });

    if (!_firstTimeFlag) {
      _animationController.reverse();
    }
  }

  submitAnswerAction () {
    debugPrint(index.toString()+":"+choiceValues.indexOf(true).toString());
    AssessmentApiProvider().addAssessment(index, choiceValues.indexOf(true).toString());
  }

  Widget _moodWidget() {
    return SvgPicture.asset(answers[index][_selectedValue], height: 160,);
  }

  void onTapIndex(int index, bool boolValue) {
  setState(() {
    _animationController.reverse();
    choiceValues = [false, false, false, false, false];
    choiceValues[index] = boolValue;
  });
}

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(
                    isBack: true,
                    type: SliverAppBarType.AssessmentDetailPage,
                    titleText: DateFormat("yyyy MMMdd日", 'zh_HK').format(
                        DateTime.now()),
                    widgetList: <Widget>[ Container(
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(right: 20, top: 4),
                      child: CircularPercentIndicator(
                        radius: 50.0,
                        lineWidth: 2.0,
                        percent: level / 100,
                        center: new Text(
                          "$level%",
                          textScaleFactor: 1.0,
                          style: new TextStyle(fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                        ),
                        progressColor: Colors.white,
                      ),
                    )
                    ],
                    backCallBack: () {
                      showDialog(
                          barrierDismissible: true,
                          context: context,
                          builder: (buildContext) {
                            return CustomAlertDialog(title: Lang.getString("assessment_exit"), content: Lang.getString(
                                "assessment_saved"), leftButtonText: Lang.getString(
                                "cancel_button"),rightButtonText:Lang.getString(
                                "exit_button") ,buildContext: buildContext,
                                rightButtonFunction: () async {
                                  Navigator.pop(buildContext);
                                  Navigator.pop(context);
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(builder: (context) =>
                                        AssessmentListingController()),
                                        (Route<dynamic> route) => true, /**/
                                  );
                                });

                          }
                      );
                    },
                  ),
                  SliverSpacer(),
                  SliverToBoxAdapter(
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Container(
                          color: kLightHealthTheme.primaryColor,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Container(
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: new BorderRadius.only(
                                      topLeft:
                                      const Radius.circular(20.0),
                                      topRight: const Radius.circular(
                                          20.0))),
                              padding: EdgeInsets.only(top: 20, bottom: 210),
                              child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                          height: 70,
                                          width: 70,
                                          margin: EdgeInsets.only(left: 40),
                                          decoration: new BoxDecoration(
                                            image: DecorationImage(
                                              image: new AssetImage('assets/images/' + _images[index]),
//                    fit: BoxFit.fitWidth,
                                            ),
                                            //                    shape: BoxShape.circle,
                                          )),
                                      Container(
                                        width: SizeConfig().safeBlockHorizontal * 68,
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(left: topPadding),
                                          child: Text(
                                            questions[index],
                                            maxLines: 5,
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: true,
                                            style: new TextStyle(fontSize: 19, fontWeight: FontWeight.w700, color: assessmentTextColor),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  (index == 3) ?
                                  Container(
                                    child: Column(
                                      children: <Widget>[
                                        Slider(
                                          value: _value,
                                          onChangeStart: setValue,
                                          onChanged: setValue,
                                          divisions: answers[index].length - 1,
                                          activeColor: kLightHealthTheme.primaryColor,
                                          inactiveColor: Color(0xFFE6E6E6),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 20, right: 20),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text((index == 5) ? Lang.getString("assessment_0min") : ((index == 6) ? Lang.getString("assessment_hydration_2") : Lang.getString("assessment_poor")),
                                                  style: new TextStyle(
                                                      color: Color(0xFF9291AC),
                                                      fontWeight: FontWeight.w600, fontSize: 16)),
                                              Text((index == 5) ? Lang.getString("assessment_30mins") : ((index == 6) ? Lang.getString("assessment_hydration_1") : Lang.getString("assessment_good")),
                                                  style: new TextStyle(
                                                      color: Color(0xFF9291AC),
                                                      fontWeight: FontWeight.w600, fontSize: 16))
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                  :Container(),
                                  (index == 3) ? _moodWidget() :
                                  Column(
                                    children: [
                                      answers[index].length > 4 ?  AssessmentDetailRow(questionIndex: index,answerIndex: 4, function: (bool value) {
                                       onTapIndex(4, value);

                                  },
                                          choiceValues: choiceValues,answers: answers) : Container(),
                                      answers[index].length > 3 ?
                                      Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 3,
                                              child: Container(
                                                alignment: Alignment.centerLeft,
                                                margin: EdgeInsets.only(left: 20),
                                                child:  Text(
                                                  answers[index][3],
                                                  style: index == 3 ? new TextStyle(fontSize: 35) : new TextStyle(fontSize: 16, color: assessmentTextColor),
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 5,
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 1,
                                              child: Container(
                                                alignment: Alignment.topRight,
                                                margin: EdgeInsets.only(right: 20),
                                                child: CustomCheckbox(
                                                  isInactiveShowCircle: true,
                                                  value: choiceValues[3],
                                                  backgroundColor: Colors.transparent,
                                                  onChanged: (bool value) {
                                                    onTapIndex(3, value);
                                                  },
                                                ),
                                              ),
                                            )

                                          ],
                                        ),
                                      ) : Container(),
                                      answers[index].length > 2 ?  AssessmentDetailRow(questionIndex: index,answerIndex: 2, function: (bool value) {

                                        onTapIndex(2, value);
                                      },choiceValues: choiceValues,answers: answers) : Container(),
                                      AssessmentDetailRow(questionIndex: index,answerIndex: 1, function: (bool value) {
                                        onTapIndex(1, value);
                                      },choiceValues: choiceValues,answers: answers),
                                      AssessmentDetailRow(questionIndex: index,answerIndex: 0, function: (bool value) {
                                        onTapIndex(0, value);
                                      },choiceValues: choiceValues,answers: answers),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SlideTransition(
            position: _offset,
            child:bottomButton()
          )
        ],
      ),
    );
  }

  Widget bottomButton() {
    bool finishAssessment = nextCount >= 6 ? true : false;
    return BottomButton(buttonText:finishAssessment ? Lang.getString("finish_button") : Lang.getString("next_button"),onPressed: () async {
      submitAnswerAction();
      Navigator.pop(context);
      if (finishAssessment) {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BaseViewController(AssessmentCompleteController())));
      } else {
        if(index == 0){
          ga.gaEvent(name: "assessment_sleep_button");
        } else if(index == 1){
          ga.gaEvent(name: "assessment_energy_button");
        } else if(index == 2){
          ga.gaEvent(name: "assessment_appetite_button");
        } else if(index == 3){
          ga.gaEvent(name: "assessment_mood_button");
        } else if(index == 4){
          ga.gaEvent(name: "assessment_sociability_button");
        } else if(index == 5){
          ga.gaEvent(name: "assessment_exercise_button");
        } else if(index == 6){
          ga.gaEvent(name: "assessment_hydration_button");
        }

        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => BaseViewController(AssessmentDetailController(
            index: (index+1)%7,
          ))),
              (Route<dynamic> route) => true,/**/
        );
      }
    });
  }
}



class AssessmentDetailRow extends StatelessWidget {

  final int questionIndex;
  final int answerIndex;
  final List choiceValues;
  final List<List> answers;
  final Function function;

  AssessmentDetailRow({
    Key key,
    this.questionIndex,
    this.answerIndex,
    this.choiceValues,
    this.answers,
    this.function
  }) :super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 20),
            child: (questionIndex == 3) ? Image.asset(answers[questionIndex][answerIndex]) : Text(
              answers[questionIndex][answerIndex],
              style: questionIndex == 3 ? new TextStyle(fontSize: 35) : new TextStyle(fontSize: 15, color: assessmentTextColor),
              overflow: TextOverflow.ellipsis,
              maxLines: 5,
            ),
          ),
        ),

        Expanded(
          flex: 1,
          child: Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(right: 20),
            child: CustomCheckbox(
              value: choiceValues[answerIndex],
              backgroundColor: Colors.transparent,
              isInactiveShowCircle: true,
              onChanged: function,
            ),
          ),
        ),

      ],
    )
    );
  }

}