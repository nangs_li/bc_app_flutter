import 'package:flutter/material.dart';
import 'package:icanfight/view/bottomButton.dart';
//import 'package:firebase_auth/firebase_auth.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:icanfight/utilities/lang.dart';
// not use again
class ResetPasswordController extends StatefulWidget {
  @override
  _ResetPasswordControllerState createState() =>
      _ResetPasswordControllerState();
}

class _ResetPasswordControllerState
    extends State<ResetPasswordController> {

  List<FocusNode> _formNodes = [
    FocusNode(),FocusNode()
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initFirebase();
  }

  void loginAction() async {

//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => BottomNavigationBarController()),);
  }

  void initFirebase() async {

//    _handleSignIn()
//        .then((FirebaseUser user) => (user.getIdToken(refresh: true)
//        .then((IdTokenResult result) => debugPrint(result.token.toString()))))
//        .catchError((e) => debugPrint(e.toString()));

  }

//  Future<FirebaseUser> _handleSignIn() async {
//
//    final FirebaseUser user = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: "test@echo.com", password: "Aa123456")).user;
//    return user;
//  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          SafeArea(
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: false,
                      titleText: "Reset your password",
                      type: SliverAppBarType.ResetPasswordPage
                  ),
                  SliverToBoxAdapter(
                    child: Stack(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              color: kLightHealthTheme.primaryColor,
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top: 0, left: 20, right: 20, bottom: 50),
                              child: Text(Lang.getString("profile_confirm_message"),
                                style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                            ),
                            WaveBar(),
                          ],
                        ),
                        Positioned(
                          child: Container(
                            alignment: Alignment(0, 0),
                            margin: EdgeInsets.only(top: 135),
                            child: Center(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      margin: EdgeInsets.only(left: 20, right: 20),
                                      child: Card(
                                        color: Colors.white,
                                        child: Container(
                                          padding: EdgeInsets.only(top: 30, bottom: 30, left: 20, right: 20),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              CustomInputField(iconData: 0xe0be, inputTitle: "Email", focusNode: _formNodes[0]),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.bottomCenter,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                              margin: EdgeInsets.only(top: 100, bottom: 20),
                                              child: ButtonTheme(
                                                minWidth: 300,
                                                child: RaisedButton(
                                                  elevation: 8,
                                                  padding: EdgeInsets.all(12),
                                                  child: Text("Send", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                                                  onPressed: () async {
                                                    Navigator.pop(context);
                                                  },
                                                  shape: RoundedRectangleBorder(side: BorderSide(color: kLightHealthTheme.buttonColor), borderRadius: BorderRadius.all(Radius.circular(30))),
                                                  textColor: Colors.white,
                                                  color: kLightHealthTheme.buttonColor,
                                                ),
                                              )
                                          ),
                                          InkWell(
                                            child: Text("Cancel  ", style: TextStyle(color: Color(0xFF555555), fontSize: 16, fontWeight: FontWeight.normal
                                                , decoration: TextDecoration.none),),
                                            onTap: () {
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}