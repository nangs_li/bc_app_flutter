import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:icanfight/api_provider/articleApiProvider.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/category.dart';
import 'package:icanfight/model/tag.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/postCardSkeletonView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:icanfight/view/webViewPage.dart';
import 'baseViewController.dart';
import 'fullArticleController.dart';
import 'learnController.dart';
import 'learnMenuController.dart';
import 'learnSearchController.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:html/dom.dart' as dom;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_skeleton/flutter_skeleton.dart';


class LearnCategoryController extends StatefulWidget {


  int tabIdx = 0;

  LearnCategoryController({ int index = 0 }) {
    tabIdx = index;
  }

  @override
  _LearnCategoryControllerState createState() =>
      _LearnCategoryControllerState(index: tabIdx);
}

class _LearnCategoryControllerState
    extends State<LearnCategoryController> with TickerProviderStateMixin {

  List<Post> postArray;
  List<Category> _catArray;
  List<Tag> _tagArray;
  SingletonShareData data;
  int filterIdx = -1;
  int tabIdx = 0;
  TabController _tabController;
  ScrollController _scrollViewController;
  DragStartDetails startVerticalDragDetails;
  DragUpdateDetails updateVerticalDragDetails;

  _LearnCategoryControllerState({ int index = 0 }) {
    tabIdx = index;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ga.gaSetCurrentScreen(screenName: "learn_page");
    data = SingletonShareData();
    initModel();
    _tabController = TabController(length: _catTabs().length, vsync: this, initialIndex: tabIdx);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
  }

  initModel()  {
    postArray = List();
    _catArray = List();
    _tagArray = List();

    data.postApiCallBack.listen((List<Post> postList) {

      _tagArray = data.tagList;

      List<Post> posts = data.fullPostList;

      posts.forEach((Post post) {
        _tagArray.forEach((Tag tag) {
          if (post.tags.contains(tag.id)) {
            post.appendTag(tag);
          }
        });

        String content = post.content.rendered;
        int haveUploadLink = content.indexOf(
            "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/wp-content/uploads");
        if (haveUploadLink >
            0) {
        //  print('indexOf2:${content.indexOf("\"", haveUploadLink)}');
          if (content.indexOf("\"", haveUploadLink) > 0) {
            String substr = content.substring(haveUploadLink,
                content.indexOf("\"", haveUploadLink));
            post.thumbnail = substr;
          }
        }
      });


      postArray = posts;

      data.allCategories = List();

        List<Category> categories = data.categoryList;

        categories.forEach((Category cat) {
          postArray.forEach((Post post) {
            _tagArray.forEach((Tag tag) {
              if (post.tags.contains(tag.id)) {
                post.appendTag(tag);
              }
            });

            if (post.categories.contains(cat.id)) {
              String content = post.content.rendered;
              int haveUploadLink = content.indexOf(
                  "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/wp-content/uploads");
              if (haveUploadLink >
                  0) {
               // print('indexOf2:${content.indexOf("\"", haveUploadLink)}');
                if (content.indexOf("\"", haveUploadLink) > 0) {
                  String substr = content.substring(haveUploadLink,
                      content.indexOf("\"", haveUploadLink));
                  post.thumbnail = substr;
                }
              }
              cat.appendPost(post);
            }
          });


          categories.forEach((Category subCat) {
            if (cat.id == subCat.parent) {
              cat.appendSubCategory(subCat);
            }
          });

          data.allCategories.add(cat);
        });

        print("123");
        data.allCategories.removeWhere((Category c) {
          return c.posts.length == 0 || c.parent != 0 || c.id == 153 ||
              c.id == 152;
        });

        Category lastCat = data.allCategories.firstWhere((cat) => (cat.id == 18));

        data.allCategories.removeWhere((cat) => cat.id == 18);
        data.allCategories.add(lastCat);

        setState(() {
          data.postReady = true;
          _catArray = data.filterCategoryPostFromQuestionAnswer(data.allCategories);
          print("_catArray:$_catArray");
          filterIdx = data.allCategories[0].id;
          clickTabBar(0);
        });
        _tabController = TabController(
            length: _catTabs().length, vsync: this, initialIndex: tabIdx);
    });
  }

  contextView() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
              (context,index) =>
              (data.postReady) ?  GestureDetector(
                      onHorizontalDragStart: (dragDetails) {
                        startVerticalDragDetails = dragDetails;
                      },
                      onHorizontalDragUpdate: (dragDetails) {
                        updateVerticalDragDetails = dragDetails;
                      },
                      onHorizontalDragEnd: (endDetails) {
                        double dx = updateVerticalDragDetails.globalPosition.dx -
                            startVerticalDragDetails.globalPosition.dx;
                        double dy = updateVerticalDragDetails.globalPosition.dy -
                            startVerticalDragDetails.globalPosition.dy;


                        print("dx:$dx");
                        if(dx > 0) {
                          if(tabIdx - 1 >= 0){
                            clickTabBar(tabIdx - 1);
                          }

                        } else if(dx < 0) {

                          if(tabIdx + 1 < _catTabs().length){
                            clickTabBar(tabIdx + 1);
                          }
                        }
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left:defaultPadding, right: defaultPadding),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(top: defaultPadding, bottom: defaultPadding),
                                  child: Container(
                                    child: Column(
                                      children: (filterIdx == -1) ? allArticleWidgetList() : _catArray.where((Category c) {
                                        return (filterIdx < 0 || c.id == filterIdx);
                                      }).toList()[index].posts.asMap().map((i,Post p) {

                                        return  MapEntry(i,PostCard(p,i));
                                      }).values.toList(),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      )
                  ) : PostCardSkeletonView(topPadding: 0),

          childCount: childCount()
      ),
    );
  }

  List allArticleWidgetList() {
   List widget = data.allPostList.asMap().map((i,Post p) {
      //print("indexi:$i");
      return  MapEntry(i,PostCard(p,i));
    }).values.toList();
    return widget;
  }

  int childCount() {
    int childCount;
    if(!data.postReady){
      childCount = 1;
    } else if(_catArray == null){
      childCount = 0;
    } else if(filterIdx == -1){
      childCount = 1;
    } else {
//      childCount = _catArray.where((Category c) {
//        return (c.id == filterIdx);
//      }).length;
      childCount = 1;
    }
    return childCount;
  }

  _catTabs() {
    List<GestureDetector> tabs = List();

    if (_catArray != null) {
      _catArray.asMap().forEach((int index,Category cat) {
        tabs.add(GestureDetector(
            child:Tab(
          child: DecoratedBox(
              decoration: BoxDecoration(
                  color: (tabIdx == _catArray.indexOf(cat)) ? Colors.white : Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                child: Text(cat.name, style: TextStyle(color: (tabIdx == _catArray.indexOf(cat)) ? kLightHealthTheme.primaryColor : Colors.white ),),
              )
          ),
        )
        ,onTap:(){
          clickTabBar(index);
        }));
      });
    }

    int length = 0;
    if (_catArray != null) {
      length = _catArray.length;
    }

    tabs.add(GestureDetector(
        child:Tab(
      child: DecoratedBox(
          decoration: BoxDecoration(
              color: (tabIdx == length) ? Colors.white : Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            child: Text("全部文章", style: TextStyle(color: (tabIdx == length) ? kLightHealthTheme.primaryColor : Colors.white ),),
          )

      ),
    )
    ,onTap: (){
      clickTabBar(tabs.length - 1);
    }));

    return tabs;
  }

  clickTabBar (int idx){
    idx = idx ?? 0;
    _tabController.animateTo(idx);
    print("tabIndex:$idx");
    if(idx == 0){
      ga.gaEvent(name: "learn_categories_blog");
    } else  if(idx == 1){
      ga.gaEvent(name: "learn_categories_latestnews");
    } else  if(idx == 2){
      ga.gaEvent(name: "learn_categories_understandingBC");
    } else  if(idx == 3){
      ga.gaEvent(name: "learn_categories_howtotreat");
    } else  if(idx == 4){
      ga.gaEvent(name: "learn_categories_livingwell");
    } else  if(idx == 5){
      ga.gaEvent(name: "learn_categories_doihaveBC");
    }

    int length = 0;
    if (_catArray != null) {
      length = _catArray.length;
    }
    if (idx >= 0 && idx < length) {
      setState(() {
        filterIdx = _catArray[idx].id;
        tabIdx = idx;
      });
    } else if (idx == length) {
      setState(() {
        filterIdx = -1;
        tabIdx = idx;
      });
    } else {
      setState(() {
        tabIdx = idx;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: true,type: SliverAppBarType.LearnCategoryPage, titleText: Lang.getString("learn_title"),widgetList:[
                    Padding(
                        padding: EdgeInsets.only(top: topPadding),
                        child: InkWell(
                          child: Container(
                            margin: EdgeInsets.only(right: defaultPadding),
                            child: Icon(
                              Icons.bookmark_border,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                          onTap: () {
                            ga.gaEvent(name: "learn_bookmark_button");
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                BaseViewController(LearnController(
                                        articles: postArray,
                                        catName: "",
                                      ))),
                            );
                          },
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: topPadding),
                        child: InkWell(
                          child: Container(
                            margin: EdgeInsets.only(right: defaultPadding),
                            child: Icon(
                              Icons.search,
                              color: Colors.white,
                              size: 35,
                            ),
                          ),
                          onTap: () {
                            if(postArray == null||postArray.length == 0){
                            } else {
                              ga.gaEvent(name: "learn_search_button");
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        BaseViewController(LearnSearchController(
                                          catName: "",
                                          tagList: _tagArray,
                                        ))),
                              );
                            }

                          },
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: topPadding),
                        child: InkWell(
                          onTap: () async {
                            int tapIndex;
                        if(postArray == null||postArray.length == 0){
                        } else {
                          ga.gaEvent(name: "learn_hashtaglist_button");
                           tapIndex = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    BaseViewController(LearnMenuController(
                                      categories: _catArray,
                                      articles: postArray,
                                      catName: "",
                                      tagList: _tagArray,
                                    ))),
                          );
                        }
                         if(tapIndex == 0 || tapIndex == 1) {
                           clickTabBar(tapIndex);
                         }
                          },
                          child: Padding(
                            padding: EdgeInsets.only(right: defaultPadding),
                            child: Icon(Icons.menu, color: Colors.white, size: 35),
                          ),
                        )),
                  ],
                    bottom:
                PreferredSize(preferredSize:Size.fromHeight(60), child:
                      Stack(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: defaultPadding, right: defaultPadding),
                            child: TabBar(
                                isScrollable: true,
                                labelStyle: TextStyle(color: Colors.white, backgroundColor: Colors.transparent,
                                    fontSize: 17, fontWeight: FontWeight.w600
                                ),
                                unselectedLabelStyle: TextStyle(color: Color(0xFFDE7199), backgroundColor: Colors.transparent,
                                    fontSize: 17, fontWeight: FontWeight.w500
                                ),
                                labelPadding: EdgeInsets.symmetric(horizontal: 2.0, vertical: 5.0),
                                labelColor: Colors.white,
                                indicatorColor: Colors.transparent,
                                controller: _tabController,
                                tabs: _catTabs()
                            ),
                          ),
                          Container(
                            margin: rowPadding,
                            alignment: Alignment.topLeft,
                            child: tabIdx == 0 ? Container() : Icon(Icons.keyboard_arrow_left, color: Colors.white, ),
                          ),
                          Container(
                            margin: rowPadding,
                            alignment: Alignment.topRight,
                            child: tabIdx == 6 ? Container() : Icon(Icons.keyboard_arrow_right, color: Colors.white,),
                          ),
                        ],
                      )
                ),
                  ),
                  SliverToBoxAdapter(
                    child: WaveBar(),
                  ),
                 contextView(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


class PostCard extends StatelessWidget {
//  var _keyRed = GlobalKey();
  Widget fetchedHtml(Post post){

    bool isAlreadyFetchedArticle = false;
    SingletonShareData data = SingletonShareData();

    if(data.alreadyFetchedArticle != null) {
      data.alreadyFetchedArticle.forEach((p) {
        if (p == post.id) {
          isAlreadyFetchedArticle = true;
         // print("alreadyFetchedArticle: ${data.alreadyFetchedArticle}");
        }
      });
    }
    if(isAlreadyFetchedArticle){
      return Container();
    } else {
      data.alreadyFetchedArticle.add(post.id);
    }

    print("start getting html image");
    return Html(
        data: post.content.rendered,
        customRender: (node, children) {
          debugPrint("node:"+node.toString());
          if (node is dom.Element) {
            switch (node.localName) {
              case "figcaption":
                debugPrint("figcaption");
                return Column(
                  children: children.map((c) => Center(child: c,)).toList(),
                );
            }
          }
          return Column(children: children,);
        },
        customTextStyle: (node, baseStyle) {
          if (node is dom.Element) {
            switch (node.localName) {
              case "ref":
                return baseStyle.merge(TextStyle(fontSize: 24, color: kLightHealthTheme.primaryColor, fontWeight: FontWeight.w700));
              case "img":
                return baseStyle.merge(TextStyle(fontSize: 24, color: kLightHealthTheme.primaryColor, backgroundColor: Colors.white, fontWeight: FontWeight.w700));
            }
          }
          return baseStyle;
        },
        defaultTextStyle: TextStyle(color: Color(0xFF6A6969), fontSize: 18.0 * 1.0, decoration: TextDecoration.none, fontFamily: "PTSerif" ),
        onImageTap: (source) {

        }
    );
  }

  final Post p;
  final int index;
  PostCard(this.p,this.index);

  @override
  Widget build(BuildContext context) {

    return  Container(
      margin: EdgeInsets.only(bottom: defaultPadding),
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Post post = p;
            String embeddedUrl;
            if (post.id == 1353 || post.id == 1351 || post.id == 1348 || post.id == 1125 ) {
               // embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/26/%e5%a6%b3%e9%97%9c%e5%bf%83%e8%87%aa%e5%b7%b1%e7%9a%84%e4%b9%b3%e6%88%bf%e5%81%a5%e5%ba%b7%e5%97%8e-%e7%97%87%e7%8b%80";
              embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/26/%e5%a6%b3%e9%97%9c%e5%bf%83%e8%87%aa%e5%b7%b1%e7%9a%84%e4%b9%b3%e6%88%bf%e5%81%a5%e5%ba%b7%e5%97%8e-%e8%97%a5%e7%89%a9%e6%b2%bb%e7%99%82/";
            }
            if (post.id == 85) {
                embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/09/herceptin-trastuzumab";
            }
            if (post.id == 200) {
                embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/08/kadcyla-trastuzumab-emtansine";
            }
            if (post.id == 229) {
                embeddedUrl = "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/2019/09/07/perjeta-pertuzumab";
            }
            if(embeddedUrl != null){
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BaseViewController(WebViewPage(webViewUrl: embeddedUrl,))));
            } else {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => BaseViewController(FullArticleController(article: post,bookmarkList: false))));
         }
          },
          child: Container(
            color: Colors.transparent,
            child: Stack(
              children: <Widget>[
                Container(child: fetchedHtml(p),width: 0, height: 0),
                  Container(
                 decoration: BoxDecoration(borderRadius:defaultBorderRadius,color: Colors.white,boxShadow: defaultBoxShadow),
                  margin: EdgeInsets.only(bottom: 60),
                  child: ClipRRect(
                    borderRadius: defaultBorderRadius,
                    child: Container(
                      width: SizeConfig().blockSizeHorizontal * 100,
                      child: CachedNetworkImage(imageUrl: (p.thumbnail == "")? "http://ec2-13-229-39-41.ap-southeast-1.compute.amazonaws.com:8000/wp-content/uploads/2019/09/Export-Master_TP01-KV-1024x910.png": p.thumbnail,
                        // placeholder: (context, string) {
                        //   return Image.asset("", height: 200, fit: BoxFit.contain, alignment: Alignment.topCenter,);
                        // }, height: 200, width: MediaQuery.of(context).size.width, fit: BoxFit.contain, alignment: Alignment.topCenter,),
                        placeholder: (context, string) {
                          return Container(
                            height: SizeConfig().blockSizeHorizontal * 50,
                            child: CardSkeleton(
//                            key:_keyRed ,
                                style: SkeletonStyle(
                                  theme: SkeletonTheme.Light,
                                  isShowAvatar: true,
                                  isCircleAvatar: false,
                                  borderRadius: defaultBorderRadius,
                                  padding: EdgeInsets.all(20),
                                  barCount: 2,
                                  colors: [Color(0xffffffff)],
                                  backgroundColor: Color(0xFFf5f5f5),
                                  isAnimation: false,
                                )
                            ),
                          );
                        }, height: SizeConfig().blockSizeHorizontal * 50, fit: BoxFit.fitWidth, alignment: Alignment.topCenter,),
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 160, bottom: 10, left: defaultPadding, right: defaultPadding),
                    child: ClipRRect(
                      borderRadius: defaultBorderRadius,
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              left: BorderSide(
                                  color: (index % 2 == 0 ) ? postListPinkColor: lightGreenColor,
                                  width: 10),
                            ),
//                          borderRadius: new BorderRadius.all(Radius.circular(10)),
                            boxShadow: defaultBoxShadow
                        ),
                        child:
                        Padding(
                          padding: EdgeInsets.only(left: defaultPadding,right: defaultPadding),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top:defaultPadding),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(p.title.rendered, style: TextStyle(color: (index % 2 == 0 ) ? postListPinkColor: lightGreenColor, fontSize: 22, fontWeight: FontWeight.w700), textAlign: TextAlign.left,),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom:defaultPadding),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: (p.getTagList().length > 0) ?
                                      Wrap(
                                        children: (p.getTagList()).getRange(0, (p.getTagList().length > 4) ? 5 : p.getTagList().length).map((Tag tag) {
                                          return Container(
                                            color: Colors.white,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 4,top: 8),
                                              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
                                              decoration: BoxDecoration(
                                                  color: Colors.transparent,
                                                  border: Border.all(color: (index % 2 == 0 ) ? postListPinkColor: lightGreenColor),
                                                  borderRadius: defaultBorderRadius
                                              ),
                                              child: Text(tag.name, style: TextStyle(color: (index % 2 == 0 ) ? postListPinkColor: lightGreenColor, fontSize: 14, fontWeight: FontWeight.w400),),
                                            ),
                                          );
                                        }).toList(),
                                      ) : Container(),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),

                      ),
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}