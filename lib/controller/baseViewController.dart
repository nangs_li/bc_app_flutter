import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/lang.dart';

class BaseViewController extends StatefulWidget {
  final Widget childWidget;

  BaseViewController(this.childWidget, { Key key }) : super(key: key);
  @override


  _BaseViewControllerState createState() => new _BaseViewControllerState();

}

class _BaseViewControllerState
    extends State<BaseViewController> {

  @override
  void initState() {
    super.initState();
    checkConnection();
  }

  checkConnection() async {

    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          data.isConnected = false;
        });
      } else {
        setState(() {
          data.isConnected = true;
        });
      }
    });

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      debugPrint("WIFI");
      setState(() {
        data.isConnected = true;
      });
    } else {
      debugPrint("NONE");
      setState(() {
        data.isConnected = false;
      });
    }

  }

  //calculator end

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
    InheritedContext(
    child: widget.childWidget),
        (data.isConnected) ? Container() : SafeArea(top: true,
        child: Container(
          height: 100,
          width: MediaQuery.of(context).size.width,
          child: Card(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: InkWell(child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Lang.getString("internet_connection"), style: TextStyle(color: Color(0xFF555555), fontSize: 16, fontWeight: FontWeight.w500),),
                  Text(Lang.getString("popup_ok"), style: TextStyle(color: Color(0xFFE14672)),),
                ],
              )
                , onTap: () {
                  setState(() {
                    data.isConnected = true;
                  });
// Navigator.of(context).pop();
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => BottomNavigationBarController(index: _selectedIndex,)),);
                },
              ),
            ),
          ),
        )
        ),
      ],
    );

  }
}

class InheritedContext extends InheritedWidget {

  //searchbar Animation part
//  final Function() searchPressed;
//  final Function() changeCustomAppBar;
  //searchbar Animation part end

  //Calculator part
//  final Function(SingletonCalculatorShareData, dynamic ,dynamic) getWeeksExcelList;
//  final Function(List <dynamic> ,dynamic ,SingletonCalculatorShareData) testGettingExcelDataCorrect;
//  final Function(List <dynamic>) get30mgDrugName;
//  final Function(dynamic) selectCard;
  //Calculator part end


  InheritedContext({
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  static InheritedContext of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(InheritedContext);
  }

  //是否重建widget就取决于数据是否相同
  @override
  bool updateShouldNotify(InheritedContext oldWidget) {
    return true;
  }
}






