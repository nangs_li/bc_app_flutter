import 'package:flutter/material.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/utilities/themes.dart';

class LearnMoreController extends StatefulWidget {

  int _index;

  LearnMoreController({index: int}) {
    _index = index;
  }

  @override
  _LearnMoreControllerState createState() =>
      _LearnMoreControllerState(index: _index);
}

class _LearnMoreControllerState
    extends State<LearnMoreController> with SingleTickerProviderStateMixin {

  ScrollController _scrollController;
  int _index;

  List<List<String>> _titles = [
    ["第一期","第二期","第三期","第四期","","",""],
    ["HER2", "", "", "","","","",""],
    ["荷爾蒙受體", "", "", "","","","",""],
    ["","","","","","",""],
    ["標靶藥物","","","","","",""],
    ["激素/荷爾蒙治療","","","","","",""],
    ["化學治療（「化療」）","","","","","",""],
    ["放射治療（「電療」）","","","","","",""],
    ["","","","","","",""],
    ["標靶藥物","激素/荷爾蒙治療","化學治療","放射治療","","",""],
    ["標靶藥物","激素/荷爾蒙治療","化學治療","放射治療","","",""],

  ];

  List<List<String>> _contents = [
    ["第一期乳癌是指您的腫瘤很細小並受控於乳房組織之內，或在乳房附近的淋巴結之中。","第二期乳癌是指您的腫瘤出現於乳房組織、乳房附近的淋巴結之中或同時在這兩個範圍出現。癌細胞並未在乳房、腋下的淋巴結及胸骨附近的淋巴結以外出現。","第三期乳癌表示您的癌細胞已經從乳房轉移到附近的淋巴結、乳房皮膚或胸壁。","第四期乳癌表示您的癌細胞已經轉移到身體其他的部分（如骨、肺、肝或腦）。","","",""],
    ["HER2 (第二型人類表皮生長因子受體)自然地存在於乳房細胞內，是一種附在細胞表面的受體。正常細胞會製造少量的 HER2 受體，用作接收生長因子，將生長訊息由細胞外傳送至細胞內，以控制細胞生長。\n\n當乳腺細胞有過量 HER2 受體，令細胞不受控制地增生，最終演化成腫瘤，這就是 HER2 型 (又稱 HER2 過度表現或 HER2 陽性) 乳癌。與其他種類的乳癌相比，HER2 型乳癌腫瘤的生長及擴散速度比較快，在有對抗 HER2 的標靶藥物治療前，患者的存活期一般較短。","","","","","",""],
    ["荷爾蒙受體是一種存在於細胞表面並可與荷爾蒙結合的蛋白。乳癌細胞與正常細胞一樣，會有特定的受體蛋白能夠與ER（雌激素）或PR（孕酮, 亦被稱為黃體酮）結合。乳癌細胞可能會受其中一種或兩種荷爾蒙同時刺激而生長，又或者完全不依賴這些荷爾蒙受體來生長。\n\n荷爾蒙受體陽性的乳癌細胞擁有一種或以上的荷爾蒙受體，接受荷爾蒙治療可以幫助這些患者降低荷爾蒙的製造或阻截荷爾蒙對乳癌細胞的作用。", "", "", "","","","",""],
    ["","","","","","",""],
    ["標靶藥物對特定的癌細胞有針對性，能夠識別、鎖定並針對攻擊癌細胞，不會對正常細胞造成傷害。標靶藥物可能會與化療或其他治療一起使用，以提升療效。","","","","","",""],
    ["激素/荷爾蒙治療是治療的其中一種，專門針對荷爾蒙受體陽性的乳癌。荷爾蒙治療的原理是透過減少雌激素，或抑制雌激素的作用，從而減緩或停止乳癌細胞的生長。有時不同的荷爾蒙治療可以混合使用; 有時也可以和化療先後使用。","","","","","",""],
    ["化學治療（「化療」）即是利用抗癌藥物來破壞癌細胞，但同時亦會殺死代謝較快的正常細胞。治療分口服和靜脈注射兩種形式，每次療程只需幾天，隨後是數星期的休息。療程的次數需視乎癌症種類及妳對藥物的反應而定。","","","","","",""],
    ["放射治療（「電療」）是利用高能量射線來局部性消滅癌細胞，希望在治療癌症的同時，能減低對正常細胞的傷害。","","","","","",""],
    ["","","","","","",""],
    ["標靶藥物對特定的癌細胞有針對性，能夠識別、鎖定並針對攻擊癌細胞，不會對正常細胞造成傷害。標靶藥物可能會與化療或其他治療一起使用，以提升療效。","激素/荷爾蒙治療是治療的其中一種，專門針對荷爾蒙受體陽性的乳癌。荷爾蒙治療的原理是透過減少雌激素，或抑制雌激素的作用，從而減緩或停止乳癌細胞的生長。有時不同的荷爾蒙治療可以混合使用; 有時也可以和化療先後使用。","化學治療（「化療」）即是利用抗癌藥物來破壞癌細胞，但同時亦會殺死代謝較快的正常細胞。治療分口服和靜脈注射兩種形式，每次療程只需幾天，隨後是數星期的休息。療程的次數需視乎癌症種類及妳對藥物的反應而定。","放射治療（「電療」）是利用高能量射線來局部性消滅癌細胞，希望在治療癌症的同時，能減低對正常細胞的傷害。","","",""],
    ["標靶藥物對特定的癌細胞有針對性，能夠識別、鎖定並針對攻擊癌細胞，不會對正常細胞造成傷害。標靶藥物可能會與化療或其他治療一起使用，以提升療效。","激素/荷爾蒙治療是治療的其中一種，專門針對荷爾蒙受體陽性的乳癌。荷爾蒙治療的原理是透過減少雌激素，或抑制雌激素的作用，從而減緩或停止乳癌細胞的生長。有時不同的荷爾蒙治療可以混合使用; 有時也可以和化療先後使用。","化學治療（「化療」）即是利用抗癌藥物來破壞癌細胞，但同時亦會殺死代謝較快的正常細胞。治療分口服和靜脈注射兩種形式，每次療程只需幾天，隨後是數星期的休息。療程的次數需視乎癌症種類及妳對藥物的反應而定。","放射治療（「電療」）是利用高能量射線來局部性消滅癌細胞，希望在治療癌症的同時，能減低對正常細胞的傷害。","","",""],
  ];

  _LearnMoreControllerState({ index: int }) {
   _index = index;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void agreeAction() async {

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: false,titleText: "",type: SliverAppBarType.LearnMorePage,widgetList: [Container(
                    margin: EdgeInsets.only(top: topPadding, right: 20),
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.close, color: Colors.white, size: 30,),
                    ),
                  )]),
                  SliverToBoxAdapter(
                    child: Container(
                      color: kLightHealthTheme.primaryColor,
                      alignment: Alignment(0, 0),
                      margin: EdgeInsets.only(top: 0),
                      child: Center(
                        child: SingleChildScrollView(
                          controller: _scrollController,
                          scrollDirection: Axis.vertical,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                child: Container(
                                  decoration: new BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(20.0),
                                        topRight: const Radius.circular(20.0)),
                                  ),
                                  padding: EdgeInsets.only(top: 42),
                                  child: Container(
                                    padding:EdgeInsets.symmetric(horizontal: 20.0),
                                    child: Column(
                                      crossAxisAlignment:CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "乳癌類型",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 27,
                                              color: kLightHealthTheme.primaryColor
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top:40.0, bottom: 10.0),
                                          child: Column(
                                            crossAxisAlignment:CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                _titles[_index][0],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF555555),
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Container(
                                              color: (_titles[_index][0] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                              height: 1,
                                              margin: EdgeInsets.only(bottom: 5),
                                              ),
                                              Text(
                                                _contents[_index][0],
                                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                              ),                                    
                                            ],
                                          ),
                                        ),

                                        Container(
                                          margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                          child: Column(
                                            crossAxisAlignment:CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                _titles[_index][1],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF555555),
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Container(
                                              color: (_titles[_index][1] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                              height: 1,
                                              margin: EdgeInsets.only(bottom: 5),
                                              ),
                                              Text(
                                                _contents[_index][1],
                                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                              ),                                    
                                            ],
                                          ),
                                        ),

                                        Container(
                                          margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                          child: Column(
                                            crossAxisAlignment:CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                _titles[_index][2],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF555555),
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Container(
                                              color: (_titles[_index][2] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                              height: 1,
                                              margin: EdgeInsets.only(bottom: 5),
                                              ),
                                              Text(
                                                _contents[_index][2],
                                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                              ),                                    
                                            ],
                                          ),
                                        ),
                                        
                                        Container(
                                          margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                          child: Column(
                                            crossAxisAlignment:CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                _titles[_index][3],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF555555),
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Container(
                                              color: (_titles[_index][3] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                              height: 1,
                                              margin: EdgeInsets.only(bottom: 5),
                                              ),
                                              Text(
                                                _contents[_index][3],
                                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                              ),

                                              Container(
                                                margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                                child: Column(
                                                  crossAxisAlignment:CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      _titles[_index][4],
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        color: Color(0xFF555555),
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Container(
                                                      color: (_titles[_index][4] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                                      height: 1,
                                                      margin: EdgeInsets.only(bottom: 5),
                                                    ),
                                                    Text(
                                                      _contents[_index][4],
                                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                                child: Column(
                                                  crossAxisAlignment:CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      _titles[_index][5],
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        color: Color(0xFF555555),
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Container(
                                                      color: (_titles[_index][5] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                                      height: 1,
                                                      margin: EdgeInsets.only(bottom: 5),
                                                    ),
                                                    Text(
                                                      _contents[_index][5],
                                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top:10.0, bottom: 10.0),
                                                child: Column(
                                                  crossAxisAlignment:CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      _titles[_index][6],
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        color: Color(0xFF555555),
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Container(
                                                      color: (_titles[_index][6] == "") ? Color(0x00FFFFFF) : Color(0xFFD0D0D0),
                                                      height: 1,
                                                      margin: EdgeInsets.only(bottom: 5),
                                                    ),
                                                    Text(
                                                      _contents[_index][6],
                                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}