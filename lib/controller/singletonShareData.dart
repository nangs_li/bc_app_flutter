import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icanfight/controller/baseViewController.dart';
import 'package:icanfight/controller/bottomNavigationBarController.dart';
import 'package:icanfight/controller/signUpQaController.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/model/UpdateVersionModel.dart';
import 'package:icanfight/model/category.dart';
import 'package:icanfight/model/disclaimer.dart';
import 'package:icanfight/model/smtp.dart';
import 'package:icanfight/model/tnc.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/tag.dart';
import 'package:icanfight/psp/app/app_state.dart';
import 'package:icanfight/psp/app/userState.dart';
import 'package:icanfight/psp/models/PspHomePageData.dart';
import 'package:icanfight/psp/uti/IO/app_io.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/view/customAlertDialog.dart';
import 'package:local_auth/local_auth.dart';
import 'package:package_info/package_info.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:redux/redux.dart';
import 'package:rxdart/rxdart.dart';
import 'package:device_info/device_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../api_provider/crudModel.dart';
import 'package:flutter/services.dart';
import 'dart:io' show Platform;
import '../utilities/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:icanfight/psp/app/data/dataState.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

enum ErrorType {
  Login,
  ForgetPassword
}

enum BiometricPage {
  SignInPage,
  SignUpPage
}

class SingletonShareData {
  static final SingletonShareData _instance = SingletonShareData._internal();

  factory SingletonShareData() {
    return _instance;
  }

  List<List<dynamic>> maintenanceExcelList = [];
  CRUDModel crudModel = CRUDModel();
  List<Post> postList;
  List<Post> fullPostList;
  List<Post> allPostList;
  List<Tag> tagList;
  List<Category> categoryList;
  List<int> alreadyFetchedArticle = [];
  Disclaimer disclaimer;
  TNC tnc;
  List<String> localStorageAccount;
  FirebaseUser user;
  TextEditingController q9A1TextFieldController;
  TextEditingController q10A5TextFieldController;
  TextEditingController q11A8TextFieldController;
  TextEditingController q11A12TextFieldController;

  final BehaviorSubject<List<Post>> postApiCallBack = BehaviorSubject<
      List<Post>>();
  final BehaviorSubject<bool> refreshBookMarkPageCallBack = BehaviorSubject<bool>();
  final BehaviorSubject<TNC> tncApiCallBack = BehaviorSubject<TNC>();
  final BehaviorSubject<List<List<bool>>> refreshProfilePageCallBack = BehaviorSubject<List<List<bool>>>();
  final BehaviorSubject<UpdateVersionModel> checkVersionUpdateCallBack = BehaviorSubject<UpdateVersionModel>();
  final BehaviorSubject<PspHomePageData> pspHomePageDataCallBack = BehaviorSubject<PspHomePageData>();
  ProgressDialog progressDialog;
  bool alreadyLogin;
  bool notAnswerCloseApp;
  bool registerCase = false;

  List<List<bool>> choiceValues;
  List<List<bool>> disableQuestionAnswer;
  List<List<bool>> backupChoiceValues;
  List<List<bool>> backupDisableQuestionAnswer;

  bool disableAllDrug = false;
  //psp
  bool alreadyLoadingPspPage = false;

  List<Category> allCategories = List();

  Store<AppState> store;

  Smtp smtp;

  bool isAlreadySubmitPSPForm = false;

  String nameEng;

  String incomeRecordType = "";

  PspHomePageData pspHomePageData;

  List<TextEditingController> registerTextControllers;

  bool postReady = false;

  bool isConnected = true;

  SingletonShareData._internal() {
    q9A1TextFieldController = TextEditingController();
    q10A5TextFieldController = TextEditingController();
    q11A8TextFieldController = TextEditingController();
    q11A12TextFieldController = TextEditingController();
  }

  Future<Disclaimer> getDisclaimer() async {
    disclaimer = await crudModel.getDisclaimerById('disclaimer');
    return disclaimer;
  }

  Future<TNC> getTNC() async {
    tnc = await crudModel.getTNCById('tnc');
    tncApiCallBack.sink.add(tnc);
    return tnc;
  }

  Future<List<Post>> getPost() async {
    postList = await crudModel.getPostResponseById('testPosts');
    fullPostList = List.from(postList);
    //for testing call back
    //    Future.delayed(const Duration(milliseconds: 5000), () {
    if (postList != null && tagList != null && categoryList != null) {
      postApiCallBack.sink.add(postList);
    }
//    for(Post post in postList) {
//      print("post.id:${post.id},post.title:${post.title.rendered}");
//    }
    print("subject.sink.add(postList)");
    //    });
    return postList;
  }

  Future<List<Tag>> getTag() async {
    tagList = await crudModel.getTagResponseById('testTags');
    if (postList != null && tagList != null && categoryList != null) {
      postApiCallBack.sink.add(postList);
    }
    return tagList;
  }

  Future<List<Category>> getCategory() async {
    categoryList = await crudModel.getCategoryResponseById('testCategories');
    if (postList != null && tagList != null && categoryList != null) {
      postApiCallBack.sink.add(postList);
    }
    return categoryList;
  }

  Future<Smtp> getSmpt() async {
    smtp = await crudModel.getSmtpLink('smtp');
    return smtp;
  }

  Future<PspHomePageData> getPspHomePageData() async {
    pspHomePageData = await crudModel.getPspHomePageData(id: 'supportPageData');
    pspHomePageDataCallBack.sink.add(pspHomePageData);
    return pspHomePageData;
  }

  Future<String> deviceId() async {
    String deviceId;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceId = androidInfo.fingerprint;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      deviceId = iosDeviceInfo.identifierForVendor;
    }
    return deviceId;
  }

  void showLoading(BuildContext context) {
    progressDialog = new ProgressDialog(context,isDismissible:false);
    progressDialog.style(
        message: '載入中...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    progressDialog.show();
  }

  void hideLoading(BuildContext context) {
    progressDialog.hide();
  }

  void initBiometric(BuildContext context, BiometricPage page) async {
    var localAuth = LocalAuthentication();
    bool canCheckBiometrics = await localAuth.canCheckBiometrics;
    SingletonShareData data = SingletonShareData();
    debugPrint(localAuth.toString());

    List<BiometricType> availableBiometrics =
    await localAuth.getAvailableBiometrics();

    try {
      bool didAuthenticate = await localAuth.authenticateWithBiometrics(
          localizedReason: 'Please authenticate to login');

      if (didAuthenticate) {
        localStorageAccount = await Utils.getAccount();
        showLoading(context);
        final AuthResult result = await _auth.signInWithEmailAndPassword(
          email: localStorageAccount[0],
          password: localStorageAccount[1],
        );
        hideLoading(context);
        print("didAuthenticate");
        if (result.user != null) {
          print("login successful");
          user = result.user;
          ga.gaLogLogin(user:result.user);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
            (page == BiometricPage.SignInPage)
                ? BaseViewController(BottomNavigationBarController())
                : BaseViewController(SignUpQaController())),
          );
        }
      }
    } on PlatformException catch (e) {
      print("PlatformExceptionError:$e");
    }

    if (availableBiometrics.contains(BiometricType.face)) {

    } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
      debugPrint("fingerprint");
    }
  }

  Future<FirebaseUser> getCurrentUser() async {
    user = await _auth.currentUser();
    return user;
  }

  Future<dynamic> reloadUser() async {
    user.reload();
    user = await _auth.currentUser();
    alreadyLogin = true;
  }


  void signOut() async {
    await _auth.signOut();
    SingletonShareData data = SingletonShareData();
    data.alreadyLogin = false;
    data.choiceValues = null;
    data.disableQuestionAnswer = null;
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
    cleanLocalFile('appData.json');
    data.store.state.dataState = Data_State.initial();
    data.store.state.userState = User_State.initial();
    q9A1TextFieldController.text = "";
    q10A5TextFieldController.text = "";
    q11A8TextFieldController.text = "";
    q11A12TextFieldController.text = "";
  }

  void initSignUpQaData({bool forceReset}) {
    if (choiceValues == null || forceReset) {
      choiceValues = [
        [false, false, false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false],
        [false, false, false, false, false, false, false],
        [
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false
        ]
      ];

      disableQuestionAnswer = [
        [false, false, false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false, false],
        [false, false],
        [false, false, false, false, false, false, false],
        [
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false,
          false
        ]
      ];
    }
  }

  void saveChoiceValuesIntoFireBase({BuildContext context,bool q11}) async {
    print("user:$user");
    try {
      await Firestore.instance.collection('BCAppDatabase')
          .document("Users")
          .setData({
        user.uid: {
          'choiceValues': jsonEncode(choiceValues),
          'disableQuestionAnswer': jsonEncode(disableQuestionAnswer),
          'q9A1': q9A1TextFieldController.text,
          'q10A5': q10A5TextFieldController.text,
          'q11A8': q11A8TextFieldController.text,
          'Q11A12': q11A12TextFieldController.text,

        }
      }, merge: true);
      if(allCategories.length > 4){
        allCategories = filterCategoryPostFromQuestionAnswer(allCategories);
      }
      for (var questionIndex = 0; questionIndex < choiceValues.length; questionIndex++) {
       List<bool> boolChoiceValues =  choiceValues[questionIndex];
       for (var answerIndex = 0; answerIndex < boolChoiceValues.length; answerIndex++) {
         bool click = boolChoiceValues[answerIndex];
         if(!q11){
           if(click){
         if (questionIndex == 0) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q1_stage1");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q1_stage2");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q1_stage3");
             }

             if (answerIndex == 3) {
               ga.gaEvent(name: "signup_Q1_stage4");
             }
             if (answerIndex == 4) {
               ga.gaEvent(name: "signup_Q1_idontknow");
             }
         }
           if (questionIndex == 1) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q2_isHER2");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q2_notHER2");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q2_idontknow");
             }
           }

           if (questionIndex == 2) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q3_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q3_no");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q3_idontknow");
             }
           }

           if (questionIndex == 3) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q4_had_surgery");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q4_planned_surgery_have_treatment");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q4_planned_surgery_no_treatment");
             }

             if (answerIndex == 3) {
               ga.gaEvent(name: "signup_Q4_no_surgery");
             }
           }

           if (questionIndex == 4) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q5_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q5_no");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q5_idontknow");
             }
           }

           if (questionIndex == 5) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q6_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q6_no");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q6_idontknow");
             }
           }

           if (questionIndex == 6) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q7_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q7_no");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q7_idontknow");
             }
           }

           if (questionIndex == 7) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q8_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q8_no");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q8_idontknow");
             }
           }

           if (questionIndex == 8) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q9_yes");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q9_no");
             }
           }

           if (questionIndex == 9) {
             if (answerIndex == 0) {
               ga.gaEvent(name: "signup_Q10_targeted_therapy");
             }

             if (answerIndex == 1) {
               ga.gaEvent(name: "signup_Q10_hormone_therapy");
             }

             if (answerIndex == 2) {
               ga.gaEvent(name: "signup_Q10_chemotherapy");
             }
             if (answerIndex == 3) {
               ga.gaEvent(name: "signup_Q10_radiotherapy");
             }

             if (answerIndex == 4) {
               ga.gaEvent(name: "signup_Q10_others");
             }

             if (answerIndex == 5) {
               ga.gaEvent(name: "signup_Q10_no_planned_therapy");
             }

             if (answerIndex == 6) {
               ga.gaEvent(name: "signup_Q10_planned_therapy_unknown");
             }
           }

          
         }

       } else {

            if(questionIndex == 10) {
              if(click) {
                if (answerIndex == 0) {
                  ga.gaEvent(name: "profile_Q11_herceptin");
                }

                if (answerIndex == 1) {
                  ga.gaEvent(name: "profile_Q11_perjeta");
                }

                if (answerIndex == 2) {
                  ga.gaEvent(name: "profile_Q11_kadcyla");
                }
                if (answerIndex == 3) {
                  ga.gaEvent(name: "profile_Q11_tecentriq");
                }

                if (answerIndex == 4) {
                  ga.gaEvent(name: "profile_Q11_avastin");
                }

                if (answerIndex == 5) {
                  ga.gaEvent(name: "profile_Q11_lapatinib");
                }

                if (answerIndex == 6) {
                  ga.gaEvent(name: "profile_Q11_neratinib");
                }

                if (answerIndex == 7) {
                  ga.gaEvent(name: "profile_Q11_other_targeted_therapy");
                }

                if (answerIndex == 8) {
                  ga.gaEvent(
                      name: "profile_Q11_other_targeted_therapy_freetext");
                }

                if (answerIndex == 9) {
                  ga.gaEvent(name: "profile_Q11_idontknow");
                }
                if (answerIndex == 10) {
                  ga.gaEvent(name: "profile_Q11_hormone_therapy");
                }

                if (answerIndex == 11) {
                  ga.gaEvent(name: "profile_Q11_radiotherapy");
                }

                if (answerIndex == 12) {
                  ga.gaEvent(name: "profile_Q11_others_therapy");
                }

                if (answerIndex == 13) {
                  ga.gaEvent(name: "profile_Q11_others_therapy_freetext");
                }

                if (answerIndex == 14) {
                  ga.gaEvent(name: "profile_Q11_no_planned_therapy");
                }
              }
            }
            }
       }

      }

    } on PlatformException catch (e) {
      print("PlatformExceptionError:$e");
      hideLoading(context);
      showDialog(
          barrierDismissible: true,
          context: context,
          builder: (buildContext) {
            return CustomAlertDialog(title: Lang.getString("login_error"),
                content: e.message,
                leftButtonText: null,
                rightButtonText: Lang.getString(
                    "login_close"),
                buildContext: buildContext,
                rightButtonFunction: () {
                  Navigator.of(context).pop();
                });
          }
      );
    }
  }

  Future<bool> getChoiceValuesFromFireBase(FirebaseUser currentUser) async {
    initSignUpQaData(forceReset: false);
    bool getChoiceValuesSuccess = true;
    if(currentUser == null){

      getPost();
    } else {
      DocumentSnapshot result = await Firestore.instance.collection(
          'BCAppDatabase')
          .document("Users").get();
      dynamic userData = result.data[currentUser.uid];
      String choiceValuesString = userData['choiceValues'];
      if(choiceValuesString == null){
        return false;
      }
      String disableQuestionAnswerString = userData['disableQuestionAnswer'];
      q9A1TextFieldController.text = userData['q9A1'];
      q10A5TextFieldController.text = userData['q10A5'];
      q11A8TextFieldController.text = userData['q11A8'];
      q11A12TextFieldController.text = userData['Q11A12'];
      List<dynamic> choiceValuesList = jsonDecode(choiceValuesString);
      List<dynamic> disableQuestionAnswerList = jsonDecode(
          disableQuestionAnswerString);
      for (var i = 0; i < choiceValuesList.length; i++) {
        for (var j = 0; j < choiceValuesList[i].length; j++) {
          bool boolValue = choiceValuesList[i][j];
          // print("choiceValuesList${i}_$j$boolValue");
          if (boolValue != null) {
            choiceValues[i][j] = boolValue;
          }
          bool disableBoolValue = disableQuestionAnswerList[i][j];
          if (disableBoolValue != null) {
            disableQuestionAnswer[i][j] = disableBoolValue;
          }
          // print("disableBoolValue:${i}_$j$disableBoolValue");
        }
      }

      getPost();
      isAlreadySubmitForm();
      refreshProfilePageCallBack.sink.add(choiceValues);
    }
    return getChoiceValuesSuccess;
  }

  Future<bool> clearUserPasswordData({FirebaseUser user}) async {
    await Firestore.instance.collection('BCAppDatabase')
        .document("Users")
        .setData({
      user.uid: {
        'password': "",
        'confirmPassword': "",
      }
    }, merge: true);

    return true;
  }

  bool checkIfHaveSelectQ11Drug() {
    return choiceValues[10][0] || choiceValues[10][1] || choiceValues[10][2] ||
        choiceValues[10][3] || choiceValues[10][4];
  }

  bool isNotShowQ11() {
    return choiceValues[3][2] ||
        (choiceValues[4][1] && (choiceValues[5][1] || choiceValues[5][2]) &&
            (choiceValues[6][1] || choiceValues[6][2]));
  }

  void resetQ11Data() {
    int question11Index = 10;
    choiceValues[question11Index] =
        List.filled(choiceValues[question11Index].length, false).toList();
    disableQuestionAnswer[question11Index] =
        List.filled(choiceValues[question11Index].length, false).toList();
  }

  String isNotEmptyStringAddNewLine(String string, String addString) {
    if (string != "") {
      string += "\n";
    }
    string += addString;
    return string;
  }

  List<Category> filterCategoryPostFromQuestionAnswer(
      List<Category> allCategory) {
    if(choiceValues == null){
      initSignUpQaData();
    }
    var q1 = choiceValues[0];
    var q2 = choiceValues[1];
    var q3 = choiceValues[2];
    var q11 = choiceValues[10];
    allPostList = List<Post>();

    if(allCategory.length > 2) {
      Category category3 = allCategory[2];
      category3.posts = List<Post>();
      if (q1[0]) {
        category3 = addPostsIntoCategory(id: 49, category: category3);
      } else if (q1[1]) {
        category3 = addPostsIntoCategory(id: 52, category: category3);
      } else if (q1[2]) {
        category3 = addPostsIntoCategory(id: 113, category: category3);
      } else if (q1[3]) {
        category3 = addPostsIntoCategory(id: 117, category: category3);
      } else if (q1[4]) {
        category3 = addPostsIntoCategory(id: 49, category: category3);
        category3 = addPostsIntoCategory(id: 52, category: category3);
        category3 = addPostsIntoCategory(id: 113, category: category3);
        category3 = addPostsIntoCategory(id: 117, category: category3);
      }
      if (q2[0]) {
        category3 = addPostsIntoCategory(id: 2027, category: category3);
      } else if (q2[1]) {
        category3 = addPostsIntoCategory(id: 2029, category: category3);
      } else if (q2[2]) {
        category3 = addPostsIntoCategory(id: 2031, category: category3);
      }

      if (q3[0]) {
        category3 = addPostsIntoCategory(id: 2033, category: category3);
      } else if (q3[1]) {
        category3 = addPostsIntoCategory(id: 2035, category: category3);
      } else if (q3[2]) {
        category3 = addPostsIntoCategory(id: 2038, category: category3);
      }
      if (q2[1] && q3[1]) {
        category3 = addPostsIntoCategory(id: 2040, category: category3);
      }

      if(!alreadyLogin){
        category3.posts = List<Post>();
        category3 = addPostsIntoCategory(id: 117, category: category3);
        category3 = addPostsIntoCategory(id: 2031, category: category3);
        category3 = addPostsIntoCategory(id: 2038, category: category3);
      }
    }

    if(allCategory.length > 3) {
      Category category4 = allCategory[3];
      category4.posts = List<Post>();
      category4 = addPostsIntoCategory(id: 1232, category: category4);
      category4 = addPostsIntoCategory(id: 1224, category: category4);
      category4 = addPostsIntoCategory(id: 1100, category: category4);
      category4 = addPostsIntoCategory(id: 1094, category: category4);
      category4 = addPostsIntoCategory(id: 1087, category: category4);
      category4 = addPostsIntoCategory(id: 1097, category: category4);
      if (q11[0]) {
        if ((q1[0] || q1[1] || q1[2])) {
          category4 = addPostsIntoCategory(id: 2087, category: category4);
        } else if (q1[3]) {
          category4 = addPostsIntoCategory(id: 2050, category: category4);
        } else if (q1[4]) {
          category4 = addPostsIntoCategory(id: 2052, category: category4);
        }
      }
      if (q11[1]) {
        if ((q1[0] || q1[1] || q1[2])) {
          category4 = addPostsIntoCategory(id: 2090, category: category4);
        } else if (q1[3]) {
          category4 = addPostsIntoCategory(id: 2057, category: category4);
        } else if (q1[4]) {
          category4 = addPostsIntoCategory(id: 2059, category: category4);
        }
      }
      if (q11[2]) {
        if ((q1[0] || q1[1] || q1[2])) {
          category4 = addPostsIntoCategory(id: 2084, category: category4);
        } else if (q1[3]) {
          category4 = addPostsIntoCategory(id: 2062, category: category4);
        } else if (q1[4]) {
          category4 = addPostsIntoCategory(id: 2054, category: category4);
        }
      }
      if (q11[3]) {
        category4 = addPostsIntoCategory(id: 2065, category: category4);
      }
    }
    if(allCategory.length > 4) {
      Category category5 = allCategory[4];
      category5.posts = List<Post>();
      category5 = addPostsIntoCategory(id: 2067, category: category5);
      category5 = addPostsIntoCategory(id: 2069, category: category5);
      category5 = addPostsIntoCategory(id: 289, category: category5);
      category5 = addPostsIntoCategory(id: 56, category: category5);
      category5 = addPostsIntoCategory(id: 292, category: category5);
      category5 = addPostsIntoCategory(id: 2071, category: category5);
      category5 = addPostsIntoCategory(id: 2073, category: category5);
    }

    if(allCategory.length > 5) {
      Category category6 = allCategory[5];
      category6.posts = List<Post>();
      category6 = addPostsIntoCategory(id: 2075, category: category6);
      category6 = addPostsIntoCategory(id: 2077, category: category6);
      category6 = addPostsIntoCategory(id: 2079, category: category6);
      category6 = addPostsIntoCategory(id: 1108, category: category6);
      allPostList = addPostsIntoAllPostList(id: 49);
      allPostList = addPostsIntoAllPostList(id: 52);
      allPostList = addPostsIntoAllPostList(id: 113);
      allPostList = addPostsIntoAllPostList(id: 117);
      allPostList = addPostsIntoAllPostList(id: 2031);
      allPostList = addPostsIntoAllPostList(id: 2038);
      allPostList = addPostsIntoAllPostList(id: 2040);
      allPostList = addPostsIntoAllPostList(id: 1232);
      allPostList = addPostsIntoAllPostList(id: 1224);
      allPostList = addPostsIntoAllPostList(id: 1100);
      allPostList = addPostsIntoAllPostList(id: 1094);
      allPostList = addPostsIntoAllPostList(id: 1087);
      allPostList = addPostsIntoAllPostList(id: 1097);
      if (q11[0]) {
        if ((q1[0] || q1[1] || q1[2])) {
          allPostList = addPostsIntoAllPostList(id: 2087);
        } else if (q1[3]) {
          allPostList = addPostsIntoAllPostList(id: 2050);
        } else if (q1[4]) {
          allPostList = addPostsIntoAllPostList(id: 2052);
        }
      }
      if (q11[1]) {
        if ((q1[0] || q1[1] || q1[2])) {
          allPostList = addPostsIntoAllPostList(id: 2090);
        } else if (q1[3]) {
          allPostList = addPostsIntoAllPostList(id: 2057);
        } else if (q1[4]) {
          allPostList = addPostsIntoAllPostList(id: 2059);
        }
      }
      if (q11[2]) {
        if ((q1[0] || q1[1] || q1[2])) {
          allPostList = addPostsIntoAllPostList(id: 2084);
        } else if (q1[3]) {
          allPostList = addPostsIntoAllPostList(id: 2062);
        } else if (q1[4]) {
          allPostList = addPostsIntoAllPostList(id: 2054);
        }
      }
      if (q11[3]) {
        allPostList = addPostsIntoAllPostList(id: 2065);
      }
      allPostList = addPostsIntoAllPostList(id: 2067);
      allPostList = addPostsIntoAllPostList(id: 2069);
      allPostList = addPostsIntoAllPostList(id: 289);
      allPostList = addPostsIntoAllPostList(id: 56);
      allPostList = addPostsIntoAllPostList(id: 292);
      allPostList = addPostsIntoAllPostList(id: 2071);
      allPostList = addPostsIntoAllPostList(id: 2073);
      allPostList = addPostsIntoAllPostList(id: 2075);
      allPostList = addPostsIntoAllPostList(id: 2077);
      allPostList = addPostsIntoAllPostList(id: 2079);
      allPostList = addPostsIntoAllPostList(id: 1108);
    }
    return allCategory;
  }

  List<Post> addPostsIntoAllPostList({int id}) {
    for (Post post in fullPostList) {
      if (id == post.id) {
       // print("post.title.rendered:${post.title.rendered}");
        post.title.rendered = removeUseLessString(post.title.rendered);
       // print("after ->post.title.rendered:${post.title.rendered}");
        allPostList.add(post);
      }
    }
    return allPostList;
  }

  Category addPostsIntoCategory({int id, Category category}) {
    for (Post post in fullPostList) {
      if (id == post.id) {
        //print("post.title.rendered:${post.title.rendered}");
        post.title.rendered = removeUseLessString(post.title.rendered);
        //print("after ->post.title.rendered:${post.title.rendered}");
        category.posts.add(post);
      }
    }
    return category;
  }

  String removeUseLessString(String title) {
    title = title.replaceAll('(eBC)', '');
    title = title.replaceAll('(mBC)', '');
    title = title.replaceAll('(Non-stage specific)', '');
    title = title.replaceAll('(HR+ve)', '');
    title = title.replaceAll('(HER2+ve)', '');
    title = title.replaceAll('(HER2-ve)', '');
    title = title.replaceAll('(HR+ve)', '');
    title = title.replaceAll('(HR-ve)', '');
    title = title.replaceAll("(I Don't Know)", '');
    title = title.replaceAll("(I Dont Know)", '');
    title = title.replaceAll("(I Don&#8217;t Know)", '');
    return title;
  }

  Widget imageIcon({String imagePath,Function function,double width,double height,BoxFit boxFit}) {

    width = (width == null) ? SizeConfig().blockSizeHorizontal * 5 : width;
    height = (height == null) ? SizeConfig().blockSizeHorizontal * 5 : height;
    boxFit = (boxFit == null) ? BoxFit.cover : boxFit;

    Widget image;
    if(imagePath.contains(".com") || imagePath.contains("amazonaws") || imagePath.contains("http")){
      image = CachedNetworkImage(imageUrl: imagePath,
          imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              )),
          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
          errorWidget: (context, url, error) => Icon(Icons.error));
    } else if(imagePath.contains(".png")){
      image = Image.asset(
        imagePath,
        fit: boxFit,
      );
    } else {
      image = SvgPicture.asset(
        imagePath,
        fit: boxFit,
      );
    }

    return SizedBox(
        width: width,
        height: height,
        child: function != null ? IconButton(
            icon: image,
            onPressed: function
        ) : image
    );
  }

  Future<String> setVersion() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    print("appName: $appName");
    print("packageName $packageName");
    print("version $version");
    print("buildNumber $buildNumber");
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("version", version);
    return version;

  }

  Future<String> getVersion() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    print("appName: $appName");
    print("packageName $packageName");
    print("version $version");
    print("buildNumber $buildNumber");
    return version;

  }

  String getErrorMessage({PlatformException error,ErrorType type}){
   String errorMessage;
    if(error.code == "ERROR_USER_NOT_FOUND"){
      if(type == ErrorType.ForgetPassword){

        errorMessage = "沒有此登記用戶。";
      } else if(type == ErrorType.Login){

        errorMessage = "沒有此登記用戶。";
      } else {
        errorMessage = error.message;
      }

    } else if (error.code == "ERROR_EMAIL_ALREADY_IN_USE") {
     errorMessage = Lang.getString("error_email_already_in_use");
   } else if(error.code == "ERROR_INVALID_EMAIL"){
     errorMessage = "請輸入正確電郵";

   } else if(error.code == "ERROR_WRONG_PASSWORD"){
     errorMessage = "請輸入正確密碼";

   } else {
      errorMessage = error.message;
    }
    return errorMessage;

  }

  void alreadySubmitForm() async {
    print("user:$user");

      await Firestore.instance.collection('BCAppDatabase')
          .document("Users")
          .setData({
        user.uid: {
         'alreadySubmitForm':true
        }
      }, merge: true);
    }

  Future<bool> isAlreadySubmitForm() async {
    print("user:$user");

    DocumentSnapshot result = await Firestore.instance.collection(
        'BCAppDatabase')
        .document("Users").get();
    dynamic userData = result.data[user.uid];
    bool isSubmitForm = userData['alreadySubmitForm'] ?? false;

    isAlreadySubmitPSPForm = isSubmitForm;
    return isAlreadySubmitPSPForm;
  }

  Future<UpdateVersionModel> checkVersionUpdate() async {
    SingletonShareData data = SingletonShareData();
    String version = await data.getVersion();
    UpdateVersionModel updateVersionModel = await crudModel.getVersionById(id:'version');
    if (updateVersionModel.status == 200) {
      if(updateVersionModel.data.experienceAppUpdateType == "hard"){
        int update = updateVersionModel.data.experienceAppVersion.compareTo(version);
        if(update > 0){
          checkVersionUpdateCallBack.add(updateVersionModel);
        }
      }
    }
    return updateVersionModel;
  }

  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  showVersionDialog({BuildContext context,String updateAppLink}) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "New Update Available";
        String message =
            "There is a newer version of app available please update it now.";
        String btnLabel = "Update Now";
//        String btnLabelCancel = "Later";
        return Platform.isIOS
            ? new CupertinoAlertDialog(
          title: Text(title, ),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(btnLabel),
              onPressed: () => launchURL(updateAppLink),
            ),
//            FlatButton(
//              child: Text(btnLabelCancel),
//              onPressed: () => Navigator.pop(context),
//            ),
          ],
        )
            : new AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text(btnLabel),
              onPressed: () => launchURL(updateAppLink),
            ),
//            FlatButton(
//              child: Text(btnLabelCancel),
//              onPressed: () => Navigator.pop(context),
//            ),
          ],
        );
      },
    );
  }
}

SingletonShareData data = SingletonShareData();