import 'package:flutter/material.dart';
import 'package:icanfight/controller/disclaimerController.dart';
import 'package:icanfight/api_provider/articleApiProvider.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customQuestionAnswer.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'baseViewController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';
import 'bottomNavigationBarController.dart';
import 'learnMoreController.dart';

class TreatmentController extends StatefulWidget {
  @override
  _TreatmentControllerState createState() => _TreatmentControllerState();
}

class _TreatmentControllerState extends State<TreatmentController> {

  List<FocusNode> _formNodes = [
    FocusNode(),FocusNode()
  ];

  SingletonShareData data = SingletonShareData();
  String title = Lang.getString("profile_drug_treatment_q");
  List question = SingletonShareData().choiceValues[10];
  int q11 = 10;
  List<String> rawAnswersText = [
    Lang.getString("profile_drug_treatment_1"),
    Lang.getString("profile_drug_treatment_2"),
    Lang.getString("profile_drug_treatment_3"),
    Lang.getString("profile_drug_treatment_4"),
    Lang.getString("profile_drug_treatment_5"),
    Lang.getString("profile_drug_treatment_6"),
    Lang.getString("profile_drug_treatment_7"),
    Lang.getString("profile_drug_treatment_8"),
    Lang.getString("profile_drug_treatment_9"),
    Lang.getString("profile_drug_treatment_10"),
    Lang.getString("profile_drug_treatment_11"),
    Lang.getString("profile_drug_treatment_12"),
    Lang.getString("profile_drug_treatment_13"),
  ];
  List<String> answersText = [
    Lang.getString("profile_drug_treatment_1"),
    Lang.getString("profile_drug_treatment_2"),
    Lang.getString("profile_drug_treatment_3"),
    Lang.getString("profile_drug_treatment_4"),
    Lang.getString("profile_drug_treatment_5"),
    Lang.getString("profile_drug_treatment_6"),
    Lang.getString("profile_drug_treatment_7"),
    Lang.getString("profile_drug_treatment_8"),
    Lang.getString("profile_drug_treatment_9"),
    Lang.getString("profile_drug_treatment_10"),
    Lang.getString("profile_drug_treatment_11"),
    Lang.getString("profile_drug_treatment_12"),
    Lang.getString("profile_drug_treatment_13"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = SingletonShareData();
    data.initSignUpQaData(forceReset: false);
    filterLogic();
    ga.gaSetCurrentScreen(screenName: "profile_drug_profile_page");
  }

  filterLogic() async {

    if(data.choiceValues[2][1]||data.choiceValues[5][1]||data.choiceValues[5][2]){
      answersText[9]="";
    }

    if(data.choiceValues[0][3]){
      answersText[6]="";
    }

    if(data.choiceValues[1][1]){
      answersText[0]="";
      answersText[1]="";
    }

    if(data.choiceValues[4][1]){
      answersText[0]="";
      answersText[1]="";
      answersText[2]="";
      answersText[3]="";
      answersText[4]="";
      answersText[5]="";
      answersText[6]="";
      answersText[7]="";
      answersText[8]="";
    }

    if(data.choiceValues[6][1]||data.choiceValues[6][2]){
      answersText[10]="";
    }

    setState(() {
      answersText = answersText;
    });

  }

  bool isNotSelectQ11AllDrug (){
    return !question[0]&&!question[1]&&!question[2]&&!question[3]&&!question[4]&&!question[5]&&!question[6]&&!question[7];
  }

  onTickQuestion(String answer,int answerListIndex) {
    List<bool> disableAnswerRow;

    // if question questionIndex multiple choose 1. set answer bool reverse (default false) or single choose by 1.set all to false and 2.set choose answer to true
      if (answer == Lang.getString("profile_drug_treatment_13")){
        question[answerListIndex] = !question[answerListIndex]; // set false to true when select answer

        if(question[answerListIndex]){
          question = List.filled(question.length, false).toList(); // reset choiceValues to all false list
          question[answerListIndex] = true;
          disableAnswerRow = List.filled(question.length, true).toList();
          disableAnswerRow[answerListIndex] = false;
          data.disableAllDrug = true;
        } else {
          question = List.filled(question.length, false).toList(); // reset
          disableAnswerRow = List.filled(question.length, false).toList();
          data.disableAllDrug = false;
        }

      } else {
        question[answerListIndex] = !question[answerListIndex];
        disableAnswerRow = data.disableQuestionAnswer[q11];

        if (answer == Lang.getString("profile_drug_treatment_3") ||
            answer == Lang.getString("profile_drug_treatment_7")) {
          if (question[answerListIndex]) {
            disableAnswerRow[10] = true;
          } else {
            if (!question[2] &&
                !question[6]) {
              disableAnswerRow[10] = false;
            }
          }
        }

        if (answer == Lang.getString("profile_drug_treatment_11")) {

          if(question[answerListIndex]){
            disableAnswerRow[2] = true;
            disableAnswerRow[6] = true;
          } else if(!question[answerListIndex]&&isNotSelectQ11AllDrug()){
            disableAnswerRow[2] = false;
            disableAnswerRow[6] = false;
          }
        }

        if (answer == Lang.getString("profile_drug_treatment_4")) {
          disableAnswerRow[9] =
          question[answerListIndex];
        }


        if (answer == Lang.getString("profile_drug_treatment_10")) {
          if(question[answerListIndex]){
            disableAnswerRow[3] = true;
          } else if(!question[answerListIndex]&&isNotSelectQ11AllDrug()){
            disableAnswerRow[3] = false;
          }

        }

        if (answer == Lang.getString("profile_drug_treatment_1")||answer == Lang.getString("profile_drug_treatment_2")) {
          if(question[answerListIndex]) {
            disableAnswerRow[0] = false;
            disableAnswerRow[1] = false;
            disableAnswerRow[2] = true;
            disableAnswerRow[3] = true;
            disableAnswerRow[4] = true;
            disableAnswerRow[5] = true;
            disableAnswerRow[6] = true;
            disableAnswerRow[7] = true;
            disableAnswerRow[8] = true;
            if (answer == Lang.getString("profile_drug_treatment_2")) {
              question[0] = true;
            }
          } else {
            if (answer == Lang.getString("profile_drug_treatment_1")) {
              question[1] = false;
              disableAnswerRow[2] = false;
              disableAnswerRow[3] = false;
              disableAnswerRow[4] = false;
              disableAnswerRow[5] = false;
              disableAnswerRow[6] = false;
              disableAnswerRow[7] = false;
              disableAnswerRow[8] = false;

              if(!question[answerListIndex]){
                if (question[10]) {
                  disableAnswerRow[2] = true;
                  disableAnswerRow[6] = true;
                }
                if (question[9]) {
                  disableAnswerRow[3] = true;
                }
              }
            }

          }
        }

      if (answer == Lang.getString("profile_drug_treatment_3") ||
          answer == Lang.getString("profile_drug_treatment_4") ||
          answer == Lang.getString("profile_drug_treatment_5") ||
          answer == Lang.getString("profile_drug_treatment_6") ||
          answer == Lang.getString("profile_drug_treatment_7") ||
          answer == Lang.getString("profile_drug_treatment_8") ||
          answer == Lang.getString("profile_drug_treatment_9")) {

        disableAnswerRow[0] = question[answerListIndex];
        disableAnswerRow[1] = question[answerListIndex];
        disableAnswerRow[2] = question[answerListIndex];
        disableAnswerRow[3] = question[answerListIndex];
        disableAnswerRow[4] = question[answerListIndex];
        disableAnswerRow[5] = question[answerListIndex];
        disableAnswerRow[6] = question[answerListIndex];
        disableAnswerRow[7] = question[answerListIndex];
        disableAnswerRow[8] = question[answerListIndex];
        if(!question[answerListIndex]){
          if (question[10]) {
            disableAnswerRow[2] = true;
            disableAnswerRow[6] = true;
          }
          if (question[9]) {
            disableAnswerRow[3] = true;
          }
        }

        disableAnswerRow[answerListIndex] = false;
      }

      //diable and enable AnswerNo12
        if(question[answerListIndex]) {
          disableAnswerRow[12] = true;
          question[12] = false;
        } else {
          bool enableAnswerNo12 = true;
          for (var i = 0; i < question.length; i++) {
            bool boolValue = question[i];
            if(i!=12){
              if(boolValue){
                enableAnswerNo12 = false;
              }
            }
          }
         if(enableAnswerNo12){
           disableAnswerRow[12] = false;
         }
        }
      }
    setState(() {
      data.disableQuestionAnswer[q11] = disableAnswerRow;
      question = question;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child:
          SizedBox.expand(
            child: SafeArea(
              bottom: false,
              child: Material(
                color: Colors.white,
                child: CustomScrollView(
                  physics: new ClampingScrollPhysics(),
                  slivers: <Widget>[
                    CustomSliverAppBarView(isBack: true,type: SliverAppBarType.TreatmentPage,titleText: Lang.getString("profile_drug_treatment"),
                       ),
                    SliverSpacer(),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                          (context, index) => Container(
                                color: kLightHealthTheme.primaryColor,
                                child: SingleChildScrollView(
                                  primary: false,
                                  physics: NeverScrollableScrollPhysics(),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                          width:MediaQuery.of(context).size.width,
                                          decoration: new BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  new BorderRadius.only(
                                                      topLeft:
                                                          const Radius.circular(
                                                              20.0),
                                                      topRight:
                                                          const Radius.circular(
                                                              20.0))),
                                          padding: EdgeInsets.only(top: 42),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  child: Column(
                                                    children: <Widget>[

                                                          Container(
                                                            child: Text(title,
                                                              maxLines: 3,
                                                              style: TextStyle(fontSize: 19, fontWeight: FontWeight.w700, color: Color(0xff555555)),),
                                                              margin: EdgeInsets.only(left: defaultPadding, right: defaultPadding),
                                                          ),
                                                          InkWell(
                                                            onTap: () {
                                                              Navigator.push(
                                                                context,
                                                                MaterialPageRoute(builder: (context) => BaseViewController(LearnMoreController(index: 5,))));
                                                            },
                                                            child: Container(
                                                              alignment: Alignment.centerLeft,
                                                              padding: EdgeInsets.only(top: defaultPadding, bottom: 0, left: defaultPadding, right: defaultPadding),
                                                              child: Row(children: <Widget>[
                                                                Icon(Icons.info_outline, color: kLightHealthTheme.primaryColor),
                                                                Container(width: 5,),
                                                                Text(Lang.getString("signupqa_learn_more"),
                                                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: kLightHealthTheme.primaryColor),),
                                                              ],) 
                                                            ),
                                                          ),
                                                      (data.choiceValues[4][1]) ? Container() :Container(
                                                        color: Colors.white,
                                                        margin: EdgeInsets.only(top: 10),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: <Widget>[
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              children: <Widget>[
                                                                Expanded(
                                                                  child: Container(
                                                                    alignment: Alignment.centerLeft,
                                                                    margin: EdgeInsets.only(left: 40, right: 20),
                                                                    child: Text(
                                                                      Lang.getString("profile_drug_treatment_0"),
                                                                      style: new TextStyle(color: data.disableAllDrug ? disableColor : Colors.black,fontSize: 16, fontWeight: FontWeight.w400),
                                                                      overflow: TextOverflow.ellipsis,
                                                                      maxLines: 5,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  alignment: Alignment.centerRight,
                                                                  margin: EdgeInsets.only(right: 10, top: 0),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Column(
                                                        children: answersText.asMap().entries.map((MapEntry entry) {
                                                          String answer = entry.value;
                                                          if(answer == ""){
                                                            return Container();
                                                          } else {
                                                          return CustomQuestionAnswer(choiceValues: data.choiceValues,answer:answer,onTapFunction:(){
                                                            if(!data.disableQuestionAnswer[q11][entry.key]){
                                                              //_scrollController.animateTo( 200.0, duration: Duration(milliseconds: 300), curve: Curves.ease);
                                                              onTickQuestion(answer,entry.key);
                                                            }
                                                          },questionIndex: q11,answerNo:entry.key,onChangedFunction:(bool value){
                                                            if(!data.disableQuestionAnswer[q11][entry.key]){
                                                             // _scrollController.animateTo( 200.0, duration: Duration(milliseconds: 300), curve: Curves.ease);
                                                              onTickQuestion(answer,entry.key);
                                                            }
                                                          },type: data.disableQuestionAnswer[q11][entry.key] ? QuestionAnswerType.Disable :QuestionAnswerType.Enable);
                                                          }
                                                        }).toList(),
                                                      )
                                                      ,BottomButton(buttonText:Lang.getString("profile_save_change"),onPressed: () async {
                                                        if(data.choiceValues[4][2]&& (data.choiceValues[10][0] || data.choiceValues[10][1] || data.choiceValues[10][2] || data.choiceValues[10][3] || data.choiceValues[10][4] || data.choiceValues[10][5] || data.choiceValues[10][6] || data.choiceValues[10][7])){
                                                          data.choiceValues[4][0] = true;
                                                          data.choiceValues[4][2] = false;
                                                        }
                                                        ga.gaEvent(name: "profile_submit_button");
                                                        data.saveChoiceValuesIntoFireBase(context: context,q11: true);

                                                        Navigator.pop(context);

                                                        if (SingletonShareData().checkIfHaveSelectQ11Drug()) {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(builder: (context) =>
                                                                  BaseViewController(DisclaimerController(
                                                                      isAuth: true, isFirst: true))));
                                                        } else if (SingletonShareData().registerCase) {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => BaseViewController(BottomNavigationBarController(index: 2))));
                                                        }
                                                      }),
                                                    ],
                                                  ))
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                          childCount: 1),
                    )
                  ],
                ),
              ),
            ),
          ),
    );
  }
}