import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icanfight/controller/signUpQaController.dart';
import 'package:icanfight/controller/treatmentController.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customAlertDialog.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'baseViewController.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:icanfight/api_provider/articleApiProvider.dart';

import 'package:icanfight/controller/editProfileController.dart';
import 'package:icanfight/controller/changePwController.dart';
import 'package:icanfight/controller/setProfilePhotoController.dart';
import 'package:icanfight/controller/settingController.dart';
import 'package:icanfight/utilities/lang.dart';

class ProfileController extends StatefulWidget {
  @override
  _ProfileControllerState createState() => _ProfileControllerState();
}

class _ProfileControllerState extends State<ProfileController> {

  var profileImage;
  int _percentage = 0;
  bool displaySurgery = true;
  SingletonShareData data = SingletonShareData();
  FirebaseAuth _auth = FirebaseAuth.instance;
  List<String> records;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = SingletonShareData();
    data.initSignUpQaData(forceReset: false);
    loadProfileImage();
    data.refreshProfilePageCallBack.listen((List<List<bool>> choiceValue) {
      loadData();
    });
    ga.gaSetCurrentScreen(screenName: "profile_page");

  }

  loadData() async {
    records = ["","","","","","",""];
    for (var j = 0; j < 4; j++) {
      int question1Index = j;
      if (data.choiceValues[question1Index] != null) {
        for (var i = 0; i < data.choiceValues[question1Index].length; i++) {
          if (data.choiceValues[question1Index][i]) {
            records[question1Index] = Lang.getString(
                "signupqa_content_${question1Index + 1}_${i + 1}");
          }
        }
      } else {
        records[question1Index] = "";
      }
    }


    String records4 = records[4];
    if(!data.choiceValues[4][0]&&!data.choiceValues[5][0]&&!data.choiceValues[6][0]&&!data.choiceValues[7][0]&&!data.choiceValues[8][0]){
      records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_5_2"));
    } else {
      if(data.choiceValues[4][0]){
        records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_10_1"));
      }
      if(data.choiceValues[5][0]){
        records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_10_2"));
      }
      if(data.choiceValues[6][0]){
        records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_10_3"));
      }
      if(data.choiceValues[7][0]){
        records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_10_4"));
      }
      if(data.choiceValues[8][0]){
        records4 = data.isNotEmptyStringAddNewLine(records4, Lang.getString("signupqa_content_10_5"));
      }
    }
    records[4] = records4;

    String records5 = records[5];

      if(data.choiceValues[9][0]){
        records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_1"));
      }
      if(data.choiceValues[9][1]){
        records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_2"));
      }
      if(data.choiceValues[9][2]){
        records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_3"));
      }
      if(data.choiceValues[9][3]){
        records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_4"));
      }
      if(data.choiceValues[9][4]){
        records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_5"));
      }
    if(data.choiceValues[9][5]){
      records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_6"));
    }
    if(data.choiceValues[9][6]){
      records5 = data.isNotEmptyStringAddNewLine(records5, Lang.getString("signupqa_content_10_7"));
    }
     records[5] = records5;


    for (var i = 0; i < 13; i++) {
      if(data.choiceValues[10][i]){
        if(data.choiceValues[10][i] && i == 7){
          records[6] = data.isNotEmptyStringAddNewLine(records[6], "其他標靶藥物");
        } else if(data.choiceValues[10][i] && i == 8){
          records[6] = data.isNotEmptyStringAddNewLine(records[6], "標靶藥物但不清楚");
        } else {
          records[6] = data.isNotEmptyStringAddNewLine(records[6], Lang.getString("profile_drug_treatment_${i+1}"));
        }
      }
    }

    if (records[0] == Lang.getString("signupqa_content_1_4")) {
      setState(() {
        displaySurgery = false;
      });
    }

    String item0String = await ArticleApiProvider().getStringRecordItem(0);
    int item0 = int.parse(item0String);
    String item5 = await ArticleApiProvider().getStringRecordItem(5);

    if (item0 >= 0) {
      if (_percentage < 75) {

        setState(() {
          _percentage += 50;
        });

      }
    }

    if (item5 != null && item5.length > 0) {
      if (_percentage < 100) {
        setState(() {
          _percentage += 25;
        });
      }
    }

    setState(() {
      records = records;
    });


  }

  Future<File> loadProfileImage() async {
    debugPrint("Load profile image");
    File file;
    try {
      final directory = await getApplicationDocumentsDirectory();
      final path = directory.path;

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      String fileName = sharedPreferences.getString("userProfile");

      file = File('$path/'+fileName);

      if (file != null) {
        if (await file.length() > 0) {
          profileImage = file;
          if (_percentage < 100) {
            setState(() {
              _percentage += 25;
            });
          }
        }
      }

    } catch (e) {
      print("Couldn't read file");
    }
    return file;
  }

  Future navigateToSubPage(context, pageType) async {
    if (pageType == subPageType.setting) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(SettingController())));
    } else if (pageType == subPageType.editProfile) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(EditProfileController())));
    } else if (pageType == subPageType.setProfileImage) {
      final loadImageResult = await Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(SetProfilePhotoController())));
        final loadImageResult2 = await loadProfileImage();
        setState(() {
          profileImage = profileImage;
        });

    } else if (pageType == subPageType.changePw) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(ChangePWController())));
    } else if (pageType == subPageType.medicalRecord) {
      final result = await Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(SignUpQaController(isAuth: true, isStatic: false,))));
      if(data.backupChoiceValues != null){
      data.choiceValues = data.backupChoiceValues.map((element)=>element).toList();
      data.disableQuestionAnswer = data.backupDisableQuestionAnswer.map((element)=>element).toList();
      data.backupChoiceValues = null;
      data.backupDisableQuestionAnswer = null;
      }
      final loadDataResult = await loadData();
      setState(() {
      records = records;
      });
    } else if (pageType == subPageType.drugTreatment) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => BaseViewController(TreatmentController()))).then((value) {
        setState(() {
          loadData();
        });
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    SingletonShareData data = SingletonShareData();


    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: false,
                      type: SliverAppBarType.ProfilePage,
                      loadImageFunction: loadProfileImage,
                      titleText: Lang.getString("profile_title"),
                      widgetList: [
                        Padding(
                            padding: EdgeInsets.only(top: topPadding),
                            child: Container(
                                width: 35)),
                        Padding(
                            padding: EdgeInsets.only(top: topPadding),
                            child: Container(
                                width: 35)),
                        Container(
                            alignment: Alignment.centerRight,
                            child: GestureDetector(
                                onTap: () {
                                  ga.gaEvent(name: "setting_button");
                                  navigateToSubPage(context,
                                      subPageType.setting);
                                },
                                child: Container(
//                                          color: Colors.red,
                                  width: 30,
                                  height: 30,
                                  margin: EdgeInsets.only(
                                      top: 10, right: defaultPadding),
                                  decoration: new BoxDecoration(
                                    image: DecorationImage(
                                      image: new AssetImage(
                                          'assets/images/icon_settings.png'),
                                    ),
                                    //                    shape: BoxShape.circle,
                                  ),
                                )))
                      ],
                      ),
       SliverList(delegate: SliverChildBuilderDelegate(
                        (context, index) => Container(
                              color: kLightHealthTheme.primaryColor,
//                              height: 1000,
                              child:  (records == null) ? Center(child: Container()) : SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(20.0),
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: <Widget>[
                                                GestureDetector(
                                                  onTap: () {
                                                    ga.gaEvent(name: "profile_upload_image");
                                                    navigateToSubPage(context,
                                                        subPageType.setProfileImage);
                                                  },
                                                  child: Container(
                                                      margin: EdgeInsets.only(top: 10),
                                                      width: 80.0,
                                                      height: 80.0,
                                                      decoration: new BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          border: Border.all(
                                                            color: Colors.white,
                                                            width: 2.0,
                                                          ),
                                                          image: new DecorationImage(
                                                              fit: BoxFit.cover,
//                                                        image: new NetworkImage("https://i.imgur.com/BoN9kdC.png")
                                                              image: profileImage != null
                                                                  ? FileImage(profileImage)
                                                                  : AssetImage(
                                                                  'assets/images/avatar_v2.png')
                                                          )
                                                      )),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(top: 5),
                                                  child: Text(
                                                    SingletonShareData().user.displayName ?? "",
                                                    textScaleFactor: 1.0,
                                                    style: new TextStyle(
                                                        fontSize: 17,
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w700),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: new BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: new BorderRadius.only(
                                                topLeft:
                                                    const Radius.circular(20.0),
                                                topRight: const Radius.circular(
                                                    20.0))),
                                        padding: EdgeInsets.only(top: 42),
//                                        height: 1000,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                                              child: Stack(
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Container(
//                                                color: Colors.black,
                                                        // height: 37,
                                                        // padding:
                                                        // EdgeInsets.only(left: 20),
                                                        child: Text(Lang.getString("profile_complete"),
                                                            textAlign: TextAlign.left,
                                                            style: new TextStyle(
                                                                color:
                                                                Color(0xFFF36E98),
                                                                fontSize: 19,
                                                                fontWeight:
                                                                FontWeight.w700)),
                                                      ),
                                                      Spacer(),
                                                      Container(
                                                        // height: 30,
                                                        alignment: Alignment.centerRight,
                                                        // margin: EdgeInsets.only(left: 115),
                                                        child: Text(
                                                          _percentage.toString()+"%",
                                                          style: new TextStyle(
                                                              fontSize: 19,
                                                              fontWeight: FontWeight.w700,
                                                              color: Color(0xFF9291AC)),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(
                                                    height: 14,
                                                    alignment: Alignment.center,
                                                    margin: EdgeInsets.only(top: 45),
                                                    width: MediaQuery.of(context)
                                                        .size
                                                        .width,
                                                    decoration: BoxDecoration(
                                                      color: Color(0xFFE6E6E6),
                                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    ),
                                                  ),
                                                  Container(
                                                    height: 14,
                                                    margin: EdgeInsets.only(top: 45),
                                                    alignment: Alignment.center,
                                                    width: MediaQuery.of(context)
                                                        .size
                                                        .width * _percentage / 100,
                                                    decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                        stops: [0.1, 0.9],
                                                        colors: [Color(0xFFEFF9392), Color(0xFFFF5689)]
                                                      ),
                                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                                onTap: () {
//                                                  navigateToSubPage(
//                                                      context,
//                                                      subPageType
//                                                          .editProfile);
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                                                  color: Colors.white,
                                                  width: MediaQuery.of(context).size.width,
                                                  child:Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    // crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(
//                                                color: Colors.black,
                                                        // height: 37,
                                                        child: Text(Lang.getString("profile_about"),
                                                            textAlign: TextAlign.left,
                                                            style: new TextStyle(
                                                                color:
                                                                Color(0xFFF36E98),
                                                                fontSize: 19,
                                                                fontWeight:
                                                                FontWeight.bold),
                                                        )
                                                      ),
                                                      Container(
                                                        // alignment: Alignment.center,
                                                        // height: 37,
                                                        child: Container(
                                                            height: 20,
                                                            width: 20,
                                                            margin:
                                                            EdgeInsets.only(left: 20),
//                                                            decoration:BoxDecoration(image:DecorationImage(image: new AssetImage('assets/images/icon_edit.png'),),)
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ),
                                            Container(
                                                height: 1,
                                                margin: EdgeInsets.only(top: 5.0),
                                                width: MediaQuery.of(context).size.width,
                                                color: Color(0xFFE6E6E6)),
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(left: 15, right: 10),
                                                  padding: EdgeInsets.only(top:10),
                                                  child: Container(
                                                    width: 30,
                                                    child: Icon(Icons.person, color: kLightHealthTheme.primaryColor, size: 25)
                                                  ),
                                                ),
                                                Container(
                                                  // height: 30,
                                                  padding: EdgeInsets.only(top:10),
                                                  // width: (MediaQuery.of(context).size.width -54) * 0.4,
                                                  alignment:Alignment.centerLeft,
                                                  child: Text(
                                                    Lang.getString("profile_username"),
                                                    style: new TextStyle(fontSize: 16, fontWeight:FontWeight.w700,color:Color(0xFF9291AC)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              // height: 30,
                                              alignment: Alignment.centerLeft,
                                              margin: EdgeInsets.only(left: 55, bottom: 10),
                                              child: Text(
                                                SingletonShareData().user.displayName ?? "",
                                                style: new TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Color(0xFF555555)),
                                              ),
                                            ),
                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context).size.width,
                                                color: Color(0xFFE6E6E6)),

                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(left: 15, right: 10),
                                                  padding: EdgeInsets.only(top:10, left: 2),
                                                  child: Icon(Icons.lock, color: kLightHealthTheme.primaryColor, size: 25),
                                                ),
                                                Container(
                                                  // height: 30,
                                                  padding: EdgeInsets.only(top:10, left: 3),
                                                  // width: (MediaQuery.of(context).size.width -54) *0.4,
                                                  alignment:Alignment.centerLeft,
                                                  child: Text(
                                                    Lang.getString("profile_password"),
                                                    style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color:Color(0xFFA8A9BD)),
                                                  ),
                                                ),

                                              ],
                                            ),
                                            GestureDetector(
                                            onTap: () async {
                                              data.showLoading(context);
                                              try {
                                                await _auth.sendPasswordResetEmail(
                                                  email: data.user.email,
                                                );
                                                data.hideLoading(context);
                                                showDialog(
                                                    barrierDismissible: true,
                                                    context: context,
                                                    builder: (buildContext) {
                                                      return CustomAlertDialog(title: Lang.getString("login_message"), content: Lang.getString(
                                                          "login_forgetpw_content"), leftButtonText: null,rightButtonText:Lang.getString(
                                                          "login_close") ,buildContext: buildContext,
                                                          rightButtonFunction:  () {
                                                            Navigator.of(context).pop();
                                                          });
                                                    }
                                                );

                                              } on PlatformException catch (e) {
                                                data.hideLoading(context);
                                                print("PlatformExceptionError:$e");
                                                showDialog(
                                                    barrierDismissible: true,
                                                    context: context,
                                                    builder: (buildContext) {
                                                      return CustomAlertDialog(title: Lang.getString("login_error"), content: data.getErrorMessage(error:e,type:ErrorType.ForgetPassword), leftButtonText: null,rightButtonText:Lang.getString(
                                                          "login_close") ,buildContext: buildContext,
                                                          rightButtonFunction:  () {
                                                            Navigator.of(context).pop();
                                                          });

                                                    }
                                                );
                                              }
                                            },
                                            child: Container(
                                              // height: 30,
                                              alignment: Alignment.centerLeft,
                                              margin: EdgeInsets.only(left: 55, bottom: 10),
                                              child: Text(
                                                Lang.getString("profile_change_password"),
                                                style: new TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Color(0xFFFA719B)),
                                              ),
                                            ),),
                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context).size.width,
                                                color: Color(0xFFE6E6E6)),

                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(left: 15, right: 10),
                                                  padding: EdgeInsets.only(top:10, left: 5),
                                                  child: Container(
                                                    child: Icon(Icons.email, color:kLightHealthTheme.primaryColor, size:22)
                                                  ),
                                                ),
                                                Container(
                                                  // height: 30,
                                                  padding: EdgeInsets.only(top:10, left:3),
                                                  width: (MediaQuery.of(context).size.width -54) *0.3,
                                                  alignment:Alignment.centerLeft,
                                                  child: Text(
                                                    Lang.getString("profile_email"),
                                                    style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color:Color(0xFFA8A9BD)),
                                                  ),
                                                ),

                                              ],
                                            ),

                                            Container(
                                              // height: 30,
                                              alignment: Alignment.centerLeft,
                                              margin: EdgeInsets.only(left: 55, bottom: 10),
                                              child: Text(
                                                data.user.email,
                                                style: new TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Color(0xFF555555)),
                                              ),
                                            ),

                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context).size.width,
                                                color: Color(0xFFE6E6E6)),
                                            Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                                width: MediaQuery.of(context).size.width,
                                                padding:
                                                EdgeInsets.only(top: 53),
//                                        height: 100,
                                                child: Column(
                                                  children: <Widget>[
                                                    GestureDetector(
                                                        onTap: () {
                                                          ga.gaEvent(name: "profile_edit_button");
                                                          navigateToSubPage(context,subPageType.medicalRecord);
                                                        },
                                                        child: Container(
                                                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                                                          color: Colors.white,
                                                          width: MediaQuery.of(context).size.width,
                                                          child:Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            // crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Container(
//                                                color: Colors.black,
                                                                // height: 37,
                                                                // padding:
                                                                // EdgeInsets.only(left: 20),
                                                                child: Text(Lang.getString("profile_medical_record"),
                                                                    textAlign:TextAlign.left,
                                                                    style: new TextStyle(color:Color(0xFFF36E98),fontSize: 19,fontWeight:FontWeight.bold)),
                                                              ),
                                                              Container(
                                                                // padding: EdgeInsets.only(
                                                                //     right: 20),
                                                                // alignment:
                                                                // Alignment.topRight,
                                                                // height: 37,
                                                                child: Container(
                                                                    height: 20,
                                                                    width: 20,
                                                                    margin:
                                                                    EdgeInsets.only(left: 20),
                                                            decoration:BoxDecoration(image:DecorationImage(image: new AssetImage('assets/images/icon_edit.png'),),
                                                            )),
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        margin: EdgeInsets.only(top: 5.0),
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),

                                                    Row(
                                                      children: <Widget>[
                                                        Container(
                                                          padding: EdgeInsets.only(top:10, bottom: 10),
                                                          // width: (MediaQuery.of(context).size.width) *0.4 -20,
                                                          margin:EdgeInsets.only(left: 20),
                                                          alignment: Alignment.centerLeft,
                                                          child: Text(
                                                            Lang.getString("profile_stage"),
                                                            style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFFA8A9BD)),
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                    Container(
                                                      // height: 30,
                                                      alignment: Alignment.centerLeft,
                                                      margin: EdgeInsets.only(left: 20, bottom: 10),
                                                      child: Text(
                                                        records[0],
                                                        style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                      ),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),

                                                    Row(
                                                      children: <Widget>[
                                                        Container(
                                                          padding: EdgeInsets.only(top:10, bottom: 5),
                                                          // width: (MediaQuery.of(context).size.width) *0.4 -20,
                                                          margin:EdgeInsets.only(left: 20),
                                                          alignment: Alignment.centerLeft,
                                                          child: Text(
                                                            Lang.getString("profile_her2promin"),
                                                            style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFFA8A9BD)),
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                    Container(
                                                      // height: 30,
                                                      alignment: Alignment.centerLeft,
                                                      margin: EdgeInsets.only(left: 20, bottom: 10),
                                                      child: Text(
                                                        records[1],
                                                        style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                      ),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(context).size.width,
                                                        color:Color(0xFFE6E6E6)),

                                                    Row(
                                                      children: <Widget>[
                                                        Container(
                                                          padding: EdgeInsets.only(top:10, bottom: 10),
                                                          // width: (MediaQuery.of(context).size.width) *0.4 -20,
                                                          margin:EdgeInsets.only(left: 20),
                                                          alignment: Alignment.centerLeft,
                                                          child: Text(
                                                            Lang.getString("profile_herpromin"),
                                                            style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFFA8A9BD)),
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                    Container(
//                                                      height: 30,
                                                      alignment: Alignment.centerLeft,
                                                      margin: EdgeInsets.only(left: 20, bottom: 10),
                                                      child: Text(
                                                        records[2],
                                                        style: new TextStyle(fontSize: 16,fontWeight:FontWeight.bold,color: Color(0xFF555555)),
                                                      ),
                                                    ),
                                                    (!displaySurgery) ? Container() : Container(
                                                        height: 1,
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        color:
                                                            Color(0xFFE6E6E6)),
                                                    (!displaySurgery) ? Container() : Container(
//                                                      height: 55,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                              // height: 30,
                                                              margin: EdgeInsets.only(left: 20),
                                                              padding: EdgeInsets.only(top:10),
                                                              alignment: Alignment.centerLeft,
                                                              child: Text(
                                                                Lang.getString("profile_surgery"),
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: Color(
                                                                        0xFFA8A9BD)),
                                                              )),
                                                          Container(
//                                                              height: 55,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 20,
                                                                      right: 20,
                                                                      bottom:
                                                                          13),
                                                              padding: EdgeInsets.only(top:8),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                records[3],
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: Color(
                                                                        0xFF555555)),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(
                                                            context)
                                                            .size
                                                            .width,
                                                        color:
                                                        Color(0xFFE6E6E6)),
                                                    data.choiceValues[3][2] ? Container() : Container(
//                                                      height: 55,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                              // height: 30,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                  left: 20, right: 20),
                                                              padding: EdgeInsets.only(top:10),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                Lang.getString("profile_therapy"),
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                    16,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    color: Color(
                                                                        0xFFA8A9BD)),
                                                              )),
                                                          Container(
//                                                              height: 55,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                  left: 20,
                                                                  right: 20,
                                                                  bottom:
                                                                  15),
                                                              padding: EdgeInsets.only(top:8),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                records[4],
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                    16,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    color: Color(
                                                                        0xFF555555)),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                    data.choiceValues[3][2] ? Container() : Container(
                                                        height: 1,
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        color:
                                                            Color(0xFFE6E6E6)),




                                                    Container(
//                                                      height: 55,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            // height: 30,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                  left: 20, right: 20),
                                                              padding: EdgeInsets.only(top:10),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                Lang.getString("profile_future_therapy"),
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                    16,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    color: Color(
                                                                        0xFFA8A9BD)),
                                                              )),
                                                          Container(
//                                                              height: 55,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                  left: 20,
                                                                  right: 20,
                                                                  bottom:
                                                                  15),
                                                              padding: EdgeInsets.only(top:8),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                records[5],
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                    16,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    color: Color(
                                                                        0xFF555555)),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                        height: 1,
                                                        width: MediaQuery.of(
                                                            context)
                                                            .size
                                                            .width,
                                                        margin:
                                                        EdgeInsets.only(bottom: 53),
                                                        color:
                                                        Color(0xFFE6E6E6)),












                                                    data.choiceValues[3][2] || data.isNotShowQ11() ? Container() : GestureDetector(
                                                        onTap: () {
                                                          ga.gaEvent(name: "profile_drug_edit_button");
                                                          navigateToSubPage(
                                                              context,
                                                              subPageType
                                                                  .drugTreatment);
                                                        },
                                                        child: Container(
                                                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                                                          color: Colors.white,
                                                          width: MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                          child:
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            // crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Container(
                                                                    alignment: Alignment.topLeft,
//                                                color: Colors.black,
                                                                    // height: 37,
                                                                    // padding:
                                                                    // EdgeInsets.only(left: 20),
                                                                    child: Text(Lang.getString("profile_drug_treatment"),
                                                                        textAlign: TextAlign.left,
                                                                        style: new TextStyle(
                                                                            color:
                                                                            Color(0xFFF36E98),
                                                                            fontSize: 19,
                                                                            fontWeight:
                                                                            FontWeight.bold)),
                                                                  ),
                                                                  (records[6].length != 0) ? Container() : Container(child: Icon(Icons.error, color: kLightHealthTheme.primaryColor), margin: EdgeInsets.only(top: 2),),
                                                                ],
                                                              ),
                                                              Container(
                                                                // padding: EdgeInsets.only(
                                                                //     right: 20),
                                                                // alignment:
                                                                // Alignment.topRight,
                                                                // height: 37,
                                                                child: Container(
                                                                    height: 20,
                                                                    width: 20,
                                                                    margin:
                                                                    EdgeInsets.only(
                                                                        left: 20),
                                                                    decoration:BoxDecoration(image:DecorationImage(image: new AssetImage('assets/images/icon_edit.png'),),)),
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                    ),
                                                    data.choiceValues[3][2] || data.isNotShowQ11() ? Container() : Container(
                                                        height: 1,
                                                        margin: EdgeInsets.symmetric(vertical: 5.0),
                                                        width: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                        color:
                                                            Color(0xFFE6E6E6)),

                                                    data.choiceValues[3][2] || data.isNotShowQ11() ? Container() : Container(
                                                      // height: 30,
                                                        padding: EdgeInsets.only(top:10),
                                                        margin: EdgeInsets
                                                            .only(
                                                            left: 20, right: 20),
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                          Lang.getString("profile_prescription"),
                                                          style: new TextStyle(
                                                              fontSize:
                                                              16,
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              color: Color(
                                                                  0xFFA8A9BD)),
                                                        )),
                                                    data.choiceValues[3][2] || data.isNotShowQ11() ? Container() : Container(
//                                                              height: 55,
                                                        margin: EdgeInsets
                                                            .only(
                                                            left: 20, right: 20,
                                                            bottom:
                                                            15),
                                                        padding: EdgeInsets.only(top:8),
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                          records[6],
                                                          style: new TextStyle(
                                                              fontSize:
                                                              16,
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              color: Color(
                                                                  0xFF555555)),
                                                        )),
                                                  ],
                                                ))
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                        childCount: 1),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
