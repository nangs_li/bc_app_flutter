import 'package:flutter/material.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/model/UpdateVersionModel.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/view/bottomButton.dart';

import 'baseViewController.dart';
import 'tncController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';
// not have customSliverAppBarView
class WelcomeController extends StatefulWidget {
  @override
  _WelcomeControllerState createState() =>
      _WelcomeControllerState();
}

class _WelcomeControllerState
    extends State<WelcomeController> {

  int step = 0;

  List<String> titles = [
    Lang.getString("welcome_title_1"),
    Lang.getString("welcome_title_2"),
  ];

  List<String> contents = [
    Lang.getString("welcome_content_1"),
    Lang.getString("welcome_content_2"),
  ];

  List<String> buttons = [
    Lang.getString("welcome_button_1"),
    Lang.getString("welcome_button_2"),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SingletonShareData data = SingletonShareData();
    data.checkVersionUpdate();
    data.checkVersionUpdateCallBack.listen((UpdateVersionModel updateVersionModel) {
      data.showVersionDialog(context: context,updateAppLink: updateVersionModel.data.experienceAppUpdateUrl);
    });
    ga.gaSetCurrentScreen(screenName: "onboarding_welcome_page1");
  }

  void nextAction() async {
    if (step == 0) {
      ga.gaSetCurrentScreen(screenName: "onboarding_welcome_page2");
      ga.gaEvent(name: "onboarding_weclome_next_button");
      setState(() {
        step = 1;
      });
    } else {
      ga.gaEvent(name: "onboarding_weclome_start_button");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BaseViewController(TncController())));
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig(context: context);
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child: Image.asset("assets/images/WelcomeScreenBKG.png",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 42, right: 42, bottom: 100),
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(bottom: 40),
                    child: Text(titles[step], style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.none,
                      fontFamily: "Raleway"),
                    ),
                  ),
                  Container(
                    child: Text(contents[step],
                      style: TextStyle(
                        fontSize: 19,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        decoration: TextDecoration.none,
                        fontFamily: "Raleway"),
                      ),
                  ),
                ],
              ),
            ),
          ),
          BottomButton(buttonText: buttons[step], onPressed: () async {
            nextAction();
          }),

        ],
      ),
    );
  }
}