//import 'package:flutter/material.dart';
//import 'package:icanfight/controller/baseViewController.dart';
//import 'package:icanfight/utilities/style.dart';
//
//
//import 'package:icanfight/view/customInputField.dart';
//import 'package:icanfight/view/customSliverAppBarView.dart';
//import 'appointmentController.dart';
//import 'package:icanfight/utilities/themes.dart';
//import 'package:icanfight/model/event.dart';
//import 'package:icanfight/utilities/utils.dart';
//import 'package:icanfight/utilities/lang.dart';
//
//import 'package:table_calendar/table_calendar.dart';
//import 'package:intl/intl.dart';
//import 'package:simple_gesture_detector/simple_gesture_detector.dart';
//import 'package:flutter_svg/flutter_svg.dart';
//
//// not use again
//class CustomCalendarController extends StatefulWidget {
//  @override
//  _CalendarControllerState createState() => _CalendarControllerState();
//}
//
//class _CalendarControllerState extends State<CustomCalendarController> {
//  CalendarController _calendarController;
//  ScrollController _scrollController;
//
//  Map<DateTime, List<Event>> _events = Map();
//  List<Event> _selectedEvents = List();
//  DateTime _selectedDate = DateTime.now().subtract(Duration(minutes: DateTime.now().minute));
//  List<Map<String, String>> _typeList = [
//    {
//      "icon": "assets/images/CreateNewReminder_Appointment.svg",
//      "title": Lang.getString("calendar_appointment")
//    },
//    {
//      "icon": "assets/images/CreateNewReminder_Exercise.svg",
//      "title": Lang.getString("calendar_exercise")
//    },
//    {
//      "icon": "assets/images/CreateNewReminder_Medication.svg",
//      "title": Lang.getString("calendar_medication")
//    },
//    {"icon": "assets/images/CreateNewReminder_Event.svg", "title": Lang.getString("calendar_event")},
//    {"icon": "assets/images/CreateNewReminder_Others.svg", "title": Lang.getString("calendar_other")},
//  ];
//  bool reminderFlag = false;
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    _calendarController = CalendarController();
//    _scrollController = ScrollController();
//    _scrollController.addListener(() {
//      if (_scrollController.offset > 0) {
//        _calendarController.swipeCalendarFormat(isSwipeUp: true);
//      }
//    });
//
//    getEventMap();
//  }
//
//  getEventMap() async {
//    DateTime _sDateTime = await EventApiProvider().getCurrentEventDate();
//    debugPrint("_sDateTime:"+_sDateTime.toString());
//    setState(() {
//      _selectedDate = _sDateTime;
//    });
//
//    _calendarController.setSelectedDay(_selectedDate);
//    List<Event> events = await EventApiProvider().getEvent();
//    final _selectedDay = DateTime.parse(_selectedDate.year.toString() +
//            _selectedDate.month.toString().padLeft(2, "0") +
//            _selectedDate.day.toString().padLeft(2, "0"))
//        .toString();
//
//    events.forEach((event) {
//      DateTime dateTime = DateTime.parse(event.date);
//      DateTime endDateTime = DateTime.parse(event.endDate);
//
//      while (endDateTime.compareTo(dateTime) >= 0) {
//        List<Event> currentList = List();
//        if (_events != null) {
//          if (_events.containsKey(dateTime)) {
//            currentList = _events[dateTime];
//          }
//          currentList.add(event);
//          debugPrint(dateTime.toString());
//          _events[dateTime] = currentList;
//        }
//        dateTime = dateTime.add(Duration(days: 1));
//      }
//    });
//
//    DateTime dateTime = DateTime.parse(_selectedDay);
//    List<Event> localEvents = _events[dateTime] ?? [];
//    localEvents.sort((e1,e2) {
//      if ((e1.time.substring(0,2).compareTo(e2.time.substring(0,2)) == 0)) {
//        return int.parse(e1.time.substring(3,5)).compareTo(int.parse(e2.time.substring(3,5)));
//      } else {
//        return int.parse(e1.time.substring(0,2)).compareTo(int.parse(e2.time.substring(0,2)));
//      }
//
//    });
//
//    setState(() {
//      _selectedEvents = localEvents;
//    });
//  }
//
//  @override
//  void dispose() {
//    _calendarController.dispose();
//    super.dispose();
//  }
//
//  Widget _buildEventList() {
//    return Container(
//      margin: EdgeInsets.only(top: 20),
//      width: MediaQuery.of(context).size.width,
//      child: Container(
//          decoration: new BoxDecoration(
//              color: Colors.white,
//              borderRadius: new BorderRadius.only(
//                  topLeft: const Radius.circular(20.0),
//                  topRight: const Radius.circular(20.0)),
//              boxShadow: defaultBoxShadow),
//          child: Stack(
//            children: <Widget>[
//              SingleChildScrollView(
//                scrollDirection: Axis.vertical,
//                controller: _scrollController,
//                child: Stack(
//                  children: <Widget>[
//                    Container(
//                        margin: EdgeInsets.only(top: 40, left: 20, right: 20),
//                        child: Row(
//                          children: <Widget>[
//                            Text(
//                              (_selectedEvents.length == 0)
//                                  ? Lang.getString("calendar_no_reminder")
//                                  : "${_selectedEvents.length}"+Lang.getString("calendar_reminder_for"),
//                              style: TextStyle(
//                                  color: Color(0xFF6A6969),
//                                  fontSize: 18,
//                                  fontWeight: FontWeight.w700),
//                            ),
//                            Text(
//                              DateFormat("EEEEE MMMdd日",'zh_HK').format(_selectedDate),
//                              style: TextStyle(
//                                  color: Color(0xFF6A6969),
//                                  fontSize: 18,
//                                  fontWeight: FontWeight.w700),
//                            ),
//                          ],
//                        )),
//                    (_selectedEvents.length == 0)
//                        ? Container()
//                        : Container(
//                      padding: EdgeInsets.only(top: 75),
//                      child: Column(
//                        children: _selectedEvents
//                            .map((event) => Container(
//                          decoration: BoxDecoration(
//                            border: Border(
//                              bottom: BorderSide(
//                                  width: 0.8,
//                                  color: Color(0xFFD3D4DE)),
//                            ),
//                          ),
//                          margin: const EdgeInsets.symmetric(
//                              horizontal: 3.0, vertical: 4.0),
//                          child: ListTile(
//                            title: Container(
//                              child: Row(
//                                mainAxisAlignment:
//                                MainAxisAlignment.start,
//                                children: <Widget>[
//                                  Expanded(
//                                    flex: 2,
//                                    child: Container(
//                                      alignment: Alignment.centerLeft,
//                                      margin: EdgeInsets.only(
//                                          right: 20,
//                                          bottom: 10,
//                                          top: 10),
//                                      child: Text(
//                                        event.getTimePeriod(
//                                            _selectedDate),
//                                        style: TextStyle(
//                                            color: Color(0xFF6A6969),
//                                            fontWeight:
//                                            FontWeight.bold,
//                                            fontSize: 16),
//                                      ),
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 2,
//                                    child: Container(
//                                        width: 30,
//                                        margin: EdgeInsets.only(
//                                            right: 10),
//                                        child: Row(
//                                          children: <Widget>[
//                                            Container(
//                                              alignment: Alignment.center,
//                                              width: 30,
//                                              child: SvgPicture.asset(
//                                                _typeList[event.getType() -
//                                                    1]['icon'],
//                                                height: 27,
//                                              ),
//                                            ),
//                                            Container(width: 10,),
//                                            Column(
//                                              crossAxisAlignment: CrossAxisAlignment.start,
//                                              children: <Widget>[
//                                                Text(event.title,
//                                                    style: TextStyle(
//                                                        color: Color(Utils
//                                                            .mapColorFromType(
//                                                            event
//                                                                .type)),
//                                                        fontWeight:
//                                                        FontWeight
//                                                            .bold,
//                                                        fontSize:
//                                                        17)),
//                                                (event.doctor == "")
//                                                    ? Container()
//                                                    : Text(
//                                                  event.doctor,
//                                                  style: TextStyle(
//                                                      color: Color(
//                                                          0xFFCECEDA),
//                                                      fontWeight:
//                                                      FontWeight
//                                                          .bold,
//                                                      fontSize:
//                                                      11),
//                                                  maxLines: 1,
//                                                ),
//                                              ],
//                                            )
//                                          ],
//                                        )),
//                                  ),
//                                ],
//                              ),
//                            ),
//                            onTap: () {
//                              Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                    builder: (context) =>
//                                  BaseViewController(AppointmentController(
//                                          isNew: false,
//                                          title: event.title,
//                                          type: event.type,
//                                          event: event,
//                                        ))),
//                              );
//                            },
//                          ),
//                        ))
//                            .toList(),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//              Container(
//                height: 50,
//                decoration: BoxDecoration(
//                    gradient: LinearGradient(
//                        begin: Alignment.topCenter,
//                        end: Alignment.bottomCenter,
//                        colors: <Color>[
//                          Color(0xFFFFFFFF),
//                          Color(0x00FFFFFF)
//                        ]),
//                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
//              ),
//            ],
//          )),
//    );
//  }
//
//  Widget _buildEventsMarker(DateTime date, List events) {
//    return AnimatedContainer(
//      duration: const Duration(milliseconds: 300),
//      decoration: BoxDecoration(
//        shape: BoxShape.circle,
//        color: Colors.white,
//      ),
//      width: 4.0,
//      height: 4.0,
//      child: Center(
//        child: Text(
//          '',
//          style: TextStyle().copyWith(
//            color: Colors.white,
//            fontSize: 13,
//          ),
//        ),
//      ),
//    );
//  }
//
//  Widget _reminderWidget() {
//    return reminderFlag
//        ? Container(
//            alignment: Alignment.center,
//            decoration: BoxDecoration(
//              gradient: LinearGradient(
//                  begin: Alignment.topCenter,
//                  end: Alignment.bottomCenter,
//                  colors: <Color>[Color(0xFFFFB2D2), Color(0xFFFFFFFF)]),
//              boxShadow: [
//                BoxShadow(
//                  color: Colors.transparent,
//                )
//              ],
//            ),
//            child: Container(
//              alignment: Alignment.bottomRight,
//              margin: EdgeInsets.only(bottom: 100, right: 20),
//              child: Container(
//                height: 400,
//                width: 230,
//                alignment: Alignment.bottomRight,
//                child: Stack(
//                  children: <Widget>[
//                    Container(
//                      alignment: Alignment.topRight,
//                      margin: EdgeInsets.only(right: 2),
//                      child: Text(
//                        Lang.getString("calendar_create_reminder"),
//                        style: TextStyle(
//                            color: Color(0xFF404040),
//                            fontWeight: FontWeight.bold,
//                            fontSize: 19),
//                        textAlign: TextAlign.end,
//                      ),
//                    ),
//                    Container(
//                      margin: EdgeInsets.only(top: 30),
//                      child: ListView.builder(
//                        itemCount: 5,
//                        itemBuilder: (buildContext, index) => Container(
//                          margin: EdgeInsets.only(top: 5, bottom: 5, left: 20),
//                          child: Card(
//                            child: InkWell(
//                              onTap: () {
//                                if (index != 3) {
//                                  Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                        builder: (context) =>
//                                            AppointmentController(
//                                              isNew: true,
//                                              title: _typeList[index]['title'],
//                                              type: (index + 1).toString(),
//                                              selectedDate: _selectedDate,
//                                            )),
//                                  );
//                                }
//                              },
//                              child: Container(
//                                margin: EdgeInsets.all(10),
//                                child: Row(
//                                  children: <Widget>[
//                                    Container(
//                                      alignment: Alignment.center,
//                                      width: 30,
//                                      margin:
//                                          EdgeInsets.only(left: 15, right: 20),
//                                      child: (index == 3) ? SvgPicture.asset(
//                                        _typeList[index]['icon'],
//                                        height: 27,
//                                      ) : SvgPicture.asset(
//                                        _typeList[index]['icon'],
//                                        height: 27,
//                                      ),
//                                    ),
//                                    Text(
//                                      _typeList[index]['title'],
//                                      style: TextStyle(
//                                          color: (index == 3) ? Color(0xFFCDCDCD) : Color(0xFF6A6969),
//                                          fontSize: 16, fontWeight: FontWeight.w600),
//                                    )
//                                  ],
//                                ),
//                              ),
//                            ),
//                            elevation: 4,
//                            shape: RoundedRectangleBorder(
//                                side: BorderSide(color: Colors.white),
//                                borderRadius:
//                                    BorderRadius.all(Radius.circular(8))),
//                          ),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//            ),
//          )
//        : Container();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      color: kLightHealthTheme.primaryColor,
//      child: Stack(
//        children: <Widget>[
//          SafeArea(
//            child: Material(
//              color: Color(0xFFF9EBEC),
//              child: CustomScrollView(
//                slivers: <Widget>[
//                  CustomSliverAppBarView(isBack: false,titleText:Lang.getString("calendar_title"), type: SliverAppBarType.CalendarPage
//              ),
//
//                ],
//              ),
//            ),
//          ),
//          Container(
//            margin: EdgeInsets.only(top: 80),
//            child: Column(
//              children: <Widget>[
//                TableCalendar(
//                  rowHeight: 40.0,
//                  locale: 'zh_HK',
//                  initialSelectedDay: _selectedDate,
//                  initialCalendarFormat: CalendarFormat.month,
//                  simpleSwipeConfig: SimpleSwipeConfig(
//                      verticalThreshold: 1.0,
//                      swipeDetectionBehavior:
//                          SwipeDetectionBehavior.continuous),
//                  calendarStyle: CalendarStyle(
//                    outsideDaysVisible: true,
//                    weekdayStyle: TextStyle().copyWith(color: Colors.white),
//                    weekendStyle: TextStyle().copyWith(color: Colors.white),
//                    holidayStyle: TextStyle().copyWith(color: Colors.white),
//                  ),
//                  daysOfWeekStyle: DaysOfWeekStyle(
//                    dowTextBuilder: (date, locale) =>
//                        DateFormat.E(locale).format(date),
//                    weekdayStyle: TextStyle().copyWith(color: Colors.white),
//                    weekendStyle: TextStyle().copyWith(color: Colors.white),
//                  ),
//                  headerStyle: HeaderStyle(
//                    centerHeaderTitle: true,
//                    formatButtonVisible: false,
//                    formatButtonDecoration: BoxDecoration(
//                        color: Colors.transparent,
//                        border: Border.all(color: Colors.white70),
//                        borderRadius:
//                            new BorderRadius.all(Radius.circular(10))),
//                    formatButtonTextStyle:
//                        TextStyle().copyWith(color: Colors.white),
//                    titleTextStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w600).copyWith(color: Colors.white),
//                    leftChevronIcon:
//                        Icon(Icons.chevron_left, color: Colors.white),
//                    rightChevronIcon:
//                        Icon(Icons.chevron_right, color: Colors.white),
//                  ),
//                  calendarController: _calendarController,
//                  events: _events,
//                  availableCalendarFormats: const {
//                    CalendarFormat.month: '',
//                  },
//                  availableGestures: AvailableGestures.horizontalSwipe,
//                  onDaySelected: (datetime, dynamics) async {
//                    setState(() {
//                      _selectedDate = datetime;
//                    });
//
//                    final _selectedDay = DateTime.parse(
//                            _selectedDate.year.toString() +
//                                _selectedDate.month.toString().padLeft(2, "0") +
//                                _selectedDate.day.toString().padLeft(2, "0"))
//                        .toString();
//
//                    DateTime dateTime = DateTime.parse(_selectedDay);
//                    List<Event> localEvents = _events[dateTime] ?? [];
//
//                    localEvents.sort((e1,e2) {
//                      if ((e1.time.substring(0,2).compareTo(e2.time.substring(0,2)) == 0)) {
//                        return int.parse(e1.time.substring(3,5)).compareTo(int.parse(e2.time.substring(3,5)));
//                      } else {
//                        return int.parse(e1.time.substring(0,2)).compareTo(int.parse(e2.time.substring(0,2)));
//                      }
//
//                    });
//
//                    setState(() {
//                      _selectedEvents = localEvents;
//                    });
//                  },
//                  builders:
//                      CalendarBuilders(outsideDayBuilder: (context, date, _) {
//                    return Container(
//                      width: 100,
//                      height: 100,
//                      color: Colors.transparent,
//                      child: Center(
//                        child: Text(
//                          '${date.day}',
//                          style: TextStyle().copyWith(
//                              fontSize: 18.0, fontWeight: FontWeight.w400, color: Color(0xFFBB5577)),
//                        ),
//                      ),
//                    );
//                  }, outsideWeekendDayBuilder: (context, date, _) {
//                    return Container(
//                      width: 100,
//                      height: 100,
//                      color: Colors.transparent,
//                      child: Center(
//                        child: Text(
//                          '${date.day}',
//                          style: TextStyle().copyWith(
//                              fontSize: 18.0, fontWeight: FontWeight.w400, color: Color(0xFFBB5577)),
//                        ),
//                      ),
//                    );
//                  }, dayBuilder: (context, date, _) {
//                    return Container(
//                      width: 100,
//                      height: 100,
//                      color: Colors.transparent,
//                      child: Center(
//                        child: Text(
//                          '${date.day}',
//                          style: TextStyle()
//                              .copyWith(fontSize: 18.0, fontWeight: FontWeight.w400, color: Colors.white),
//                        ),
//                      ),
//                    );
//                  }, selectedDayBuilder: (context, date, _) {
//                    return Container(
//                      decoration: BoxDecoration(
//                        shape: BoxShape.circle,
//                        color: Colors.white,
//                      ),
//                      child: Center(
//                        child: Text(
//                          '${date.day}',
//                          style: TextStyle().copyWith(
//                              fontSize: 18.0,
//                              fontWeight: FontWeight.bold,
//                              color: kLightHealthTheme.primaryColor),
//                        ),
//                      ),
//                    );
//                  }, todayDayBuilder: (context, date, _) {
//                    return Container(
//                      width: 100,
//                      height: 100,
//                      color: Colors.transparent,
//                      child: Center(
//                        child: Text(
//                          '${date.day}',
//                          style: TextStyle()
//                              .copyWith(fontSize: 18.0, color: Colors.white),
//                        ),
//                      ),
//                    );
//                  }, markersBuilder: (context, date, events, holidays) {
//                    final children = <Widget>[];
//
//                    children.add(
//                      Positioned(
//                        right: 10,
//                        bottom: 22,
//                        child: _buildEventsMarker(date, events),
//                      ),
//                    );
//
//                    return children;
//                  }),
//                ),
//                Expanded(
//                    child: (_selectedEvents.length == 0)
//                        ? Container(
//                          margin: EdgeInsets.only(top: 20),
//                            width: MediaQuery.of(context).size.width,
//                            decoration: new BoxDecoration(
//                                color: Colors.white,
//                                borderRadius: new BorderRadius.only(
//                                    topLeft: const Radius.circular(20.0),
//                                    topRight: const Radius.circular(20.0)),
//                                boxShadow: [
//                                  BoxShadow(
//                                    color: Color.fromARGB(50, 255, 255, 255),
//                                    blurRadius: 5.0,
//                                    offset: Offset(0.0, -3.0),
//                                  )
//                              ]
//                            ),
//                            child: Container(
//                                child: SingleChildScrollView(
//                              scrollDirection: Axis.vertical,
//                              child: Stack(
//                                children: <Widget>[
//                                  Container(
//                                    margin: EdgeInsets.only(
//                                        top: 40,
//                                        left: 20,
//                                        right: 20,
//                                        bottom: 30),
//                                    child: Text(
//                                      Lang.getString("calendar_no_reminder") +
//                                          DateFormat("MMMdd日",'zh_HK')
//                                              .format(_selectedDate),
//                                      style: TextStyle(
//                                          color: Color(0xFF6A6969),
//                                          fontSize: 18,
//                                          fontWeight: FontWeight.w700),
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            )),
//                          )
//                        : _buildEventList()),
//              ],
//            ),
//          ),
//          _reminderWidget(),
//          Container(
//            alignment: Alignment.bottomRight,
//            margin: EdgeInsets.only(bottom: 20, right: 20),
//            child: FloatingActionButton(
//              onPressed: () {
//                setState(() {
//                  reminderFlag = !reminderFlag;
//                });
//              },
//              child: Stack(
//                children: <Widget>[
//                  Container(
//                    child: Icon(
//                      (!reminderFlag) ? Icons.add : Icons.clear,
//                      color: Colors.white,
//                      size: 25,
//                    ),
//                    decoration: BoxDecoration(
//                        gradient: LinearGradient(
//                            begin: Alignment.topCenter,
//                            end: Alignment.bottomCenter,
//                            colors: <Color>[
//                              Color(0xFFFF5689),
//                              Color(0xFFFF9192)
//                            ]),
//                        boxShadow: defaultBoxShadow,
//                        borderRadius: BorderRadius.all(Radius.circular(30.0))),
//                    width: 60,
//                    height: 60,
//                  ),
//                ],
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//}
