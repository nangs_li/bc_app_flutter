import 'package:flutter/material.dart';
import 'package:icanfight/model/category.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/tag.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'baseViewController.dart';
import 'fullArticleController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:icanfight/utilities/lang.dart';

import 'learnCategoryController.dart';


class LearnSearchController extends StatefulWidget {

  List<Post> posts;
  List<Tag> tags;
  String catTitle;

  LearnSearchController({String catName, List<Tag> tagList }) {
    catTitle = catName;
    tags = tagList;
  }

  @override
  _LearnSearchControllerState createState() =>
      _LearnSearchControllerState(catName: catTitle, tagList: tags);
}

class _LearnSearchControllerState
    extends State<LearnSearchController> with TickerProviderStateMixin {

  List<Post> postArray = [];
  List<Post> rawPostArray = [];
  List<Tag> tagArray = [];
  String catTitle;
  TextEditingController _textEditingController = TextEditingController(text: "");
  SingletonShareData data;

  _LearnSearchControllerState({ String catName, List<Tag> tagList  }) {
    catTitle = catName;
    tagArray = tagList;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = SingletonShareData();
    rawPostArray = data.allPostList;
    postArray = data.allPostList;
    filterBookmark();
  }

  filterBookmark () async {

    List<Post> newPosts = List();

    rawPostArray.forEach((Post p) {

      if(true) {
        newPosts.add(p);
      }

    });

    setState(() {
      postArray = newPosts;
    });

    if (catTitle.length > 0) {
      _textEditingController.text = catTitle;
      filterByText(catTitle);
    }
  }

  filterByText (String text) async {
    List<Post> newPosts = List();

    List<Post> dummyPosts = rawPostArray;

    dummyPosts.forEach((Post p) {

      if (p.title.rendered.toLowerCase().contains(text.toLowerCase())) {
        newPosts.add(p);
      }

    });

    dummyPosts.forEach((Post p) {

      bool flag = false;

      p.getTagList().forEach((Tag t) {
        if (t.name.toLowerCase().contains(text.toLowerCase())) {
          flag = true;
        }
      });

      if (flag) {
        if (!newPosts.contains(p)) {
          newPosts.add(p);
        }
      }

    });

    dummyPosts.forEach((Post p) {

      if (p.content.rendered.toLowerCase().contains(text.toLowerCase())) {
        if (!newPosts.contains(p)) {
          newPosts.add(p);
        }
      }

    });

    setState(() {
      postArray = newPosts;
    });
  }

  filterByTags (Tag tag) async {

    List<Post> newPosts = List();

    rawPostArray.forEach((Post p) {

      bool flag = false;

      p.getTagList().forEach((Tag t) {
        if (t.id == tag.id) {
          flag = true;
        }
      });

      if(flag) {
        if (!newPosts.contains(p)) {
          newPosts.add(p);
        }
      }

    });

    setState(() {
      postArray = newPosts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            top: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: true,type: SliverAppBarType.SearchPage,titleText: Lang.getString("learn_search"),

                  ),
                  SliverToBoxAdapter(
                    child: WaveBar(),
                  ),
                  SliverToBoxAdapter(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20, top: 0),
                          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 3.0),
                          alignment: Alignment.center,
                          decoration: new BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.all(Radius.circular(10.0))),
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                alignment: Alignment.centerLeft,
                                child: InkWell(
                                  child: Icon(Icons.search, color: kLightHealthTheme.primaryColor, size: 40),
                                  onTap: () {

                                  },
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 40, right: 40),
                                alignment: Alignment.center,
                                child: TextFormField(
                                  controller: _textEditingController,
                                  onFieldSubmitted: (String t) {
                                    filterByText(t);
                                  },
                                  decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.only(left: 16, right: 16, top: 4, bottom: 4),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(10)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(10)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.transparent),
                                        borderRadius: BorderRadius.circular(10)),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  child: Icon(Icons.close, color: Color(0xFF9291AC), size: 40),
                                  onTap: () {
                                    _textEditingController.text = "";
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: defaultPadding, right: defaultPadding,bottom: defaultPadding),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: FractionalOffset(0.0, 0.0),
                                child: Wrap(
                                  children: tagArray.getRange(0, 3).map((Tag tag) {
                                    return InkWell(
                                        onTap: () {
                                          filterByTags(tag);
                                          _textEditingController.text = tag.name;
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(top: 10 ),
                                          child: Container(
                                            margin: EdgeInsets.only(right: 5),
                                            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                                            decoration: BoxDecoration(
                                                color: Color(0xFFE14672),
                                                border: Border.all(color: Color(0xFFE14672)),
                                                borderRadius: new BorderRadius.all(Radius.circular(16))
                                            ),
                                            child: Text(tag.name, style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.normal),),
                                          ),
                                        )
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (context,index) =>
                          Container(
                            margin: EdgeInsets.only(left: 20, right: 20, bottom: 0),
                            width: MediaQuery.of(context).size.width * 0.80,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: Container(
                                color: Colors.transparent,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => FullArticleController(article: postArray[index])));
                                  },
                                  child: GestureDetector(
                                    onPanUpdate: (onPanUpdateDetail) {

                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
                                      color: Colors.transparent,
                                      child: Stack(
                                        children: <Widget>[
                                          PostCard(postArray[index],index)

                                        ],
                                      ),
                                    ),
                                  )
                                ),
                              ),
                            ),
                          ),
                      childCount: postArray.length
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}