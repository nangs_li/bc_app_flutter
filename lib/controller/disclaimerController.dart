import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:icanfight/api_provider/crudModel.dart';
import 'package:icanfight/googleAnalytics/googleAnalytics.dart';
import 'package:icanfight/model/disclaimer.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/controller/bottomNavigationBarController.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:icanfight/view/bottomButton.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'package:provider/provider.dart';
import 'baseViewController.dart';

class DisclaimerController extends StatefulWidget {

  bool _isAuth;
  bool _isStatic;
  bool _isFirst;


  DisclaimerController({
    bool isAuth = false,
    bool isStatic = false,
    bool isFirst = false,
  }) {
    _isAuth = isAuth;
    _isStatic = isStatic;
    _isFirst = isFirst;

  }


  @override

  _DisclaimerControllerState createState() =>
      _DisclaimerControllerState();
}

class _DisclaimerControllerState extends State<DisclaimerController>
    with SingleTickerProviderStateMixin {
  String tncData = "";
  List choiceValues = [true, true, true];
  Animation<Offset> _offset;
  AnimationController _animationController;
  int leftWidthPercent = 2;
  int rightWidthPercent = 4;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    _offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 1.0))
        .animate(_animationController);
    _animationController.forward();

  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  void generalCheck() {
    if (choiceValues.where((flag) => flag).length == (widget._isFirst ? 3 : 2)) {
      _animationController.reverse();
    }
  }

  void agreeAction() async {
    ga.gaEvent(name: "privacypolicy_agree_button");
    SingletonShareData data = SingletonShareData();
    if (widget._isAuth) {
      data.registerCase = false;
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BaseViewController(BottomNavigationBarController(index: 0))));
    }
  }

  Widget disclaimerView() {
    Disclaimer disclaimer = SingletonShareData().disclaimer;
    print("disclaimers:${disclaimer.toJson()}");

    SingletonShareData data = SingletonShareData();
    List<Widget> widgetList;
    String treatmentDrug = "\n" + disclaimer.treatment;
    if(data.checkIfHaveSelectQ11Drug()) {

      for (var i = 0; i < 7; i++) {
        if (data.choiceValues[10][i]) {
          treatmentDrug += "\n";
          treatmentDrug += Lang.getString("profile_drug_treatment_${i + 1}");
        }
      }
    }


      widgetList = [
        Container(
          margin: EdgeInsets.only(
              top: 30, bottom: 30),
          child: Row(
            mainAxisAlignment:
            MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment
                .start,
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                  child: Container(
                    alignment: Alignment
                        .topLeft,
                    // margin: EdgeInsets.only(left: 20),
                    child: Text(
                      disclaimer.topText,
                      style: disclaimerControllerText,
                    ),
                  ),
                  onTap: () {

                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment:
            MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment
                .start,
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                  child: Container(
                    alignment: Alignment
                        .centerLeft,
                    margin: EdgeInsets.only(
                      // left: 20,
                        bottom: 150),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets
                              .only(
                              top: topPadding),
                          child: Text(
                            disclaimer
                                .centerText,
                            style: disclaimerControllerText,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets
                              .only(
                              top: topPadding),
                          child: Flex(
                            crossAxisAlignment: CrossAxisAlignment
                                .start,
                            direction: Axis
                                .horizontal,
                            children: <Widget>[
                          Expanded(
                            flex: leftWidthPercent,
                          child: Text(
                                disclaimer
                                    .emailTitle,
                                style: disclaimerControllerSmallText,
                              )),
                              Expanded(
                                flex: rightWidthPercent,
                                child: Text(
                                  disclaimer
                                      .email,
                                  style: disclaimerControllerSmallText,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets
                              .only(
                              top: topPadding),
                          child: Flex(
                            crossAxisAlignment: CrossAxisAlignment
                                .start,
                            direction: Axis
                                .horizontal,
                            children: <Widget>[
                            Expanded(
                            flex: leftWidthPercent,
                            child:Text(
                                disclaimer
                                    .phoneTitle,
                                style: disclaimerControllerSmallText,
                              )),
                              Expanded(
                                flex: rightWidthPercent,
                                child: Text(
                                  disclaimer
                                      .phone,
                                  style: disclaimerControllerSmallText,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets
                              .only(
                              top: topPadding),
                          child: Flex(
                            crossAxisAlignment: CrossAxisAlignment
                                .start,
                            direction: Axis
                                .horizontal,
                            children: <Widget>[
                            Expanded(
                            flex: leftWidthPercent,
                            child:Text(
                                disclaimer
                                    .taxTitle,
                                style: disclaimerControllerSmallText,
                              )),
                              Expanded(
                                flex: rightWidthPercent,
                                child: Text(
                                  disclaimer
                                      .tax,
                                  style: disclaimerControllerSmallText,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets
                              .only(
                              top: topPadding),
                          child: Flex(
                            direction: Axis
                                .horizontal,
                            crossAxisAlignment: CrossAxisAlignment
                                .start,
                            children: <Widget>[
                            Expanded(
                            flex: leftWidthPercent,
                            child:Text(
                                disclaimer
                                    .addressTitle,
                                style: disclaimerControllerSmallText,
                              )),
                              Expanded(
                                flex: rightWidthPercent,
                                child: Text(
                                  disclaimer
                                      .address,
                                  style: disclaimerControllerSmallText,
                                ),
                              )
                            ],
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              treatmentDrug,
                              style: disclaimerControllerSmallText,
                            ),
                          ],
                        )
                      ]),
                  ),
                  onTap: () {

                  },
                ),
              ),
            ],
          ),
        )
      ];

    return SafeArea(
      bottom: false,
      child:Stack(
        children: <Widget>[
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height,
            margin: EdgeInsets.symmetric(horizontal: 15.0),
            child: Container(
                decoration: defaultBoxDecoration,
              child: Card(
                margin: EdgeInsets.only(bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),)),
                color: Colors.white,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Padding(
                    padding: EdgeInsets.all(defaultPadding),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment
                          .center,
                      children: widgetList,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 50,
            margin: EdgeInsets.symmetric(horizontal: 15.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[
                      Color(0xFFFFFFFF),
                      Color(0x00FFFFFF)
                    ]),
                borderRadius: BorderRadius.all(
                    Radius.circular(20.0))),
          ),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    _animationController.reverse();

    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: (widget._isAuth || widget._isStatic) ? true : false ,type: SliverAppBarType.DisclaimerPage,titleText: Lang.getString("profile_privacy"), ),
                  SliverToBoxAdapter(
                    child: WaveBar(),
                  ),
                ],
              ),
            ),
          ),
          Container(
              alignment: Alignment(0, 0),
              margin: EdgeInsets.only(top: 100),
              decoration: BoxDecoration(boxShadow: defaultBoxShadow),
              child: disclaimerView()
          ),
          SlideTransition(
            position: _offset,
            child: BottomButton(buttonText:Lang.getString("privacy_agree"), onPressed: () async {
              agreeAction();
            }),
          )
        ],
      ),
    );
  }
}
