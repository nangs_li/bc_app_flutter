import 'package:flutter/material.dart';
import 'package:icanfight/utilities/sizeConfig.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/category.dart';
import 'baseViewController.dart';
import 'learnSearchController.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/tag.dart';
// not have customSliverAppBarView
class LearnMenuController extends StatefulWidget {

  List<Category> cats;
  List<Post> posts;
  List<Tag> tags;
  String catTitle;

  LearnMenuController({ List<Category> categories, List<Post> articles, String catName, List<Tag> tagList }) {
    cats = categories;
    posts = articles;
    catTitle = catName;
    tags = tagList;
  }

  @override
  _LearnMenuControllerState createState() =>
      _LearnMenuControllerState(categories: cats, articles: posts, catName: catTitle, tagList: tags);
}

class _LearnMenuControllerState
    extends State<LearnMenuController> {

  List<Category> cats;
  List<Post> postArray;
  List<Tag> tagArray;
  String catTitle;

  _LearnMenuControllerState({ List<Category> categories, List<Post> articles, String catName, List<Tag> tagList }) {
    cats = categories;
    postArray = articles;
    catTitle = catName;
    tagArray = tagList;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context).copyWith(dividerColor: kLightHealthTheme.primaryColor);

    return Container(
        color: kLightHealthTheme.primaryColor,
        child: Stack(
        children: <Widget>[
        SafeArea(
        bottom: true,
        child: Material(
        color: kLightHealthTheme.primaryColor,
    child: CustomScrollView(
    physics: new ClampingScrollPhysics(),
    slivers: <Widget>[
            CustomSliverAppBarView(isBack: false,titleText: "",type: SliverAppBarType.LearnMenuPage,widgetList: <Widget>[Padding(
              padding: EdgeInsets.only(top:topPadding,right:defaultPadding),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  IconData(0xe14c,
                      fontFamily: 'MaterialIcons'),
                  color: Colors.white, size: 35,),
              ),
            )]
            ),
          SliverToBoxAdapter(
            child:  Container(
              height: SizeConfig().blockSizeVertical * 80,
              color: kLightHealthTheme.primaryColor,
              margin: EdgeInsets.only(top: defaultPadding * 2),
              padding: EdgeInsets.symmetric(horizontal: 4.0),
              child: ListView.builder(
                  itemCount: cats.length,
                  itemBuilder: (buildContext, index) {
                    print("cats[index].name:${cats[index].name}");
                    return Theme(data: theme, child: ListTileTheme(
                        contentPadding: EdgeInsets.only(left: defaultPadding),
                        child:ExpansionTile(
                      backgroundColor: Colors.transparent,
                      trailing:  Text(""),
                      title: (cats[index].name == "網誌" || cats[index].name == "最新消息") ? GestureDetector(
                    // When the child is tapped, show a snackbar.
                    onTap: () {
                      if(cats[index].name == "網誌"){
                        Navigator.pop(context, 0);
                      } else if(cats[index].name == "最新消息"){
                        Navigator.pop(context, 1);
                      }

                    },
                    // The custom button
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: Colors.white, width: 0.5))
                        ),
                        child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(cats[index].name, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white),),
                            cats[index].subCats.length > 0 ? Icon(Icons.arrow_drop_down, color: Colors.white) : Container(),
                         ]
                        ),
                      )) : Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(color: Colors.white, width: 0.5))
                        ),
                        child:
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(cats[index].name, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white),),
                              cats[index].subCats.length > 0 ? Icon(Icons.arrow_drop_down, color: Colors.white) : Container(),
                            ]
                        ),
                      ),
                      children: cats[index].subCats.map((Category subCat) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(left: defaultPadding, right: defaultPadding - 4),
                         
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.white, width: 0.5))
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => BaseViewController(LearnSearchController(catName: subCat.name, tagList: tagArray))));
                            },
                            child: Text(subCat.name, style: TextStyle(fontSize: 19, fontWeight: FontWeight.normal, color: Colors.white)),
                          ),
                        );
                      }).toList(),
                    )
                    )
                    );
                  }
              ),
            )
          ),
          ],
    ),
        ),
        )
        ]));
  }
}