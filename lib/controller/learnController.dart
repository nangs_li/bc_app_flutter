import 'package:flutter/material.dart';
import 'package:icanfight/controller/singletonShareData.dart';
import 'package:icanfight/utilities/style.dart';
import 'package:icanfight/view/customAlertDialog.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/model/post.dart';
import 'package:icanfight/model/tag.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/waveBar.dart';
import 'baseViewController.dart';
import 'fullArticleController.dart';
import 'package:icanfight/api_provider/articleApiProvider.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:icanfight/utilities/lang.dart';
import 'bottomNavigationBarController.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'learnCategoryController.dart';


class LearnController extends StatefulWidget {

  List<Post> posts;
  String catTitle;

  LearnController({ List<Post> articles, String catName }) {
    posts = articles;
    catTitle = catName;
  }

  @override
  _LearnControllerState createState() =>
      _LearnControllerState(articles: posts, catName: catTitle);
}

class _LearnControllerState
    extends State<LearnController> with TickerProviderStateMixin {

  List<Post> postArray;
  String catTitle;

  _LearnControllerState({ List<Post> articles, String catName  }) {
    postArray = articles;
    catTitle = catName;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    SingletonShareData().refreshBookMarkPageCallBack.listen((bool refreshBook) {
      filterBookmark ();
    });
    filterBookmark();
  }

  filterBookmark () async {
    List<String> bookmarks = await ArticleApiProvider().getBookmarkPost();

    List<Post> newPosts = List();

    if(postArray !=null) {
      postArray.forEach((Post p) {
        if (bookmarks.contains(p.id.toString())) {
          newPosts.add(p);
        }
      });
    }

    setState(() {
      postArray = newPosts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            top: false,
            child: Material(
              color: Color(0xFFF9EBEC),
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                   CustomSliverAppBarView(isBack: true,type: SliverAppBarType.SearchPage,titleText: Lang.getString("learn_bookmark_title"),),
                   SliverToBoxAdapter(
                     child: WaveBar(),
                   ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (context,index) =>
                          Container(
                            margin: EdgeInsets.only(left: 20, right: 20, bottom: 0),
                            width: MediaQuery.of(context).size.width * 0.80,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: Container(
                                color: Colors.transparent,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => FullArticleController(article: postArray[index], articles: postArray, bookmarkList: true,)));
                                  },
                                  child: GestureDetector(
                                    onPanUpdate: (onPanUpdateDetail) {
                                      if (onPanUpdateDetail.delta.dx > 20) {
                                        showDialog(
                                            barrierDismissible: true,
                                            context: context,
                                            builder: (buildContext) {
                                              return CustomAlertDialog(title: Lang.getString("learn_bookmark_remove_message"), content: null, leftButtonText: Lang.getString(
                                                  "cancel_button"),rightButtonText:Lang.getString(
                                                  "remove_button") ,buildContext: buildContext,
                                                  rightButtonFunction:  () async {
                                                    ArticleApiProvider().removeBookmark(postArray[index]);
                                                    postArray.removeAt(index);
                                                    setState(() {
                                                      postArray = postArray;
                                                    });
                                                    Navigator.of(buildContext).pop();
                                                  });
                                          }
                                        );
                                      }
                                    },
                                    child: PostCard(postArray[index],index),
                                  )
                                ),
                              ),
                            ),
                          ),
                      childCount: (postArray == null) ? 0 : postArray.length
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}