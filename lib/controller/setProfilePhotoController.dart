import 'package:flutter/material.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/sliverSpacer.dart';
import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';


import 'package:camera/camera.dart';
import 'package:icanfight/view/customInputField.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:icanfight/utilities/lang.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetProfilePhotoController extends StatefulWidget {
  @override
  _SetProfilePhotoControllerState createState() =>
      _SetProfilePhotoControllerState();
}

class _SetProfilePhotoControllerState extends State<SetProfilePhotoController> {


  File imageFile;


  _openGallery(context) async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    this.setState(() {
    });

    var fileName = basename(imageFile.path);
    await imageFile.copy('$path/'+fileName);

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("userProfile", fileName);

    Navigator.of(context).pop();

  }

  _openCamera(context) async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    this.setState(() {
    });
    var fileName = basename(imageFile.path);
    await imageFile.copy('$path/'+fileName);

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("userProfile", fileName);

    debugPrint(fileName);

    Navigator.of(context).pop();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            bottom: false,
            child: Material(
              color: Colors.white,
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                 CustomSliverAppBarView(isBack: true,titleText: Lang.getString("profile_set_profile"),type: SliverAppBarType.SetProFilePhotoPage,),
                  SliverSpacer(),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (context, index) => Container(
                              color: kLightHealthTheme.primaryColor,
//                              height: 1000,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: new BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: new BorderRadius.only(
                                                topLeft:
                                                    const Radius.circular(20.0),
                                                topRight: const Radius.circular(
                                                    20.0))),
                                        padding: EdgeInsets.only(top: 42),
//                                        height: 1000,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                color: Color(0xFFE6E6E6)),
                                            GestureDetector(
                                              onTap: () {},
                                              child: Row(
                                                children: <Widget>[
                                                  GestureDetector(
                                                    onTap: () {
                                                      _openCamera(context);
                                                    },
                                                    child: Container(
                                                        width: 24,
                                                        margin: EdgeInsets.only(
                                                            left: 20,
                                                            right: 10),
                                                        child: Container(
                                                          height: 55,
                                                          decoration:
                                                              new BoxDecoration(
                                                            image:
                                                                DecorationImage(
                                                              image: new AssetImage(
                                                                  'assets/images/camera_alt.png'),
//                                                          fit: BoxFit.fitWidth,
                                                            ),
                                                          ),
                                                        )),
                                                  ),
                                                  GestureDetector(
                                                      onTap: () {
                                                        _openCamera(context);
                                                      },
                                                      child: Container(
                                                        height: 55,
                                                        // width: (MediaQuery.of(
                                                        //                 context)
                                                        //             .size
                                                        //             .width -
                                                        //         54) *
                                                        //     0.4,
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                          Lang.getString("profile_take_photo"),
                                                          style: new TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                              color: Color(
                                                                  0xFF555555)),
                                                        ),
                                                      )),
                                                ],
                                              ),
                                            ),
                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                color: Color(0xFFE6E6E6)),
                                            Row(
                                              children: <Widget>[
                                                GestureDetector(
                                                    onTap: () {
                                                      _openGallery(context);
                                                    },
                                                    child: Container(
                                                      width: 24,
                                                      margin: EdgeInsets.only(
                                                          left: 20, right: 10),
                                                      child: Container(
                                                        height: 55,
                                                        decoration:
                                                            new BoxDecoration(
                                                          image:
                                                              DecorationImage(
                                                            image: new AssetImage(
                                                                'assets/images/Path855.png'),
//                                                          fit: BoxFit.fitWidth,
                                                          ),
                                                        ),
                                                      ),
                                                    )),
                                                GestureDetector(
                                                    onTap: () {
                                                      _openGallery(context);
                                                    },
                                                    child: Container(
                                                      height: 55,
                                                      // width: (MediaQuery.of(
                                                      //                 context)
                                                      //             .size
                                                      //             .width -
                                                      //         54) *
                                                      //     0.4,
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Text(
                                                        Lang.getString("profile_choose_photo"),
                                                        style: new TextStyle(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color: Color(
                                                                0xFF555555)),
                                                      ),
                                                    )),
                                              ],
                                            ),
                                            Container(
                                                height: 1,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                color: Color(0xFFE6E6E6)),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                        childCount: 1),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
