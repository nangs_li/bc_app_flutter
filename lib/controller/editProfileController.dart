import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:icanfight/view/bottomButton.dart';


import 'package:icanfight/view/customInputField.dart';
import 'package:icanfight/view/customSliverAppBarView.dart';
import 'package:icanfight/view/reminderDropdownInputField.dart';
import 'bottomNavigationBarController.dart';
import 'signUpController.dart';
import 'package:icanfight/utilities/themes.dart';
import 'package:icanfight/utilities/lang.dart';

class EditProfileController extends StatefulWidget {
  @override
  _EditProfileControllerState createState() => _EditProfileControllerState();
}

//not use again
class _EditProfileControllerState extends State<EditProfileController> {


  ScrollController _scrollController;
  List<FocusNode> _formNodes = [
    FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode(),FocusNode()
  ];
  List<TextEditingController> _textEditingControllers = [
    TextEditingController(text: ""),TextEditingController(text: ""),TextEditingController(text: "Stage II"),
    TextEditingController(text: "HER2+/HR+"),TextEditingController(text: ""),TextEditingController(text: ""),
  ];
  List<String> _cancerItems = [
    "HER2+/HR+", "HER2+/HR-", "HER2-/HR+", "HER2-/HR-"
  ];
  List<String> _stageItems = [
    "Stage I", "Stage II", "Stage III", "Stage IV"
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = ScrollController();
  }

  VoidCallback inputFieldScrollOnFocus(double offset) => () {
    _scrollController.animateTo(offset, duration: Duration(seconds: 1), curve: Curves.ease);
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightHealthTheme.primaryColor,
      child: Stack(
        children: <Widget>[
          SafeArea(
            child: Material(
              color: kLightHealthTheme.primaryColor,
              child: CustomScrollView(
                physics: new ClampingScrollPhysics(),
                slivers: <Widget>[
                  CustomSliverAppBarView(isBack: true,titleText:Lang.getString("profile_about"), type: SliverAppBarType.EditProfilePage
                          ),
                  SliverToBoxAdapter(
                    child: Container(
                      color: kLightHealthTheme.primaryColor,
//                              height: 1000,
                      child: Container(
//                                controller: _scrollController,
                        child: Column(
                          children: <Widget>[
                            Container(
//                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                                width:
                                MediaQuery.of(context).size.width,
                                decoration: new BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                        topLeft:
                                        const Radius.circular(20.0),
                                        topRight: const Radius.circular(
                                            20.0))),
                                padding: EdgeInsets.only(top: 42),
                                height: 1000,
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  children: <Widget>[
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                              top: 10,
                                              bottom: 10),
                                          child: CustomInputField(
                                              focusNode: _formNodes[0], nextFocusNode: _formNodes[1], textInputAction: TextInputAction.next,
                                              iconData: 0xe7fd,
                                              inputTitle: Lang.getString("inputfield_username_title"),
                                              hint: "Beryl Leung"),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                              top: 10,
                                              bottom: 10),
                                          child: CustomInputField(
                                              focusNode: _formNodes[1], nextFocusNode: _formNodes[2], textInputAction: TextInputAction.next,
                                              iconData: 0xe0be,
                                              inputTitle: Lang.getString("inputfield_email_title"),
                                              hint:
                                              "beryl@echoroaster.com"),
                                        ),

                                      ],
                                    ),
//                                            Container(
////                                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
//                                                width: MediaQuery.of(context)
//                                                    .size
//                                                    .width,
//                                                padding:
//                                                    EdgeInsets.only(top: 54),
////                                        height: 100,
//                                                child: Column(
//                                                  children: <Widget>[
//                                                    Container(
////                                                color: Colors.black,
//                                                      height: 37,
//                                                      padding: EdgeInsets.only(
//                                                          left: 20, bottom: 15),
//                                                      alignment:
//                                                          Alignment.centerLeft,
//                                                      child: Text(
//                                                          "Medical Record",
//                                                          textAlign:
//                                                              TextAlign.left,
//                                                          style: new TextStyle(
//                                                              color: Color(
//                                                                  0xFFF36E98),
//                                                              fontSize: 19,
//                                                              fontWeight:
//                                                                  FontWeight
//                                                                      .bold)),
//                                                    ),
//                                                    Container(
//                                                        height: 1,
//                                                        width: MediaQuery.of(
//                                                                context)
//                                                            .size
//                                                            .width,
//                                                        color:
//                                                            Color(0xFFE6E6E6)),
//                                                    Container(
//                                                      margin: EdgeInsets.only(
//                                                          left: 20,
//                                                          right: 20,
//                                                          top: 10,
//                                                          bottom: 10),
//                                                      child: ReminderDropdownInputField(
//                                                          items: _stageItems,
//                                                          focusNode: _formNodes[2], nextFocusNode: _formNodes[3],
//                                                          focusNodeCallBack: inputFieldScrollOnFocus(1000),
//                                                          inputTitle:
//                                                          "Stage of Cancer",
//                                                          textEditingController: _textEditingControllers[2]),
//                                                    ),
//                                                Container(
//                                                  margin: EdgeInsets.only(
//                                                      left: 20,
//                                                      right: 20,
//                                                      top: 10,
//                                                      bottom: 10),
//                                                  child: ReminderDropdownInputField(
//                                                      items: _cancerItems,
//                                                      focusNode: _formNodes[3], nextFocusNode: _formNodes[4],
//                                                      inputTitle: "Cancer Type", textEditingController: _textEditingControllers[3] ),
//                                                ),
//
//                                                    Container(
//                                                      margin: EdgeInsets.only(
//                                                          left: 20,
//                                                          right: 20,
//                                                          top: 10,
//                                                          bottom: 10),
//                                                      child: CustomInputField(
//                                                          focusNode: _formNodes[4], nextFocusNode: _formNodes[5], textInputAction: TextInputAction.next,
//                                                          maxLines: 5,
//                                                          iconData: 0,
//                                                          inputTitle:
//                                                              "Treatment",
//                                                          hint:
//                                                              "Planned for surgery, but currently receiving drug treatment before surgery"),
//                                                    ),
//                                                    Container(
//                                                      margin: EdgeInsets.only(
//                                                          left: 20,
//                                                          right: 20,
//                                                          top: 10,
//                                                          bottom: 100),
//                                                      child: CustomInputField(
//                                                          focusNode: _formNodes[5], textInputAction: TextInputAction.done,
//                                                          focusNodeCallBack: inputFieldScrollOnFocus(500),
//                                                          maxLines: 5,
//                                                          iconData: 0,
//                                                          inputTitle:
//                                                              "Prescription",
//                                                          hint:
//                                                              "Perjeta (pertuzumab) & Herceptin (trastuzumab)"),
//                                                    ),
//                                                  ],
//                                                ))
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          BottomButton(buttonText:Lang.getString("profile_save_change"),onPressed: () async {
            Navigator.pop(context);
          }),
          Positioned(
            child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                    height: 400,
                    child: Visibility(
                        visible: false, //Default is true,
                        child: CupertinoPicker(
                            magnification: 1.5,
                            backgroundColor: Colors.white,
                            children: <Widget>[
                              Text(
                                "TextWidget",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              ),
                              Text(
                                "TextWidget",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 20),
                              )
                            ],
                            itemExtent: 50,
                            //height of each item
                            looping: true,
                            onSelectedItemChanged: (int index) {
//                                                            selectitem = index;
                            })))),
          )
        ],
      ),
    );
  }
}
